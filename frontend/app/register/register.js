'use strict';

angular.module('myApp.register', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/register', {
        templateUrl: 'register/register_v2.html',
        controller: 'RegisterCtrl'
    });
}])

.directive('linkedInLogin', [function() {
  return {
    restrict: 'E',
    template: '<script type="in/Login"><\/script><ul id="js-messages"><\/ul>',
    link: function (scope, element, attrs) {
      if ((typeof IN !== "undefined") && (typeof IN.parse !== "undefined")) {
          IN.parse();
      }
    }
  };
}])      

.controller('RegisterCtrl', [ '$window', '$scope', '$loading', '$http', '$location', '$rootScope', 'AuthProxy', 'vcRecaptchaService', 'MessagesFactory',
    function($window, $scope, $loading, $http, $location, $rootScope, AuthProxy, vcRecaptchaService, MessagesFactory)
    {

        $scope.user = {};
        $rootScope.oldUrl = "register.html";
        $loading.finish('register');
        $scope.showModal = false;

        $scope.user.image = null;
        $scope.user.education = null;
        $scope.user.interests = null;
        $scope.header = "registro";

        /**
         * Funcion que se ejecuta cuando se da click al boton de login
         */
        $scope.registerUser = function()
        {
            if(vcRecaptchaService.getResponse() === "")
            {
                $scope.messages = MessagesFactory.createMessages();
                $scope.messages.error.push("Por favor resuelva el captcha y acepte");
            }
            else
            {
                $loading.start('register');
                $scope.user.captcha = vcRecaptchaService.getResponse();
                console.log('sending the captcha response to the server', $scope.user.captcha);
                AuthProxy.userRegister($scope.user, success, error);
            }
        };


        /**
         * Metodo que se ejecuta cuando la autenticacion es exitosa
         * @param res respuesta del servicio
         */
        var success = function (res)
        {
            $loading.finish('register');
            $location.path('/login/');
        };

        /**
         * Metodo que se ejecuta cuando la autenticacion es exitosa
         * @param res respuesta del servicio
         */
        var error = function (res)
        {
          $loading.finish('register');
          if(res.error == "unauthorized")
          {
            $location.path('/login/');
          }
          else
          {
            console.log('Failed validation');
            $scope.messages = MessagesFactory.createMessages();
            $scope.messages.error.push(res.message);
            vcRecaptchaService.reload($scope.widgetId);
          }

        };

      $window.Linkedin = {
        // Setup an event listener to make an API call once auth is complete
        onLinkedInLoad : function(){
          //console.log('onLinkedInLoad')
          IN.Event.on(IN, "auth", getProfileData);
        }
      };

      // Handle the successful return from the API call
      function onSuccess(data) {
        $loading.finish('register');
        //console.log('onSuccess::data::'+JSON.stringify(data));
        $scope.user.name = data.firstName;
        $scope.user.lastname = data.lastName;
        $scope.user.email = data.emailAddress;
        $scope.user.image = data.pictureUrl;

        //$scope.user.education = data.education.schoolName + ". " + data.education.grade;
        $scope.user.interests = data.interests;

        $scope.$apply();

        IN.User.logout();
      }

      // Handle an error response from the API call
      function onError(error) {
        $loading.finish('register');
        console.log(error);
      }

      // Use the API call wrapper to request the member's basic profile data
      function getProfileData() {
        IN.API.Raw("/people/~:(id,first-name,last-name,picture-url,emailAddress,interests,educations)").result(onSuccess).error(onError);
        $loading.start('register');
      }

  }]);
