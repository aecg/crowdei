'use strict';

angular.module('myApp.forgotPassword', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/forgotPassword.old', {
        templateUrl: 'forgotPassword/forgotPassword.html',
        controller: 'ForgotPassword'
    });
}])
.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/forgotPassword', {
        templateUrl: 'forgotPassword/forgotPassword_v2.html',
        controller: 'ForgotPassword'
    });
}])

.controller('ForgotPassword', ['$scope', '$loading', '$http', '$location', '$rootScope', 'AuthProxy', 'MessagesFactory',
    function($scope, $loading, $http, $location, $rootScope, AuthProxy, MessagesFactory) {

    $rootScope.oldUrl = "forgotPassword.html";

    $scope.user = {};
    $rootScope.isLogged = false;
    $scope.authError = null;

    /**
     * Funcion que se ejecuta cuando se da click al boton de login
     */
    $scope.recover = function() {
        $loading.start('forgot');
        AuthProxy.retrieveUser($scope.user, success, error);
    };

    /**
     * Metodo que se ejecuta cuando la autenticacion es exitosa
     * @param res respuesta del servicio
     */
    var success = function (res)
    {
        $loading.finish('forgot');
        $scope.messages = MessagesFactory.createMessages();
        $scope.messages.success.push("Se ha enviado la informacion a su correo");
        $scope.user = {};
    };

    /**
     * Metodo que se ejecuta cuando la autenticacion es exitosa
     * @param res respuesta del servicio
     */
    var error = function (res)
    {
        $loading.finish('forgot');
        console.log('Failed recover pass');
        $scope.messages = MessagesFactory.createMessages();
        $scope.messages.error.push(res.message);
    };
}]);
