'use strict';

angular.module('myApp.productSheet', ['ngRoute',
  'ngSanitize',
  'angulike'
])

    .config(['$routeProvider', function ($routeProvider)
    {
        $routeProvider.when('/productSheet.old/:idProject', {
            templateUrl: 'productSheet/productSheet.html',
            controller: 'ProductSheetCtrl'
        });

        $routeProvider.when('/productSheet/:idProject', {
            templateUrl: 'productSheet/productSheet_v2.html',
            controller: 'ProductSheetCtrl'
        });
    }])

.controller('ProductSheetCtrl', ['$scope', '$loading', '$http', '$location', '$rootScope', '$routeParams', 'ProjectProxy', 'MessagesFactory', 'UtilsProxy','SessionService', '$localStorage', '$sce', '$window',
    function ($scope, $loading, $http, $location, $rootScope, $routeParams, ProjectProxy, MessagesFactory, UtilsProxy, SessionService, $localStorage, $sce, $window)
    {

      $loading.start('project-detail');
      SessionService.checkSession();
      $scope.showModal = false;

      $scope.ribbon_style = $localStorage.language;

      $scope.$watch(function () { 
        return $localStorage.language; 
      }, function(newVal,oldVal){
            if(oldVal!==newVal && newVal !== undefined){
              $scope.ribbon_style = $localStorage.language;
            }
      });

      /**
       * Metodo que se ejecuta cuando la autenticacion es exitosa
       * @param res respuesta del servicio
       */
      var success = function (res)
      {
        $scope.project = res;

        $scope.myModel = {
          Url: $rootScope.pagShareProject + $routeParams.idProject,
          Name: 'Proyecto: ' + $scope.project.name.substr(0,50) +' | Objetivo: ' + $scope.project.currency.symbol + $scope.project.objetive
        };

        //Modificando las etiquetas Meta
        $('meta[name="og:title"]').attr('content', 'CrowdEi - ' + $scope.project.name);
        $('meta[name="og:description"]').attr('content', $scope.project.description);
        $('meta[name="og:url"]').attr('content', 'http://crowdei.softclear.net/#/productSheet/'+$routeParams.idProject);
        $('meta[name="og:image"]').attr('content', 'http://crowdei.softclear.net/template/img/Logo_139x43_Blanco.png');

        //console.log('sucess::$scope.project.rewards::'+JSON.stringify($scope.project.rewards));
        $loading.finish('project-detail');
      };


      /**
       * Metodo que se ejecuta cuando la autenticacion es exitosa
       * @param res respuesta del servicio
       */
      var error = function (res)
      {
          $loading.finish('project-detail');
          if (res.error == "unauthorized")
          {
              $location.path('/login/');
          }
          else
          {
              console.log('Failed Invoke Service Web');
              $scope.messages = MessagesFactory.createMessages();
              $scope.messages.error.push(res.message);
          }
      };

      $scope.payInvestment = function (amountInvestment, rewardId)
      {
        //console.log('payInvestment::amountInvestment::'+amountInvestment+'::rewardId::'+rewardId);
        if (amountInvestment)
        {
          var priceInvestment = new String(amountInvestment).replace(".", "");
          priceInvestment = priceInvestment.replace(",", ".");
          $location.path('/investment/' + $scope.project.id + 
                        '/' + rewardId + '/' + priceInvestment);
        }
      };

      ProjectProxy.getProjectDetail($routeParams.idProject, success, error);

      $scope.download = function (doc)
      {
          var hiddenElement = document.createElement('a');

          hiddenElement.href = 'data:application/pdf;base64,' + doc.file;
          hiddenElement.target = '_blank';
          hiddenElement.download = doc.name;
          hiddenElement.click();
      };

      $scope.getIcono = function (doc)
      {
          var retorno = "http://placehold.it/64x39";
          if (doc.name.indexOf(".pdf") > 0)
          {
              retorno = "template/img/pdf.jpg"
          }
          if (doc.name.indexOf(".doc") > 0)
          {
              retorno = "template/img/word.png"
          }
          if (doc.name.indexOf(".xls") > 0)
          {
              retorno = "template/img/excel.png"
          }
          if (doc.name.indexOf(".png") > 0)
          {
              retorno = "data:image/png;base64," + doc.file;
          }
          if (doc.name.indexOf(".jpg") > 0)
          {
              retorno = "data:image/jpeg;base64," + doc.file;
          }
          if (doc.name.indexOf(".ppt") > 0)
          {
              retorno = "template/img/powerpoint.png"
          }
          return retorno;
      };

      $scope.sendMail = function(contact)
      {
        /**
         * Metodo que se ejecuta cuando la autenticacion es exitosa
         * @param res respuesta del servicio
         */
        var success = function (res)
        {
          console.log( 'Successful send mail' );
          $scope.messages = MessagesFactory.createMessages();
          $scope.messages.success.push( "Se ha enviado con exito el mensaje" );
        };

        var error = function ( res )
        {
          console.log( 'Failed Invoke Service Web' );
          $scope.messages = MessagesFactory.createMessages();
          $scope.messages.error.push( "No se pudo enviar su mensaje, por favor intente luego" );


        };
        $scope.contact = contact;
        $scope.contact.idProject = $scope.project.id;
        UtilsProxy.sendMailContactProject($scope.contact, success, error);
      };

      $scope.redireccionar = function(url)
      {
        if (!url)
          return;
        if((url.split(':')[0]) == "http" || (url.split(':')[0]) == "https")
          window.open(url);
        else
          window.open("http://"+url);
      };

      $scope.redireccionarTwitter = function(userTwitter)
      {
        if (!userTwitter)
          return;
        window.open("https://twitter.com/"+userTwitter.split('@')[1]);
      };


      //Compartir en Facebook


      $window.Facebook = {
        // Setup an event listener to make an API call once auth is complet

        renderLikeButton : function(){
          var watchAdded = false;
          if (!!attrs.fbLike && !scope.fbLike && !watchAdded) {
            // wait for data if it hasn't loaded yet
            watchAdded = true;
            var unbindWatch = scope.$watch('fbLike', function (newValue, oldValue) {
              if (newValue) {
                Link.renderLikeButton();
                // only need to run once
                unbindWatch();
              }
            });
            return;
          } else {
            element.html('<div class="fb-like"' + (!!scope.fbLike ? ' data-href="' + scope.fbLike + '"' : '') + ' data-layout="button_count" data-action="like" data-show-faces="true" data-share="true" ></div>');
            $window.FB.XFBML.parse(element.parent()[0]);

          }
        }

      };

      $window.Twitter = {

        renderTweetButton : function() {
          var watchAdded = false;
          if (!scope.tweet && !watchAdded) {
            // wait for data if it hasn't loaded yet
            watchAdded = true;
            var unbindWatch = scope.$watch('tweet', function (newValue, oldValue) {
              if (newValue) {
                renderTweetButton();

                // only need to run once
                unbindWatch();
              }
            });
            return;
          } else {
            element.html('<a href="https://twitter.com/share" class="twitter-share-button" data-text="' + scope.tweet + '" data-url="' + (scope.tweetUrl || $location.absUrl()) + '">Tweet</a>');
            $window.twttr.widgets.load(element.parent()[0]);
          }
        }
      };


    }
]);
