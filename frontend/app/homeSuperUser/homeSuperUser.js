'use strict';

angular.module('myApp.homeSuperUser', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/homeSuperUser', {
    templateUrl: 'homeSuperUser/homeSuperUser.html',
    controller: 'HomeSuperUserCtrl'
  });
}])

.controller('HomeSuperUserCtrl', ['$scope', '$loading', '$http', '$location', '$rootScope', 'LandingProxy', 'MessagesFactory', 'SessionService', 'ProjectProxy', 'UserProxy',
        function($scope, $loading, $http, $location, $rootScope, LandingProxy, MessagesFactory, SessionService, ProjectProxy, UserProxy) {

    $loading.start('home');
    SessionService.checkSession();
    $scope.numberProjectList = 0;
    $scope.numberUserList = 0;
    $scope.header = 'inicio';

    var success = function (res)
    {
      $scope.numberProjectList = res.count;
      $loading.finish('home');
    };

    var successUsers = function (res)
    {
      $scope.numberUserList = res.count;
      console.log("successUsers::$scope.numberUserList::"+$scope.numberUserList);
      $loading.finish('home');
    };

    /**
     * Metodo que se ejecuta cuando la autenticacion es exitosa
     * @param res respuesta del servicio
     */
    var error = function (res)
    {
      $loading.finish('home');
      if(res.error == "unauthorized")
      {
        $location.path('/login/');
      }
      else
      {
        console.log('Failed Invoke Service Web');
        $scope.messages = MessagesFactory.createMessages();
        $scope.messages.error.push(res.message);
      }
    };

    ProjectProxy.getAllProjects(false, 0, 0, success, error);
    UserProxy.getAllUsers(false, false, 0, 0, successUsers, error);
}]);
