'use strict';

angular.module('myApp.modifyPitch', ['ngRoute'])

  .config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/modifyPitch/:idProject', {
      templateUrl: 'modifyPitch/modifyPitch.html',
      controller: 'modifyPitchCtrl'
    });
  }])
  .controller('modifyPitchCtrl', ['$scope', '$location', 'ProjectProxy', '$rootScope', '$routeParams', 'MessagesFactory',' SessionService',
      function ($scope, $location,  ProjectProxy, $rootScope, $routeParams, MessagesFactory, SessionService) {
    $scope.project = {};

        SessionService.checkSession();
    /**
     * Metodo que se ejecuta cuando la autenticacion es exitosa
     * @param res respuesta del servicio
     */
    var success = function (res)
    {
      $('#myloader').hide();
      //Accion cuando el usuario es autenticado de manera exitosa
      $scope.project = res;
      $scope.project.id = $routeParams.idProject;
      console.log($scope.project);

    };

    var successTwo = function (res)
    {
      $('#myloader').hide();
      //Accion cuando el usuario es autenticado de manera exitosa
      $scope.projectTwo = res;
    };

    /**
     * Metodo que se ejecuta cuando la autenticacion es exitosa
     * @param res respuesta del servicio
     */
    var error = function (res)
    {
      $('#myloader').hide();
      if(res.error == "unauthorized")
      {
        $location.path('/login/');
      }
      else
      {
        console.log('Failed Invoke Service Web');
        $scope.messages = MessagesFactory.createMessages();
        $scope.messages.error.push(res.message);
      }
    };


    var errorTwo = function (res)
    {
      $('#myloader').hide();
    };


    ProjectProxy.getProjectDetail($routeParams.idProject, success, error);
    ProjectProxy.getProjectDetail($routeParams.idProject, successTwo, errorTwo);
    $('#myloader').show();

    $scope.documentsToDelete = [];

    $scope.download = function (doc)
    {
      var hiddenElement = document.createElement('a');

      hiddenElement.href = 'data:application/pdf;base64,' + doc.file;
      hiddenElement.target = '_blank';
      hiddenElement.download = doc.name;
      hiddenElement.click();
    };


    $scope.getIcono = function (doc)
    {
      var retorno = "http://placehold.it/64x39";
      if (doc.name.indexOf(".pdf") > 0) {
        retorno = "template/img/pdf.jpg"
      }
      if (doc.name.indexOf(".doc") > 0) {
        retorno = "template/img/word.png"
      }
      if (doc.name.indexOf(".xls") > 0) {
        retorno = "template/img/excel.png"
      }
      if (doc.name.indexOf(".png") > 0) {
        retorno = "data:image/jpeg;base64," + doc.file;
      }
      if (doc.name.indexOf(".jpg") > 0) {
        retorno = "data:image/jpeg;base64," + doc.file;
      }
      if (doc.name.indexOf(".ppt") > 0) {
        retorno = "template/img/powerpoint.png"
      }
      return retorno;
    };


    $scope.deleteDocument = function (doc)
    {

      var documents = [];
      documents = $scope.project.documents;

      for (var i = 0; i < documents.length; i++) {
        if ($scope.project.documents[i].id == doc.id) {
          $scope.documentsToDelete.push($scope.project.documents[i]);
          $scope.project.documents.splice(i,1);
        }
        if($scope.projectTwo.documents[i].id == doc.id)
        {
          $scope.projectTwo.documents.splice(i,1);
        }
      }

    };


    $scope.modifyPitch = function()
    {
      var successModify = function (res)
      {
        $('#myloader').hide();
        console.log('Succeful modify project');
        $scope.messages = MessagesFactory.createMessages();
        $scope.messages.success.push("Se ha guardado la informacion");
        ProjectProxy.getProjectDetail($routeParams.idProject, success, error);
        $('#myloader').show();
      };

      $scope.project.documentsToDelete = $scope.documentsToDelete;
      $scope.project.idPerson = $rootScope.user.person;
      $scope.project = ProjectProxy.pitchRegister($scope.project, successModify, error);
      $('#myloader').show();
    };


    $scope.setFiles = function ()
    {
      var file = document.getElementById('documents').files;
      if(file.length != 0)
      {
        var aux = 0;

        for (var i = 0; i < file.length; i++)
        {
          var docFile = file[i];
          var reader = new FileReader();

          reader.onloadend = function(e)
          {
            var data = e.target.result;
            var doc = {};
            doc.file = window.btoa(data);
            doc.name = file[aux].name;
            aux++;
            $scope.projectTwo.documents.push(doc);
            $scope.project.documents.push(doc);

            if(file.length == aux)
            {
              $scope.$apply();
            }

          };
          reader.readAsBinaryString(docFile);
        }
      }
    };


  }]);
