angular.module('myApp').
  directive('modalSendMail', function ($rootScope)
  {
    return {
      restrict: 'E',
      templateUrl: 'messages/modalSendMail.html',
      transclude: true,
      replace: true,
      scope: true,
      link: function postLink(scope, element, attrs)
      {
        scope.title = attrs.title;
        scope.contact = {};

        scope.$watch(attrs.visible, function (value)
        {
          if (value == true)
          {
            document.formMail.name.disabled = false;
            document.formMail.lastName.disabled = false;
            document.formMail.email.disabled = false;
            if ($rootScope.oldUrl == 'businessOpportunitiesUser.html' )
            {
              scope.contact.name = $rootScope.nameUser;
              scope.contact.lastName = $rootScope.lastname;
              scope.contact.email = $rootScope.email;
              document.formMail.name.disabled = true;
              document.formMail.lastName.disabled = true;
              document.formMail.email.disabled = true;
            }
            $(element).modal('show');
          }
          else
            $(element).modal('hide');
        });

        $(element).on('shown.bs.modal', function ()
        {

          scope.$apply(function ()
          {
            scope.$parent[attrs.visible] = true;
          });
        });

        $(element).on('hidden.bs.modal', function ()
        {
          scope.$apply(function ()
          {
            scope.$parent[attrs.visible] = false;
          });
        });

        scope.save = function() {
          scope.sendMail(scope.contact);
          $(element).modal('hide');
        };

      }
    };
  });
