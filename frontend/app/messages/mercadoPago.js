angular.module('myApp').
    directive('mercadoPago', function ()
    {
        return {
            restrict: 'E',
            templateUrl: 'messages/mercadoPago.html',
            transclude: true,
            replace: true,
            scope: true,
            link: function postLink(scope, element, attrs)
            {
              scope.title = attrs.title;
              scope.contact = {};

              scope.$watch(attrs.visible, function (value)
              {
                if (value == true)
                {

                  $(element).modal('show');
                }
                else
                  $(element).modal('hide');
              });

              $(element).on('shown.bs.modal', function ()
              {

                scope.$apply(function ()
                {
                  scope.$parent[attrs.visible] = true;
                });
              });

              $(element).on('hidden.bs.modal', function ()
              {
                scope.$apply(function ()
                {
                  scope.$parent[attrs.visible] = false;
                });
              });

              scope.save = function() {
                scope.payTransferencia();
                $(element).modal('hide');
              };
            }
        };
    });
