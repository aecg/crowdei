angular.module('myApp').
directive('messages', function() {
    return {
      restrict: 'E',
      templateUrl: 'messages/messages.html',
        scope: {
            messages: '=src'
        }
    };
  });
