'use strict';

angular.module('myApp.myInvestments', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/myInvestments.old', {
    templateUrl: 'myInvestments/myInvestments.html',
    controller: 'MyInvestmentsCtrl'
  });

  $routeProvider.when('/myInvestments', {
    templateUrl: 'myInvestments/myInvestments_v2.html',
    controller: 'MyInvestmentsCtrl'
  });
}])

.controller('MyInvestmentsCtrl',  ['$scope', '$loading', '$http', '$location', '$rootScope', '$modal', '$localStorage', 'ProjectProxy', 'UtilsProxy', 'InvestmentProxy', 'MessagesFactory', 'SessionService',
      function($scope, $loading, $http, $location, $rootScope, $modal, $localStorage, ProjectProxy, UtilsProxy, InvestmentProxy, MessagesFactory, SessionService)
{

  $loading.start('my-invesments');
  SessionService.checkSession();
  $scope.projectList = [];
  $scope.openedSearch = false;
  $scope.parameters = {};
  $scope.parameters.endDate = new Date();
  $scope.parameters.startDate = new Date();
  $scope.showModal = false;

  /**
   * Metodo que se ejecuta cuando la autenticacion es exitosa
   * @param res respuesta del servicio
   */
  var success = function (res)
  {
    $loading.finish('my-invesments');
    //Accion cuando el usuario es autenticado de manera exitosa
    $scope.projectList = res.projects;

    if ($scope.projectList.length == 0)
    {
      $scope.messages = MessagesFactory.createMessages();
      $scope.messages.error.push("Usted no ha realizado ninguna inversión");
      $('#titulo').hide();
    }

    //console.log(JSON.stringify($scope.projectList));
  };

  /**
   * Metodo que se ejecuta cuando la autenticacion es exitosa
   * @param res respuesta del servicio
   */
  var successFind = function (res)
  {
    $loading.finish('my-invesments');
    //Accion cuando el usuario es autenticado de manera exitosa
    $scope.projectList = res.projects;

    if ($scope.projectList.length == 0)
    {
      $scope.messages = MessagesFactory.createMessages();
      $scope.messages.error.push("No existen proyectos que coincidan con los parámetros de búsqueda");
      $scope.showModal = !$scope.showModal;
    }
    console.log($scope.projectList);
  };

  /**
   * Metodo que se ejecuta cuando la autenticacion es exitosa
   * @param res respuesta del servicio
   */
  var error = function (res)
  {
    $loading.finish('my-invesments');
    if (res.error == "unauthorized")
    {
      $location.path('/login/');
    }
    else
    {
      console.log('Failed Invoke Service Web');
      $scope.messages = MessagesFactory.createMessages();
      $scope.messages.error.push(res.message);
    }
  };

  /**
   * Funcion que se ejecuta cuando se da click al boton de busqueda
   */
  $scope.openSearch = function ()
  {
    $scope.openedSearch = !$scope.openedSearch;
  };


  /**
   * Funcion que se ejecuta cuando se da click al boton de buscar
   */
  $scope.find = function ()
  {
    $scope.parameters.idUser = $rootScope.user.person.id;

    if ($scope.parameters.startDate != null && $scope.parameters.endDate != null)
    {
      $scope.parameters.startDate.setHours(0, 0, 0, 0);
      $scope.parameters.endDate.setHours(23, 59, 59, 0);
      ProjectProxy.findProjectsInvestmentXIdXParams($scope.parameters, successFind, error);
      $loading.start('my-invesments');
      $scope.messages = null;
    }
    else if ($scope.parameters.startDate == null && $scope.parameters.endDate == null)
    {
      ProjectProxy.findProjectsInvestmentXIdXParams($scope.parameters, successFind, error);
      $loading.start('my-invesments');
      $scope.messages = null;
    }
    else
    {
      $scope.messages = MessagesFactory.createMessages();
      $scope.messages.error.push("Para realizar la b\u00fasqueda por fechas es importante ingresar el rango");
    }
  };

  /**
   * Abre el modal para el envío de correo
   */
  $scope.openSendMailModal = function (projectId)
  {
    $scope.showSendMailModal = true;
    $scope.projectId = projectId;
  };

  /**
   * Abre modal con pagos del proyecto
   */
  $scope.openPaymentsModal = function (projectId)
  {
    $modal.open({
        templateUrl: 'payments-modal.html',
        controller: ['$scope', '$modalInstance', 'data', PaymentsCtrl],
        resolve: {
          'data': function ()
          {
            return {
              personId: $localStorage.personId,
              projectId: projectId
            };
          }
        }
    });    
  };

  /**
   * Controlador de payments modal
   */
  var PaymentsCtrl = function ($scope, $modalInstance, data)
  {
    $scope.close = function () {
        $modalInstance.dismiss('close');
    };                    

    var success = function (resp)
    {
      $scope.investmentList = resp;
      $scope.investmentTotal = 0;
      //console.log('PaymentsCtrl::success::$scope.investmentList::'+JSON.stringify($scope.investmentList));
      for (var i in $scope.investmentList.investments) {
        if ($scope.investmentList.investments[i].status == "ACTIVE") {
          $scope.investmentTotal += $scope.investmentList.investments[i].amount;
          $scope.projectName = $scope.investmentList.investments[i].project.name;
        }
      }
    };
    var error = function () 
    {

    };
    InvestmentProxy.getInvestmentsXIdProjectXIdUser(data.personId, data.projectId, 0, 1000, success, error);
  };

  /**
   * Envía el correo cuando se hace clic en enviar en el modal
   */
  $scope.sendMail = function(contact)
  {
    var success = function (res)
    {
      console.log( 'Successful send mail' );
      $scope.messages = MessagesFactory.createMessages();
      $scope.messages.success.push( "Se ha enviado con exito el mensaje" );
    };

    var error = function ( res )
    {
      console.log( 'Failed Invoke Service Web' );
      $scope.messages = MessagesFactory.createMessages();
      $scope.messages.error.push( "No se pudo enviar su mensaje, por favor intente luego" );


    };
    $scope.contact = contact;
    $scope.contact.idProject = $scope.projectId;
    UtilsProxy.sendMailContactProject($scope.contact, success, error);
  };  

  ProjectProxy.getAllProjectsInvestment($rootScope.user.person.id, success, error);
}]);
