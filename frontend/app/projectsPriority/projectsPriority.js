'use strict';

angular.module('myApp.projectsPriority', ['ngRoute'])

  .config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/projectsPriority/:priority', {
      templateUrl: 'projectsPriority/projectsPriority.html',
      controller: 'ProjectsPriorityCtrl'
    });
  }])

  .controller('ProjectsPriorityCtrl',  ['$scope', '$http', '$location', '$rootScope', '$routeParams' ,'LandingProxy', 'MessagesFactory','SessionService',
      function($scope, $http, $location, $rootScope, $routeParams ,LandingProxy, MessagesFactory, SessionService) {

        SessionService.checkSession();
    $scope.projectList = [];

    $rootScope.oldUrl = "projectsPriority.html";
    $rootScope.priority = $routeParams.priority;

    /**
     * Metodo que se ejecuta cuando la autenticacion es exitosa
     * @param res respuesta del servicio
     */
    var success = function (res)
    {
      $('#myloader').hide();
      //Accion cuando el usuario es autenticado de manera exitosa
      $scope.projectList = res.projects;
      console.log($scope.projectList );
    };

    /**
     * Metodo que se ejecuta cuando la autenticacion es exitosa
     * @param res respuesta del servicio
     */
    var error = function (res)
    {
      $('#myloader').hide();
      if(res.error == "unauthorized")
      {
        $location.path('/login/');
      }
      else
      {
        console.log('Failed Invoke Service Web');
        $scope.messages = MessagesFactory.createMessages();
        $scope.messages.error.push(res.message);
      }
    };

    LandingProxy.getProjectsPriorityType($routeParams.priority , success, error);
    $('#myloader').show();

  }]);
