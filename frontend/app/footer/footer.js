angular.module('myApp').
  directive('ngFooterOld', function() {
    return {
      restrict: 'E',
      templateUrl: 'footer/footer.html'
    };
  }).
  directive('ngFooter', function() {
    return {
      restrict: 'E',
      templateUrl: 'footer/footer_v2.html',
      controller: 'FooterCtrl'
    };
  });
