angular.module('myApp').
  directive('ngMenu', function() {
    return {
      restrict: 'E',
      templateUrl: 'menu/menu.html'
    };
  })
  .directive('ngMenuSu', function(){
    return {
      restrict: 'E',
      templateUrl: 'menu/menu_su.html'
      };
});
