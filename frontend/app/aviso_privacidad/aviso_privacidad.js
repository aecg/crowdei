'use strict';

angular.module('myApp.avisoPrivacidad', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/aviso_privacidad', {
    templateUrl: 'aviso_privacidad/aviso_privacidad.html',
    controller: 'AvisoPrivacidadCtrl'
  });
}])

.controller('AvisoPrivacidadCtrl',  ['$scope', '$location', '$anchorScroll', 'SessionService', '$localStorage', '$translate', function($scope, $location, $anchorScroll, SessionService, $localStorage, $translate)
{
	$scope.currentLanguage = $localStorage.language;
}]);
