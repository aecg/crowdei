'use strict';

angular.module('myApp.projects', ['ngRoute'])

	.config(['$routeProvider', function ($routeProvider)
	{
		$routeProvider.when('/oldProjects', {
			templateUrl: 'projects/projects.html',
			controller: 'ProjectsCtrl'
		});
		$routeProvider.when('/projects', {
			templateUrl: 'projects/projects_v2.html',
			controller: 'ProjectsCtrl'
		});
	}])

	.controller('ProjectsCtrl', ['$scope', '$loading', '$http', '$location', '$rootScope', 'ProjectProxy', 'MessagesFactory','UtilsProxy', 'SessionService',
		function ($scope, $loading, $http, $location, $rootScope, ProjectProxy, MessagesFactory, UtilsProxy,SessionService)
	{


		$loading.start('my-projects');
		SessionService.checkSession();
		$scope.openedSearch = false;
		$scope.projectList = [];
		$scope.parameters = {};

		$scope.idEconomicActivity = {};

		$rootScope.oldUrl = "projects.html";

		/**
		 * Metodo que se ejecuta cuando la autenticacion es exitosa
		 * @param res respuesta del servicio
		 */
		var success = function (res)
		{
			$loading.finish('my-projects');
			//Accion cuando el usuario es autenticado de manera exitosa
			$scope.projectList = res.projects;
			//console.log(JSON.stringify($scope.projectList));
		};

		/**
		 * Metodo que se ejecuta cuando la autenticacion es exitosa
		 * @param res respuesta del servicio
		 */
		var successFind = function (res)
		{
			$loading.finish('my-projects');
			//Accion cuando el usuario es autenticado de manera exitosa
			$scope.projectList = res.projects;

			if ($scope.projectList.length == 0)
			{
				$scope.messages = MessagesFactory.createMessages();
				$scope.messages.error.push("No existen proyectos que coincidan con los parámetros de búsqueda");
				$scope.showModal = !$scope.showModal;
			}
			console.log($scope.projectList);
		};

		/**
		 * Metodo que se ejecuta cuando la autenticacion es exitosa
		 * @param res respuesta del servicio
		 */
		var successDelete = function (res)
		{
			$loading.finish('my-projects');
			$scope.messages = MessagesFactory.createMessages();
			$scope.messages.success.push("Se ha eliminado el proyecto exitosamente");

			$loading.start('my-projects');
			ProjectProxy.getProjects($rootScope.user.person.id, null, null, success, error);
		};

		/**
		 * Metodo que se ejecuta cuando la autenticacion es exitosa
		 * @param res respuesta del servicio
		 */
		var error = function (res)
		{
			$loading.finish('my-projects');
			//console.log('ProjectsCtrl::error::res::'+JSON.stringify(res));
			if (res.error == "unauthorized")
			{
				$location.path('/login/');
			}
			else
			{
				console.log('Failed Invoke Service Web');
				$scope.messages = MessagesFactory.createMessages();
				$scope.messages.error.push(res.message);
			}
		};

		/**
		 * Funcion que se ejecuta cuando se da click al boton de login
		 */
		$scope.openSearch = function ()
		{
			$scope.openedSearch = !$scope.openedSearch;
		};

		/**
		 * Funcion que se ejecuta cuando se da click al boton de buscar
		 */
		$scope.find = function ()
		{
			$loading.start('my-projects');
			$scope.parameters.idUser = $rootScope.user.person.id;

			$scope.parameters.idEconomicActivity = $scope.idEconomicActivity.id;

			ProjectProxy.findProjectsXIdXParams($scope.parameters, successFind, error);
			$scope.messages = null;
		};

		/**
		 * Metodo que se ejecuta cuando la consulta actividades economicas es exitosa
		 * @param res respuesta del servicio
		 */
		var successEconomicActivities = function (res)
		{
			$scope.economicActivities = res.list;
			angular.forEach($scope.economicActivities, function(value, key)
			{
				if ($scope.idEconomicActivity != null && value.id == $scope.idEconomicActivity.id) {
					$scope.idEconomicActivity = value;
				}
			});
		};

		/**
		 * Funcion que se ejecuta cuando se da click al boton de login
		 */
		$scope.deleteProject = function (id)
		{
			$loading.start('my-projects');
			ProjectProxy.deleteProject(id, successDelete, error);
		};

		UtilsProxy.getEconomicActivity(successEconomicActivities, error);
		ProjectProxy.getProjects($rootScope.user.person.id, null, null, success, error);
	}
]);
