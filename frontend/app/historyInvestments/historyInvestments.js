'use strict';

angular.module('myApp.historyInvestments', ['ngRoute'])

  .config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/historyInvestments/:idProject', {
      templateUrl: 'historyInvestments/historyInvestments.html',
      controller: 'HistoryInvestmentsCtrl'
    });
  }])

  .controller('HistoryInvestmentsCtrl',  ['$scope', '$http', '$location', '$rootScope', 'ProjectProxy',
    'InvestmentProxy', 'MessagesFactory', '$routeParams', 'SessionService', '$localStorage', function($scope, $http, $location, $rootScope, ProjectProxy,
                                                                   InvestmentProxy, MessagesFactory, $routeParams, SessionService, $localStorage)
{
  SessionService.checkSession();
  $scope.investmentList = [];
  $scope.project = null;

  /**
   * Metodo que se ejecuta cuando la autenticacion es exitosa
   * @param res respuesta del servicio
   */
  var successProject = function (res)
  {
    //$('#myloader').hide();
    //Accion cuando el usuario es autenticado de manera exitosa
    $scope.project = res;
    console.log($scope.project);
  };

  /**
   * Metodo que se ejecuta cuando la autenticacion es exitosa
   * @param res respuesta del servicio
   */
  var successInvestment = function (res)
  {
    //$('#myloader').hide();
    //Accion cuando el usuario es autenticado de manera exitosa
    $scope.investmentList = res;
    console.log($scope.investmentList);
  };

  /**
   * Metodo que se ejecuta cuando la autenticacion es exitosa
   * @param res respuesta del servicio
   */
  var errorProject = function (res)
  {
    //$('#myloader').hide();
    if (res.error == "unauthorized")
    {
      $location.path('/login/');
    }
    else
    {
      console.log('Failed Invoke Service Web');
      $scope.messages = MessagesFactory.createMessages();
      $scope.messages.error.push(res.message);
    }
  };

  /**
   * Metodo que se ejecuta cuando la autenticacion es exitosa
   * @param res respuesta del servicio
   */
  var errorInvestment = function (res)
  {
    //$('#myloader').hide();
    if (res.error == "unauthorized")
    {
      $location.path('/login/');
    }
    else
    {
      console.log('Failed Invoke Service Web');
      $scope.messages = MessagesFactory.createMessages();
      $scope.messages.error.push(res.message);
    }
  };

  ProjectProxy.getProjectDetail($routeParams.idProject, successProject, errorProject);
  InvestmentProxy.getInvestmentsXIdProjectXIdUser($localStorage.personId, $routeParams.idProject, 0, 10, successInvestment, errorInvestment);
  //$('#myloader').show();

}]);
