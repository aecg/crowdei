'use strict';

angular.module('myApp.userProfile', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/oldUserProfile', {
        templateUrl: 'userProfile/userProfile.html',
        controller: 'UserProfileCtrl'
    });
    $routeProvider.when('/userProfile', {
        templateUrl: 'userProfile/userProfile_v2.html',
        controller: 'UserProfileCtrl'
    });
}])

.controller('UserProfileCtrl', ['$scope', '$loading', '$http', '$location', '$anchorScroll', '$rootScope', 'UtilsProxy','UserProxy', 'MessagesFactory', 'SessionService',
    function($scope, $loading, $http, $location, $anchorScroll, $rootScope, UtilsProxy, UserProxy, MessagesFactory, SessionService)  {

    $loading.start('my-profile');
    SessionService.checkSession();
    $rootScope.oldUrl = "userProfile.html";

    $scope.user = {};
    $scope.user.person = {};
    $scope.user.person.mailPreferenceList = [];
    $scope.user.person.reasonInvestmentList = [];
    $scope.user.person.businessTypeList = [];
    $scope.user.person.economicActivityList = [];
    $scope.user.person.location = {};

    $scope.selectionBusiness = [];
    $scope.selectionReason = [];
    $scope.selectionMailPreferences = [];
    $scope.selectEconomicActivities = [];

    // toggle selection for a given fruit by name
    $scope.toggleSelectionBusiness = function toggleSelection(business)
    {
        var idx = $scope.selectionBusiness.indexOf(business);

        // is currently selected
        if (idx > -1) {
            $scope.selectionBusiness.splice(idx, 1);
        }

        // is newly selected
        else {
            $scope.selectionBusiness.push(business);
        }
    };

    // toggle selection for a given fruit by name
    $scope.toggleSelectionReason = function toggleSelection(reason)
    {
        var idx = $scope.selectionReason.indexOf(reason);

        // is currently selected
        if (idx > -1) {
            $scope.selectionReason.splice(idx, 1);
        }

        // is newly selected
        else {
            $scope.selectionReason.push(reason);
        }
    };

    function selectionMailChecked()
    {
        var retorno = false;

        angular.forEach($scope.user.person.mailPreferenceList, function(value, key)
        {
            angular.forEach($scope.mailPreferences, function(mail, key)
            {
                if (value.id == mail.id) {
                    if ($scope.selectionMailPreferences.indexOf(mail) == -1) {
                        $scope.selectionMailPreferences.push(mail);
                    }
                }
            });

        });
        return retorno;
    }

    // toggle selection for a given fruit by name
    $scope.toggleSelectionMailPreferences = function toggleSelection(preference)
    {
        var idx = $scope.selectionMailPreferences.indexOf(preference);

        // is currently selected
        if (idx > -1) {
            $scope.selectionMailPreferences.splice(idx, 1);
        }

        // is newly selected
        else {
            $scope.selectionMailPreferences.push(preference);
        }
    };

    /**
     * Metodo que se ejecuta cuando la consulta de paises es exitosa
     * @param res respuesta del servicio
     */
    var successCountries = function (res)
    {
        $scope.countries = res.list;

        if(($scope.user.person.location != null) && 
            ($scope.user.person.location.city != null))
        {
            angular.forEach($scope.countries, function(value, key)
            {
                if(value.id == $scope.user.person.location.city.state.country.id)
                {
                    $scope.countrySelected = value;
                    $scope.getStates();
                }
            });
        }
    };

    /**
     * Metodo que se ejecuta cuando la consulta de razones de inversiones es exitosa
     * @param res respuesta del servicio
     */
    var successReason = function (res)
    {
        $scope.reasonInvestments = res.list;
        selectionReasonChecked();
    };

    function selectionReasonChecked()
    {
        var retorno = false;

        angular.forEach($scope.user.person.reasonInvestmentList, function(value, key)
        {
            angular.forEach($scope.reasonInvestments, function(obj, key)
            {
                if (value.id == obj.id) {
                    if ($scope.selectionReason.indexOf(obj) == -1) {
                        $scope.selectionReason.push(obj);
                    }
                }
            });

        });
        return retorno;
    }

    /**
     * Metodo que se ejecuta cuando la consulta de tipos de negocio es exitosa
     * @param res respuesta del servicio
     */
    var successBusinessType = function (res)
    {
        $scope.businessTypes = res.list;
        selectionBusinessTypeChecked();
    };

    function selectionBusinessTypeChecked()
    {
        var retorno = false;

        angular.forEach($scope.user.person.businessTypeList, function(value, key)
        {
            angular.forEach($scope.businessTypes, function(obj, key)
            {
                if (value.id == obj.id) {
                    if ($scope.selectionBusiness.indexOf(obj) == -1) {
                        $scope.selectionBusiness.push(obj);
                    }
                }
            });

        });
        return retorno;
    }

    /**
     * Metodo que se ejecuta cuando la consulta actividades economicas es exitosa
     * @param res respuesta del servicio
     */
    var successEconomicActivities = function (res)
    {
        $scope.economicActivities = res.list;
        selectionEconomicActivitiesChecked();
    };

    function selectionEconomicActivitiesChecked()
    {
        var retorno = false;

        angular.forEach($scope.user.person.economicActivityList, function(value, key)
        {
            angular.forEach($scope.economicActivities, function(obj, key)
            {
                if (value.id == obj.id) {
                    if ($scope.selectEconomicActivities.indexOf(obj) == -1) {
                        $scope.selectEconomicActivities.push(obj);
                    }
                }
            });

        });
        return retorno;
    }

    // toggle selection for a given fruit by name
    $scope.toggleEconomicActivities = function toggleSelection(activity)
    {
        var idx = $scope.selectEconomicActivities.indexOf(activity);

        // is currently selected
        if (idx > -1) {
            $scope.selectEconomicActivities.splice(idx, 1);
        }

        // is newly selected
        else {
            $scope.selectEconomicActivities.push(activity);
        }
    };

    /**
     * Metodo que se ejecuta cuando la consulta niveles de experiencia es exitosa
     * @param res respuesta del servicio
     */
    var successExperienceLevels = function (res)
    {
        $scope.experienceLevels = res.list;
        angular.forEach($scope.experienceLevels, function(value, key)
        {
            if ($scope.user.person.experienceLevel != null && value.id == $scope.user.person.experienceLevel.id) {
                $scope.user.person.experienceLevel = value;
            }
        });
    };

    /**
     * Metodo que se ejecuta cuando la consulta niveles de experiencia es exitosa
     * @param res respuesta del servicio
     */
    var successHowFind = function (res)
    {
        $scope.howFinds = res.list;

        angular.forEach($scope.howFinds, function(value, key)
        {
            if ($scope.user.person.howFind != null && value.id == $scope.user.person.howFind.id) {
                $scope.user.person.howFind = value;
            }
        });
    };

    /**
     * Metodo que se ejecuta cuando la consulta niveles de experiencia es exitosa
     * @param res respuesta del servicio
     */
    var successMailPreferences = function (res)
    {
        $scope.mailPreferences = res.list;
        selectionMailChecked();
    };

    /**
     * Metodo que se ejecuta cuando la autenticacion es exitosa
     * @param res respuesta del servicio
     */
    var error = function (res)
    {
        $loading.start('my-profile');
        if(res.error == "unauthorized")
        {
            $location.path('/login/');
        }
        else
        {
            console.log('Failed Invoke Service Web');
            $scope.messages = MessagesFactory.createMessages();
            $scope.messages.error.push(res.message);
        }
    };

    /**
     * funcion que invoca el servicio para obtener los estados de una ciudad
     */
    $scope.getStates = function()
    {
        /**
         * Metodo que se ejecuta cuando la consulta de paises es exitosa
         * @param res respuesta del servicio
         */
        var success = function (res)
        {
            $loading.finish('my-profile');
            $scope.states = res.list;
            if($scope.user.person.location.city != null)
            {
                angular.forEach($scope.states, function(value, key)
                {
                    if(value.id == $scope.user.person.location.city.state.id)
                    {
                        $scope.stateSelected = value;
                        $scope.getCities();
                    }
                });
            }
        };

        $loading.start('my-profile');
        UtilsProxy.getStates($scope.countrySelected, success, error);
    };

    /**
     * Metodo que obtiene las ciudades de un estado
     */
    $scope.getCities = function()
    {
        /**
         * Metodo que se ejecuta cuando la consulta de paises es exitosa
         * @param res respuesta del servicio
         */
        var success = function (res)
        {
            $loading.finish('my-profile');
            $scope.cities = res.list;
            if($scope.user.person.location.city != null)
            {
                angular.forEach($scope.cities, function(value, key)
                {
                    if(value.id == $scope.user.person.location.city.id)
                    {
                        $scope.citySelected = value;
                    }
                });
            }
        };

        $loading.start('my-profile');
        UtilsProxy.getCities($scope.stateSelected, success, error);
    };

    /**
     * Metodo que obtiene las ciudades de un estado
     */
    $scope.save = function()
    {
        $loading.start('my-profile');
        $scope.user.person.mailPreferenceList = $scope.selectionMailPreferences;
        $scope.user.person.businessTypeList = $scope.selectionBusiness;
        $scope.user.person.reasonInvestmentList = $scope.selectionReason;
        $scope.user.person.economicActivityList = $scope.selectEconomicActivities;
        if ($scope.user.person.location != null) {
            $scope.user.person.location.city = $scope.citySelected;
        }

        /**
         * Metodo que se ejecuta cuando la consulta de paises es exitosa
         * @param res respuesta del servicio
         */
        var success = function (res)
        {
            console.log('Successful update user');
            consult();
            $scope.messages = MessagesFactory.createMessages();
            $scope.messages.success.push("Se ha guardado la informacion");
            $anchorScroll();
        };

        //console.log('$scope.user::'+JSON.stringify($scope.user));
        UserProxy.updateUser($scope.user, success, error);
    };

    /**
     * Metodo que obtiene las ciudades de un estado
     */
    function consult()
    {
        /**
         * Metodo que se ejecuta cuando la consulta de paises es exitosa
         * @param res respuesta del servicio
         */
        var success = function (res)
        {
            $loading.finish('my-profile');
            $scope.user = res;
            //console.log('consult::$scope.user.person.image::'+$scope.user.person.image);
            SessionService.registerAvatar($scope.user.person.image);
            if($scope.user.person.amount != null) {
                $scope.user.person.amount = parseFloat($scope.user.person.amount);
            }

            UtilsProxy.getReasonInvestments(successReason, error);
            UtilsProxy.getBusinessTypes(successBusinessType, error);
            UtilsProxy.getMailPreferences(successMailPreferences, error);
            UtilsProxy.getCountries(successCountries, error);
            UtilsProxy.getExperienceLevel(successExperienceLevels, error);
            UtilsProxy.getHowFind(successHowFind, error);
            UtilsProxy.getEconomicActivity(successEconomicActivities, error);
        };

        //console.log('consult::$rootScope.userName::'+$rootScope.userName);
        UserProxy.getUser($rootScope.userName, success, error);
    }

    consult();

    $scope.redirecToHome = function()
    {
        $location.path("#/homeUser");
    };

    $scope.setContactPhoto = function ()
    {
        var image = null;

        image = document.getElementById( 'photo' ).files[0];

        if ( image != null )
        {

            var newSizeFile = convertsize(image.size);
            var numero = (newSizeFile.split(' ')[0]);
            var tipo = (newSizeFile.split(' ')[1]);

            if ((tipo == 'Bytes') || (tipo == 'KB') || ((tipo == 'MB') && (numero <= 1)))
            {
                var reader = new FileReader();

                reader.onloadend = function (e) {

                    var extension = image.name.split(".")[1].toLowerCase();

                    if(( extension == 'jpg') || (extension == 'jpeg') || (extension == 'png'))
                    {
                        var data = e.target.result;
                        $scope.user.person.bytesPicture = window.btoa(data);
                        $scope.user.person.namePicture = image.name;
                        $scope.user.person.extensionPicture = "."+ extension;
                        $scope.messages= null;
                        $scope.$apply();
                    }
                    else
                    {
                        console.log( 'Error loading contact photo' );
                        $scope.messages = MessagesFactory.createMessages();
                        $scope.messages.error.push( "Los formatos permitidos para la imagen son: jpg, jpeg y png" );
                        $scope.$apply();
                    }

                };

                reader.readAsBinaryString(image);
            }
            else
            {
                console.log( 'Error loading image' );
                $scope.messages = MessagesFactory.createMessages();
                $scope.messages.error.push( "Tamaño máximo permitido del archivo 1Mb" );
                $scope.$apply();
            }
        }
    };
        
    /**
     * Metodo que convierte el tamaño del archivo a bytes, kb, mb...
     * @param sizeFile tamaño del archivo
     * @returns {string}
     */
    function convertsize(sizeFile)
    {
        var bytessize = sizeFile;
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytessize == 0)
            $scope.convertedsize= '0 Byte';
        var i = parseInt(Math.floor(Math.log(bytessize) / Math.log(1024)));
        var retorno = Math.round(bytessize / Math.pow(1024, i), 2) + ' ' + sizes[i];

        return retorno;
    };

}]);