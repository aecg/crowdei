// lazyload config

var jp_config = {
  easyPieChart:   [   '../components/jquery/jquery.easy-pie-chart/dist/jquery.easypiechart.fill.js'],
  sparkline:      [   '../components/jquery/jquery.sparkline/dist/jquery.sparkline.retina.js'],
  plot:           [   '../components/jquery/flot/jquery.flot.js',
                      '../components/jquery/flot/jquery.flot.pie.js', 
                      '../components/jquery/flot/jquery.flot.resize.js',
                      '../components/jquery/flot.tooltip/js/jquery.flot.tooltip.min.js',
                      '../components/jquery/flot.orderbars/js/jquery.flot.orderBars.js',
                      '../components/jquery/flot-spline/js/jquery.flot.spline.min.js'],
  moment:         [   '../components/jquery/moment/moment.js'],
  screenfull:     [   '../components/jquery/screenfull/dist/screenfull.min.js'],
  slimScroll:     [   '../components/jquery/slimscroll/jquery.slimscroll.min.js'],
  sortable:       [   '../components/jquery/html5sortable/jquery.sortable.js'],
  nestable:       [   '../components/jquery/nestable/jquery.nestable.js',
                      '../components/jquery/nestable/jquery.nestable.css'],
  filestyle:      [   '../components/jquery/bootstrap-filestyle/src/bootstrap-filestyle.js'],
  slider:         [   '../components/jquery/bootstrap-slider/bootstrap-slider.js',
                      '../components/jquery/bootstrap-slider/bootstrap-slider.css'],
  chosen:         [   '../components/jquery/chosen/chosen.jquery.min.js',
                      '../components/jquery/chosen/bootstrap-chosen.css'],
  TouchSpin:      [   '../components/jquery/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js',
                      '../components/jquery/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css'],
  wysiwyg:        [   '../components/jquery/bootstrap-wysiwyg/bootstrap-wysiwyg.js',
                      '../components/jquery/bootstrap-wysiwyg/external/jquery.hotkeys.js'],
  dataTable:      [   '../components/jquery/datatables/media/js/jquery.dataTables.min.js',
                      '../components/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.js',
                      '../components/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.css'],
  vectorMap:      [   '../components/jquery/bower-jvectormap/jquery-jvectormap-1.2.2.min.js', 
                      '../components/jquery/bower-jvectormap/jquery-jvectormap-world-mill-en.js',
                      '../components/jquery/bower-jvectormap/jquery-jvectormap-us-aea-en.js',
                      '../components/jquery/bower-jvectormap/jquery-jvectormap.css'],
  footable:       [   '../components/jquery/footable/v3/js/footable.min.js',
                          '../components/jquery/footable/v3/css/footable.bootstrap.min.css'],
  fullcalendar:   [   '../components/jquery/moment/moment.js',
                      '../components/jquery/fullcalendar/dist/fullcalendar.min.js',
                      '../components/jquery/fullcalendar/dist/fullcalendar.css',
                      '../components/jquery/fullcalendar/dist/fullcalendar.theme.css'],
  daterangepicker:[   '../components/jquery/moment/moment.js',
                      '../components/jquery/bootstrap-daterangepicker/daterangepicker.js',
                      '../components/jquery/bootstrap-daterangepicker/daterangepicker-bs3.css'],
  tagsinput:      [   '../components/jquery/bootstrap-tagsinput/dist/bootstrap-tagsinput.js',
                      '../components/jquery/bootstrap-tagsinput/dist/bootstrap-tagsinput.css']
                      
};
