
'use strict';

angular.module('myApp.businessOpportunities', ['ngRoute'])

  .config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/businessOpportunities', {
      templateUrl: 'businessOpportunities/businessOpportunities_v2.html',
      controller: 'businessOpportunitiesCtrl'
    });
  }])

  .controller('businessOpportunitiesCtrl',  ['$scope', '$loading', '$http', '$location', '$rootScope', 'ProjectProxy', 'MessagesFactory', 'UtilsProxy', 'SessionService', 'LandingProxy',
      function($scope, $loading, $http, $location, $rootScope, ProjectProxy, MessagesFactory, UtilsProxy, SessionService, LandingProxy)
  {

    $loading.start('projects');
    SessionService.checkSession();
    $scope.openedSearch = false;
    $scope.outstandingProjectsList = [];
    $scope.newProjectsList = [];
    $scope.successfulProjectsList = [];
    $scope.allProjectsList = [];
    $scope.parameters = {};

    $scope.idEconomicActivity = {};
    $scope.selection = [];

    $scope.idSearchType = {};
    $scope.header = "proyectos";

    $rootScope.oldUrl = "businessOpportunities.html";

    /**
     * Metodo que se ejecuta cuando la autenticacion es exitosa
     * @param res respuesta del servicio
     */
    var success = function (res)
    {
      $loading.finish('projects');
      //Accion cuando el usuario es autenticado de manera exitosa
      $scope.projectList = res.projects;
      //console.log('success::$scope.projectList::'+JSON.stringify($scope.projectList));
    };

    var successAll = function (res)
    {
      var array0 = [];
      var array1 = array0.concat($scope.outstandingProjectsList);
      var array2 = array1.concat($scope.newProjectsList);
      var array3 = array2.concat($scope.successfulProjectsList);
      $scope.allProjectsList = array3.concat(res.projects);
      //console.log('successAll::$scope.allProjectsList::'+JSON.stringify($scope.allProjectsList));
      //console.log('successAll::$scope.allProjectsList.length::'+$scope.allProjectsList.length);
      $loading.finish('projects');
    };

    var successSuccessful = function (res)
    {
      $loading.finish('projects');
      $scope.successfulProjectsList = res.projects;
      //console.log('successSuccessful::$scope.successfulProjectsList::'+JSON.stringify($scope.successfulProjectsList));
      //console.log('successSuccessful::$scope.successfulProjectsList.length::'+$scope.successfulProjectsList.length);
      LandingProxy.getProjectsPriorityType('REGULAR' , successAll, error);
    };

    var successNew = function (res)
    {
      $scope.newProjectsList = res.projects;
      //console.log('successNew::$scope.newProjectsList::'+JSON.stringify($scope.newProjectsList));
      //console.log('successNew::$scope.newProjectsList.length::'+$scope.newProjectsList.length);
      LandingProxy.getProjectsPriorityType('SUCCESSFULL' , successSuccessful, error);
    };

    var successOutstanding = function (res)
    {
      $scope.outstandingProjectsList = res.projects;
      //console.log('successOutstanding::$scope.outstandingProjectsList::'+JSON.stringify($scope.outstandingProjectsList));
      //console.log('successOutstanding::$scope.outstandingProjectsList.length::'+$scope.outstandingProjectsList.length);
      LandingProxy.getProjectsPriorityType('NEW' , successNew, error);
    };

    /**
     * Metodo que se ejecuta cuando la autenticacion es exitosa
     * @param res respuesta del servicio
     */
    var successFind = function (res)
    {
      $loading.finish('projects');
      //Accion cuando el usuario es autenticado de manera exitosa
      $scope.projectList = res.projects;

      if($scope.projectList.length == 0)
      {
        $scope.messages = MessagesFactory.createMessages();
        $scope.messages.error.push("No existen proyectos que coincidan con los parámetros de búsqueda");
        $scope.showModal = !$scope.showModal;
      }
      console.log($scope.projectList );
    };

    /**
     * Metodo que se ejecuta cuando la autenticacion es exitosa
     * @param res respuesta del servicio
     */
    var error = function (res)
    {
      $loading.finish('projects');
      if(res.error == "unauthorized")
      {
        $location.path('/login/');
      }
      else
      {
        console.log('Failed Invoke Service Web');
        $scope.messages = MessagesFactory.createMessages();
        $scope.messages.error.push(res.message);
      }
    };


    /**
     * Funcion que se ejecuta cuando se da click al boton de login
     */
    $scope.openSearch = function()
    {
      $scope.openedSearch = !$scope.openedSearch;
    };

    /**
     * Funcion que se ejecuta cuando se da click al boton de buscar
     */
    $scope.find = function()
    {
      $scope.parameters.idUser = $rootScope.userId;

      $scope.parameters.idEconomicActivity = $scope.idEconomicActivity.id;

      ProjectProxy.findProjects($scope.parameters, successFind, error);
      $loading.start('projects');
      $scope.messages = null;
    };

    /**
     * Metodo que se ejecuta cuando la consulta actividades economicas es exitosa
     * @param res respuesta del servicio
     */
    var successEconomicActivities = function (res)
    {
      $scope.economicActivities = res.list;
      angular.forEach($scope.economicActivities, function(value, key)
      {
        if ($scope.idEconomicActivity != null && value.id == $scope.idEconomicActivity.id) {
          $scope.idEconomicActivity = value;
        }
      });
    };

    UtilsProxy.getEconomicActivity(successEconomicActivities, error);

    //ProjectProxy.getAllProjects( success, error);
    LandingProxy.getProjectsPriorityType('OUTSTANDING' , successOutstanding, error);


    // toggle selection for a given fruit by name
    $scope.toggleSelection = function toggleSelection(idActivity) {
      var idx = $scope.selection.indexOf(idActivity);

      if (idx > -1) {
        $scope.selection.splice(idx, 1);
      }
      else {
        $scope.selection.push(idActivity);
      }
    };

  }]);
