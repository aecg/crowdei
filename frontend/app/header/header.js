angular.module('myApp')
.directive('ngHeader', function() {
	return {
		restrict: 'E',
		controller: 'IndexCtrl',
		templateUrl: 'header/header_v2.html'
	};
})
.directive('ngHeaderLoggeado', function() {
	return {
		restrict: 'E',
		templateUrl: 'header/header_loggeado.html'
	};
});