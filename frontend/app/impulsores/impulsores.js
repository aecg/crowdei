'use strict';

angular.module('myApp.impulsores', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/impulsores', {
    templateUrl: 'impulsores/impulsores.html',
    controller: 'ImpulsoresCtrl'
  });
}])

.controller('ImpulsoresCtrl',  ['$scope', '$location', '$anchorScroll', 'SessionService', '$localStorage', function($scope, $location, $anchorScroll, SessionService, $localStorage)
{
    $scope.header = "impulsores";

    $scope.carousel_image_1 = "assets/img/impulsores/img1_" + $localStorage.language + ".jpg";
    $scope.carousel_image_2 = "assets/img/impulsores/img2_" + $localStorage.language + ".jpg";
    $scope.carousel_image_3 = "assets/img/impulsores/img3_" + $localStorage.language + ".jpg";

    $scope.$watch(function () { 
      return $localStorage.language; 
    }, function(newVal,oldVal){
          if(oldVal!==newVal && newVal !== undefined){
            $scope.carousel_image_1 = "assets/img/impulsores/img1_" + $localStorage.language + ".jpg";
            $scope.carousel_image_2 = "assets/img/impulsores/img2_" + $localStorage.language + ".jpg";
            $scope.carousel_image_3 = "assets/img/impulsores/img3_" + $localStorage.language + ".jpg";
          }
    });
}]);
