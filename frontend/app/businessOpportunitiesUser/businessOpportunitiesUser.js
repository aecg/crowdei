'use strict';

angular.module('myApp.businessOpportunitiesUser', ['ngRoute'])

  .config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/businessOpportunitiesUser', {
      templateUrl: 'businessOpportunitiesUser/businessOpportunitiesUser.html',
      controller: 'businessOpportunitiesUserCtrl'
    });
  }])

  .controller('businessOpportunitiesUserCtrl',  ['$scope', '$http', '$location', '$rootScope', 'ProjectProxy', 'MessagesFactory', 'UtilsProxy', 'SessionService',
      function($scope, $http, $location, $rootScope, ProjectProxy, MessagesFactory, UtilsProxy, SessionService)
  {

    SessionService.checkSession();
    $scope.openedSearch = false;
    $scope.projectList = [];
    $scope.parameters = {};

    $scope.idEconomicActivity = {};

    $rootScope.oldUrl = "businessOpportunitiesUser.html";

    /**
     * Metodo que se ejecuta cuando la autenticacion es exitosa
     * @param res respuesta del servicio
     */
    var success = function (res)
    {
      $('#myloader').hide();
      //Accion cuando el usuario es autenticado de manera exitosa
      $scope.projectList = res.projects;
      console.log($scope.projectList );
    };

    /**
     * Metodo que se ejecuta cuando la autenticacion es exitosa
     * @param res respuesta del servicio
     */
    var successFind = function (res)
    {
      $('#myloader').hide();
      //Accion cuando el usuario es autenticado de manera exitosa
      $scope.projectList = res.projects;

      if($scope.projectList.length == 0)
      {
        $scope.messages = MessagesFactory.createMessages();
        $scope.messages.error.push("No existen proyectos que coincidan con los parámetros de búsqueda");
        $scope.showModal = !$scope.showModal;
      }
      console.log($scope.projectList );
    };

    /**
     * Metodo que se ejecuta cuando la autenticacion es exitosa
     * @param res respuesta del servicio
     */
    var error = function (res)
    {
      $('#myloader').hide();
      if(res.error == "unauthorized")
      {
        $location.path('/login/');
      }
      else
      {
        console.log('Failed Invoke Service Web');
        $scope.messages = MessagesFactory.createMessages();
        $scope.messages.error.push(res.message);
      }
    };


    /**
     * Funcion que se ejecuta cuando se da click al boton de login
     */
    $scope.openSearch = function()
    {
      $scope.openedSearch = !$scope.openedSearch;
    };

    /**
     * Funcion que se ejecuta cuando se da click al boton de buscar
     */
    $scope.find = function()
    {
      $scope.parameters.idUser = $rootScope.user.person.id;

      $scope.parameters.idEconomicActivity = $scope.idEconomicActivity.id;

      ProjectProxy.findProjects($scope.parameters, successFind, error);
      $('#myloader').show();
      $scope.messages = null;
    };

    /**
     * Metodo que se ejecuta cuando la consulta actividades economicas es exitosa
     * @param res respuesta del servicio
     */
    var successEconomicActivities = function (res)
    {
      $scope.economicActivities = res.list;
      angular.forEach($scope.economicActivities, function(value, key)
      {
        if ($scope.idEconomicActivity != null && value.id == $scope.idEconomicActivity.id) {
          $scope.idEconomicActivity = value;
        }
      });
    };

    UtilsProxy.getEconomicActivity(successEconomicActivities, error);

    ProjectProxy.getAllProjects(true, 1, 10, success, error);
    $('#myloader').show();





  }]);
