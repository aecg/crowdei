'use strict';

// Declare app level module which depends on views, and components
var app = angular.module('myApp', [
        'ngRoute',
        'ngAnimate',
        'ngCookies',
        'ngStorage',
        'ngMessages',
        'ngMessageFormat',
        'ui.load',
        'ui.jq',
        'vcRecaptcha',
        'ui.utils',
        'pascalprecht.translate',
        'myApp.homeUser',
        'myApp.homeSuperUser',
        'myApp.home',
        'myApp.userProfile',
        'myApp.productSheet',
        'myApp.version',
        'myApp.login',
        'myApp.howWork',
        'myApp.howWorkUser',
        'myApp.changePassword',
        'myApp.investment',
        'myApp.myInvestments',
        'myApp.projects',
        'myApp.projectsSuperUser',
        'myApp.createPitch',
        'myApp.editPitch',
        'myApp.forgotPassword',
        'myApp.register',
        'myApp.modifyPitch',
        'myApp.businessOpportunitiesUser',
        'myApp.businessOpportunities',
        'myApp.historyInvestments',
        'myApp.projectsPriority',
        'myApp.contact',
        'myApp.aboutUs',
        'myApp.frequentQuestions',
        'myApp.corredores',
        'myApp.impulsores',
        'myApp.terminosCondiciones',
        'myApp.avisoPrivacidad',
        'myApp.users',
        'tmh.dynamicLocale',
        'darthwade.dwLoading',
        'anguvideo',
        'angular.morris'
    ]).
        config(['$routeProvider', function ($routeProvider)
        {
            $routeProvider.otherwise({redirectTo: '/home'});
        }]).
    /**
     * Metodo donde se declaran las variables globales de la aplicación
     */
        run(['$rootScope', '$localStorage', function ($rootScope, $localStorage)
        {
            $rootScope.showHeader = true;
            $rootScope.isLogged = false;
            $rootScope.claseContent = "app-content-full";
            $rootScope.PAYPAL_URL = "https://www.sandbox.paypal.com/cgi-bin/webscr";
            //publicKey de captcha
            $rootScope.publicKey = "6LesUA8UAAAAANYnXQpNoaE8xO62WdL81_y0iQKr";
            $rootScope.dominioBackend = "http://localhost:8080";
            $rootScope.dominioFrontend = "http://localhost:9000";
            $rootScope.dominioVideo = "http://d2vgv37inur632.cloudfront.net/";
            $rootScope.dominioImage = "http://d2vgv37inur632.cloudfront.net/";
            $localStorage.dominioImage = $rootScope.dominioImage;

            $rootScope.pagShareProject = "http://crowdei.softclear.net/#/productSheet/"
            $localStorage.language = 'es_MX'; // lenguaje predefinido
        }]).
        config(['$translateProvider', function ($translateProvider)
        {
            $translateProvider.useStaticFilesLoader({
                prefix: 'resources/locale-',
                suffix: '.json'
            });

            $translateProvider.preferredLanguage('es_MX');
            $translateProvider.useSanitizeValueStrategy('escaped');
        }])
        .config(function(tmhDynamicLocaleProvider) {
            tmhDynamicLocaleProvider.localeLocationPattern('bower_components/angular-i18n/angular-locale_{{locale}}.js');
        })
        .controller('IndexCtrl', ['$scope', '$translate', '$location', '$localStorage', 'AuthProxy', 'SessionService', 'tmhDynamicLocale', 
            function ($scope, $translate, $location, $localStorage, AuthProxy, SessionService, tmhDynamicLocale)
        {
            $scope.changeLanguage = function (language)
            {
                console.log('language::'+language);
                SessionService.registerLanguage(language);
                $translate.use($localStorage.language);

                if(language == "es_MX"){
                    tmhDynamicLocale.set('es-mx');
                }
                else{
                    tmhDynamicLocale.set('en-us');
                }
                
            };

            $scope.logout = function ()
            {
                SessionService.unregisterSession();
                AuthProxy.logout(function ()
                {
                    //console.log('logout::redirecting to home');
                    $location.path('/home');
                });
            };

            $scope.isUserLoggedIn = function () {
                return $localStorage.isLogged;
            };

            if ($localStorage.image == null) {
                $scope.header_avatar = 'template/img/user.png';
            }
            else {
                $scope.header_avatar = $localStorage.dominioImage + $localStorage.image;
            }
            //console.log('IndexCtrl::init::$scope.header_avatar::'+$scope.header_avatar);

            $scope.$watch(function () { 
              return $localStorage.image; 
            }, function(newVal,oldVal){
                if(oldVal!==newVal && newVal !== undefined){
                    if ($localStorage.image == null) {
                        $scope.header_avatar = 'template/img/user.png';
                    }
                    else {
                        $scope.header_avatar = $localStorage.dominioImage + $localStorage.image;
                    }
                    //console.log('IndexCtrl::updating::$scope.header_avatar::'+$scope.header_avatar);
                }
            });

        }])
        .controller('FooterCtrl', ['$scope', '$translate', '$localStorage', 'tmhDynamicLocale', 
            function ($scope, $translate, $localStorage, tmhDynamicLocale)
        {
            //console.log('FooterCtrl::$localStorage.language::'+$localStorage.language);
            if (($localStorage.language != null) && ($localStorage.language != $translate.use())) {
                $translate.use($localStorage.language);
            }
            if ($localStorage.language != null) {
                //console.log('FooterCtrl::$localStorage.language::'+$localStorage.language);
                if($localStorage.language == "es_MX"){
                    tmhDynamicLocale.set('es-mx');
                }
                else{
                    tmhDynamicLocale.set('en-us');
                }
            }
        }])
        //.constant('BASE_URL', 'http://localhost:8080/backend')
        .constant('BASE_URL', 'http://www.crowdei.com/backend-crowdei')
        .constant('CONEKTA_PUBLIC_KEY', 'key_KJysdbf6PotS2ut2') // Testing
    /**
     * jQuery plugin config use ui-jq directive , config the js and css files that required
     * key: function name of the jQuery plugin
     * value: array of the css js file located
     */
        .constant('JQ_CONFIG', {
            easyPieChart: ['components/jquery/jquery.easy-pie-chart/dist/jquery.easypiechart.fill.js'],
            sparkline: ['components/jquery/jquery.sparkline/dist/jquery.sparkline.retina.js'],
            plot: ['components/jquery/flot/jquery.flot.js',
                'components/jquery/flot/jquery.flot.pie.js',
                'components/jquery/flot/jquery.flot.resize.js',
                'components/jquery/flot.tooltip/js/jquery.flot.tooltip.min.js',
                'components/jquery/flot.orderbars/js/jquery.flot.orderBars.js',
                'components/jquery/flot-spline/js/jquery.flot.spline.min.js'],
            moment: ['components/jquery/moment/moment.js'],
            screenfull: ['components/jquery/screenfull/dist/screenfull.min.js'],
            slimScroll: ['components/jquery/slimscroll/jquery.slimscroll.min.js'],
            sortable: ['components/jquery/html5sortable/jquery.sortable.js'],
            nestable: ['components/jquery/nestable/jquery.nestable.js',
                'components/jquery/nestable/jquery.nestable.css'],
            filestyle: ['components/jquery/bootstrap-filestyle/src/bootstrap-filestyle.js'],
            slider: ['components/jquery/bootstrap-slider/bootstrap-slider.js',
                'components/jquery/bootstrap-slider/bootstrap-slider.css'],
            chosen: ['components/jquery/chosen/chosen.jquery.min.js',
                'components/jquery/chosen/bootstrap-chosen.css'],
            TouchSpin: ['components/jquery/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js',
                'components/jquery/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css'],
            wysiwyg: ['components/jquery/bootstrap-wysiwyg/bootstrap-wysiwyg.js',
                'components/jquery/bootstrap-wysiwyg/external/jquery.hotkeys.js'],
            dataTable: ['components/jquery/datatables/media/js/jquery.dataTables.min.js',
                'components/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.js',
                'components/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.css'],
            vectorMap: ['components/jquery/bower-jvectormap/jquery-jvectormap-1.2.2.min.js',
                'components/jquery/bower-jvectormap/jquery-jvectormap-world-mill-en.js',
                'components/jquery/bower-jvectormap/jquery-jvectormap-us-aea-en.js',
                'components/jquery/bower-jvectormap/jquery-jvectormap.css'],
            footable: ['components/jquery/footable/v3/js/footable.min.js',
                'components/jquery/footable/v3/css/footable.bootstrap.min.css'],
            fullcalendar: ['components/jquery/moment/moment.js',
                'components/jquery/fullcalendar/dist/fullcalendar.min.js',
                'components/jquery/fullcalendar/dist/fullcalendar.css',
                'components/jquery/fullcalendar/dist/fullcalendar.theme.css'],
            daterangepicker: ['components/jquery/moment/moment.js',
                'components/jquery/bootstrap-daterangepicker/daterangepicker.js',
                'components/jquery/bootstrap-daterangepicker/daterangepicker-bs3.css'],
            tagsinput: ['components/jquery/bootstrap-tagsinput/dist/bootstrap-tagsinput.js',
                'components/jquery/bootstrap-tagsinput/dist/bootstrap-tagsinput.css']

        }
    )
        .config(
        ['$controllerProvider', '$compileProvider', '$filterProvider', '$provide',
            function ($controllerProvider, $compileProvider, $filterProvider, $provide)
            {

                // lazy controller, directive and service
                app.controller = $controllerProvider.register;
                app.directive = $compileProvider.directive;
                app.filter = $filterProvider.register;
                app.factory = $provide.factory;
                app.service = $provide.service;
                app.constant = $provide.constant;
                app.value = $provide.value;
            }
        ])

        .filter( 'camelCase', 
            function ()
            {
                var camelCaseFilter = function ( input )
                {
                    var words = input.split( ' ' );
                    for ( var i = 0, len = words.length; i < len; i++ ) {
                        words[i] = words[i].charAt( 0 ).toUpperCase() + words[i].slice( 1 );
                    }
                    return words.join( ' ' );
                };
                return camelCaseFilter;
            }
        )

        .directive('numbersOnly', 
            function numbersOnly() {
                var directive = {
                    require: 'ngModel',
                    link: function (scope, element, attr, ngModelCtrl) {
                        function fromUser(text) {
                            if (text) {
                                var transformedInput = text.replace(/[^0-9]/g, '');

                                if (transformedInput !== text) {
                                    ngModelCtrl.$setViewValue(transformedInput);
                                    ngModelCtrl.$render();
                                }
                                return transformedInput;
                            }
                            return undefined;
                        }
                        ngModelCtrl.$parsers.push(fromUser);
                    }
                };
                return directive;
            }
        )
        .directive('onlyDigits', 
            function onlyDigits () {
                var directive = {
                    restrict: 'EA',
                    require: '?ngModel',
                    scope:{
                        allowDecimal: '@',
                        allowNegative: '@',
                        minNum: '@',
                        maxNum: '@'
                    },
                    link: function (scope, element, attrs, ngModel) {
                        if (!ngModel) return;
                        ngModel.$parsers.unshift(function (inputValue) {
                            var decimalFound = false;
                            var digits = inputValue.split('').filter(function (s,i) {
                                var b = (!isNaN(s) && s != ' ');
                                if (!b && attrs.allowDecimal && attrs.allowDecimal == "true") {
                                    if (s == "." && decimalFound == false) {
                                        decimalFound = true;
                                        b = true;
                                    }
                                }
                                if (!b && attrs.allowNegative && attrs.allowNegative == "true") {
                                    b = (s == '-' && i == 0);
                                }
                                return b;
                            }).join('');
                            if (attrs.maxNum && !isNaN(attrs.maxNum) && parseFloat(digits) > parseFloat(attrs.maxNum)) {
                                digits = attrs.maxNum;
                            }
                            if (attrs.minNum && !isNaN(attrs.minNum) && parseFloat(digits) < parseFloat(attrs.minNum)) {
                                digits = attrs.minNum;
                            }
                            ngModel.$viewValue = digits;
                            ngModel.$render();

                            return digits;
                        });
                    }
                };
                return directive;
            }
        )
    ;
