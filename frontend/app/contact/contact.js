'use strict';

angular.module('myApp.contact', ['ngRoute'])

  .config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/contact', {
      templateUrl: 'contact/contact_v2.html',
      controller: 'ContactCtrl'
    });
  }])

  .controller('ContactCtrl',  ['$scope', '$http', '$location', '$rootScope', 'UtilsProxy', 'vcRecaptchaService', 'MessagesFactory', '$routeParams', function($scope, $http, $location, $rootScope, UtilsProxy, vcRecaptchaService, MessagesFactory, $routeParams)
  {
    $scope.contact = {};

    /**
     * Metodo que se ejecuta cuando la autenticacion es exitosa
     * @param res respuesta del servicio
     */
    var success = function (res)
    {
      console.log( 'Succeful send mail' );
      $scope.messages = MessagesFactory.createMessages();
      $scope.messages.success.push( "Se ha enviado con éxito el mensaje" );

      clearContact();
    };

    var error = function ( res )
    {
      console.log( 'Failed Invoke Service Web' );
      $scope.messages = MessagesFactory.createMessages();
      $scope.messages.error.push( "No se pudo enviar su mensaje, por favor intente luego" );


    };

    var clearContact = function()
    {
      $scope.contact = null;
      $scope.$apply();
    };

    $scope.redirecToHome = function()
    {
      $location.path("#/home");
    };

    /**
     * Funcion que se ejecuta cuando se da click al boton de login
     */
    $scope.sendMessage = function()
    {
        if(vcRecaptchaService.getResponse() === "")
        {
            $scope.messages = MessagesFactory.createMessages();
            $scope.messages.error.push("Por favor resuelva el captcha y acepte");
        }
        else
        {
            $scope.user.captcha = vcRecaptchaService.getResponse();
            console.log('sending the captcha response to the server', $scope.response);
            UtilsProxy.sendMail($scope.contact, success, error);
        }
    };

  }]);
