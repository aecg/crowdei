'use strict';
angular.module('myApp').factory('MessagesFactory', [MessagesFactory]);

function MessagesFactory() {
    return {
        createMessages: function() {
            return {
                success: [],
                error: []
            };
        }
    };
}

