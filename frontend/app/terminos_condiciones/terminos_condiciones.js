'use strict';

angular.module('myApp.terminosCondiciones', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/terminos_condiciones', {
    templateUrl: 'terminos_condiciones/terminos_condiciones.html',
    controller: 'TerminosCondicionesCtrl'
  });
}])

.controller('TerminosCondicionesCtrl',  ['$scope', '$location', '$anchorScroll', 'SessionService', '$localStorage', function($scope, $location, $anchorScroll, SessionService, $localStorage)
{

}]);
