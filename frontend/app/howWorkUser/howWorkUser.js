'use strict';

angular.module('myApp.howWorkUser', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/howWorkUser', {
    templateUrl: 'howWorkUser/howWorkUser.html',
    controller: 'HowWorkUserCtrl'
  });
}])
.controller('HowWorkUserCtrl',  ['$scope', '$location', '$anchorScroll', 'SessionService', function($scope, $location, $anchorScroll, SessionService)
  {
    SessionService.checkSession();

    $(document).ready(function() {

      $("#crowdei").click(function() {
        $('html,body').animate({scrollTop: $("#irCrowdei").offset().top}, 2000);
      });

      $("#publicar_proyecto").click(function() {
        $('html,body').animate({scrollTop: $("#publicarProyecto").offset().top}, 2000);
      });

      $("#invertir_proyecto").click(function() {
        $('html,body').animate({scrollTop: $("#invertirProyecto").offset().top}, 2000);
      });

    });

  }]);
