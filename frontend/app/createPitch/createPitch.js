'use strict';

angular.module('myApp.createPitch', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/oldCreatePitch', {
    templateUrl: 'createPitch/createPitch.html',
    controller: 'CreatePitchCtrl'
  });
  $routeProvider.when('/createPitch', {
    templateUrl: 'createPitch/createPitch_v2.html',
    controller: 'CreatePitchCtrl'
  });
}])

.controller('CreatePitchCtrl',  ['$scope', '$loading', '$location', '$anchorScroll', 'ProjectProxy', '$rootScope', 'MessagesFactory', 'UtilsProxy', 'SessionService', '$filter', '$localStorage',
      function($scope, $loading, $location, $anchorScroll, ProjectProxy, $rootScope, MessagesFactory, UtilsProxy, SessionService, $filter, $localStorage)
  {

    $scope.header = "sube-proyectos";
    SessionService.checkSession();
    $rootScope.oldUrl = "createPitch.html";
    $scope.searchParameters = {};
    $scope.listCompanies = [];
    $scope.listContacts = [];
    $scope.existingContact = false;
    $scope.existingCompany = false;
    $scope.phoneRegex = '^\\+?\\d{1,3}?[- .]?\\(?(?:\\d{2,3})\\)?[- .]?\\d\\d\\d[- .]?\\d\\d\\d\\d$';
    $scope.videoUrlRegex = '^(((http:\/\/|https:\/\/)?((((www\.)?youtube\.com\/(watch\\?v=|embed\/))|youtu.be\/)([A-Za-z0-9_\-]+)))|((http:\/\/|https:\/\/)(vimeo\.com)\/?([A-Za-z0-9._%-]+)(\&\S+)?)$)';
    $scope.limiteDescripcion = 300000;

    function inicializarProjecto()
    {
      $loading.start('new-project');

      if ( $rootScope.user === undefined )
      {
        $location.path( '/login/' );
      }
      else {
        $scope.project = {};
        $scope.project.personType = 'FISICA';
        $scope.project.crowdfundingType = 'RECOMPENSA';
        $scope.project.contact = {};
        $scope.project.company = {};
        $scope.project.location = {};
        $scope.project.contactInformation = {};
        $scope.project.internetInformation = {};

        $scope.project.calendars = [];
        $scope.project.rewards = [];
        $scope.project.resources = [];
        $scope.project.documents = [];

        $scope.project.idPerson = $rootScope.user.person;
        $scope.project.status = "INACTIVE";
        $scope.project.priorityType = "NOTAPPLY";
        $scope.project.position = 0;
        var newDate = new Date();
        newDate.setHours(0,0,0,0);
        $scope.project.dateRegister = newDate;

        $scope.countrySelected = null;
        $scope.stateSelected = null;
        $scope.citySelected = null;
        $scope.tipoRecurso = 'CANTIDAD';
        $loading.finish('new-project');
      }
    }

    var validarCamposTab1 = function()
    {
      var retorno = true;
      //console.log('validarCamposTab1::$scope.project.personType::'+$scope.project.personType);

      if ($scope.project.personType == null || $scope.project.personType == '')
      {
        //console.log('Project contact type empty');
        $scope.messages.error.push($filter('translate')('message-error-no-contact-type'));

        retorno = false;
      }

      if($scope.project.personType == 'FISICA')
      {
        //console.log('validando persona fisica');
        //console.log('$scope.project.contact::'+JSON.stringify($scope.project.contact));

        if ($scope.project.contact && $scope.project.contact.fullname == null || $scope.project.contact.fullname == '')
        {
          //console.log('Project contact fullname empty');
          $scope.messages.error.push($filter('translate')('message-error-no-contact-fullname'));

          retorno = false;
        }
        if ($scope.project.contact 
          && $scope.existingContact == false
          && ($scope.project.contact.bytesPicture == null 
              || $scope.project.contact.bytesPicture == ''))
        {
          //console.log('Project contact photo image empty');
          $scope.messages.error.push($filter('translate')('message-error-no-contact-photo'));

          retorno = false;
        }

      }
      else if ($scope.project.personType == 'MORAL')
      {
        //console.log('validando persona moral');
        //console.log('$scope.project.company::'+JSON.stringify($scope.project.company));

        if ($scope.project.company && $scope.project.company.name == null || $scope.project.company.name == '')
        {
          //console.log('Project company name empty');
          $scope.messages.error.push($filter('translate')('message-error-no-company-name'));

          retorno = false;
        }
        if ($scope.project.company && $scope.project.company.rfc == null || $scope.project.company.rfc == '')
        {
          //console.log('Project company rfc empty');
          $scope.messages.error.push($filter('translate')('message-error-no-company-rfc'));

          retorno = false;
        }
      }

      //console.log('$scope.project.location::'+JSON.stringify($scope.project.location));
      if ($scope.project.location && $scope.project.location.address == null || $scope.project.location.address == '')
      {
        //console.log('Project location address empty');
        $scope.messages.error.push($filter('translate')('message-error-no-location-address'));

        retorno = false;
      }
      //console.log('$scope.countrySelected::'+JSON.stringify($scope.countrySelected));
      if ($scope.countrySelected == null || $scope.countrySelected.id == '')
      {
        //console.log('Project location countrySelected empty');
        $scope.messages.error.push($filter('translate')('message-error-no-location-country'));

        retorno = false;
      }
      //console.log('$scope.stateSelected::'+JSON.stringify($scope.stateSelected));
      if ($scope.stateSelected == null || $scope.stateSelected.id == '')
      {
        //console.log('Project location stateSelected empty');
        $scope.messages.error.push($filter('translate')('message-error-no-location-state'));

        retorno = false;
      }
      //console.log('$scope.citySelected::'+JSON.stringify($scope.citySelected));
      if ($scope.citySelected == null || $scope.citySelected.id == '')
      {
        //console.log('Project location citySelected empty');
        $scope.messages.error.push($filter('translate')('message-error-no-location-city'));

        retorno = false;
      }
      if ($scope.project.location && $scope.project.location.municipality == null || $scope.project.location.municipality == '')
      {
        //console.log('Project location municipality empty');
        $scope.messages.error.push($filter('translate')('message-error-no-location-municipality'));

        retorno = false;
      }
      if ($scope.project.location && $scope.project.location.postalCode == null || $scope.project.location.postalCode == '')
      {
        //console.log('Project location postalCode empty');
        $scope.messages.error.push($filter('translate')('message-error-no-location-postalCode'));

        retorno = false;
      }

      //console.log('$scope.project.contactInformation::'+JSON.stringify($scope.project.contactInformation));
      if ($scope.project.contactInformation && $scope.project.contactInformation.phoneOne == null || $scope.project.contactInformation.phoneOne == '')
      {
        //console.log('Project contactInformation phoneOne empty');
        $scope.messages.error.push($filter('translate')('message-error-no-contactInformation-phoneOne'));

        retorno = false;
      }
      if ($scope.project.contactInformation && $scope.project.contactInformation.email == null || $scope.project.contactInformation.email == '')
      {
        //console.log('Project contactInformation email empty');
        $scope.messages.error.push($filter('translate')('message-error-no-contactInformation-email'));

        retorno = false;
      }

      //console.log('$scope.project.internetInformation::'+JSON.stringify($scope.project.internetInformation));

      return retorno;
    };

    var validarCamposTab2 = function()
    {
      //console.log('validarCamposTab2');
      var retorno = true;

      //console.log('$scope.project.name::'+$scope.project.name);
      if ($scope.project && $scope.project.name == null || $scope.project.name == '')
      {
        //console.log('Project name empty');
        $scope.messages.error.push($filter('translate')('message-error-no-project-name'));

        retorno = false;
      }

      //console.log('$scope.project.image::'+$scope.project.image);
      if ($scope.project && $scope.project.image == null || $scope.project.image == '')
      {
        //console.log('Project project image empty');
        $scope.messages.error.push($filter('translate')('message-error-no-project-image'));

        retorno = false;
      }

      //console.log('$scope.project.video::'+$scope.project.video);
      if ($scope.project && $scope.project.video == null || $scope.project.video == '')
      {
        //console.log('Project project video empty');
        $scope.messages.error.push($filter('translate')('message-error-no-project-video'));

        retorno = false;
      }

      //console.log('$scope.project.briefDescription::'+$scope.project.briefDescription);
      if ($scope.project && $scope.project.briefDescription == null || $scope.project.briefDescription == '')
      {
        //console.log('Project brief description empty');
        $scope.messages.error.push($filter('translate')('message-error-no-project-brief-description'));

        retorno = false;
      }

      //console.log('$scope.project.crowdfundingType::'+$scope.project.crowdfundingType);
      if ($scope.project && $scope.project.crowdfundingType == null || $scope.project.crowdfundingType == '')
      {
        //console.log('Project crowdfunding type empty');
        $scope.messages.error.push($filter('translate')('message-error-no-crowdfunding-type'));

        retorno = false;
      }

      //console.log('$scope.project.executionPlace::'+$scope.project.executionPlace);
      if ($scope.project && $scope.project.executionPlace == null || $scope.project.executionPlace == '')
      {
        //console.log('Project execution place empty');
        $scope.messages.error.push($filter('translate')('message-error-no-execution-place'));

        retorno = false;
      }

      //console.log('$scope.project.generalObjetive::'+$scope.project.generalObjetive);
      //console.log('$scope.project.specificObjetives::'+$scope.project.specificObjetives);

      //console.log('$scope.project.impactSector::'+JSON.stringify($scope.project.impactSector));
      if ($scope.project && $scope.project.impactSector == null)
      {
        //console.log('Project impact sector empty');
        $scope.messages.error.push($filter('translate')('message-error-no-impact-sector'));

        retorno = false;
      }

      //console.log('$scope.project.economicActivity::'+JSON.stringify($scope.project.economicActivity));
      if ($scope.project && $scope.project.economicActivity == null)
      {
        //console.log('Project economic activity empty');
        $scope.messages.error.push($filter('translate')('message-error-no-economic-activity'));

        retorno = false;
      }

      //console.log('$scope.project.monthExecution::'+$scope.project.monthExecution);
      if ($scope.project && $scope.project.monthExecution == null || $scope.project.monthExecution == '')
      {
        //console.log('Project month execution empty');
        $scope.messages.error.push($filter('translate')('message-error-no-month-execution'));

        retorno = false;
      }

      //console.log('$scope.project.calendars::'+JSON.stringify($scope.project.calendars));
      //console.log('$scope.project.canvasModel::'+$scope.project.canvasModel);

      if ($scope.project && $scope.project.description != null && $scope.project.description.length > $scope.limiteDescripcion)
      {
        //console.log('Project month execution empty');
        $scope.messages.error.push($filter('translate')('message-error-descripcion'));

        retorno = false;
      }

      return retorno;
    };

    var validarCamposTab3 = function()
    {
      //console.log('validarCamposTab3');
      var retorno = true;

      //console.log('$scope.project.crowdfundingType::'+$scope.project.crowdfundingType);
      //console.log('$scope.project.rewards::'+JSON.stringify($scope.project.rewards));
      if ($scope.project && ($scope.project.crowdfundingType == 'RECOMPENSA' && ($scope.project.rewards == null || $scope.project.rewards.length == 0)))
      {
        //console.log('Project company shares empty');
        $scope.messages.error.push($filter('translate')('message-error-no-project-rewards'));

        retorno = false;
      }

      return retorno;
    }

    var validarCamposTab4 = function()
    {
      //console.log('validarCamposTab4');
      var retorno = true;

      //console.log('$scope.project.companyShares::'+$scope.project.companyShares);
      if ($scope.project && ($scope.project.crowdfundingType == 'INVERSION' && ($scope.project.companyShares == null || $scope.project.companyShares == '')))
      {
        //console.log('Project company shares empty');
        $scope.messages.error.push($filter('translate')('message-error-no-project-company-share'));

        retorno = false;
      }

      //console.log('$scope.project.minimumInvesment::'+$scope.project.minimumInvesment);
      if ($scope.project && ($scope.project.crowdfundingType == 'INVERSION' && ($scope.project.minimumInvesment == null || $scope.project.minimumInvesment == '')))
      {
        //console.log('Project minimum investment empty');
        $scope.messages.error.push($filter('translate')('message-error-no-project-minimum-investment'));

        retorno = false;
      }

      //console.log('$scope.project.invesmentGoal::'+$scope.project.invesmentGoal);
      if ($scope.project && ($scope.project.crowdfundingType == 'INVERSION' && ($scope.project.invesmentGoal == null || $scope.project.invesmentGoal == '')))
      {
        //console.log('Project investment goal empty');
        $scope.messages.error.push($filter('translate')('message-error-no-project-investment-goal'));

        retorno = false;
      }

      return retorno;
    }

    var validarCamposTab5 = function()
    {
      //console.log('validarCamposTab5');
      var retorno = true;

      //console.log('$scope.project.objetive::'+$scope.project.objetive);
      if ($scope.project && $scope.project.objetive == null || $scope.project.objetive == '')
      {
        //console.log('Project objetive empty');
        $scope.messages.error.push($filter('translate')('message-error-no-project-objetive'));

        retorno = false;
      }

      //console.log('$scope.project.currency::'+JSON.stringify($scope.project.currency));
      if ($scope.project && $scope.project.currency == null)
      {
        //console.log('Project currency empty');
        $scope.messages.error.push($filter('translate')('message-error-no-currency'));

        retorno = false;
      }

      //console.log('$scope.project.resources::'+JSON.stringify($scope.project.resources));

      return retorno;
    }

    var validarCamposTab6 = function()
    {
      //console.log('validarCamposTab6');
      var retorno = true;

      //console.log('$scope.project.documents.length::'+$scope.project.documents.length);
      if ($scope.project && ($scope.project.documents == null || $scope.project.documents.length == 0))
      {
        //console.log('Project documents empty');
        $scope.messages.error.push($filter('translate')('message-error-no-project-documents'));

        retorno = false;
      }

      return retorno;
    }

    var validarCamposTab7 = function()
    {
      //console.log('validarCamposTab7');
      var retorno = true;

      //console.log('$scope.accept::'+$scope.accept);
      if (typeof $scope.accept === "undefined") {
        //console.log('Project accept empty');
        $scope.messages.error.push($filter('translate')('message-error-no-project-accept'));

        retorno = false;
      }
      

      return retorno;
    }

    inicializarProjecto();

    $scope.notBlackListed = function ( value )
    {
      var blacklist = [ 'bad@domain.com', 'verybad@domain.com' ];
      return blacklist.indexOf( value ) === -1;
    };

    $scope.val = 15;
    var updateModel = function ( val )
    {
      $scope.$apply( function ()
      {
        $scope.val = val;
      } );
    };

    angular.element( "#slider" ).on( 'slideStop', function ( data )
    {
      updateModel( data.value );
    } );

    $scope.select2Number = [
      { text: 'First', value: 'One' },
      { text: 'Second', value: 'Two' },
      { text: 'Third', value: 'Three' }
    ];

    $scope.list_of_string = [ 'tag1', 'tag2' ]
    $scope.select2Options =
    {
      'multiple': true,
      'simple_tags': true,
      'tags': [ 'tag1', 'tag2', 'tag3', 'tag4' ]  // Can be empty list.
    };

    angular.element( "#LinkInput" ).bind( 'click', function ( event )
    {
      event.stopPropagation();
    } );

    $scope.datePicker = function ( start, end, label )
    {

    };

    var validarCampos = function()
    {
      $scope.messages = MessagesFactory.createMessages();

      var val1 = validarCamposTab1();
      var val2 = validarCamposTab2();
      var val3 = validarCamposTab3();
      var val4 = validarCamposTab4();
      var val5 = validarCamposTab5();
      var val6 = validarCamposTab6();
      var val7 = validarCamposTab7();

      return val1 && val2 && val3 && val4 && val5 && val6 && val7;
    };

    $scope.createPitch = function ( form ) {
      var success = function (res) {
        //console.log('Successful project saving');
        $scope.messages = MessagesFactory.createMessages();
        $scope.messages.success.push($filter('translate')('message-success-create-project'));

        form.$setPristine();
        form.$setUntouched();
        form.$setValidity();
        $scope.accept = null;
        $('#summernote').summernote('code', '');
        document.getElementById("createform").reset();

        inicializarProjecto();
        $scope.project.personType = 'FISICA';
        $scope.project.crowdfundingType = 'RECOMPENSA';
        UtilsProxy.getCountries(successCountries, error);

        $('#rootwizard').bootstrapWizard('show', 0);

        $loading.finish('new-project');
      };

      if (validarCampos())
      {
        $scope.project.location.city = $scope.citySelected;
        //$scope.project.company.location.city = $scope.cityCompanySelected;

        //console.log('$scope.createPitch::calling ProjectProxy.pitchRegister');
        ProjectProxy.pitchRegister( $scope.project, success, error );
        $loading.start('new-project');
      }
    };

    $scope.setFiles = function ()
    {

      var file = document.getElementById( 'documents' ).files;

      if ( file.length != 0 )
      {
        var docList = [];
        var aux = 0;
        var longitudArchivo = file.length;

        for ( var i = 0; i < file.length; i++ )
        {
          var docFile = file[ i ];

          var newSizeFile = convertsize(docFile.size);
          var numero = (newSizeFile.split(' ')[0]);
          var tipo = (newSizeFile.split(' ')[1]);

          if ((tipo == 'Bytes') || (tipo == 'KB') || ((tipo == 'MB') && (numero <= 1))) {

            var reader = new FileReader();

            reader.onloadend = function (e) {
              var data = e.target.result;
              var doc = {};
              doc.file = window.btoa(data);
              doc.name = file[aux].name;
              aux++;
              docList.push(doc);
              $scope.project.documents.push(doc);

              if (docList.length == longitudArchivo) {

                if (docList.length == file.length)
                  $scope.messages = null;

                $scope.$apply();
              }

            };

            reader.readAsBinaryString(docFile);

          }
          else
          {
            longitudArchivo--;
            console.log( 'Error loading file' );
            $scope.messages = MessagesFactory.createMessages();
            $scope.messages.error.push( "Tamaño máximo permitido del archivo 1Mb" );
            $scope.$apply();
          }

        }
      }
    };


    $scope.deleteFile = function ( file )
    {
      var idx = $scope.project.documents.indexOf( file );
      $scope.files = null;
      // is currently selected
      if ( idx > -1 )
      {
        $scope.project.documents.splice( idx, 1 );
      }

    };

    $scope.showModal = false;

    $scope.getIcono = function ( doc )
    {
      var retorno = "http://placehold.it/64x39";
      if ( doc.name.indexOf( ".pdf" ) > 0 )
      {
        retorno = "template/img/pdf.jpg"
      }
      if ( doc.name.indexOf( ".doc" ) > 0 )
      {
        retorno = "template/img/word.png"
      }
      if ( doc.name.indexOf( ".xls" ) > 0 )
      {
        retorno = "template/img/excel.png"
      }
      if ( doc.name.indexOf( ".png" ) > 0 )
      {
        retorno = "data:image/jpeg;base64," + doc.file;
      }
      if ( doc.name.indexOf( ".jpg" ) > 0 )
      {
        retorno = "data:image/jpeg;base64," + doc.file;
      }
      if ( doc.name.indexOf( ".ppt" ) > 0 )
      {
        retorno = "template/img/powerpoint.png"
      }
      return retorno;
    };

    var error = function ( res )
    {
      $loading.finish('new-project');

      if ( res.error == "unauthorized" )
      {
        $location.path( '/login/' );
      }
      else
      {
        console.log( 'Failed Invoke Service Web' );
        $scope.messages = MessagesFactory.createMessages();
        $scope.messages.error.push( res.message );
      }

    };

    /**
     * Metodo que convierte el tamaño del archivo a bytes, kb, mb...
     * @param sizeFile tamaño del archivo
     * @returns {string}
     */
    function convertsize(sizeFile)
    {
      var bytessize = sizeFile;
      var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
      if (bytessize == 0)
        $scope.convertedsize= '0 Byte';
      var i = parseInt(Math.floor(Math.log(bytessize) / Math.log(1024)));
      var retorno = Math.round(bytessize / Math.pow(1024, i), 2) + ' ' + sizes[i];

      return retorno;
    };

    /**
     * Metodo que agrega las actividades al proyecto
     */
    $scope.addActivity = function(){
      $scope.project.calendars.push({})
    }

    $scope.addActivityCalendar = function(){
      //console.log('addActivityCalendar::$scope.concepto_actividad::'+$scope.concepto_actividad+'::$(\'#fecha_actividad\').val()::'+$('#fecha_actividad').val());
      //console.log('addActivityCalendar::$(\'#fecha_actividad\').data(\'DateTimePicker\').date()::'+$('#fecha_actividad').data('DateTimePicker').date());
      if (($scope.concepto_actividad != "") && ($('#fecha_actividad').val() != "")) {
        //var monthInt = new Date($('#fecha_actividad').data("DateTimePicker").date());
        var monthDate = new Date($('#fecha_actividad').data("DateTimePicker").date());
        monthDate.setHours(0,0,0,0);

        var activityObj = {
            activity: $scope.concepto_actividad,
            estimatedDate: monthDate,
            monthFormatted: $('#fecha_actividad').val()
        };
        $scope.project.calendars.push(activityObj);

        $scope.concepto_actividad = "";
        $('#fecha_actividad').val("");
      }
      //console.log('addActivityCalendar::$scope.project.calendars::'+JSON.stringify($scope.project.calendars));
    }

    $scope.addReward = function(){
      $scope.messages = MessagesFactory.createMessages();
      //console.log('addReward::$scope.descripcion_recompesa::'+$scope.descripcion_recompesa);
      //console.log('addReward::$scope.monto_recompensa::'+$scope.monto_recompensa+'::$scope.disponibles_recompensa::'+$scope.disponibles_recompensa+'::$(\'#fecha_estimada_entrega_recompensas\').val()::'+$('#fecha_estimada_entrega_recompensas').val());
      //console.log('addReward::$(\'#fecha_estimada_entrega_recompensas\').data(\'DateTimePicker\').date()::'+$('#fecha_estimada_entrega_recompensas').data('DateTimePicker').date());
      if ((typeof $scope.descripcion_recompesa !== "undefined") && ($scope.descripcion_recompesa.trim() !== "") && 
        ($scope.monto_recompensa != "") && 
          ($scope.disponibles_recompensa != "") && ($('#fecha_estimada_entrega_recompensas').val() != "")) {
        //var monthInt = new Date($('#fecha_estimada_entrega_recompensas').data("DateTimePicker").date());
        var monthDate = new Date($('#fecha_estimada_entrega_recompensas').data("DateTimePicker").date());
        monthDate.setHours(0,0,0,0);

        var rewardObj = {
            description: $scope.descripcion_recompesa,
            amount: $scope.monto_recompensa,
            available: $scope.disponibles_recompensa,
            estimatedDate: monthDate,
            monthFormatted: $('#fecha_estimada_entrega_recompensas').val()
        };
        $scope.project.rewards.push(rewardObj);

        $scope.descripcion_recompesa = "";
        $scope.monto_recompensa = "";
        $scope.disponibles_recompensa = "";
        $('#fecha_estimada_entrega_recompensas').val("");
      }
      else
      {
        $scope.messages.error.push($filter('translate')('message-error-project-add-reward'));
      }
      //console.log('addReward::$scope.project.rewards::'+JSON.stringify($scope.project.rewards));
    }

    /**
     * Metodo que elimina las actividades del proyecto
     */
    $scope.deleteActivity = function(activity)
    {
      var idx = $scope.project.calendars.indexOf( activity );
      // is currently selected
      if ( idx > -1 )
      {
        $scope.project.calendars.splice( idx, 1 );
      }
    }

    /**
     * Metodo que agrega los recursos al proyecto
     */
    $scope.addResource = function()
    {
      $scope.project.resources.push({})
    };

    $scope.addFinancialResource = function()
    {
      $scope.messages = MessagesFactory.createMessages();
      //console.log('addFinancialResource::$scope.recurso_cantidad::'+$scope.recurso_cantidad+
      //            '::$scope.recurso_porcentaje::'+$scope.recurso_porcentaje+
      //            '::$scope.tipoRecurso::'+$scope.tipoRecurso+
      //            '::$scope.recurso_concepto::'+$scope.recurso_concepto);
      var amountPercentage = 0;
      var amount = 0;
      var percentage = 0;
      if ($scope.tipoRecurso == 'CANTIDAD')
      {
        amount = $scope.recurso_cantidad;
        amountPercentage = 1;
      }
      else
      {
        percentage = $scope.recurso_porcentaje;
        amountPercentage = 1;
      }
      //console.log('addFinancialResource::amountPercentage::'+amountPercentage);

      if ((amountPercentage != 0) && (typeof $scope.recurso_concepto !== "undefined") && ($scope.recurso_concepto.trim() !== "")) {
        var resourceObj = {
            concept: $scope.recurso_concepto,
            amount: amount,
            percentage: percentage
        };
        //console.log('addFinancialResource::resourceObj::'+JSON.stringify(resourceObj));
        $scope.project.resources.push(resourceObj);

        $scope.recurso_porcentaje = "";
        $scope.recurso_cantidad = "";
        $scope.recurso_concepto = "";
      }
      else
      {
        $scope.messages.error.push($filter('translate')('message-error-project-add-resource'));
      }
    };

    $scope.deleteResource = function(resource)
    {
      var idx = $scope.project.resources.indexOf( resource );
      // is currently selected
      if ( idx > -1 )
      {
        $scope.project.resources.splice( idx, 1 );
      }
    };

    /**
     * Metodo que valida el tamaño de la imagen del proyecto y su extension
     */
    $scope.setImageProject = function ()
    {
      var image = null;

      image = document.getElementById( 'imageProject' ).files[0];

      if ( image != null )
      {

        var newSizeFile = convertsize(image.size);
        var numero = (newSizeFile.split(' ')[0]);
        var tipo = (newSizeFile.split(' ')[1]);

        if ((tipo == 'Bytes') || (tipo == 'KB') || ((tipo == 'MB') && (numero <= 1)))
        {
          var reader = new FileReader();

          reader.onloadend = function (e) {

            var extension = image.name.split(".")[1].toLowerCase();

            if(( extension == 'jpg') || (extension == 'jpeg') || (extension == 'png'))
            {
              var data = e.target.result;
              $scope.project.image = window.btoa(data);
              $scope.project.nameImg = image.name;
              $scope.project.extensionImg = "."+ extension;
              $scope.messages= null;
              $scope.$apply();
            }
            else
            {
              console.log( 'Error loading image' );
              $scope.messages = MessagesFactory.createMessages();
              $scope.messages.error.push( "Los formatos permitidos para la imagen son: jpg, jpeg y png" );
              $scope.$apply();
            }

          };

          reader.readAsBinaryString(image);
        }
        else
        {
          console.log( 'Error loading image' );
          $scope.messages = MessagesFactory.createMessages();
          $scope.messages.error.push( "Tamaño máximo permitido del archivo 1Mb" );
          $scope.$apply();
        }

      }
    };

    /**
     * Metodo que valida el tamaño del video y su extension
     */
    $scope.setVideoProject = function ()
    {
      var video = null;

      video = document.getElementById( 'videoProject' ).files[0];

      if ( video != null )
      {

        var newSizeFile = convertsize(video.size);
        var numero = (newSizeFile.split(' ')[0]);
        var tipo = (newSizeFile.split(' ')[1]);

        if ((tipo == 'Bytes') || (tipo == 'KB') || ((tipo == 'MB') && (numero <= 3)))
        {
          var reader = new FileReader();

          reader.onloadend = function (e) {

            var extension = video.name.split(".")[1].toLowerCase();

            if(( extension == 'mp4') || (extension == 'avi') || (extension == 'mov') || (extension == 'mpeg') || (extension == 'wmv'))
            {
              var data = e.target.result;
              $scope.project.video = window.btoa(data);
              $scope.project.nameVideo = video.name;
              $scope.project.extension = "."+ extension;
              $scope.messages= null;
              $scope.$apply();
            }
            else
            {
              console.log( 'Error loading image' );
              $scope.messages = MessagesFactory.createMessages();
              $scope.messages.error.push( "Los formatos permitidos para el video son: mp4, avi, mov, mpeg y wmv" );
              $scope.$apply();
            }

          };

          reader.readAsBinaryString(video);
        }
        else
        {
          console.log( 'Error loading image' );
          $scope.messages = MessagesFactory.createMessages();
          $scope.messages.error.push( "Tamaño máximo permitido del archivo 3Mb" );
          $scope.$apply();
        }

      }
    };

    /**
     * Metodo que se ejecuta cuando la consulta de la compañia es exitosa
     * @param res respuesta del servicio
     */
    var successCompanies = function (res)
    {
      $loading.finish('new-project');
      $scope.listCompanies = res.companies;
    };

    /**
     * Método que consulta todas las compañias creadas por el usuario logeado
     * y por el nombre de la compañia
     * @param nameCompany nombre de la compania
     */
    $scope.searchCompany = function(nameCompany)
    {
      $loading.start('new-project');
      $scope.searchParameters.id = $rootScope.user.person.id;
      $scope.searchParameters.searchParameter = nameCompany;
      UtilsProxy.getCompanies( $scope.searchParameters, successCompanies, error );
    };


    function bloquearCamposCompania(valor)
    {
      if (document.form.nameCompany) {
        document.form.nameCompany.disabled = valor;
      }
      if (document.form.rfcCompany) {
        document.form.rfcCompany.disabled = valor;
      }
    };

    function bloquearCamposContacto(valor)
    {
      if (document.form.fullname) {
        document.form.fullname.disabled = valor;
      }
      if (document.form.photo) {
        document.form.photo.disabled = valor;
      }
    };
    
    function bloquearCamposDireccion(valor)
    {
      document.form.address.disabled = valor;
      document.form.country.disabled = valor;
      document.form.state.disabled = valor;
      document.form.city.disabled = valor;
      document.form.municipality.disabled = valor;
      document.form.codePostal.disabled = valor;
      document.form.ubicationReference.disabled = valor;
    };
    
    function bloquearCamposInformacionContacto(valor)
    {
      document.form.phoneOne.disabled = valor;
      document.form.phoneTwo.disabled = valor;
      document.form.movil.disabled = valor;
      document.form.email.disabled = valor;
    };

    function bloquearCamposInformacionInternet(valor)
    {
      document.form.webPage.disabled = valor;
      document.form.facebook.disabled = valor;
      document.form.twitter.disabled = valor;
      document.form.linkedIn.disabled = valor;
    };

    /**
     * Metodo que carga la informacion de la compañia selecionada en
     * los campos de la pestaña
     * @param idCompany identificador de la compañia seleccionada
     */
    $scope.loadingCompanyInformation = function (idCompany)
    {
      //console.log('loadingCompanyInformation::'+idCompany);
      for ( var i = 0; i < $scope.listCompanies.length; i++ )
      {
        if($scope.listCompanies[i].id == idCompany)
        {
          $scope.project.company = $scope.listCompanies[i];
          $scope.project.location = $scope.listCompanies[i].location;
          $scope.project.contactInformation = $scope.listCompanies[i].contactInformation;
          $scope.project.internetInformation = $scope.listCompanies[i].internetInformation;
          UtilsProxy.getCountries(successCountries, error);
          break;
        }
      }

      var valor = true;
      bloquearCamposCompania(valor);
      bloquearCamposDireccion(valor);
      bloquearCamposInformacionContacto(valor);
      bloquearCamposInformacionInternet(valor);

      $scope.listCompanies = null;
      $scope.existingCompany = true;
      //console.log('loadingCompanyInformation::$scope.project.company ::'+JSON.stringify($scope.project.company ));
    };

    /**
     * Metodo que limpia todos los campos de la pestaña compañia
     */
    function clearFormTab1  ()
    {

      $scope.project.contact = {};
      $scope.project.company = {};
      $scope.project.location = {};
      $scope.project.contactInformation = {};
      $scope.project.internetInformation = {};

      $scope.countrySelected = null;
      $scope.stateSelected = null;
      $scope.citySelected = null;

      var valor = false;
      bloquearCamposCompania(valor);
      bloquearCamposContacto(valor);
      bloquearCamposDireccion(valor);
      bloquearCamposInformacionContacto(valor);
      bloquearCamposInformacionInternet(valor);

      //$scope.$apply();

    };

    /**
     * Metodo que se ejecuta cuando la consulta de la sontactos es exitosa
     * @param res respuesta del servicio
     */
    var successContacts = function (res)
    {
      $loading.finish('new-project');
      $scope.listContacts = res.contacts;
      //console.log('successContacts::$scope.listContacts::'+JSON.stringify($scope.listContacts));
    };

    /**
     * Método que consulta todas los contactos de una compañia
     * y por el nombre del contacto
     * @param nameContact nombre del contacto
     */
    $scope.searchContact = function(nameContact)
    {
      $loading.start('new-project');
      //console.log('searchContact::$scope.project.company::'+JSON.stringify($scope.project.company));
      if($scope.project.company && $scope.project.company.id) {
        $scope.searchParameters.id = $scope.project.company.id;
      }
      $scope.searchParameters.searchParameter = nameContact;
      //console.log('searchContact::$scope.searchParameters.id::'+$scope.searchParameters.id+'::nameContact::'+nameContact);
      //console.log('searchContact::$scope.searchParameters::'+JSON.stringify($scope.searchParameters));
      UtilsProxy.getContacts( $scope.searchParameters, successContacts, error );
    };

    /**
     * Metodo que carga la informacion del contacto selecionado en
     * los campos de la pestaña
     * @param idContact identificador del contacto seleccionada
     */
    $scope.loadingContactInformation = function (idContact)
    {
      for ( var i = 0; i < $scope.listContacts.length; i++ )
      {
        if($scope.listContacts[i].id == idContact) {
          //console.log('$scope.listContacts[i]::'+JSON.stringify($scope.listContacts[i]));
          $scope.project.contact = $scope.listContacts[i];
          $scope.project.location = $scope.listContacts[i].location;
          $scope.project.contactInformation = $scope.listContacts[i].contactInformation;
          $scope.project.internetInformation = $scope.listContacts[i].internetInformation;
          UtilsProxy.getCountries(successCountries, error);
          break;
        }
      }

      var valor = true;
      bloquearCamposContacto(valor);
      bloquearCamposDireccion(valor);
      bloquearCamposInformacionContacto(valor);
      bloquearCamposInformacionInternet(valor);

      $scope.listContacts = null;
      $scope.existingContact = true;
    };

    /**
     * Metodo que limpia todos los campos de la pestaña contacto
     */
    $scope.clearContact = function()
    {
      $scope.project.contact = null;

      var valor = false;
      bloquearCamposContacto(valor);

      $scope.$apply();

    };

    /**
     * Metodo que se ejecuta cuando la consulta sector de impacto es exitosa
     * @param res respuesta del servicio
     */
    var successImpactSectors = function (res)
    {
      $scope.impactSectors = res.list;
      angular.forEach($scope.impactSectors, function(value, key)
      {
        if ($scope.project.impactSector != null && value.id == $scope.project.impactSector.id) {
          $scope.project.impactSector = value;
        }
      });
    };

    UtilsProxy.getImpactSector(successImpactSectors, error);

    /**
     * Metodo que se ejecuta cuando la consulta actividades economicas es exitosa
     * @param res respuesta del servicio
     */
    var successEconomicActivities = function (res)
    {
      $scope.economicActivities = res.list;
      angular.forEach($scope.economicActivities, function(value, key)
      {
        if ($scope.project != null && $scope.project.economicActivity != null && value.id == $scope.project.economicActivity.id) {
          $scope.project.economicActivity = value;
        }
      });
    };

    UtilsProxy.getEconomicActivity(successEconomicActivities, error);

    /**
     * Metodo que se ejecuta cuando la consulta el tipo de moneda es exitosa
     * @param res respuesta del servicio
     */
    var successCurrency= function (res)
    {
      $scope.currency = res.list;
      angular.forEach($scope.currency, function(value, key)
      {
        //console.log('successCurrency::value::'+JSON.stringify(value));
        if ($scope.project.currency != null && value.id == $scope.project.currency.id) {
          $scope.project.currency = value;
        }
      });
    };

    UtilsProxy.getCurrency(successCurrency, error);


    /**
     * Metodo que obtiene las ciudades de un estado
     * de la compañia
     */
    $scope.getCities = function()
    {
      /**
       * Metodo que se ejecuta cuando la consulta de paises es exitosa
       * @param res respuesta del servicio
       */
      var success = function (res)
      {
        $loading.finish('new-project');
        $scope.cities = res.list;
        if($scope.project.location.city != null)
        {
          angular.forEach($scope.cities, function(value, key)
          {
            if(value.id == $scope.project.location.city.id)
            {
              $scope.citySelected = value;
            }
          });
        }
      };

      //console.log('$scope.getCities::$scope.stateSelected::'+JSON.stringify($scope.stateSelected));
      if ($scope.stateSelected != null) {
        UtilsProxy.getCities($scope.stateSelected, success, error);
        $loading.start('new-project');
      }
    };

    /**
     * funcion que invoca el servicio para obtener los estados de una ciudad
     * de la compañia
     */
    $scope.getStates = function()
    {
      /**
       * Metodo que se ejecuta cuando la consulta de paises es exitosa
       * @param res respuesta del servicio
       */
      var success = function (res)
      {
        $loading.finish('new-project');
        $scope.states = res.list;
        if($scope.project.location.city != null)
        {
          angular.forEach($scope.states, function(value, key)
          {
            if(value.id == $scope.project.location.city.state.id)
            {
              $scope.stateSelected = value;
              $scope.getCities();
            }
          });
        }
      };

      UtilsProxy.getStates($scope.countrySelected, success, error);

      $loading.start('new-project');
    };

    /**
     * Metodo que se ejecuta cuando la consulta de paises
     * para la compañia es exitosa
     * @param res respuesta del servicio
     */
    var successCountries = function (res)
    {
      $scope.countries = res.list;
      var country = 154;

      //console.log('successCountries::$scope.project.location::'+JSON.stringify($scope.project.location));
      if($scope.project.location && $scope.project.location.city != null)
      {
        country = $scope.project.location.city.state.country.id
      }
      angular.forEach($scope.countries, function(value, key)
      {
        if(value.id == country)
        {
          $scope.countrySelected = value;
          $scope.getStates();
          return false;
        }
      });
    };

    UtilsProxy.getCountries(successCountries, error);

    /**
     * Metodo que obtiene las ciudades de un estado
     * de ll proyecto
     */
    $scope.getCitiesProject = function()
    {
      /**
       * Metodo que se ejecuta cuando la consulta de paises es exitosa
       * @param res respuesta del servicio
       */
      var success = function (res)
      {
        $loading.finish('new-project');
        $scope.citiesProject = res.list;
        if($scope.project.location.city != null)
        {
          angular.forEach($scope.citiesProject, function(value, key)
          {
            if(value.id == $scope.project.location.city.id)
            {
              $scope.cityProjectSelected = value;
            }
          });
        }
      };

      UtilsProxy.getCities($scope.stateProjectSelected, success, error);
      $loading.start('new-project');
    };

    /**
     * funcion que invoca el servicio para obtener los estados de una ciudad
     * del proyecto
     */
    $scope.getStatesProject = function()
    {
      /**
       * Metodo que se ejecuta cuando la consulta de paises es exitosa
       * @param res respuesta del servicio
       */
      var success = function (res)
      {
        $loading.finish('new-project');
        $scope.statesProject = res.list;
        if($scope.project.location.city != null)
        {
          angular.forEach($scope.statesProject, function(value, key)
          {
            if(value.id == $scope.project.location.city.state.id)
            {
              $scope.stateProjectSelected = value;
              $scope.getCitiesProject();
            }
          });
        }
      };

      UtilsProxy.getStates($scope.countryProjectSelected, success, error);

      $loading.start('new-project');
    };

    /**
     * Metodo que se ejecuta cuando la consulta de paises
     * para el proyecto es exitosa
     * @param res respuesta del servicio
     */
    var successCountriesProject = function (res)
    {
      $scope.countriesProject = res.list;

      if($scope.project.location.city != null)
      {
        angular.forEach($scope.countriesProject, function(value, key)
        {
          if(value.id == $scope.project.location.city.state.country.id)
          {
            $scope.countryProjectSelected = value;
            $scope.getStatesProject();
          }
        });
      }
    };

    UtilsProxy.getCountries(successCountriesProject, error);

    /**
     * Método que suma y valida los recursos financieros
     */
    $scope.sumarRecursos = function()
    {
      $scope.totalResource = 0;
      $scope.messages = MessagesFactory.createMessages();

      for ( var i = 0; i < $scope.project.resources.length; i++ )
      {
        if ($scope.project.resources[i].amount != null)
          $scope.totalResource = $scope.totalResource + $scope.project.resources[i].amount;
      }

      if ($scope.totalResource > $scope.project.objetive)
      {
        console.log('La suma de los recursos supera el monto total requerido"');
        $scope.messages.error.push("La suma de los recursos supera el monto total requerido");
      }
      else if ($scope.totalResource < $scope.project.objetive)
      {
        console.log('La suma de los recursos aun no alcanza el monto total requerido"');
        $scope.messages.error.push("La suma de los recursos es menor que el monto total requerido");
      }
      else
      {
        $scope.messages = null;
      }

    };

    $scope.validarMultimedia = function()
    {
      $scope.messages = MessagesFactory.createMessages();

      if ($scope.project.image == null)
        $scope.messages.error.push("Falta cargar la imagen del proyecto");
      if ($scope.project.video == null)
        $scope.messages.error.push("Falta cargar el video del proyecto");
      if ($scope.project.documents.length == 0)
        $scope.messages.error.push("Debe cargar al menos un documento");

    };

    $scope.redirecToHome = function()
    {
      $location.path("#/homeUser");
    };

    $scope.setContactPhoto = function ()
    {
      var image = null;

      image = document.getElementById( 'photo' ).files[0];

      if ( image != null )
      {

        var newSizeFile = convertsize(image.size);
        var numero = (newSizeFile.split(' ')[0]);
        var tipo = (newSizeFile.split(' ')[1]);

        if ((tipo == 'Bytes') || (tipo == 'KB') || ((tipo == 'MB') && (numero <= 1)))
        {
          var reader = new FileReader();

          reader.onloadend = function (e) {

            var extension = image.name.split(".")[1].toLowerCase();

            if(( extension == 'jpg') || (extension == 'jpeg') || (extension == 'png'))
            {
              var data = e.target.result;
              $scope.project.contact.bytesPicture = window.btoa(data);
              $scope.project.contact.namePicture = image.name;
              $scope.project.contact.extensionPicture = "."+ extension;
              $scope.messages= null;
              $scope.$apply();
            }
            else
            {
              console.log( 'Error loading contact photo' );
              $scope.messages = MessagesFactory.createMessages();
              $scope.messages.error.push( "Los formatos permitidos para la imagen son: jpg, jpeg y png" );
              $scope.$apply();
            }

          };

          reader.readAsBinaryString(image);
        }
        else
        {
          console.log( 'Error loading image' );
          $scope.messages = MessagesFactory.createMessages();
          $scope.messages.error.push( "Tamaño máximo permitido del archivo 1Mb" );
          $scope.$apply();
        }

      }
    };

    var currentLanguage = "es";
    var summernote_lang = "es-ES";
    if ($localStorage.language) {
      currentLanguage = $localStorage.language.substr(0, 2);
      if (currentLanguage == "en") {
        summernote_lang = "en-US";
      }
    }

    $('.datepicker').datetimepicker({
      locale: currentLanguage,
      format: 'MMMM YYYY',
      viewMode: 'months', 
      useCurrent: false,
      minDate: moment()
    });
  
    $scope.$watch(function () { 
      return $localStorage.language; 
    }, function(newVal,oldVal){
          if(oldVal!==newVal && newVal !== undefined){
            currentLanguage = $localStorage.language.substr(0, 2);
            $('.datepicker').data("DateTimePicker").locale(currentLanguage);
            console.log('currentLanguage::'+currentLanguage);
            if (currentLanguage == "en") {
              summernote_lang = "en-US";
            }
            else {
              summernote_lang = "es-ES";
            }
            console.log('summernote_lang::'+summernote_lang);

            $('#summernote').summernote('destroy');
            $('#summernote').summernote({
              minHeight: 200,
              placeholder: $filter('translate')('label-sube-proyecto-descripcion-placeholder'),
              onChange: updateProjectDescription,
              lang: summernote_lang
            });
          }
    });

    $('#summernote').summernote({
      minHeight: 200,
      placeholder: $filter('translate')('label-sube-proyecto-descripcion-placeholder'),
      onChange: updateProjectDescription,
      lang: summernote_lang
    });

    // same as above
    $("#summernote").on("summernote.change", function (e) {   // callback as jquery custom event 
      //console.log('it is changed');
      updateProjectDescription();
    });

    // and trigger sumernote.change event 
    $("#summernote").trigger('summernote.change'); 

    function updateProjectDescription()
    {
      var markupStr = $('#summernote').summernote('code');
      //console.log('updateProjectDescription::markupStr::'+markupStr);
      if ($scope.project) {
        //console.log('updateProjectDescription::updating project description');
        $scope.project.description = markupStr;
      }
    };

    $('#summernote').on('summernote.keyup', function (e)
    {
      var markupStr = $('#summernote').summernote('code');
      var length = markupStr.length;
      //console.log('summernote.keyup::length::'+length);
      $('#maxContentPost').text($scope.limiteDescripcion - length);
    });

    $('#rootwizard').bootstrapWizard({
      onTabShow: function (tab, navigation, index) {
        //console.log('onTabShow::tab::'+tab+'::index::'+index);
        var $total = navigation.find('li').length;
        var $current = index + 1;
        var $percent = ($current / $total) * 100;
        var wizard = $('#rootwizard');
  
        wizard.find('.progress-bar').css({width: $percent + '%'});
  
        if ($current >= $total) {
          wizard.find('.pager .next').hide();
          wizard.find('.pager .finish').show().removeClass('disabled');
        } else {
          wizard.find('.pager .next').show();
          wizard.find('.pager .finish').hide();
        }
      },
      onTabClick: function () {
        return false;
      },
      onNext: function(tab, navigation, index) {
        //console.log('onNext::tab::'+JSON.stringify(tab)+'::navigation::'+JSON.stringify(navigation)+'::index::'+index);
        var retorno = true;
        $scope.messages = MessagesFactory.createMessages();
        switch (index)
        {
          case 1:
            retorno = validarCamposTab1();
            break;
          case 2:
            retorno = validarCamposTab2();
            break;
          case 3:
            retorno = validarCamposTab3();
            break;
          case 4:
            retorno = validarCamposTab4();
            break;
          case 5:
            retorno = validarCamposTab5();
            break;
          case 6:
            retorno = validarCamposTab6();
            break;
          case 7:
            retorno = validarCamposTab7();
            break;
        }
        //console.log('onNext::retorno::'+retorno);

        $anchorScroll();

        $scope.$apply();

        return retorno;
      }
    });
    //$('#rootwizard').bootstrapWizard('show',1);

    $scope.changePersonType = function(value) {
      //console.log('changePersonType::'+value);
      clearFormTab1();
      $scope.existingContact = false;
      $scope.listContacts = null;
      $scope.existingCompany = false;
      $scope.listCompanies = null;
      $scope.project.company = null;
      $scope.searchParameters = {};
    };

    /**
     * Metodo que valida el tamaño de la imagen del proyecto y su extension
     */
    $scope.setCanvasProject = function ()
    {
      var image = null;

      image = document.getElementById( 'canvasModel' ).files[0];

      if ( image != null )
      {

        var newSizeFile = convertsize(image.size);
        var numero = (newSizeFile.split(' ')[0]);
        var tipo = (newSizeFile.split(' ')[1]);

        if ((tipo == 'Bytes') || (tipo == 'KB') || ((tipo == 'MB') && (numero <= 1)))
        {
          var reader = new FileReader();

          reader.onloadend = function (e) {

            var extension = image.name.split(".")[1].toLowerCase();

            if(( extension == 'jpg') || (extension == 'jpeg') || (extension == 'png') ||
                (extension == 'doc') || (extension == 'docx') ||
                (extension == 'xls') || (extension == 'xlsx') ||
                (extension == 'pdf'))
            {
              var data = e.target.result;
              $scope.project.canvasModel = window.btoa(data);
              $scope.project.nameImgCanvas = image.name;
              $scope.project.extensionImgCanvas = "."+ extension;
              $scope.messages= null;
              $scope.$apply();
            }
            else
            {
              console.log( 'Error loading image' );
              $scope.messages = MessagesFactory.createMessages();
              $scope.messages.error.push( "Los formatos permitidos para la imagen son: jpg, jpeg y png" );
              $scope.$apply();
            }

          };

          reader.readAsBinaryString(image);
        }
        else
        {
          console.log( 'Error loading image' );
          $scope.messages = MessagesFactory.createMessages();
          $scope.messages.error.push( "Tamaño máximo permitido del archivo 1Mb" );
          $scope.$apply();
        }

      }
    };

    $scope.removeReward = function ( reward )
    {
      var idx = $scope.project.rewards.indexOf( reward );
      if ( idx > -1 )
      {
        $scope.project.rewards.splice( idx, 1 );
      }
    };

}]);
