'use strict';

angular.module('myApp.login', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider)
    {
        $routeProvider.when('/login', {
            templateUrl: 'login/login_v2.html',
            controller: 'LoginCtrl'
        });
    }])

    .controller('LoginCtrl', ['$scope', '$http', '$location', '$rootScope', 'AuthProxy', 'UserProxy', 'MessagesFactory','SessionService', '$filter',
        function ($scope, $http, $location, $rootScope, AuthProxy, UserProxy, MessagesFactory, SessionService, $filter)
    {

        $scope.user = {};
        $scope.header = "registro";

        /**
         * Funcion que se ejecuta cuando se da click al boton de login
         */
        $scope.login = function (messages)
        {
            var retorno = AuthProxy.login($scope.user, success, error);
        };


        /**
         * Metodo que se ejecuta cuando la autenticacion es exitosa
         * @param res respuesta del servicio
         */
        var successGetData = function (res)
        {
            //console.log('successGetData::res::'+JSON.stringify(res));
            $scope.authError = null;
            $rootScope.showHeader = true;
            $rootScope.isLogged = true;
            $rootScope.claseContent = "app-content";
            $rootScope.userName = $scope.user.username;
            $rootScope.userId = res.id;
            $rootScope.nameUser = res.person.name;
            $rootScope.email = res.person.email;
            $rootScope.lastname = res.person.lastName;
            $rootScope.image = res.person.image;
            $rootScope.personId = res.person.id;

            $rootScope.user = $scope.user;
            $rootScope.user.person = {};
            $rootScope.user.person.id = res.person.id;
            $rootScope.user.id = $rootScope.userId;
            $rootScope.user.authorities = res.authorities;

            //console.log('successGetData::$rootScope.user::'+JSON.stringify($rootScope.user));
            if ($rootScope.user.authorities.indexOf('ROLE_SUPER_ADMIN') != -1)
            {
                $location.path('/homeSuperUser');
            }
            else
            {
                $location.path('/homeUser');
            }

            SessionService.registerSession();


        };


        /**
         * Metodo que se ejecuta cuando la autenticacion es exitosa
         * @param res respuesta del servicio
         */
        var success = function (res)
        {
            //console.log('success::res.data::'+JSON.stringify(res.data));
            $http.defaults.headers.common['Authorization'] = 'Bearer ' + res.data.access_token;
            SessionService.registerEncabezado($http.defaults.headers.common['Authorization'] );
            var retorno = UserProxy.getUserDataLogin(successGetData, error);
        };


        /**
         * Metodo que se ejecuta cuando la autenticacion es exitosa
         * @param res respuesta del servicio
         */
        var error = function (res)
        {
            //console.log('error::res::'+JSON.stringify(res));
            $scope.messages = MessagesFactory.createMessages();
            $scope.messages.error.push($filter('translate')('message-error-login'));
        };
    }]);






