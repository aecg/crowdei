'use strict';

angular.module('myApp.home', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/home', {
    templateUrl: 'home/home_v2.html',
    controller: 'HomeCtrl'
  });
}])

.controller('HomeCtrl',  ['$scope', '$http', '$location', '$rootScope', 'LandingProxy', 'MessagesFactory', '$localStorage', function($scope, $http, $location, $rootScope, LandingProxy, MessagesFactory, $localStorage) {

    $rootScope.claseContent = "app-content-full";

    $scope.projectList = [];

    $rootScope.oldUrl = "home.html";
    $scope.header = "";

    $scope.carousel_image_1 = "assets/img/index/img1_" + $localStorage.language + ".jpg";
    $scope.carousel_image_2 = "assets/img/index/img2_" + $localStorage.language + ".jpg";
    $scope.carousel_image_3 = "assets/img/index/img3_" + $localStorage.language + ".jpg";
    $scope.how_works_image = "assets/img/index/como_funciona_" + $localStorage.language + ".jpg";

    $scope.$watch(function () { 
      return $localStorage.language; 
    }, function(newVal,oldVal){
          if(oldVal!==newVal && newVal !== undefined){
            $scope.carousel_image_1 = "assets/img/index/img1_" + $localStorage.language + ".jpg";
            $scope.carousel_image_2 = "assets/img/index/img2_" + $localStorage.language + ".jpg";
            $scope.carousel_image_3 = "assets/img/index/img3_" + $localStorage.language + ".jpg";
            $scope.how_works_image = "assets/img/index/como_funciona_" + $localStorage.language + ".jpg";
          }
    });

    /**
     * Metodo que se ejecuta cuando la autenticacion es exitosa
     * @param res respuesta del servicio
     */
    var success = function (res)
    {
      $('#myloader').hide();
      //Accion cuando el usuario es autenticado de manera exitosa
      $scope.projectList = res.projects;
      //console.log( JSON.stringify($scope.projectList) );
    };

    /**
     * Metodo que se ejecuta cuando la autenticacion es exitosa
     * @param res respuesta del servicio
     */
    var error = function (res)
    {
      $('#myloader').hide();
      if(res.error == "unauthorized")
      {
        $location.path('/login/');
      }
      else
      {
        console.log('Failed Invoke Service Web');
        $scope.messages = MessagesFactory.createMessages();
        $scope.messages.error.push(res.message);
      }
    };

    LandingProxy.getProjectsPriorityType('OUTSTANDING' , success, error);

    $('#myloader').show();

    $scope.seeMore = function(priority)
    {
      $location.url("/projectsPriority/"+ priority);
    };


}]);
