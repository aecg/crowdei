'use strict';

angular.module('myApp.investment', ['ngRoute', 'ui.bootstrap'])

    .config(['$routeProvider', function ($routeProvider)
    {
        $routeProvider.when('/investment.old/:idProject',
            {
                templateUrl: 'investment/investment.html',
                controller: 'InvestmentCtrl'
            });

        $routeProvider.when('/investment/:idProject/:idReward/:amountInvestment',
            {
                templateUrl: 'investment/investment_v2.html',
                controller: 'InvestmentCtrl'
            });
    }])

    .controller('InvestmentCtrl', ['$window', '$scope', '$loading', '$http', '$location', '$rootScope', '$routeParams', '$localStorage', '$modal','ProjectProxy', 'MessagesFactory', 'SessionService', 'InvestmentProxy', 'CONEKTA_PUBLIC_KEY',
        function ($window, $scope, $loading, $http, $location, $rootScope, $routeParams, $localStorage, $modal, ProjectProxy, MessagesFactory, SessionService, InvestmentProxy, CONEKTA_PUBLIC_KEY)
    {
        $scope.paymentMethod = 'CONEKTA';
        $scope.showPayConekta = true;
        $scope.showPayTransfer = true;
        $loading.finish('invesment');
        var initYear = new Date().getFullYear();
        var finishYear = initYear + 5;

        var rangeYears = [];
        for(var i = initYear; i < finishYear; i++) {
            rangeYears.push(i);
        }
        $scope.rangeYears = rangeYears;

        var initMonth = 1;
        var finishMonth = initMonth + 12;

        var rangeMonths = [];
        for(var i = initMonth; i < finishMonth; i++) {
            rangeMonths.push(i);
        }
        $scope.rangeMonths = rangeMonths;

        SessionService.checkSession();
        /**
         * Metodo que inicializa todos los parametros de la consulta
         */
        function init()
        {
            if ( $rootScope.user === undefined ||  $rootScope.user === null )
            {
                $location.path( '/login/' );
            }
            else {
                $loading.start('project');
                $scope.tipo = "pago";
                $scope.acepto = false;
                $scope.mensaje = "";
                $scope.showModal = false;
                $scope.isPago = true;
                $scope.isTransferencia = false;
                $scope.project = {};
                $scope.reward = {};
                $scope.paymentId = "";
                $scope.mercadoPago = {};

                ProjectProxy.getProjectDetail($routeParams.idProject, success, error);
            }
        }


        /**
         * Metodo que se ejecuta cuando la consulta de los datos del proyecto es exitosa
         * @param res respuesta del servicio
         */
        var success = function (res)
        {
            //Accion cuando el usuario es autenticado de manera exitosa
            $scope.project = res;
            $scope.project.id = $routeParams.idProject;
            $scope.priceInvestment = $routeParams.amountInvestment;
            $scope.reward.id = $routeParams.idReward;

            var data = {};
            $loading.finish('project');
            //console.log("Inicializada la vista Investment exitosamente");
            //console.log("Se ha obtenido los datos del proyecto exitosamente::$scope.reward::"+JSON.stringify($scope.reward));
        };

        var successSendMail = function () {
          //console.log('Mail is successfully sent to the contact person');
          //$location.path("/historyInvestments/"+ $scope.project.id);
          $scope.messages.success.push("Su código de transferencia ha sido registrado satisfactoriamente");
          $scope.showPayTransfer = true;
          $loading.finish('invesment');
        };

        var errorSendMail = function () {
          //console.log('Could not send mail to the contact person');
          $scope.messages.error.push("Ha ocurrido un error al registrar su código de transferencia");
          $scope.showPayTransfer = true;
          $loading.finish('invesment');
        };

        /**
         * Metodo que se ejecuta cuando la consulta de los datos del proyecto es exitosa
         * @param res respuesta del servicio
         */
        var successPayMercadoPago = function (res)
        {
            //console.log("Se ha pagado exitosamenet");
            $rootScope.oldUrl = 'investment.html';
        };

        /**
         * Metodo que se ejecuta cuando es exitosa
         * @param res respuesta del servicio
         */
        var successTransfer = function (res)
        {
            //console.log("Se ha pagado exitosamente");
            $rootScope.oldUrl = 'investment.html';
            $scope.tipo = "confirmacion";
            $loading.start('invesment');
            $scope.showPayTransfer = false;
            $scope.messages = MessagesFactory.createMessages();
            InvestmentProxy.sendMailInvestmentTransferMyProject(res, successSendMail, errorSendMail);
        };


        /**
         * Metodo que se ejecuta cuando la consulta de los datos del proyecto es exitosa
         * @param res respuesta del servicio
         */
        var successUrlMercadoPago = function (res)
        {
            $MPC.openCheckout({
                url: res.url,
                mode: "modal",
                onreturn: function (data)
                {

                    $scope.data.data = JSON.stringify(data);
                    $scope.data.rewardId = $scope.reward.id;

                    ProjectProxy.processPayMercadoPago($rootScope.userId, $scope.project.id, $scope.data, successPayMercadoPago, error);
                }
            });
        };


        /**
         * Metodo que se ejecuta cuando la consulta de los datos del proyecto no es exitosa
         * @param res respuesta del servicio
         */
        var error = function (res)
        {
            $loading.finish('project');
            if (res.error == "unauthorized")
            {
                //console.log("No se tiene permiso para acceder a la aplicacion");
                $location.path('/login/');
            }
            else
            {
                $scope.tipo = "confirmacion";
                $scope.isPago = false;
                $scope.isTransferencia = false;
                //console.log("Ha ocurrido un error consultando los datos del proyecto");
                $scope.messages = MessagesFactory.createMessages();
                $scope.messages.error.push("Su pago ha sido rechazado.");
            }
        };

        $scope.selectPaymentMethod = function (method)
        {
            $scope.paymentMethod = method;
        };

        /**
         * Metodo que arma el objeto requerido por la funcion de paypal para realizar el pago
         */
        $scope.paypalData = function paypalData()
        {
            var htmlForm = "";
            var product = {};
            if($scope.project) {
                product.id = $scope.project.id;
                product.name = $scope.project.name;
            }
            product.qty = "1";
            product.price = parseFloat($scope.priceInvestment);


            var userData = {};
            userData.cmd = "_cart";
            userData.upload = "1";
            userData.business = "paypal-facilitator@softclear.net";
            userData.currencyCode = "USD";
            userData.lc = "US";
            userData.rm = 2;
            //url retorno paypal lado server, envia data post
            userData.successUrl = $rootScope.dominioBackend + "/backend/user/" + $rootScope.userId 
            + "/project/" + product.id + "/reward/" + $scope.reward.id
            + "/investment/paypal/";
            userData.cancelUrl = $rootScope.dominioFrontend + "/#/investment/" + product.id +'/' + $scope.reward.id + '/' + $scope.priceInvestment;
            userData.cbt = "Volver a la tienda";
            userData.formClass = "#formPaypal";


            //htmlForm += "<input type='hidden' name='item_number_1' value=" + product.id + " />";
            htmlForm += "<input type='hidden' name='item_name_1' value='" + product.name + "' />";
            htmlForm += "<input type='hidden' name='quantity_1' value=" + product.qty + " />";
            htmlForm += "<input type='hidden' name='amount_1' value=" + product.price.toFixed(2) + " />";

            htmlForm += "<input type='hidden' name='cmd' value='" + userData.cmd + "' />";
            htmlForm += "<input type='hidden' name='upload' value='" + userData.upload + "' />";
            htmlForm += "<input type='hidden' name='business' value='" + userData.business + "' />";
            htmlForm += "<input type='hidden' name='cancel_return' value='" + userData.cancelUrl + "' />";
            htmlForm += "<input type='hidden' name='cbt' value='" + userData.msgReturn + "' />";
            htmlForm += "<input type='hidden' name='return' value='" + userData.successUrl + "' />";
            htmlForm += "<input type='hidden' name='rm' value=" + userData.rm + " />";
            htmlForm += "<input type='hidden' name='lc' value='" + userData.lc + "' />";
            htmlForm += "<input type='hidden' name='currency_code' value='MXN' />";
            htmlForm += "<input type='hidden' name='cbt' value='" + userData.cbt + "' />";

            // if ($scope.acepto)
            //     htmlForm += "<input type='image' src='template/img/paypal.png' border='0' width='200' height='112' title='Paypal'  alt='Paypal' name='submit' />";
            // else
            //     htmlForm += "<input type='image' src='template/img/paypal.png' border='0' width='200' height='112' title='Paypal'  alt='Paypal' disabled name='submit' />";

            //$(userData.formClass).html("").append(htmlForm);
            $(userData.formClass).append(htmlForm);
        };


        /**
         * Metodo que realiza el pago mediante mercado pago
         */
        $scope.payMercadoPago = function ()
        {

            $scope.data = {};

            $scope.data.description = $scope.project.name;
            $scope.data.amount = parseFloat($scope.priceInvestment);
            $scope.data.userName = $rootScope.nameUser + $rootScope.lastname;
            $scope.data.email = $rootScope.email;
            $scope.data.currencyId = $scope.project.currency.code;

            ProjectProxy.getUrlMercadoPago($scope.data, successUrlMercadoPago, error);
            return false;
        };


        /**
         * Metodo que realiza el pago mediante transferencia
         */
        $scope.payTransferencia = function ()
        {

            if ( $rootScope.user === undefined )
            {
                $location.path( '/login/' );
            }
            else {
                var data = {};
                data.token = $scope.idTransferencia;
                data.amount = parseFloat($scope.priceInvestment);

                var product = $scope.project;

                data.successUrl = $rootScope.dominioBackend + "/backend/user/" + $rootScope.userId + "/project/" + product.id
                + "/investment/mercadopago/";
                data.pendingUrl = $rootScope.dominioBackend + "/backend/user/" + $rootScope.userId + "/project/" + product.id
                + "/investment/mercadopago/";
                data.cancelUrl = $rootScope.dominioFrontend + "/#/investment/" + product.id;
                data.cancelUrl = $rootScope.dominioFrontend + "/#/investment/" + product.id;
                data.rewardId = $scope.reward.id;

                ProjectProxy.processPayTransferer($rootScope.userId, $scope.project.id, data, successTransfer, error);
                return false;
            }
        };

        /**
         * Metodo que muestra el popUp de transferencia
         *
         */
        $scope.showTransferencia = function ()
        {
            $scope.idTransferencia = Math.floor((Math.random() * 1000000000000) + 1);
            $scope.showModal = true;
        };
        /**
         * Método que muestra el popUp de conekta
         */
        $scope.payConekta = function () {

            var conektaSuccess = function (token) 
            {
                //console.log('payConekta::conektaSuccess::projectData::'+JSON.stringify(projectData));
                projectData.tokenId = token.id;
                ProjectProxy.processPayConekta(projectData, 
                    function () 
                    {
                        $loading.finish('invesment');
                        $scope.messages.success.push('Pago procesado satisfactoriamente');
                        $scope.showPayConekta = true;
                        $scope.conektaParams = {};
                    }, 
                    function (error) {
                        $loading.finish('invesment');
                        //console.log('conektaError::e::'+JSON.stringify(error));
                        $scope.messages.error.push(error.message);
                        $scope.showPayConekta = true;
                    }
                );
            };

            var conektaError = function (e)
            {
                //console.log('conektaError::e::'+JSON.stringify(e));
                $scope.messages.error.push(e.message_to_purchaser);
                $scope.showPayConekta = true;
                $loading.finish('invesment');
                $scope.$apply();
            };

            if ( $rootScope.user === undefined )
            {
                $location.path( '/login/' );
            }
            else {
                $loading.start('invesment');
            	$scope.messages = MessagesFactory.createMessages();

                //console.log('payConekta::$scope.project::'+JSON.stringify($scope.project));
                var projectData = {};
                if ($scope.project && $scope.project.name)
                    projectData.description = $scope.project.name;
                if ($scope.priceInvestment)
                    projectData.amount = parseFloat($scope.priceInvestment);
                if ($rootScope.nameUser && $rootScope.lastname)
                    projectData.userName = $rootScope.nameUser + ' ' + $rootScope.lastname;
                if ($rootScope.email)
                    projectData.email = $rootScope.email;
                if ($scope.project && $scope.project.currency && $scope.project.currency.code)
                    projectData.currencyId = $scope.project.currency.code;
                if ($rootScope.userId)
                    projectData.userId = $rootScope.userId;
                if ($scope.project && $scope.project.id)
                    projectData.projectId = $scope.project.id;
                if ($scope.reward && $scope.reward.id)
                    projectData.rewardId = $scope.reward.id;
                //console.log('payConekta::projectData::'+JSON.stringify(projectData));
                Conekta.setPublicKey(CONEKTA_PUBLIC_KEY);
                Conekta.Token.create($scope.conektaParams, conektaSuccess, conektaError);
                $scope.showPayConekta = false;
            }
        };

        init();

    }]);
