'use strict';

angular.module('myApp').service('UserProxy', ['$http', 'BASE_URL', '$localStorage', UserProxy]);

function UserProxy($http, BASE_URL,$localStorage)
{


    /**
     * Metodo que realiza la invocacion del servicio que realiza el registro del usuario
     * @param user datos del usuario a registrar
     * @param success metodo que se ejecuta cuando es exitosa la respuesta
     * @param error metodo que se ejecuta cuando da error la repuesta
     */
    this.updateUser = function(user, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        $http
            .put(BASE_URL + '/web/users', user)
            .success(success)
            .error(error);
    };


    /**
     * Metodo que realiza la invocacion del servicio que realiza el registro del usuario
     * @param user datos del usuario a registrar
     * @param success metodo que se ejecuta cuando es exitosa la respuesta
     * @param error metodo que se ejecuta cuando da error la repuesta
     */
    this.getUser = function(user, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        $http
            .get(BASE_URL + '/web/users/' + user)
            .success(success)
            .error(error);
    };


    /**
     * Metodo que realiza la invocacion del servicio que realiza el registro del usuario
     * @param success metodo que se ejecuta cuando es exitosa la respuesta
     * @param error metodo que se ejecuta cuando da error la repuesta
     */
    this.getUserDataLogin = function( success, error )
    {
        if ($http.defaults.headers.common['Authorization'] == null)
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
      $http
        .get(BASE_URL + '/web/users/login')
        .success(success)
        .error(error);
    };


    this.getAllUsers = function (adminUsers = false, activeUsers = false, page = 0, size = 0, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
        {
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        }
        $http
            .get(BASE_URL + '/web/users?adminUsers='+adminUsers+'&activeUsers='+activeUsers+
                                                '&page='+page+'&size='+size)
            .success(success)
            .error(error);
    };

    this.activateUser = function (id, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
        {
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        }
        $http
            .post(BASE_URL + '/web/users/activate/' + id, {})
            .success(success)
            .error(error);
    };

    this.deactivateUser = function (id, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
        {
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        }
        $http
            .post(BASE_URL + '/web/users/deactivate/' + id, {})
            .success(success)
            .error(error);
    };

}


