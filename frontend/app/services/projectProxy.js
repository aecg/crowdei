angular.module('myApp').service('ProjectProxy', ['$http', 'BASE_URL', '$localStorage', ProjectProxy]);

/**
 * Servicio que hace la vez de Proxy para los servicios rest de la aplicacion
 * @param $http objeto requerido para la invocaciones http de los servicios
 * @param BASE_URL Constante que posee la ruta base de los servicios
 * @constructor
 */
function ProjectProxy($http, BASE_URL, $localStorage)
{

    this.pitchRegister = function (project, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
        {
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        }
        $http
            .post(BASE_URL + '/web/projects', project)
            .success(success)
            .error(error);
    };

    this.pitchEdit = function (project, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
        {
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        }
        $http
            .put(BASE_URL + '/web/projects', project)
            .success(success)
            .error(error);
    };


    /**
     * Metodo que consulta todos los proyectos de un usuario
     * @param user datos del usuario
     * @param success metodo que se ejecuta en caso de exito de la invocacion
     * @param error metodo que se ejecuta en caso de error de la invocacion
     */
    this.getProjects = function (user, page, size, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
        {
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        }
        var urlProjects = '/web/projects?id=' + user;
        if ((page != null) && (size != null)) {
            urlProjects = urlProjects + '&page=' + page + '&size=' + size;
        }
        $http
            .get(BASE_URL + urlProjects)
            .success(success)
            .error(error);
    };


    /**
     * Metodo que consulta todos los proyectos de un usuario
     * @param user datos del usuario
     * @param success metodo que se ejecuta en caso de exito de la invocacion
     * @param error metodo que se ejecuta en caso de error de la invocacion
     */
    this.deleteProject = function (user, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
        {
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        }
        $http
            .delete(BASE_URL + '/web/projects/' + user)
            .success(success)
            .error(error);
    };


    ///**
    // * Metodo que consulta todos los proyectos dado unos parametros
    // * @param parameters datos del usuario
    // * @param success metodo que se ejecuta en caso de exito de la invocacion
    // * @param error metodo que se ejecuta en caso de error de la invocacion
    //*/
    //this.findProjects = function ( parameters, success, error )
    //{
    //  $http
    //    .post( BASE_URL + '/web/projects/find', parameters )
    //    .success( success )
    //    .error( error );
    //};

    /**
     * Metodo que consulta todos los proyectos de un usuario
     * @param parameters datos del usuario
     * @param success metodo que se ejecuta en caso de exito de la invocacion
     * @param error metodo que se ejecuta en caso de error de la invocacion
     */
    this.findProjectsXIdXParams = function (parameters, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
        {
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        }
        $http
            .post(BASE_URL + '/web/projects/findXIdXParams', parameters)
            .success(success)
            .error(error);
    };


    /**
     * Metodo que procesa el pago de mercado libre
     * @param user identificador del usuario
     * @param project datos del proyecto
     * @param data datos del pago
     * @param success metodo que se ejecuta en caso de exito de la invocacion
     * @param error metodo que se ejecuta en caso de error de la invocacion
     */
    this.processPayMercadoPago = function (user, project, data, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
        {
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        }
        $http
            .post(BASE_URL + '/user/' + user + '/project/' + project + '/investment/mercadopago/', data)
            .success(success)
            .error(error);
    };

    /**
     * Método que envía los datos de pago al servidor
     * @param data datos del pago
     * @param success success metodo que se ejecuta cuando es exitoso la invocacion
     * @param error error metodo que se ejecuta cuando es fallido la invocacion
     */
    this.processPayConekta = function (data, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        $http
            .post(BASE_URL + '/user/' + data.userId + '/project/' + data.projectId + '/investment/conekta/', data)
            .success(success)
            .error(error);
    };

    /**
     * Metodo que procesa el pago de mercado libre
     * @param user identificador del usuario
     * @param project datos del proyecto
     * @param data datos del pago
     * @param success metodo que se ejecuta en caso de exito de la invocacion
     * @param error metodo que se ejecuta en caso de error de la invocacion
     */
    this.processPayTransferer = function (user, project, data, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
        {
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        }
        $http
            .post(BASE_URL + '/user/' + user + '/project/' + project + '/investment/transferer/', data)
            .success(success)
            .error(error);
    };


    /**
     * Metodo que procesa el pago de mercado libre
     * @param user identificador del usuario
     * @param project datos del proyecto
     * @param data datos del pago
     * @param success metodo que se ejecuta en caso de exito de la invocacion
     * @param error metodo que se ejecuta en caso de error de la invocacion
     */
    this.getUrlMercadoPago = function (data, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
        {
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        }
        $http
            .post(BASE_URL + '/payments/mercadopago/', data)
            .success(success)
            .error(error);
    };

    /**
     * Metodo que consulta todos los proyectos dado unos parametros
     * @param parameters datos del usuario
     * @param success metodo que se ejecuta en caso de exito de la invocacion
     * @param error metodo que se ejecuta en caso de error de la invocacion
     */
    this.findProjects = function (parameters, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
        {
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        }
        $http
            .post(BASE_URL + '/find', parameters)
            .success(success)
            .error(error);
    };

    /**
     * Metodo que consulta todos los proyectos de un usuario
     * @param user datos del usuario
     * @param success metodo que se ejecuta en caso de exito de la invocacion
     * @param error metodo que se ejecuta en caso de error de la invocacion
     */
    this.getAllProjects = function (activeProjects = false, page = 0, size = 0, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
        {
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        }
        $http
            .get(BASE_URL + '/findProjects?activeProjects='+activeProjects+
                                                '&page='+page+'&size='+size)
            .success(success)
            .error(error);
    };

    /**
     * Metodo que el detalle de un proyecto
     * @param project datos del usuario
     * @param success metodo que se ejecuta en caso de exito de la invocacion
     * @param error metodo que se ejecuta en caso de error de la invocacion
     */
    this.getProjectDetail = function (project, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
        {
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        }
        $http
            .get(BASE_URL + '/project/' + project)
            .success(success)
            .error(error);
    };

    /**
     * Metodo que consulta todos los proyectos en los que invirtio un usuario
     * @param id del usuario
     * @param success metodo que se ejecuta en caso de exito de la invocacion
     * @param error metodo que se ejecuta en caso de error de la invocacion
     */
    this.getAllProjectsInvestment = function (user, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
        {
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        }
        $http
            .get(BASE_URL + '/web/projects/findInvestment?id=' + user + '&page=1&size=10')
            .success(success)
            .error(error);
    };

    /**
     * Metodo que consulta todos los proyectos en los que invirtio un usuario
     * dado un filtro
     * @param parameters datos del usuario
     * @param success metodo que se ejecuta en caso de exito de la invocacion
     * @param error metodo que se ejecuta en caso de error de la invocacion
     */
    this.findProjectsInvestmentXIdXParams = function (parameters, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
        {
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        }
        $http
            .post(BASE_URL + '/web/projects/findProjectsInvestmentXIdXParams', parameters)
            .success(success)
            .error(error);
    };

    this.activateProject = function (id, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
        {
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        }
        $http
            .post(BASE_URL + '/web/projects/activate/' + id, {})
            .success(success)
            .error(error);
    };

    this.deactivateProject = function (id, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
        {
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        }
        $http
            .post(BASE_URL + '/web/projects/deactivate/' + id, {})
            .success(success)
            .error(error);
    };

    this.changePriority = function (project, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
        {
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        }
        $http
            .put(BASE_URL + '/web/projects/change-priority', project)
            .success(success)
            .error(error);
    };
}
