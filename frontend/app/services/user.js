'use strict';

angular.module('myApp').service('UserService', ['$http', 'BASE_URL', '$localStorage', UserService]);

function UserService($http, BASE_URL, $localStorage)
{
    var userLogged = false; // Not using rootScope because this object will be a singleton by definition

    this.login = function (user)
    {
        //this.login = function(user, messages) {

        var success = function (res)
        {
            // This if should be recorded in sessionStorage if we want "remember me" option
            if (res.data && res.data.access_token)
            {
                // Setting token for future requests
                $localStorage.Authorization = 'Bearer ' + res.data.access_token;
                $http.defaults.headers.common['Authorization'] =
                    'Bearer ' + res.data.access_token;

                // Setting user logged for all application
                userLogged = true;
            }
        };

        $http({
            method: 'post',
            url: BASE_URL + '/oauth/token',
            headers: {
                'Authorization': 'Basic ZnJvbnRlbmQ6', // Basic frontend:
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            },
            data: {
                'grant_type': 'password',
                'username': user.username,
                'password': user.password
            },
            transformRequest: function (data)
            {
                return $.param(data);
            }
        }).then(success);
    };

    this.isUserLogged = function ()
    {
        return userLogged;
    };


    this.loadUser = function (user)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        $http
            .get(BASE_URL + '/desktop/profiles')
            .success(function (res)
            {
                if (res)
                {
                    if (res.company)
                    {
                        user.name = res.company.name;
                        user.fiscalId = res.company.fiscalId;
                        user.address = res.company.address;
                    }

                  if (res.user)
                  {
                    user.username = res.user.username;
                  }
                }
            });
    };

    this.changePassword = function (user)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        //this.changePassword = function(user, messages) {
        $http
            .put(BASE_URL + '/desktop/profiles/change-password', {
                password: user.password,
                newPassword: user.newPassword1
            });
        //.success(function(res) {
        //  messages.success.push('Contraseña cambiada satisfactoriamente');
        //})
        //.error(function() {
        //  messages.error.push('Contraseña actual inválida');
        //});
    };

    this.getUsers = function (form)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        return $http.get(BASE_URL + '/web/users');
    };

    this.getUser = function (id)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        return $http.get(BASE_URL + '/web/users/' + id);
    };

    this.getUserTransactions = function (id)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        return $http.get(BASE_URL + '/web/users/' + id + '/transactions');
    };
};


