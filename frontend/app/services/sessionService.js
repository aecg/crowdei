angular.module('myApp').service('SessionService', ['$rootScope', '$localStorage', SessionService]);

function SessionService($rootScope, $localStorage)
{
    this.registerSession = function ()
    {
        $localStorage.showHeader = $rootScope.showHeader;
        $localStorage.isLogged = $rootScope.isLogged;
        $localStorage.claseContent = $rootScope.claseContent;
        $localStorage.userName = $rootScope.userName;
        $localStorage.userId = $rootScope.userId;
        $localStorage.nameUser = $rootScope.nameUser;
        $localStorage.email = $rootScope.email;
        $localStorage.lastname = $rootScope.lastname;
        $localStorage.image = $rootScope.image;
        $localStorage.user = $rootScope.user;
        $localStorage.personId = $rootScope.personId;
    };


    this.checkSession = function ()
    {
        if ($localStorage.isLogged != null && $localStorage.isLogged == true)
        {
            $rootScope.showHeader = $localStorage.showHeader;
            $rootScope.isLogged = $localStorage.isLogged;
            $rootScope.claseContent = $localStorage.claseContent;
            $rootScope.userName = $localStorage.userName;
            $rootScope.userId = $localStorage.userId;
            $rootScope.nameUser = $localStorage.nameUser;
            $rootScope.email = $localStorage.email;
            $rootScope.lastname = $localStorage.lastname;
            $rootScope.image = $localStorage.image;
            $rootScope.user = $localStorage.user;
            $rootScope.personId = $localStorage.personId;
        }
    };


    this.unregisterSession = function ()
    {
        $localStorage.showHeader = null;
        $localStorage.isLogged = false;
        $localStorage.claseContent = null;
        $localStorage.userName = null;
        $localStorage.userId = null
        $localStorage.nameUser = null;
        $localStorage.email = null;
        $localStorage.lastname = null;
        $localStorage.image = null;
        $localStorage.user = null;
        $localStorage.Authorization = null;
        $localStorage.personId = null

        $rootScope.showHeader = null;
        $rootScope.isLogged = false;
        $rootScope.claseContent = null;
        $rootScope.userName = null;
        $rootScope.userId = null
        $rootScope.nameUser = null;
        $rootScope.email = null;
        $rootScope.lastname = null;
        $rootScope.image = null;
        $rootScope.user = null;
        $rootScope.Authorization = null;
        $rootScope.personId = null
    };

    this.registerEncabezado = function (encabezado)
    {
        $localStorage.Authorization = encabezado;

    };

    this.registerLanguage = function (language)
    {
        $localStorage.language = language;
    };

    this.registerAvatar = function (avatar)
    {
        $localStorage.image = avatar;
    };
}
