angular.module('myApp').service('InvestmentProxy', ['$http', 'BASE_URL','$localStorage', InvestmentProxy]);

/**
 * Servicio que hace la vez de Proxy para los servicios rest de la aplicacion
 * @param $http objeto requerido para la invocaciones http de los servicios
 * @param BASE_URL Constante que posee la ruta base de los servicios
 * @constructor
 */
function InvestmentProxy($http, BASE_URL, $localStorage)
{
    /**
     * Metodo que consulta todos los proyectos en los que invirtio un usuario
     * @param id del usuario
     * @param success metodo que se ejecuta en caso de exito de la invocacion
     * @param error metodo que se ejecuta en caso de error de la invocacion
     */
    this.getInvestmentsXIdProjectXIdUser = function (user, project, page, size, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        var urlInvestment = '/web/investments?idUser=' + user;
        if (project != null) {
          urlInvestment = urlInvestment + '&idProject=' + project;
        }
        if ((page != null) && (size != null)) {
          urlInvestment = urlInvestment + '&page=' + page + '&size=' + size;
        }
        $http
            .get(BASE_URL + urlInvestment)
            .success(success)
            .error(error);
    };

  /**
   * Método que envia un correo a la persona contacto, estrategias integrales y la
   * persona que hizo la transferencia
   * @param parameters id de la transferencia
   * @param success success metodo que se ejecuta cuando es exitoso la invocacion
   * @param error error metodo que se ejecuta cuando es fallido la invocacion
   */
  this.sendMailInvestmentTransferMyProject = function (parameters, success, error)
  {
    if ($http.defaults.headers.common['Authorization'] == null)
      $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
    $http
      .post(BASE_URL + '/web/investments/investmentTransferMyProject', parameters)
      .success(success)
      .error(error);
  };

    this.getInvestmentsXIdProject = function (project, page, size, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        var urlInvestment = '/web/investments/investmentXIdProject?idProject=' + project;
        if ((page != null) && (size != null)) {
          urlInvestment = urlInvestment + '&page=' + page + '&size=' + size;
        }
        $http
            .get(BASE_URL + urlInvestment)
            .success(success)
            .error(error);
    };

    this.activateTransfer = function (id, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
        {
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        }
        $http
            .post(BASE_URL + '/web/investments/activate/' + id, {})
            .success(success)
            .error(error);
    };

}
