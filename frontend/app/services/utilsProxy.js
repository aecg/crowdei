angular.module('myApp').service('UtilsProxy', ['$http', 'BASE_URL','$localStorage', UtilsProxy]);

/**
 * Servicio que hace la vez de Proxy para los servicios rest de la aplicacion
 * @param $http objeto requerido para la invocaciones http de los servicios
 * @param BASE_URL Constante que posee la ruta base de los servicios
 * @constructor
 */
function UtilsProxy($http, BASE_URL, $localStorage)
{

    /**
     * Metodo que consulta la lista de paises
     * @param success
     * @param error
     */
    this.getCountries = function (success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        $http
            .get(BASE_URL + '/web/utils/locations/countries/')
            .success(success)
            .error(error);

    };


    /**
     * Metodo que consulta la lista de estado de un pais
     * @param country lista de estados
     * @param success metodo que se ejecuta cuando es exitoso la invocacion
     * @param error metodo que se ejecuta cuando es fallido la invocacion
     */
    this.getStates = function (country, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        $http
            .get(BASE_URL + '/web/utils/locations/countries/' + country.id + '/states')
            .success(success)
            .error(error);

    };


    /**
     * Metodo que consulta la lista de ciudades de un estado
     * @param state datos del estado
     * @param success metodo que se ejecuta cuando es exitoso la invocacion
     * @param error metodo que se ejecuta cuando es fallido la invocacion
     */
    this.getCities = function (state, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        $http
            .get(BASE_URL + '/web/utils/locations/states/' + state.id + '/cities')
            .success(success)
            .error(error);
    };


    /**
     * Metodo que consulta el nivel de experiencia
     * @param success metodo que se ejecuta cuando es exitoso la invocacion
     * @param error metodo que se ejecuta cuando es fallido la invocacion
     */
    this.getExperienceLevel = function (success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        $http
            .get(BASE_URL + '/web/utils/experience-levels')
            .success(success)
            .error(error);
    };


    /**
     * Metodo que consulta los tipos de negocios
     * @param success metodo que se ejecuta cuando es exitoso la invocacion
     * @param error metodo que se ejecuta cuando es fallido la invocacion
     */
    this.getBusinessTypes = function (success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        $http
            .get(BASE_URL + '/web/utils/business-types')
            .success(success)
            .error(error);
    };


    /**
     * Metodo que consulta los como nos encontro
     * @param success metodo que se ejecuta cuando es exitoso la invocacion
     * @param error metodo que se ejecuta cuando es fallido la invocacion
     */
    this.getHowFind = function (success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        $http
            .get(BASE_URL + '/web/utils/how-find')
            .success(success)
            .error(error);
    };


    /**
     * Metodo que consulta las razones de inversiones
     * @param success metodo que se ejecuta cuando es exitoso la invocacion
     * @param error metodo que se ejecuta cuando es fallido la invocacion
     */
    this.getReasonInvestments = function (success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        $http
            .get(BASE_URL + '/web/utils/reason-investments')
            .success(success)
            .error(error);
    };


    /**
     * Metodo que consulta las razones de inversiones
     * @param success metodo que se ejecuta cuando es exitoso la invocacion
     * @param error metodo que se ejecuta cuando es fallido la invocacion
     */
    this.getMailPreferences = function (success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        $http
            .get(BASE_URL + '/web/utils/mail-preferences')
            .success(success)
            .error(error);
    };

    /**
     * Metodo que envia un correo con la informacion de un contacto
     * @param parameters informacion del contacto mas el mensaje
     * @param success success metodo que se ejecuta cuando es exitoso la invocacion
     * @param error error metodo que se ejecuta cuando es fallido la invocacion
     */
    this.sendMail = function (parameters, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        $http
            .post(BASE_URL + '/contactUs', parameters)
            .success(success)
            .error(error);
    };


    /**
     * Método que trae todas las compañias creadas por un usuario
     * dado un parametro de busqueda
     * @param parameters id del usuario y el parametro de busqueda
     * @param success success metodo que se ejecuta cuando es exitoso la invocacion
     * @param error error metodo que se ejecuta cuando es fallido la invocacion
     */
    this.getCompanies = function (parameters, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        $http
            .post(BASE_URL + '/web/utils/company', parameters)
            .success(success)
            .error(error);
    };

    /**
     * Método que trae todos los contactos de una compañia
     * dado un parametro de busqueda
     * @param parameters parameters id de la compañia y el parametro de busqueda
     * @param success success metodo que se ejecuta cuando es exitoso la invocacion
     * @param error error metodo que se ejecuta cuando es fallido la invocacion
     */
    this.getContacts = function (parameters, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        $http
            .post(BASE_URL + '/web/utils/contact', parameters)
            .success(success)
            .error(error);
    };

    /**
     * Metodo que consulta los sectores de impacto
     * @param success metodo que se ejecuta cuando es exitoso la invocacion
     * @param error metodo que se ejecuta cuando es fallido la invocacion
     */
    this.getImpactSector = function (success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        $http
            .get(BASE_URL + '/web/utils/impact-sector')
            .success(success)
            .error(error);
    };

    /**
     * Metodo que consulta las actividades economicas
     * @param success metodo que se ejecuta cuando es exitoso la invocacion
     * @param error metodo que se ejecuta cuando es fallido la invocacion
     */
    this.getEconomicActivity = function (success, error)
    {

        if ($http.defaults.headers.common['Authorization'] == null)
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        $http
            .get(BASE_URL + '/economic-activity')
            .success(success)
            .error(error);
    };

    /**
     * Metodo que consulta los tipos de moneda
     * @param success metodo que se ejecuta cuando es exitoso la invocacion
     * @param error metodo que se ejecuta cuando es fallido la invocacion
     */
    this.getCurrency = function (success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        $http
            .get(BASE_URL + '/web/utils/currency')
            .success(success)
            .error(error);
    };

    /**
     * Metodo que envia un correo con la informacion de un contacto a la persona
     * contacto de un proyecto
     * @param parameters informacion del contacto mas el mensaje
     * @param success success metodo que se ejecuta cuando es exitoso la invocacion
     * @param error error metodo que se ejecuta cuando es fallido la invocacion
     */
    this.sendMailContactProject = function (parameters, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        $http
            .post(BASE_URL + '/contactUsContactProject', parameters)
            .success(success)
            .error(error);
    };

    /**
     * Metodo que consulta los tipos de prioridad de los proyectos
     * @param success metodo que se ejecuta cuando es exitoso la invocacion
     * @param error metodo que se ejecuta cuando es fallido la invocacion
     */
    this.getPriority = function (success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        $http
            .get(BASE_URL + '/web/utils/priority')
            .success(success)
            .error(error);
    };
}
