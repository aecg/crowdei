angular.module('myApp').service('LandingProxy', ['$http', 'BASE_URL', '$localStorage', LandingProxy]);

/**
 * Servicio que hace la vez de Proxy para los servicios rest de la aplicacion
 * @param $http objeto requerido para la invocaciones http de los servicios
 * @param BASE_URL Constante que posee la ruta base de los servicios
 * @constructor
 */
function LandingProxy($http, BASE_URL, $localStorage)
{
    /**
     * Metodo que consulta todos los proyectos que posean alguna prioridad
     * @param success metodo que se ejecuta en caso de exito de la invocacion
     * @param error metodo que se ejecuta en caso de error de la invocacion
     */
    this.getProjectsPriority = function (success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        $http
            .post(BASE_URL + '/priority')
            .success(success)
            .error(error);
    };

    /**
     * Metodo que consulta todos los proyectos dado una prioridad en particular
     * @param success metodo que se ejecuta en caso de exito de la invocacion
     * @param error metodo que se ejecuta en caso de error de la invocacion
     */
    this.getProjectsPriorityType = function (priority, success, error)
    {
        if ($http.defaults.headers.common['Authorization'] == null)
            $http.defaults.headers.common['Authorization'] = $localStorage.Authorization;
        $http
            .get(BASE_URL + '/priorityType/' + priority)
            .success(success)
            .error(error);
    };
}
