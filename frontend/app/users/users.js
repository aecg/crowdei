'use strict';

angular.module('myApp.users', ['ngRoute'])

	.config(['$routeProvider', function ($routeProvider)
	{
		$routeProvider.when('/users', {
			templateUrl: 'users/users.html',
			controller: 'UsersCtrl'
		});
	}])

	.controller('UsersCtrl', ['$scope', '$loading', '$http', '$location', '$rootScope', 'MessagesFactory','UtilsProxy', '$filter', 'SessionService', 'UserProxy', 'AuthProxy',
		function ($scope, $loading, $http, $location, $rootScope, MessagesFactory, UtilsProxy, $filter, SessionService, UserProxy, AuthProxy)
	{


		$loading.start('users');
		SessionService.checkSession();
		$scope.projectList = [];

		$rootScope.oldUrl = "users.html";
		
		/**
		 * Metodo que se ejecuta cuando la autenticacion es exitosa
		 * @param res respuesta del servicio
		 */
		var success = function (res)
		{
			$loading.finish('users');
			//Accion cuando el usuario es autenticado de manera exitosa
			$scope.userList = res.users;
			console.log('success::$scope.userList::'+JSON.stringify($scope.userList));
		};

		/**
		 * Metodo que se ejecuta cuando la autenticacion es exitosa
		 * @param res respuesta del servicio
		 */
		var error = function (res)
		{
			$loading.finish('users');
			//console.log('ProjectsCtrl::error::res::'+JSON.stringify(res));
			if (res.error == "unauthorized")
			{
				$location.path('/login/');
			}
			else
			{
				console.log('Failed Invoke Service Web');
				$scope.messages = MessagesFactory.createMessages();
				$scope.messages.error.push(res.message);
			}
		};

		$scope.reloadAllProjects = function ()
		{
			//console.log('reloadAllProjects');
			$loading.start('users');
			UserProxy.getAllUsers(false, false, 0, 0, success, error);
		}

		var successActivate = function (res)
		{
			//console.log('successActivate');
			$scope.messages = MessagesFactory.createMessages();
			$scope.messages.success.push($filter('translate')('label-user-activado'));
			UserProxy.getAllUsers(false, false, 0, 0, success, error);
		};
		$scope.activateUser = function (user)
		{
			console.log('activateUser::user.id::'+user.id+'::user.enabled::'+user.enabled);
			if (!user.enabled)
			{
				$loading.start('users');
				UserProxy.activateUser(user.id, successActivate, error);
			}
		};

		var successDeactivate = function (res)
		{
			//console.log('successActivate');
			$scope.messages = MessagesFactory.createMessages();
			$scope.messages.success.push($filter('translate')('label-user-desactivado'));
			UserProxy.getAllUsers(false, false, 0, 0, success, error);
		};
		$scope.deactivateUser = function (user)
		{
			console.log('deactivateUser::user.id::'+user.id+'::user.enabled::'+user.enabled);
			if (user.enabled)
			{
				$loading.start('users');
				UserProxy.deactivateUser(user.id, successDeactivate, error);
			}
		};

		var successRecover = function (res)
		{
			//console.log('successActivate');
			$scope.messages = MessagesFactory.createMessages();
			$scope.messages.success.push($filter('translate')('label-users-user-recover-success'));
			UserProxy.getAllUsers(false, false, 0, 0, success, error);
		};
		$scope.recoverUser = function(user) {
			console.log('recoverUser::user.username::'+user.username);
			$loading.start('users');
			AuthProxy.retrieveUser(user.person, successRecover, error);
		};

		$loading.start('users');
		UserProxy.getAllUsers(false, false, 0, 0, success, error);
	}
]);
