'use strict';

angular.module('myApp.projectsSuperUser', ['ngRoute'])

	.config(['$routeProvider', function ($routeProvider)
	{
		$routeProvider.when('/projectsSuperUser', {
			templateUrl: 'projectsSuperUser/projectsSuperUser.html',
			controller: 'ProjectsSuperUserCtrl'
		});
	}])

	.controller('ProjectsSuperUserCtrl', ['$scope', '$loading', '$http', '$location', '$rootScope', 'ProjectProxy', 'MessagesFactory','UtilsProxy', '$filter', 'SessionService', '$modal', 'InvestmentProxy',
		function ($scope, $loading, $http, $location, $rootScope, ProjectProxy, MessagesFactory, UtilsProxy, $filter, SessionService, $modal, InvestmentProxy)
	{


		$loading.start('my-projects');
		SessionService.checkSession();
		$scope.openedSearch = false;
		$scope.projectList = [];
		$scope.parameters = {};

		$scope.idEconomicActivity = {};
	    $scope.selection = [];

		$rootScope.oldUrl = "projects.html";
		
		$scope.priorities = ["OUTSTANDING","NEW","SUCCESSFULL","REGULAR"];
		$scope.changePriority = false;
		$scope.changePriorityProjectId = 0;

		/**
		 * Metodo que se ejecuta cuando la autenticacion es exitosa
		 * @param res respuesta del servicio
		 */
		var success = function (res)
		{
			$loading.finish('my-projects');
			//Accion cuando el usuario es autenticado de manera exitosa
			$scope.projectList = res.projects;
			//console.log('success::$scope.projectList::'+JSON.stringify($scope.projectList));
		};

		/**
		 * Metodo que se ejecuta cuando la autenticacion es exitosa
		 * @param res respuesta del servicio
		 */
		var successFind = function (res)
		{
			$loading.finish('my-projects');
			//Accion cuando el usuario es autenticado de manera exitosa
			$scope.projectList = res.projects;

			if ($scope.projectList.length == 0)
			{
				$scope.messages = MessagesFactory.createMessages();
				$scope.messages.error.push("No existen proyectos que coincidan con los parámetros de búsqueda");
				$scope.showModal = !$scope.showModal;
			}
			console.log($scope.projectList);
		};

		/**
		 * Metodo que se ejecuta cuando la autenticacion es exitosa
		 * @param res respuesta del servicio
		 */
		var successDelete = function (res)
		{
			$loading.finish('my-projects');
			$scope.messages = MessagesFactory.createMessages();
			$scope.messages.success.push("Se ha eliminado el proyecto exitosamente");

			$loading.start('my-projects');
			ProjectProxy.getProjects($rootScope.user.person.id, null, null, success, error);
		};

		/**
		 * Metodo que se ejecuta cuando la autenticacion es exitosa
		 * @param res respuesta del servicio
		 */
		var error = function (res)
		{
			$loading.finish('my-projects');
			//console.log('ProjectsCtrl::error::res::'+JSON.stringify(res));
			if (res.error == "unauthorized")
			{
				$location.path('/login/');
			}
			else
			{
				console.log('Failed Invoke Service Web');
				$scope.messages = MessagesFactory.createMessages();
				$scope.messages.error.push(res.message);
			}
		};

		/**
		 * Funcion que se ejecuta cuando se da click al boton de login
		 */
		$scope.openSearch = function ()
		{
			$scope.openedSearch = !$scope.openedSearch;
		};

		/**
		 * Funcion que se ejecuta cuando se da click al boton de buscar
		 */
		$scope.find = function ()
		{
			$loading.start('my-projects');
			$scope.parameters.idUser = $rootScope.user.person.id;

			$scope.parameters.idEconomicActivity = $scope.idEconomicActivity.id;

			ProjectProxy.findProjectsXIdXParams($scope.parameters, successFind, error);
			$scope.messages = null;
		};

		/**
		 * Metodo que se ejecuta cuando la consulta actividades economicas es exitosa
		 * @param res respuesta del servicio
		 */
		var successEconomicActivities = function (res)
		{
			$scope.economicActivities = res.list;
			angular.forEach($scope.economicActivities, function(value, key)
			{
				if ($scope.idEconomicActivity != null && value.id == $scope.idEconomicActivity.id) {
					$scope.idEconomicActivity = value;
				}
			});
		};

		/**
		 * Funcion que se ejecuta cuando se da click al boton de login
		 */
		$scope.deleteProject = function (id)
		{
			$loading.start('my-projects');
			ProjectProxy.deleteProject(id, successDelete, error);
		};

		var successActivate = function (res)
		{
			//console.log('successActivate');
			$loading.finish('my-projects');
			$scope.messages = MessagesFactory.createMessages();
			$scope.messages.success.push($filter('translate')('label-proyecto-activado'));

			$loading.start('my-projects');
			ProjectProxy.getAllProjects(false, 0, 0, success, error);
		};
		$scope.activateProject = function (project)
		{
			//console.log('activateProject::project.id::'+project.id+'::project.status::'+project.status);
			if (project.status == 'INACTIVE')
			{
				$loading.start('my-projects');
				ProjectProxy.activateProject(project.id, successActivate, error);
			}
		};

		var successDeactivate = function (res)
		{
			//console.log('successDeactivate');
			$loading.finish('my-projects');
			$scope.messages = MessagesFactory.createMessages();
			$scope.messages.success.push($filter('translate')('label-proyecto-desactivado'));

			$loading.start('my-projects');
			ProjectProxy.getAllProjects(false, 0, 0, success, error);
		};
		$scope.deactivateProject = function (project)
		{
			//console.log('activateProject::project.id::'+project.id+'::project.status::'+project.status);
			if (project.status == 'ACTIVE')
			{
				$loading.start('my-projects');
				ProjectProxy.deactivateProject(project.id, successDeactivate, error);
			}
		};

		var successPriority = function (res)
		{
			$scope.priorities = res;
			//console.log('successPriority::$scope.priorities::'+JSON.stringify($scope.priorities));
		};

		$scope.changePriorityProject = function (project)
		{
			//console.log('changePriorityProject::project.id::'+project.id);
			$scope.changePriority = true;
			$scope.changePriorityProjectId = project.id;
		};

		var successChangePriority = function (res)
		{
			//console.log('successChangePriority');
			$scope.changePriority = false;
			$scope.changePriorityProjectId = 0;
			$loading.finish('my-projects');
			$scope.messages = MessagesFactory.createMessages();
			$scope.messages.success.push($filter('translate')('label-proyecto-cambio-prioridad'));

			$loading.start('my-projects');
			ProjectProxy.getAllProjects(false, 0, 0, success, error);
		};
		$scope.submitChangePriorityProject = function (project)
		{
			//console.log('submitChangePriorityProject::project::'+JSON.stringify(project));
			$loading.start('my-projects');
			ProjectProxy.changePriority(project, successChangePriority, error);
		}

		$scope.reloadAllProjects = function ()
		{
			//console.log('reloadAllProjects');
			$loading.start('my-projects');
			ProjectProxy.getAllProjects(false, 0, 0, success, error);
		}

		/**
		 * Abre modal con pagos del proyecto
		 */
		$scope.openPaymentsModal = function (projectId)
		{
			$modal.open({
				templateUrl: 'payments-modal.html',
				controller: ['$scope', '$modalInstance', 'data', PaymentsCtrl],
				scope: $scope,
				resolve: {
					'data': function ()
					{
						return {
							projectId: projectId
						};
					}
				}
			});
		};

		/**
		 * Controlador de payments modal
		 */
		var PaymentsCtrl = function ($scope, $modalInstance, data)
		{
			$scope.close = function () {
				$scope.$parent.reloadAllProjects();
				$modalInstance.dismiss('close');
			};

			var success = function (resp)
			{
				$scope.investmentList = resp;
				$scope.investmentTotal = 0;
				//console.log('PaymentsCtrl::success::$scope.investmentList::'+JSON.stringify($scope.investmentList));
				for (var i in $scope.investmentList.investments) {
					$scope.projectName = $scope.investmentList.investments[i].project.name;
					if ($scope.investmentList.investments[i].status == "ACTIVE") {
						$scope.investmentTotal += $scope.investmentList.investments[i].amount;
					}
				}
				$loading.finish('my-projects');
			};
			var error = function () 
			{

			};

			var successActivateTransfer = function (res)
			{
				//console.log('successActivate');
				//$loading.finish('my-projects');
				$scope.messages = MessagesFactory.createMessages();
				$scope.messages.success.push($filter('translate')('label-inversion-activada'));

				//$loading.start('my-projects');
				InvestmentProxy.getInvestmentsXIdProject(data.projectId, 0, 1000, success, error);
				//ProjectProxy.getAllProjects(false, 0, 0, success, error);
			};

			$scope.activateTransfer = function (investment)
			{
				//console.log('activateTransfer::investment::'+JSON.stringify(investment));
				if (investment.status == 'INACTIVE' && investment.methodPayment == 'TRANSFER')
				{
					$loading.start('my-projects');
					InvestmentProxy.activateTransfer(investment.id, successActivateTransfer, error);
				}
			};

			$loading.start('my-projects');
			InvestmentProxy.getInvestmentsXIdProject(data.projectId, 0, 1000, success, error);
		};

		$scope.openInvestmentsModal = function (project)
		{
			$modal.open({
				templateUrl: 'investments-modal.html',
				controller: ['$scope', '$modalInstance', 'data', InvestmentsCtrl],
				scope: $scope,
				resolve: {
					'data': function ()
					{
						return {
							project: project
						};
					}
				}
			});
		};

		var InvestmentsCtrl = function ($scope, $modalInstance, data)
		{
			$scope.close = function () {
				$modalInstance.dismiss('close');
			};

			//console.log('InvestmentsCtrl::data.project::'+JSON.stringify(data.project));
			//console.log('InvestmentsCtrl::data.project.investments::'+JSON.stringify(data.project.investments));
			$scope.data = [];
			$scope.dataDonut = [];

			var montoPaypal = 0;
			var montoTransfer = 0;
			var montoConekta = 0;

			var collected = 0;
			for (var i in data.project.investments) {
				var investment = data.project.investments[i];
				//console.log('InvestmentsCtrl::investment::'+JSON.stringify(investment));

				var investmentDate = new Date(investment.date);
				var investmentDateStr = investmentDate.toLocaleDateString();

				collected += investment.amount;

				var obj = {
					y : investmentDateStr,
					a : investment.amount,
					b : collected
				};
				$scope.data.push(obj);

				if (investment.methodPayment == "CONEKTA")
				{
					montoConekta += investment.amount;
				}
				else if (investment.methodPayment == "TRANSFER")
				{
					montoTransfer += investment.amount;
				}
				else if (investment.methodPayment == "PAYPAL")
				{
					montoPaypal += investment.amount;
				}
			}

			var objConekta = {
				label : $filter('translate')('label-report-conekta'),
				value : montoConekta
			};
			var objTransfer = {
				label : $filter('translate')('label-report-transfer'),
				value : montoTransfer
			};
			var objPaypal = {
				label : $filter('translate')('label-report-paypal'),
				value : montoPaypal
			};

			$scope.dataDonut.push(objConekta);
			$scope.dataDonut.push(objTransfer);
			$scope.dataDonut.push(objPaypal);

			//console.log('$scope.data::'+JSON.stringify($scope.data));
			//console.log('$scope.dataDonut::'+JSON.stringify($scope.dataDonut));

			$scope.xaxis = 'y';
			$scope.yaxis = '["a"]';
			$scope.yaxis_area = '["b"]';
			$scope.arealabels = '["' + $filter('translate')('label-report-collected') + '"]';
			$scope.barlabels = '["' + $filter('translate')('label-report-investment') + '"]';

			$scope.totalRequired = data.project.objetive;
			$scope.totalObtained = data.project.obtained;
			$scope.totalBoosters = data.project.investors;
			var ld = new Date(data.project.limitDate);
			var ln = ld.toLocaleDateString();
			$scope.limitDate = ln;
		};

		// toggle selection for a given fruit by name
		$scope.toggleSelection = function toggleSelection(idActivity) {
			var idx = $scope.selection.indexOf(idActivity);

			if (idx > -1) {
				$scope.selection.splice(idx, 1);
			}
			else {
				$scope.selection.push(idActivity);
			}
		};

		$loading.start('my-projects');
		UtilsProxy.getEconomicActivity(successEconomicActivities, error);
		//UtilsProxy.getPriority(successPriority, error);
		ProjectProxy.getAllProjects(false, 0, 0, success, error);
	}
]);
