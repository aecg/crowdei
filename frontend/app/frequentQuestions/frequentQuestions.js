'use strict';

angular.module('myApp.frequentQuestions', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
	$routeProvider
		.when('/frequentQuestions/:section/:question', {
			templateUrl: 'frequentQuestions/frequentQuestions_v2.html',
			controller: 'FrequentQuestionsCtrl'
		})
		.when('/frequentQuestions/:section', {
			templateUrl: 'frequentQuestions/frequentQuestions_v2.html',
			controller: 'FrequentQuestionsCtrl'
		});
}])
.controller('FrequentQuestionsCtrl',  ['$scope', '$location', '$anchorScroll', 'SessionService', '$localStorage','$routeParams', function($scope, $location, $anchorScroll, SessionService, $localStorage, $routeParams)
{
    $scope.section = $routeParams.section;
    $scope.question = ($routeParams.question == '' ? '' : $routeParams.question);

}]);
