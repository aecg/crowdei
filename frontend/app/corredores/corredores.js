'use strict';

angular.module('myApp.corredores', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/corredores', {
    templateUrl: 'corredores/corredores.html',
    controller: 'CorredoresCtrl'
  });
}])

.controller('CorredoresCtrl',  ['$scope', '$location', '$anchorScroll', 'SessionService', '$localStorage', function($scope, $location, $anchorScroll, SessionService, $localStorage)
{
    $scope.header = "corredores";

    $scope.carousel_image_1 = "assets/img/corredores/img1_" + $localStorage.language + ".jpg";
    $scope.carousel_image_2 = "assets/img/corredores/img2_" + $localStorage.language + ".jpg";
    $scope.carousel_image_3 = "assets/img/corredores/img3_" + $localStorage.language + ".jpg";

    $scope.$watch(function () { 
      return $localStorage.language; 
    }, function(newVal,oldVal){
          if(oldVal!==newVal && newVal !== undefined){
            $scope.carousel_image_1 = "assets/img/corredores/img1_" + $localStorage.language + ".jpg";
            $scope.carousel_image_2 = "assets/img/corredores/img2_" + $localStorage.language + ".jpg";
            $scope.carousel_image_3 = "assets/img/corredores/img3_" + $localStorage.language + ".jpg";
          }
    });

}]);
