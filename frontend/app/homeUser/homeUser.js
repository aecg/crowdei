'use strict';

angular.module('myApp.homeUser', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/homeUser', {
    templateUrl: 'homeUser/homeUser_v2.html',
    controller: 'HomeUserCtrl'
  });
}])

.controller('HomeUserCtrl', ['$scope', '$loading', '$http', '$location', '$rootScope', 'LandingProxy', 'MessagesFactory', 'SessionService', 'ProjectProxy', 'InvestmentProxy',
        function($scope, $loading, $http, $location, $rootScope, LandingProxy, MessagesFactory, SessionService, ProjectProxy, InvestmentProxy) {

    $loading.start('home');
    SessionService.checkSession();
    $scope.numberProjectList = 0;
    $scope.numberInvestmentList = 0;
    $scope.header = 'inicio';

    /**
     * Metodo que se ejecuta cuando la autenticacion es exitosa
     * @param res respuesta del servicio
     */
    var success = function (res)
    {
      $scope.numberProjectList = res.count;
    };

    var successInvestment = function (res)
    {
      $scope.numberInvestmentList = res.countProjects;
      $loading.finish('home');
    };

    /**
     * Metodo que se ejecuta cuando la autenticacion es exitosa
     * @param res respuesta del servicio
     */
    var error = function (res)
    {
      $loading.finish('home');
      if(res.error == "unauthorized")
      {
        $location.path('/login/');
      }
      else
      {
        console.log('Failed Invoke Service Web');
        $scope.messages = MessagesFactory.createMessages();
        $scope.messages.error.push(res.message);
      }
    };

    ProjectProxy.getProjects($rootScope.user.person.id, null, null, success, error);
    InvestmentProxy.getInvestmentsXIdProjectXIdUser($rootScope.user.person.id, null, null, null, successInvestment, error);
}]);
