ALTER TABLE `crowdei`.`project` 
CHANGE COLUMN `description` `description` LONGBLOB NULL DEFAULT NULL ;
ALTER TABLE `crowdei`.`project` 
DROP COLUMN `sales_earning`,
DROP COLUMN `product_result`,
DROP COLUMN `monthly_interest`,
DROP COLUMN `final_comment`,
DROP COLUMN `location_id`,
DROP COLUMN `another_incentive`;
ALTER TABLE `crowdei`.`company` 
DROP COLUMN `web_page`,
DROP COLUMN `twitter`,
DROP COLUMN `linkedin`,
DROP COLUMN `facebook`;
ALTER TABLE `crowdei`.`project` 
CHANGE COLUMN `brief_description` `brief_description` VARCHAR(1000) NULL DEFAULT NULL ;

ALTER TABLE `crowdei`.`person` 
DROP COLUMN `city_id`,
DROP COLUMN `postal_code`,
DROP COLUMN `address`;