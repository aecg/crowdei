
drop table if exists CompanyStatus;

drop table if exists Contact;

drop table if exists HowFind;

drop table if exists experienceLevel;

drop table if exists Document;

drop table if exists Investment;

drop table if exists Currency;

drop table if exists reasonInvestment;

drop table if exists userReasonInvestment;

drop table if exists Person;

drop table if exists User;

drop table if exists userBusinessType;

drop table if exists businessType;

drop table if exists Project;

drop table if exists Company;

drop table if exists Payment;


/*==============================================================*/
/* Table: USER                                         */
/*==============================================================*/
create table user
(
   id                   int not null auto_increment,
   username              varchar(10) not null,
   contrasena           varchar(10) not null,
   enable               bool not null,
   createat				datetime not null,
   primary key (id),
   unique key AK_UNIQUE_USERNAME (username)
);

/*==============================================================*/
/* Table: PERSON                                               */
/*==============================================================*/
create table PERSON
(
   id	int not null auto_increment,
   name	varchar(50) not null,
   lastname varchar(50) not null,
   email	varchar(50) not null,
   sex	varchar(1) not null,
   address	varchar(250) not null,
   city   varchar(50) not null,
   state	varchar(50) not null,
   country	varchar(50) not null,
   image	blob not null,
   description	varchar(250) not null,
   education	varchar(50) not null,
   interests	varchar(50) not null,
   experienceDescription	varchar(250) not null,
   amount	double not null,  
   idHowFind	int not null,
   idExperienceLevel	int not null,
   idUser		int not null,
   primary key (id)
);

/*==============================================================*/
/* Table: USERREASONINVESTMENT                                               */
/*==============================================================*/
create table USERREASONINVESTMENT
(
	id int not null auto_increment,
    idUser int not null,
    idReasonInvestment int not null,
	primary key (id)
);

/*==============================================================*/
/* Table: REASONINVESTMENT                                               */
/*==============================================================*/
create table REASONINVESTMENT
(
	id 				int not null auto_increment,
	description     varchar(20) not null,
	primary key(id)
);

/*==============================================================*/
/* Table: USERBUSINESSTYPE                                               */
/*==============================================================*/
create table USERBUSINESSTYPE
(
	id int not null auto_increment,
    idUser int not null,
    idBusinessType int not null,
	primary key (id)
);

/*==============================================================*/
/* Table: BUSINESSTYPE                                               */
/*==============================================================*/
create table BUSINESSTYPE
(
	id 				int not null auto_increment,
	description     varchar(20) not null,
	primary key(id)
);

/*==============================================================*/
/* Table: COMPANYSTATUS                                               */
/*==============================================================*/
create table COMPANYSTATUS
(
	id 				int not null auto_increment,
	description     varchar(20) not null,
	primary key(id)
);

/*==============================================================*/
/* Table: CONTACT                                               */
/*==============================================================*/
create table CONTACT
(
	id 		int not null auto_increment,
	name	varchar(50) not null,
	lastname	varchar(50) not null,
	birthdate	datetime not null,
	sex 	varchar(1) not null,
	email	varchar(50) not null,
	phone	varchar(20) not null,
	primary key(id)
);


/*==============================================================*/
/* Table: HOWFIND                                               */
/*==============================================================*/
create table HOWFIND
(
	id 				int not null auto_increment,
	description     varchar(20) not null,
	primary key(id)
);

/*==============================================================*/
/* Table: EXPERIENCELEVEL                                               */
/*==============================================================*/
create table EXPERIENCELEVEL
(
	id 				int not null auto_increment,
	description     varchar(20) not null,
	primary key(id)
);

/*==============================================================*/
/* Table: DOCUMENT                                               */
/*==============================================================*/
create table DOCUMENT
(
	id 		int not null auto_increment,
	file    blob not null,
	idProject int not null,
	primary key(id)
);

/*==============================================================*/
/* Table: INVESTMENT                                               */
/*==============================================================*/
create table INVESTMENT
(
	id 		int not null auto_increment,
	date 	datetime not null,
	amount	double not null,
	idProject	int not null,
	idPerson	int not null,
	idCurrency int not null,
	primary key(id)
);

/*==============================================================*/
/* Table: CURRENCY                                               */
/*==============================================================*/
create table CURRENCY
(
	id 				int not null auto_increment,
	description     varchar(20) not null,
	primary key(id)
);

/*==============================================================*/
/* Table: PROJECT                                               */
/*==============================================================*/
create table PROJECT
(
	id 		int not null auto_increment,
	name	varchar(50) not null,
	description		varchar(250) not null,
	dateStart		datetime not null,
	dateEnd			datetime not null,
	objetive	varchar(10) not null,
	idea		varchar(250) not null,
	market		varchar(250) not null,
	person		varchar(50) not null,
	finance		varchar(50) not null,
	exitStrategy	varchar(50) not null,
	reward		varchar(50) not null,
	image		blob not null,
	idCompany	int not null,
	primary key(id)
);

/*==============================================================*/
/* Table: COMPANY                                             */
/*==============================================================*/
create table COMPANY
(
	id 		int not null auto_increment,
	cif		varchar(20) not null,
	name	varchar(50) not null,
	address		varchar(250) not null,
	numberEmployees		varchar(20) not null,
	estimaateEmployees	varchar(50) not null,
	lastClosingEntry	varchar(20) not null,
	benefits	varchar(2) not null,
	webPage		varchar(100) not null,
	linkedinPage	varchar(50) not null,
	description		varchar(250) not null,
	image		blob not null,
	idCompanyStatus	int not null,
	idContact	int not null,
	idBusinessType	int not null,
	idPayment	int not null,
	primary key(id)
);

/*==============================================================*/
/* Table: PAYMENT                                               */
/*==============================================================*/
create table PAYMENT
(
	id 				int not null auto_increment,
	description     varchar(20) not null,
	primary key(id)
);

alter table COMPANY add constraint FK_REFERENCE_COMPANYSTATUS_ID foreign key (idCompanyStatus)
      references COMPANYSTATUS (ID) on delete restrict on update restrict;

alter table COMPANY add constraint FK_REFERENCE_CONTACT_ID foreign key (idContact)
      references CONTACT (ID) on delete restrict on update restrict;

alter table COMPANY add constraint FK_REFERENCE_BUSINESSTYPE_ID foreign key (idBusinessType)
      references BUSINESSTYPE (ID) on delete restrict on update restrict;

alter table COMPANY add constraint FK_REFERENCE_TYPEPAYMENT_ID foreign key (idPayment)
      references PAYMENT (ID) on delete restrict on update restrict;

alter table PROJECT add constraint FK_REFERENCE_COMPANY_ID foreign key (idCompany)
      references COMPANY (ID) on delete restrict on update restrict;

alter table INVESTMENT add constraint FK_REFERENCE_PROJECT_ID foreign key (idProject)
      references PROJECT (ID) on delete restrict on update restrict;

alter table INVESTMENT add constraint FK_REFERENCE_PERSON_ID foreign key (idPerson)
      references PERSON (ID) on delete restrict on update restrict;

alter table INVESTMENT add constraint FK_REFERENCE_CURRENCY_ID foreign key (idCurrency)
      references CURRENCY (ID) on delete restrict on update restrict;

alter table DOCUMENT add constraint FK_REFERENCE_PROJECTDOCUMENT_ID foreign key (idProject)
      references PROJECT (ID) on delete restrict on update restrict;

alter table PERSON add constraint FK_REFERENCE_HOWFIND_ID foreign key (idHowFind)
      references HOWFIND (ID) on delete restrict on update restrict;

alter table PERSON add constraint FK_REFERENCE_EXPERIENCELEVEL_ID foreign key (idExperienceLevel)
      references EXPERIENCELEVEL (ID) on delete restrict on update restrict;

alter table PERSON add constraint FK_REFERENCE_USER_ID foreign key (idUser)
      references USER (ID) on delete restrict on update restrict;

alter table USERREASONINVESTMENT add constraint FK_REFERENCE_USERREASONINVESTMENT_ID foreign key (idUser)
      references USER (ID) on delete restrict on update restrict;

alter table USERREASONINVESTMENT add constraint FK_REFERENCE_REASONINVESTMENT_ID foreign key (idReasonInvestment)
      references REASONINVESTMENT (ID) on delete restrict on update restrict;

alter table USERBUSINESSTYPE add constraint FK_REFERENCE_USERBUSINESSTYPE_ID foreign key (idUser)
      references USER (ID) on delete restrict on update restrict;

alter table USERBUSINESSTYPE add constraint FK_REFERENCE_USERBUSINESSTYPEUSER_ID foreign key (idBusinessType)
      references BUSINESSTYPE (ID) on delete restrict on update restrict;

	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	 
	  
	  