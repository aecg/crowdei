/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     03/12/2015 6:58:11 p.m.                      */
/*==============================================================*/


drop table if exists BUSINESSTYPE;

drop table if exists COMPANY;

drop table if exists COMPANYSTATUS;

drop table if exists CONTACT;

drop table if exists CURRENCY;

drop table if exists DOCUMENT;

drop table if exists EXPERIENCELEVEL;

drop table if exists HOWFIND;

drop table if exists INVESTMENT;

drop table if exists OAUTH_ACCESS_TOKEN;

drop table if exists PAYMENTTYPE;

drop table if exists PERSON;

drop table if exists PROJECT;

drop table if exists REASONINVESTMENT;

drop table if exists USER;

drop table if exists USERBUSINESSTYPE;

drop table if exists USERREASONINVESTMENT;

/*==============================================================*/
/* Table: BUSINESSTYPE                                          */
/*==============================================================*/
create table BUSINESSTYPE
(
   ID                   int not null auto_increment,
   DESCRIPTION          varchar(100),
   primary key (ID)
);

/*==============================================================*/
/* Table: COMPANY                                               */
/*==============================================================*/
create table COMPANY
(
   ID                   int not null auto_increment,
   CIF                  varchar(20),
   NAME                 varchar(255),
   ADDRESS              varchar(1000),
   NUMBER_EMPLOYEES     int,
   ESTIMATED_EMPLOYEES  int,
   LAST_CLOSING_ENTRY   varchar(20),
   BENEFITS             bool,
   WEB_PAGE             varchar(100),
   LINKEDIN_PAGE        varchar(50),
   DESCRIPTION          varchar(1000),
   IMAGE                BLOB,
   ID_COMPANY_STATUS    int,
   ID_CONTACT           int,
   ID_BUSINESS_TYPE     int,
   ID_TYPE_PAYMENT      int,
   primary key (ID)
);

/*==============================================================*/
/* Table: COMPANYSTATUS                                         */
/*==============================================================*/
create table COMPANYSTATUS
(
   ID                   int not null auto_increment,
   DESCRIPTION          varchar(20),
   primary key (ID)
);

/*==============================================================*/
/* Table: CONTACT                                               */
/*==============================================================*/
create table CONTACT
(
   ID                   int not null auto_increment,
   COM_ID               int not null,
   NAME                 varchar(50),
   LASTNAME             varchar(50),
   BIRTHDATE            datetime,
   SEX                  varchar(1),
   EMAIL                varchar(50),
   PHONE                varchar(20),
   primary key (ID)
);

/*==============================================================*/
/* Table: CURRENCY                                              */
/*==============================================================*/
create table CURRENCY
(
   ID                   int not null auto_increment,
   DESCRIPTION          varchar(20),
   primary key (ID)
);

/*==============================================================*/
/* Table: DOCUMENT                                              */
/*==============================================================*/
create table DOCUMENT
(
   ID                   int not null auto_increment,
   FILE                 BLOB,
   ID_PROJECT           int,
   primary key (ID)
);

/*==============================================================*/
/* Table: EXPERIENCELEVEL                                       */
/*==============================================================*/
create table EXPERIENCELEVEL
(
   ID                   int not null auto_increment,
   DESCRIPTION          varchar(20),
   primary key (ID)
);

/*==============================================================*/
/* Table: HOWFIND                                               */
/*==============================================================*/
create table HOWFIND
(
   ID                   int not null auto_increment,
   DESCRIPTION          varchar(20),
   primary key (ID)
);

/*==============================================================*/
/* Table: INVESTMENT                                            */
/*==============================================================*/
create table INVESTMENT
(
   ID                   int not null auto_increment,
   DATE                 date,
   AMOUNT               Double,
   ID_PROJECT           int,
   ID_PERSON            int,
   ID_CURRENCY          int,
   primary key (ID)
);

/*==============================================================*/
/* Table: OAUTH_ACCESS_TOKEN                                    */
/*==============================================================*/
create table OAUTH_ACCESS_TOKEN
(
   TOKEN_ID             varchar(256),
   TOKEN                blob,
   AUTHENTICATION_ID    varchar(256),
   USER_NAME            varchar(256),
   CLIENT_ID            varchar(256),
   AUTHENTICATION       blob,
   REFRESH_TOKEN        varchar(256)
);

/*==============================================================*/
/* Table: PAYMENTTYPE                                           */
/*==============================================================*/
create table PAYMENTTYPE
(
   ID                   int not null auto_increment,
   DESCRIPTION          varchar(20),
   primary key (ID)
);

/*==============================================================*/
/* Table: PERSON                                                */
/*==============================================================*/
create table PERSON
(
   ID                   int not null auto_increment,
   NAME                 varchar(200),
   LAST_NAME            varchar(200),
   EMAIL                varchar(200),
   SEX                  varchar(1),
   ADDRESS              varchar(1000),
   CITY                 varchar(200),
   STATE                varchar(200),
   COUNTRY              varchar(200),
   IMAGE                BLOB,
   DESCRIPTION          varchar(1000),
   EDUCATION            varchar(1000),
   INTERESTS            varchar(50),
   EXPERIENCE_DESCRIPTION varchar(250),
   AMOUNT               double,
   ID_HOW_FIND          int,
   ID_EXPERIENCE_LEVEL  int,
   primary key (ID)
);

/*==============================================================*/
/* Table: PROJECT                                               */
/*==============================================================*/
create table PROJECT
(
   ID                   int not null,
   NAME                 varchar(50),
   DESCRIPTION          varchar(250),
   DATE_START           datetime,
   DATE_END             datetime,
   OBJETIVE             varchar(10),
   IDEA                 varchar(250),
   MARKET               varchar(250),
   PERSON               varchar(50),
   FINANCE              varchar(50),
   EXIT_STRATEGY        varchar(50),
   REWARD               varchar(50),
   IMAGE                BLOB,
   ID_COMPANY           int,
   ID_PERSON            int,
   primary key (ID)
);

/*==============================================================*/
/* Table: REASONINVESTMENT                                      */
/*==============================================================*/
create table REASONINVESTMENT
(
   ID                   int not null auto_increment,
   DESCRIPTION          varchar(20),
   primary key (ID)
);

/*==============================================================*/
/* Table: USER                                                  */
/*==============================================================*/
create table USER
(
   ID                   int not null auto_increment,
   USERNAME             varchar(50),
   PASSWORD             varchar(250),
   ENABLE               bool,
   CREATED_AT           date,
   ID_PERSON            int,
   primary key (ID)
);

/*==============================================================*/
/* Table: USERBUSINESSTYPE                                      */
/*==============================================================*/
create table USERBUSINESSTYPE
(
   ID                   int not null auto_increment,
   ID_USER              int,
   ID_BUSINESS_TYPE     int,
   primary key (ID)
);

/*==============================================================*/
/* Table: USERREASONINVESTMENT                                  */
/*==============================================================*/
create table USERREASONINVESTMENT
(
   ID                   int not null auto_increment,
   ID_USER              int,
   ID_REASON_INVESTMENT int,
   primary key (ID)
);

alter table COMPANY add constraint FK_COMPANY_BUSINESSTYPE foreign key (ID_BUSINESS_TYPE)
      references BUSINESSTYPE (ID) on delete restrict on update restrict;

alter table COMPANY add constraint FK_COMPANY_COMPANY_STATUS foreign key (ID_COMPANY_STATUS)
      references COMPANYSTATUS (ID) on delete restrict on update restrict;

alter table COMPANY add constraint FK_COMPANY_CONTACT foreign key (ID_CONTACT)
      references CONTACT (ID) on delete restrict on update restrict;

alter table COMPANY add constraint FK_PAYMENTTYPE_COMPANY foreign key (ID_TYPE_PAYMENT)
      references PAYMENTTYPE (ID) on delete restrict on update restrict;

alter table DOCUMENT add constraint FK_PROJECT_DOCUMENT foreign key (ID_PROJECT)
      references PROJECT (ID) on delete restrict on update restrict;

alter table INVESTMENT add constraint FK_CURRENCY_INVESTMENT foreign key (ID_CURRENCY)
      references CURRENCY (ID) on delete restrict on update restrict;

alter table INVESTMENT add constraint FK_PERSON_INVESTMENT foreign key (ID_PERSON)
      references PERSON (ID) on delete restrict on update restrict;

alter table INVESTMENT add constraint FK_PROJECT_INVESTMENT foreign key (ID_PROJECT)
      references PROJECT (ID) on delete restrict on update restrict;

alter table PERSON add constraint FK_PERSON_EXPERIENCE_LEVEL foreign key (ID_EXPERIENCE_LEVEL)
      references EXPERIENCELEVEL (ID) on delete restrict on update restrict;

alter table PERSON add constraint FK_PERSON_HOW_FIND foreign key (ID_HOW_FIND)
      references HOWFIND (ID) on delete restrict on update restrict;

alter table PROJECT add constraint FK_PROJECT__COMPANY foreign key (ID_COMPANY)
      references COMPANY (ID) on delete restrict on update restrict;

alter table PROJECT add constraint FK_REFERENCE_17 foreign key (ID_PERSON)
      references PERSON (ID) on delete restrict on update restrict;

alter table USER add constraint FK_USER_PERSON foreign key (ID_PERSON)
      references PERSON (ID) on delete restrict on update restrict;

alter table USERBUSINESSTYPE add constraint FK_USER_BUSINESS_TYPE_BUSINESS_TYPE foreign key (ID_BUSINESS_TYPE)
      references BUSINESSTYPE (ID) on delete restrict on update restrict;

alter table USERBUSINESSTYPE add constraint FK_USER_BUSNESS_TYPE_PERSON foreign key (ID_USER)
      references PERSON (ID) on delete restrict on update restrict;

alter table USERREASONINVESTMENT add constraint FK_USER_REASON_INVESTMENT_REASON_INVESTMENT foreign key (ID_REASON_INVESTMENT)
      references REASONINVESTMENT (ID) on delete restrict on update restrict;

alter table USERREASONINVESTMENT add constraint FK_USER_RESASON_INVESTMENT_PERSON foreign key (ID_USER)
      references PERSON (ID) on delete restrict on update restrict;

