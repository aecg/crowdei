<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<body>
<div class="contenido">

    <img src="http://crowdei.softclear.net/template/img/howWork.png" alt="banner" width="100%"/>
    <h2 style="font-family:Avenir Black; text-align:center;">CONFIRMACIÓN REGISTRO DE USUARIO</h2>
    <p style="font-family:Helvetica;">
        Hola <strong> ${nombre} ${apellido} </strong>,<br><br>

        Tu registro en CrowdEI se ha completado con éxito. Por favor accede a tu perfil de usuario para actualizar tus notificaciones.<br><br>
        Si tienes alguna duda, puedes contactarnos a este correo electrónico <strong> contacto@crowdei.com </strong> <br><br>
        Muchas gracias por la confianza!<br><br>

        Saludos,<br>
        El equipo de CrowdEI

    </p>
    <img src="http://crowdei.softclear.net/template/img/Logo_139x43_Blanco.png" alt="firma"/>

</div>
</body>
</html>