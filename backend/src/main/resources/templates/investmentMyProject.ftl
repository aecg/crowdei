<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<body>
<div class="contenido">

    <img src="http://crowdei.softclear.net/template/img/howWork.png" alt="banner" width="100%"/>
    <h2 style="font-family:Avenir Black; text-align:center;">CONFIRMACIÓN INVERSIÓN EN PROYECTO</h2>
    <p style="font-family:Helvetica;">
        Buen día,<br><br>

        Hemos recibido una notificación de pago del usuario <strong> ${nameUser} ${lastNameUser} </strong> por un monto de <strong> ${amount} ${symbolCurrency} (${descriptionCurrency}) </strong>
        en fecha <strong> ${date} </strong> por inversión en el proyecto <strong> ${nameProject} </strong>. <br/><br/>

    Saludos,<br>
    El equipo de CrowdEI

    </p>
    <img src="http://crowdei.softclear.net/template/img/Logo_139x43_Blanco.png" alt="firma"/>

</div>
</body>
</html>