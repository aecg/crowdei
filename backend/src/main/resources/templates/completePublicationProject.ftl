<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<body>
<div class="contenido">

    <img src="http://crowdei.softclear.net/template/img/howWork.png" alt="banner" width="100%"/>
    <h2 style="font-family:Avenir Black; text-align:center;">CIERRE PUBLICACIÓN PROYECTO</h2>
    <p style="font-family:Helvetica;">
        Buen día,<br><br>

        La plataforma de CrowdEI les informa que el período de publicación del proyecto <strong> ${nombreProyecto} </strong>,
       cuyo objetivo es ${objetivoGeneral}, ha culminado con una inversión de <strong> ${simbolo} ${montototal} ${descripcionMoneda} </strong>
        que representa el <strong> ${porcentaje} % </strong> del monto  total requerido por propuesto por <strong> ${simbolo} ${objetivo} ${descripcionMoneda} </strong><br><br>

        Saludos,<br>
        El equipo de CrowdEI

    </p>
    <img src="http://crowdei.softclear.net/template/img/Logo_139x43_Blanco.png" alt="firma"/>

</div>
</body>
</html>