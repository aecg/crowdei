<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<body>
<div class="contenido">

    <img src="http://crowdei.softclear.net/template/img/howWork.png" alt="banner" width="100%"/>
    <h2 style="font-family:Avenir Black; text-align:center;">CONFIRMACIÓN FICHA REGISTRO DE PROYECTO</h2>
    <p style="font-family:Helvetica;">
        El registro de tu proyecto en CrowdEI se ha completado con éxito.<br><br>

        Estrategias Integrales dispone de diez (10) días hábiles para su revisión y aprobación. En ese período pueden contactarse contigo para
        observaciones, requerimientos y actualización de la información del proyecto. En la plataforma de CrowdEI no se visualizará el proyecto
        hasta que el mismo sea aprobado.<br><br>

        Si tienes alguna duda, puedes contactarnos a este correo electrónico <strong> contacto@crowdei.com </strong><br><br>

        A continuación el detalle de la ficha de proyecto.<br><br>


    <table style="margin:px;padding:0px;width:80%;">
        <tr>
            <td style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;width:50%;">ID Usuario Proponente</td>
            <td style="font-family:Helvetica;"> ${idPerson} </td>
        </tr>
        <tr>
            <td style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">Nombre y Apellido Usuario Proponente</td>
            <td style="font-family:Helvetica; "> ${namePerson} ${lastNamePerson} </td>
        </tr>
        <tr>
            <td style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">Fecha de registro</td>
            <td style="font-family:Helvetica; "> ${dateRegister?date?string["dd MMMM, yyyy"]} </td>
        </tr>
        <tr>
            <td style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">Folio de Registro del proyecto</td>
            <td style="font-family:Helvetica; "> ${idProject} </td>
        </tr>
    </table>

    <br><br>

    <table style="margin:px;padding:0px;width:80%;">
        <tr>
            <td colspan="4" style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px; text-align:center;">Datos del Proponente</td>
        </tr>
        <tr>
            <td colspan="2" style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;width:50%;">Tipo de Persona:</td>
            <td colspan="2" style="font-family:Helvetica;"> ${personType} </td>
        </tr>
        <tr>
            <td colspan="2" style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;width:50%;">Nombre del Representante Legal y/o Responsable del proyecto:</td>
            <td colspan="2" style="font-family:Helvetica;"> ${fullnameContact} </td>
        </tr>
        <tr>
            <td colspan="2" style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;width:50%;">Nombre del emprendedor o razón social de la empresa:</td>
            <td colspan="2" style="font-family:Helvetica;"> ${nameCompany} </td>
        </tr>
        <tr>
            <td colspan="2" style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">RFC</td>
            <td colspan="2" style="font-family:Helvetica; "> ${rfcCompany} </td>
        </tr>
        <tr>
            <td colspan="4" style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;"></td>
        </tr>
        <tr>
            <td colspan="2" style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">Dirección:</td>
            <td colspan="2" style="font-family:Helvetica;"> ${address} </td>
        </tr>
        <tr>
            <td colspan="2" style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">Ciudad:</td>
            <td colspan="2" style="font-family:Helvetica;"> ${city} </td>
        </tr>
        <tr>
            <td colspan="2" style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">Municipio:</td>
            <td colspan="2" style="font-family:Helvetica;"> ${municipality} </td>
        </tr>
        <tr>
            <td colspan="2" style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">Estado:</td>
            <td colspan="2" style="font-family:Helvetica;"> ${state} </td>
        </tr>
        <tr>
            <td colspan="2" style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">Código Postal:</td>
            <td colspan="2" style="font-family:Helvetica;"> ${postalCode} </td>
        </tr>
        <tr>
            <td colspan="2" style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">País:</td>
            <td colspan="2" style="font-family:Helvetica;"> ${country} </td>
        </tr>
        <tr>
            <td colspan="2" style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">Referencia de ubicación:</td>
            <td colspan="2" style="font-family:Helvetica;"> ${reference} </td>
        </tr>
        <tr>
            <td colspan="4" style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;"></td>
        </tr>
        <tr>
            <td style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">Teléfono 1:</td>
            <td style="font-family:Helvetica;"> ${phoneOne} </td>
            <td style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">Teléfono 2:</td>
            <td style="font-family:Helvetica;"> ${phoneTwo} </td>
        </tr>
        <tr>
            <td style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">Móvil:</td>
            <td style="font-family:Helvetica;"> ${mobile} </td>
            <td style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;"></td>
            <td style="font-family:Helvetica;"></td>
        </tr>
        <tr>
            <td colspan="2" style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">Correo electrónico:</td>
            <td colspan="2" style="font-family:Helvetica;"> ${mail} </td>
        </tr>
        <tr>
            <td colspan="2" style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">Página Web:</td>
            <td colspan="2" style="font-family:Helvetica;"> ${webPage} </td>
        </tr>
        <tr>
            <td style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">Facebook:</td>
            <td style="font-family:Helvetica;"> ${facebook} </td>
            <td style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">Twitter:</td>
            <td style="font-family:Helvetica;"> ${twitter} </td>
            <td style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">LinkedIn:</td>
            <td style="font-family:Helvetica;"> ${linkedin} </td>
        </tr>

    </table>

    <br><br>

    <table style="margin:px;padding:0px;width:80%;">
        <tr>
            <td colspan="2" style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px; text-align:center;">Información General del Proyecto</td>
        </tr>
        <tr>
            <td style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;width:50%;">Nombre del proyecto:</td>
            <td style="font-family:Helvetica;"> ${nameProject} </td>
        </tr>
        <tr>
            <td style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;width:50%;">Tipo deCrowdfunding:</td>
            <td style="font-family:Helvetica;"> ${crowdfundingType} </td>
        </tr>
        <tr>
            <td colspan="2" style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px; text-align:center;">Resumen ejecutivo del proyecto</td>
        </tr>
        <tr>
            <td style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">Objetivo general:</td>
            <td style="font-family:Helvetica;"> ${generalObjetiveProject} </td>
        </tr>
        <tr>
            <td style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">Objetivos específicos:</td>
            <td style="font-family:Helvetica;"> ${specificObjetivesProject} </td>
        </tr>
        <tr>
            <td style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">Descripción del proyecto:</td>
            <td style="font-family:Helvetica;"> ${descriptionProject} </td>
        </tr>
        <tr>
            <td style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">Sector al que impactará:</td>
            <td style="font-family:Helvetica;"> ${impactSectorProject} </td>
        </tr>
        <tr>
            <td style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">Actividad económica a la que se enfocará:</td>
            <td style="font-family:Helvetica;"> ${economicActivityProject} </td>
        </tr>
        <tr>
            <td style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">Tiempo de ejecución del proyecto (número de meses):</td>
            <td style="font-family:Helvetica;"> ${monthExecutionProject} </td>
        </tr>
        <tr>
            <td colspan="2" style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px; text-align:center;">Calendario</td>
        </tr>
        <tr>
            <td style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;width:50%;">Actividad (es)</td>
            <td style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;width:50%;">Mes de realización</td>
        </tr>
        <#list activities as activity>
            <tr>
                <td style="font-family:Helvetica;"> ${activity.activity} </td>
                <td style="font-family:Helvetica;"> ${activity.estimatedDate?date?string["MMMM, yyyy"]} </td>
            </tr>
        </#list>
    </table>

    <table style="margin:px;padding:0px;width:80%;">

    </table>

    <br><br>

    <table style="margin:px;padding:0px;width:80%;">
        <tr>
            <td colspan="4" style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px; text-align:center;">Recursos financieros</td>
        </tr>
        <tr>
            <td colspan="4" style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px; text-align:center;">Recursos financieros requeridos para desarrollar el proyecto que propone:</td>
        </tr>
        <tr>
            <td style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">Monto total requerido:</td>
            <td style="font-family:Helvetica;"> ${objetiveProject?string.currency} </td>
            <td style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">Tipo de cambio:</td>
            <td style="font-family:Helvetica;"> ${currencyProject} </td>
        </tr>
    </table>
    <br><br>

    <table style="margin:px;padding:0px;width:80%;">
        <tr>
            <td colspan="3" style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px; text-align:center;">Distribución del uso del recurso solicitado:</td>
        </tr>
        <tr>
            <td style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">Concepto (rubros)</td>
            <td style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">Recurso (importe)</td>
            <td style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px;">Recurso (Porcentaje)</td>
        </tr>
        <#list resources as resource>
        <tr>
            <td style="font-family:Helvetica;"> ${resource.concept} </td>
            <td style="font-family:Helvetica;"> ${resource.amount?string.currency} </td>
            <td style="font-family:Helvetica;"> ${resource.percentage?string.percent} </td>
        </tr>
        </#list>
    </table>

    <br><br>

    <table style="margin:px;padding:0px;width:80%;">
        <tr>
            <td colspan="3" style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px; text-align:center;">Recompensas:</td>
        </tr>
    <#list rewards as reward>
        <tr>
            <td  colspan="3">Descripción: ${reward.description} </td>
        </tr>
        <tr>
            <td style="font-family:Helvetica;">Monto: ${reward.amount?string.currency} </td>
            <td style="font-family:Helvetica;">Disponible: ${reward.available} </td>
            <td style="font-family:Helvetica;">Fecha estimada: ${reward.estimatedDate?date?string["MMMM, yyyy"]} </td>
        </tr>
    </#list>
    </table>

    <br><br>

    <table style="margin:px;padding:0px;width:80%;">
        <tr>
            <td style="font-family:Avenir Black; color: #ffffff; background-color:#9b9b9b; padding: 8px; text-align:center;">Acciones:</td>
        </tr>
        <tr>
            <td style="font-family:Helvetica;">Porcentaje de participación social: ${companySharesProject?string.percent} </td>
            <td style="font-family:Helvetica;">Inversión Mínima: ${minimumInvesment?string.currency} </td>
            <td style="font-family:Helvetica;">Meta de Inversión: ${invesmentGoal?string.currency} </td>
        </tr>
    </table>

    <br><br>







        Saludos,<br>
        El equipo de CrowdEI

    </p>
    <img src="http://crowdei.softclear.net/template/img/Logo_139x43_Blanco.png" alt="firma"/>



</div>
</body>
</html>