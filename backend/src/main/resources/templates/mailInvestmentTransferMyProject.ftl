<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<body>
<div class="contenido">

    <img src="http://crowdei.softclear.net/template/img/howWork.png" alt="banner" width="100%"/>
    <h2 style="font-family:Avenir Black; text-align:center;">RECEPCIÓN TRANSFERENCIA ELECTRÓNICA</h2>
    <p style="font-family:Helvetica;">
        Buen día,<br><br>

        Hemos recibido una notificación de Transferencia Interbancaria SPEI del usuario <strong> ${nombre} ${apellido} </strong>  con el siguiente código <strong> ${codigoTransferencia} </strong>
        por un monto de <strong> ${simbolo} ${monto} ${descripcionMoneda} </strong>  en fecha <strong> ${fechaRegistro} </strong> por inversión en el proyecto <strong> ${nombreProjecto} </strong> . <br><br>
        Estrategias Integrales dispone de cinco (5) días laborales para confirmar tal información, en ese período el estatus de la transferencia será “PENDIENTE”. <br><br>

        Saludos,<br>
        El equipo de CrowdEI

    </p>
    <img src="http://crowdei.softclear.net/template/img/Logo_139x43_Blanco.png" alt="firma"/>
</div>
</body>
</html>