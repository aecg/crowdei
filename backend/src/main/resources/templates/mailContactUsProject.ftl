<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<body>
<div class="contenido">

    <img src="http://crowdei.softclear.net/template/img/howWork.png" alt="banner" width="100%"/>
    <h2 style="font-family:Avenir Black; text-align:center;">CONTACTAR RESPONSABLE DE PROYECTO</h2>
    <p style="font-family:Helvetica;">
        Hola <strong>${nombreUsuario}</strong>,<br><br>

        Han solicitado contactarte a través de la plataforma CrowdEI en relación al proyecto <strong> ${proyecto} </strong>. A continuación el detalle del mensaje y los datos de la persona interesada.<br><br>

        Nombre y Apellido: ${nombre} ${apellido} <br>
        Correo: ${correo} <br>
        Teléfono: ${telefono} <br>
        Mensaje: ${mensaje}<br><br>

        Saludos,<br>
        El equipo de CrowdEI

    </p>
    <img src="http://crowdei.softclear.net/template/img/Logo_139x43_Blanco.png" alt="firma"/>
</div>
</body>
</html>