<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<body>
<div class="contenido">

    <img src="http://crowdei.softclear.net/template/img/howWork.png" alt="banner" width="100%"/>
    <h2 style="font-family:Avenir Black; text-align:center;">RESTABLECER CONTRASEÑA DE USUARIO</h2>
    <p style="font-family:Helvetica;">
        Hola <strong> ${nombre} ${apellido} </strong>,<br><br>

        Has solicitado recuperar tu contraseña de CrowdEI.<br><br>

        Si quieres cambiar tu contraseña accede al siguiente enlace:<br><br>

        ${url}<br><br>

        En caso contrario, simplemente elimina este correo de tu bandeja de entrada y no se realizará ningún cambio.<br><br>


        Saludos,<br>
        El equipo de CrowdEI

    </p>
    <img src="http://crowdei.softclear.net/template/img/Logo_139x43_Blanco.png" alt="firma"/>

</div>
</body>
</html>