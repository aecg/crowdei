package net.softclear.crowdei.backend.repository;



import net.softclear.crowdei.backend.model.jpa.Investment;
import org.springframework.data.domain.Pageable;

import java.util.List;



/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  InvestmentRepositoryCustom
 * Descripcion:             Contrato para todas las operaciones personalizadas en la base de datos que
 * involucran la entidad investment
 * @version                 1.0
 * @author                  buchyo
 * @since                   21/01/2016
 *
 */
public interface InvestmentRepositoryCustom
{
    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  findAll
     * Descripcion:             Método que consulta todas las inversiones de un usuario y proyecto en particular.
     * @version                 1.0
     * @author                  buchyo
     * @since                   21/01/2016
     *
     * @param idUser identificador del usuario
     * @param idProject identificador del proyecto
     * @return lista de inversiones
     */
    List<Investment> findAll(Long idUser, Long idProject, Pageable pageable);

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  findAllCompletPublication
     * Descripcion:             Método que trae todas las inversiones en el cual su proyecto
     * asociado haya terminado su tiempo de publicación
     * @version                 1.0
     * @author                  buchyo
     * @since                   05/04/2016
     *
     * @return lista de inversion
     */
    List<Investment> findAllCompletPublication();

    List<Investment> findAll(Long idProject, Pageable pageable);
}
