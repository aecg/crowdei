package net.softclear.crowdei.backend.web.rest.out;

import net.softclear.crowdei.backend.model.jpa.Investment;
import net.softclear.crowdei.backend.model.jpa.Project;
import net.softclear.crowdei.backend.model.types.Status;
import net.softclear.crowdei.backend.service.InvestmentService;
import net.softclear.crowdei.backend.service.ProjectService;
import net.softclear.crowdei.backend.util.DateUtils;
import net.softclear.crowdei.backend.web.rest.out.to.projectResponse.GetProjectsCompletPublicationResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  InvestmentsOutRestController
 * Descripcion:             Clase que posee la definicion y la implementacion de los servicios
 * @version                 1.0
 * @author                  buchyo
 * @since                   07/04/2016
 *
 */
@RestController
public class InvestmentsOutRestController
{
    //region Atributos

    @Autowired
    private InvestmentService investmentService;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private DateUtils dateUtils;
    static final int CERO = 0;
    static final int PROYECTOINVERTIDOFINALIZADO = 3;

    //endegion

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  InvestmentsRestController
     * Descripcion:             Método que trae todas las inversiones en el cual su proyecto
     * asociado haya terminado su tiempo de publicación
     * @version                 1.0
     * @author                  buchyo
     * @since                   07/04/2016
     *
     * @return Objeto que posee la informacion en formato JSON
     */
    @RequestMapping( value="InvestmentsCompletPublication", method = RequestMethod.GET, produces = "application/json" )
    public @ResponseBody
    GetProjectsCompletPublicationResponse findAllCompletPublication()
    {
        GetProjectsCompletPublicationResponse.Project obj;
        List<Project> listProjects;
        GetProjectsCompletPublicationResponse response;
        List<GetProjectsCompletPublicationResponse.Project> projectsResponse;
        Pageable pageable;

        pageable = new PageRequest( 1, 1 );
        listProjects = projectService.getAllProjects(null, pageable );
        response = new GetProjectsCompletPublicationResponse();

        if(listProjects != null)
        {
            projectsResponse = new ArrayList<>();

            for (Project project : listProjects)
            {
                obj = modelMapper.map(project, GetProjectsCompletPublicationResponse.Project.class);

                final int publicationDays = project.getPublicationDays();

                if ((publicationDays - dateUtils.getDaysLeft(project.getDateRegister(), new Date())) == CERO)
                {
                    obj.setId( project.getId() );
                    obj.setNameProject( project.getName() );
                    obj.setGeneralObjetive( project.getGeneralObjetive() );
                    obj.setTotalAmountInvested( totalAmountInvested(project.getInvestments()) );
                    obj.setPercent( obtainPercent( project ) );
                    obj.setSymbolCurrency( project.getCurrency().getSymbol() );
                    obj.setDescriptionCurrency( project.getCurrency().getDescription() );
                    obj.setObjetivo( project.getObjetive() );
                    obj.setEmailPersonCreateProject( project.getPerson().getEmail() );
                    obj.setEmailContact( project.getContact().getContactInformation().getEmail() );

                    projectsResponse.add(obj);
                }
            }
            sendMail( projectsResponse );
            response.setInvestments( projectsResponse );
        }

        return response;
    }

    //region metodos privados

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  obtainPercent
     * Descripcion:             Metodo que calcula el porcentaje obtenido del objetivo
     * de un proyecto al final de su publicacion
     * @version                 1.0
     * @author                  buchyo
     * @since                   06/04/2016
     *
     * @param project el proyecto
     * @return porcentaje calculado
     */
    private double obtainPercent(Project project)
    {
        double retorno;
        final int percentege = 100;
        double obtained = 0;

        for(Investment investment : project.getInvestments())
        {
            if(investment.getStatus().equals( Status.ACTIVE ))
                obtained += investment.getAmount();
        }

        retorno = cambiarFormato( ( obtained / project.getObjetive() ) * percentege ) ;

        return retorno;
    }

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  TotalAmountInvested
     * Descripcion:             Método que suma todas las inversiones de estatus activo de
     * un proyecto al final de su publicación
     * @version                 1.0
     * @author                  buchyo
     * @since                   20/04/2016
     *
     * @param listInvestments lista de inversiones
     * @return total recaudado de la inversion
     */
    private double totalAmountInvested(List<Investment> listInvestments)
    {
        double retorno;
        double obtained = 0;

        for(Investment investment : listInvestments)
        {
            if(investment.getStatus().equals( Status.ACTIVE ))
                obtained += investment.getAmount();
        }

        retorno = obtained ;

        return retorno;
    }

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  sendMail
     * Descripcion:             Metodo que envia el correo de culminación de proyecto a todos los interesados
     * @version                 1.0
     * @author                  buchyo
     * @since                   20/04/2016
     *
     * @param projectsResponse todos los proyectos activos
     */
    private void sendMail(List<GetProjectsCompletPublicationResponse.Project> projectsResponse)
    {
        List<GetProjectsCompletPublicationResponse.Investment> investmentAux;
        investmentAux = new ArrayList<>();
        Project projectConsult;

        for ( GetProjectsCompletPublicationResponse.Project project : projectsResponse)
        {
            for ( GetProjectsCompletPublicationResponse.Investment investment : project.getInvestments() )
            {
                if (investment.getStatus().equals("ACTIVE"))
                {
                    int i = 0;
                    for (GetProjectsCompletPublicationResponse.Investment investmentInt : investmentAux)
                        if (investment.getPerson().getId() == investmentInt.getPerson().getId())
                            i++;
                    if (i == 0)
                        investmentAux.add(investment);
                }
            }
            // Recorro esta lista para obtener los email de los inversores en este proyecto sin repetirlos
            // y que la notificacion este activa de recibir un correo en el que haya invertido ha finalizado
            for ( GetProjectsCompletPublicationResponse.Investment investment : investmentAux )
            {
                if (investment.getPerson().getMailPreferenceList().size() > CERO)
                {
                    for (GetProjectsCompletPublicationResponse.MailPreference mail :
                            investment.getPerson().getMailPreferenceList())
                    {
                        if (mail.getId() == PROYECTOINVERTIDOFINALIZADO)
                            investmentService.sendMailCompletPublication(project.getId(),
                                    project.getTotalAmountInvested(), project.getPercent(),
                                    investment.getPerson().getEmail());
                    }
                }
            }
            //Persona contacto del proyecto
            investmentService.sendMailCompletPublication(project.getId(),
                    project.getTotalAmountInvested(),project.getPercent(), project.getEmailContact());

            //Dueño del proyecto
            investmentService.sendMailCompletPublication(project.getId(),
                    project.getTotalAmountInvested(),project.getPercent(), project.getEmailPersonCreateProject());

            projectConsult = projectService.getProject(project.getId());
            projectConsult.setStatus(Status.ENDPUBLICATION);
            projectService.updateProject(projectConsult);
        }

    }


    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  cambiarFormato
     * Descripcion:             Método que cambia el fotmarto ##,## a ##.##
     * @version                 1.0
     * @author                  buchyo
     * @since                   07/04/2016
     *
     * @param numero
     * @return
     */
    private double cambiarFormato(double numero)
    {
        DecimalFormatSymbols simbolo;
        simbolo = new DecimalFormatSymbols();
        simbolo.setDecimalSeparator('.');
        DecimalFormat df;
        df = new DecimalFormat("##########.##",simbolo);
        double retorno;

        retorno = Double.parseDouble(df.format(numero));

        return retorno;
    }

    //endregion

}
