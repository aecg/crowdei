package net.softclear.crowdei.backend.repository.implementation;

import com.sun.org.apache.xpath.internal.operations.Bool;
import net.softclear.crowdei.backend.model.jpa.User;
import net.softclear.crowdei.backend.model.types.Status;
import net.softclear.crowdei.backend.repository.UserRepositoryCustom;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  UserRepositoryImpl
 * Descripcion:             Clase que mimplementa el contrato para las operaciones personalizadas
 * para la entidad usuario
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
public class UserRepositoryImpl implements UserRepositoryCustom
{
    //region Atributos

	@PersistenceContext
	private EntityManager entityManager;

    //endregion

    //region Implementacion

    /**
     * Nombre:                  findByEmail.
     * Descripcion:             Metodo que obtiene un usuario dado su correo
     * electronico
     *
     * @param email email del usuario
     *
     * @return datos del usuario completo
     *
     * @version 1.0
     * @author guzmle
     * @since 12/10/2015
     */
    @Override
    public User findByEmail( String email )
    {
        List<Predicate> predicates;
        CriteriaBuilder criteriaBuilder;
        CriteriaQuery<User> criteriaQuery;
        Root<User> root;
        Join joinPerson;
        Query query;
        User response;

        predicates = new ArrayList<>(  );
        criteriaBuilder = entityManager.getCriteriaBuilder();
        criteriaQuery = criteriaBuilder.createQuery(User.class);
        root = criteriaQuery.from( User.class );
        joinPerson= root.join( "person" );



        predicates.add( criteriaBuilder.equal( joinPerson.get( "email" ), email ) );
        criteriaQuery.where( predicates.toArray( new Predicate[ predicates.size() ] ) );
        query = entityManager.createQuery(criteriaQuery);
        response= ( User ) query.getSingleResult();

        return response;
    }

    @Override
    public Boolean emailExist(String email)
    {
        List<Predicate> predicates;
        CriteriaBuilder criteriaBuilder;
        CriteriaQuery<User> criteriaQuery;
        Root<User> root;
        Join joinPerson;
        Query query;
        Boolean response;
        response = false;

        predicates = new ArrayList<>(  );
        criteriaBuilder = entityManager.getCriteriaBuilder();
        criteriaQuery = criteriaBuilder.createQuery(User.class);
        root = criteriaQuery.from( User.class );
        joinPerson= root.join( "person" );



        predicates.add( criteriaBuilder.equal( joinPerson.get( "email" ), email ) );
        criteriaQuery.where( predicates.toArray( new Predicate[ predicates.size() ] ) );
        query = entityManager.createQuery(criteriaQuery);
        if (query.getResultList().size() > 0)
            response = true;

        return response;
    }

    @Override
    public List<User> findAllUsers(Boolean activeUsers, Pageable pageable)
    {
        CriteriaBuilder criteriaBuilder;
        CriteriaQuery<User> criteriaQuery;
        Root<User> root;

        criteriaBuilder = entityManager.getCriteriaBuilder();
        criteriaQuery = criteriaBuilder.createQuery(User.class);
        root = criteriaQuery.from( User.class );

        if (activeUsers != null && activeUsers.booleanValue()) {
            criteriaQuery.where(criteriaBuilder.equal(root.get("enabled"), Boolean.TRUE));
        }

        Query query;
        query = entityManager.createQuery(criteriaQuery);
        return query.getResultList();
    }

//endregion
}
