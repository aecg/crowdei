package net.softclear.crowdei.backend.web.rest.web.util;

import net.softclear.crowdei.backend.model.jpa.Currency;
import net.softclear.crowdei.backend.service.CurrencyService;
import net.softclear.crowdei.backend.web.rest.web.util.to.GetObjectMasterResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  CurrencyRestController
 * Descripcion:             Clase que posee la definicion y la implementacion de los servicios que involucran a
 * la entidad usuario
 * @version                 1.0
 * @author                  buchyo
 * @since                   15/03/2016
 *
 */
@RestController
@RequestMapping("/web/utils*")
public class CurrencyRestController
{
    //region Attributes

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private CurrencyService service;

    //endregion

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  CurrencyRestController
     * Descripcion:             Clase que posee la definicion y la implementacion de los servicios que involucran a
     * la entidad usuario
     * @version                 1.0
     * @author                  buchyo
     * @since                   15/03/2016
     *
     * @return lista de tipo de moneda
     */
    @RequestMapping( value = "/currency", method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    GetObjectMasterResponse getAll()
    {
        List<Currency> list;
        GetObjectMasterResponse response;
        List<GetObjectMasterResponse.Item> listResponse;
        GetObjectMasterResponse.Item item;

        list = service.getAll();
        response = new GetObjectMasterResponse();

        if ( list != null )
        {
            listResponse = new ArrayList<>();
            for ( Currency obj : list )
            {
                item = modelMapper.map( obj, GetObjectMasterResponse.Item.class );
                listResponse.add( item );
            }

            response.setList( listResponse );
        }
        return response;
    }

}
