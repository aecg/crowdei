package net.softclear.crowdei.backend.repository;



import net.softclear.crowdei.backend.model.jpa.Country;
import net.softclear.crowdei.backend.model.jpa.State;

import java.util.List;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  StateRepositoryCustom
 * Descripcion:             Contrato para el respositorio de estados
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/9/2015
 *
 */
public interface StateRepositoryCustom
{
    /**
     * Nombre:                  findByCountry.
     * Descripcion:             Metodo que consulta los estados de un pais
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/9/2015
     *
     * @param country datos del pais
     * @return lista de estados
     */
	List<State> findByCountry( Country country );
}
