package net.softclear.crowdei.backend.service.implementation;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import net.softclear.crowdei.backend.config.MessagesConfig;
import net.softclear.crowdei.backend.config.SettingsConfig;
import net.softclear.crowdei.backend.exception.ServiceException;
import net.softclear.crowdei.backend.model.jpa.Calendar;
import net.softclear.crowdei.backend.model.jpa.City;
import net.softclear.crowdei.backend.model.jpa.Company;
import net.softclear.crowdei.backend.model.jpa.Contact;
import net.softclear.crowdei.backend.model.jpa.ContactInformation;
import net.softclear.crowdei.backend.model.jpa.Country;
import net.softclear.crowdei.backend.model.jpa.Document;
import net.softclear.crowdei.backend.model.jpa.InternetInfo;
import net.softclear.crowdei.backend.model.jpa.Location;
import net.softclear.crowdei.backend.model.jpa.Person;
import net.softclear.crowdei.backend.model.jpa.Project;
import net.softclear.crowdei.backend.model.jpa.Resource;
import net.softclear.crowdei.backend.model.jpa.Reward;
import net.softclear.crowdei.backend.model.jpa.State;
import net.softclear.crowdei.backend.model.types.CrowdfundingType;
import net.softclear.crowdei.backend.model.types.PersonType;
import net.softclear.crowdei.backend.model.types.Priority;
import net.softclear.crowdei.backend.model.types.Status;
import net.softclear.crowdei.backend.repository.CalendarRepository;
import net.softclear.crowdei.backend.repository.CityRepository;
import net.softclear.crowdei.backend.repository.CompanyRepository;
import net.softclear.crowdei.backend.repository.ContactRepository;
import net.softclear.crowdei.backend.repository.CountryRepository;
import net.softclear.crowdei.backend.repository.DocumentRepository;
import net.softclear.crowdei.backend.repository.PersonRepository;
import net.softclear.crowdei.backend.repository.ProjectRepository;
import net.softclear.crowdei.backend.repository.ResourceRepository;
import net.softclear.crowdei.backend.repository.RewardRepository;
import net.softclear.crowdei.backend.repository.StateRepository;
import net.softclear.crowdei.backend.service.ProjectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.mail.MailSendException;

import javax.activation.DataHandler;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import javax.persistence.NoResultException;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  ProjectServiceImpl
 * Descripcion:             Clase que implementa el contrato del servicio
 * @version                 1.0
 * @author                  buchyo
 * @since                   12/02/2015
 *
 */
@Service
@Transactional
public class ProjectServiceImpl implements ProjectService
{
    //region Atributos
    private static Logger logger = LoggerFactory.getLogger( ProjectServiceImpl.class );
    @Autowired
    private ProjectRepository  projectRepository;
    @Autowired
    private PersonRepository   personRepository;
    @Autowired
    private DocumentRepository documentRepository;
    @Autowired
    private CalendarRepository calendarRepository;
    @Autowired
    private ResourceRepository resourceRepository;
    @Autowired
    private RewardRepository rewardRepository;
    @Autowired
    private MessagesConfig     messagesConfig;
    @Autowired
    private Configuration      freemarkerConfiguration;
    @Autowired
    private JavaMailSender     mailSender;
    @Value( "${email.receivers}" )
    private String             receivers;
    @Autowired
    private ContactRepository  contactRepository;
    @Autowired
    private CompanyRepository  companyRepository;
    @Autowired
    private CityRepository cityRepository;
    @Autowired
    private StateRepository stateRepository;
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private SettingsConfig settingsConfig;

    //endregion


    //region Metodos
    @Override
    public List<Project> getProjectsXId( Long id, Pageable pageable )
    {
        logger.debug( "Entrando al metodo getProjectsXId" );
        List<Project> listProjects = null;

        try
        {
            listProjects = projectRepository.findAll( id, pageable );

        }
        catch ( Exception e )
        {
            logger.error( "Ha ocurrido un error consultando proyecto por id", e );
            throw new ServiceException( "Error reading projects from database", e );
        }

        logger.debug( "Saliendo del metodo getProjectsXId" );
        return listProjects;
    }

    @Override
    public Project createProject( Project project )
    {
        logger.debug( "Entrando al metodo createProject" );
        Project theProject;
        try
        {
            Person person;
            Contact contact;
            Company company;
            person = personRepository.findOne( project.getPerson().getId() );
            project.setPerson( person );
            if (project.getPersonType() == PersonType.FISICA)
            {
                if ((project.getContact() != null) && (project.getContact().getId() != null))
                {
                    contact = contactRepository.findOne(project.getContact().getId());
                    project.setContact(contact);
                }
            }
            else if (project.getPersonType() == PersonType.MORAL)
            {
                if ((project.getCompany() != null) && (project.getCompany().getId() != null))
                {
                    company = companyRepository.findOne( project.getCompany().getId() );
                    project.setCompany( company );
                }
            }
            for ( Document doc : project.getDocuments() )
                doc.setProject( project );
            for ( Calendar cal : project.getCalendars() )
                cal.setProject( project );
            for ( Resource res : project.getResources() )
                res.setProject( project );
            for ( Reward reward : project.getRewards() )
                reward.setProject( project );
            project.setPublicationDays(settingsConfig.projectDefaultPublicationDays);
            theProject = projectRepository.save( project );
            sendEmailCreatePitch(theProject);
        }
        catch ( Exception e )
        {
            logger.error( "Error creating project in the database", e );
            throw new ServiceException( "Error creating project in the database", e );
        }
        logger.debug( "Saliendo del metodo createProject" );
        return theProject;
    }


    @Override
    public Project deleteDocument( Document document )
    {
        logger.debug( "Entrando al metodo deleteDocument" );
        final Project theProject = null;

        try
        {
            documentRepository.delete( document );

        }
        catch ( Exception e )
        {
            logger.error( "Error deleting document in the database", e );
            throw new ServiceException( "Error deleting project in the database", e );
        }

        logger.debug( "Saliendo del metodo deleteDocument" );
        return theProject;
    }


    @Override
    public Project getProject( Long id )
    {
        logger.debug( "Entrando al metodo getProject" );
        Project theProject;

        try
        {
            theProject = projectRepository.findOne( id );
        }
        catch ( Exception e )
        {
            logger.error( "Error reading project from database", e );
            throw new ServiceException( "Error reading project from database", e );
        }

        logger.debug( "Saliendo del metodo getProject" );
        return theProject;
    }


    @Override
    public void updateProject(Project project)
    {
        logger.debug( "Entrando al metodo updateProject" );
        try
        {
            for ( Document doc : project.getDocuments() )
                doc.setProject( project );
            for ( Calendar cal : project.getCalendars() )
                cal.setProject( project );
            for ( Resource res : project.getResources() )
                res.setProject( project );
            for ( Reward reward : project.getRewards() )
                reward.setProject( project );
            projectRepository.save(project);
        }
        catch (Exception e)
        {
            logger.error( "Error modifying the project in the database", e );
            throw new ServiceException("Error modifying the project in the database", e);
        }
        logger.debug( "Saliendo del metodo updateProject" );
    }


    @Override
    public List<Project> getProjects(String nameCompany, String nameProject, Long idEconomicActivity,
            Pageable pageable)
    {
        logger.debug( "Entrando al metodo getProjects" );
        List<Project> listProjects = null;

        try
        {
            listProjects = projectRepository.findAll( nameCompany, nameProject, idEconomicActivity, pageable );

        }
        catch (Exception e)
        {
            logger.error( "Error reading projects from database", e );
            throw new ServiceException("Error reading projects from database", e);
        }

        logger.debug( "Saliendo del metodo getProjects" );
        return listProjects;
    }

    @Override
    public List<Project> getProjectsXIdXParams(Long id, String nameCompany, String nameProject, Long idEconomicActivity,
            Pageable pageable)
    {
        logger.debug( "Entrando al metodo getProjectsXIdXParams" );
        List<Project> listProjects = null;

        try
        {
            listProjects = projectRepository.findAll( id, nameCompany, nameProject, idEconomicActivity, pageable );

        }
        catch (Exception e)
        {
            logger.error( "Error reading projects from database", e );
            throw new ServiceException("Error reading projects from database", e);
        }

        logger.debug( "Saliendo del metodo getProjectsXIdXParams" );
        return listProjects;
    }

    @Override
    public void deleteProject(Long id)
    {
        logger.debug( "Entrando al metodo deleteProject" );
        try
        {
            Project project;
            project = projectRepository.findOne( id );
            project.setStatus( Status.DELETED );
            projectRepository.save(project );
        }
        catch (Exception e)
        {
            logger.error( "Error reading deleting from database", e );
            throw new ServiceException("Error deleting project from database", e);
        }

        logger.debug( "Saliendo del metodo deleteProject" );
    }

    @Override
    public List<Project> getProjectsPriority()
    {
        logger.debug( "Entrando al metodo getProjectsPriority" );
        List<Project> listProjects = null;

        try
        {
            listProjects = projectRepository.findAll();

        }
        catch (Exception e)
        {
            logger.error( "Error reading projects from database", e );
            throw new ServiceException("Error reading projects from database", e);
        }

        logger.debug( "Saliendo del metodo getProjectsPriority" );
        return listProjects;
    }

    @Override
    public List<Project> getAllProjects(Boolean activeProjects, Pageable pageable)
    {
        logger.debug( "Entrando al metodo getAllProjects" );
        List<Project> listProjects = null;

        try
        {
            listProjects = projectRepository.findAllProjects( activeProjects, pageable );

        }
        catch ( Exception e )
        {
            logger.error( "Ha ocurrido un error consultando los proyectos", e );
            throw new ServiceException( "Error reading projects from database", e );
        }

        logger.debug( "Saliendo del metodo getAllProjects" );
        return listProjects;
    }


    @Override
    public List<Project> getAllProjectsInvestment(Long id, Pageable pageable)
    {
        logger.debug( "Entrando al metodo getAllProjectsInvestment" );
        List<Project> listProjects = null;

        try
        {
            listProjects = projectRepository.findAllProjectsInvestment( id, pageable );
        }
        catch ( Exception e )
        {
            logger.error( "Ha ocurrido un error consultando los proyectos", e );
            throw new ServiceException( "Error reading projects from database", e );
        }

        logger.debug( "Saliendo del metodo getAllProjectsInvestment" );
        return listProjects;
    }

    @Override
    public List<Project> getProjectsInvestmentXIdXParams(Long id, String nameCompany, String nameProject, Date
            startDate, Date endDate, Pageable pageable)
    {
        logger.debug( "Entrando al metodo getProjectsInvestmentXIdXParams" );
        List<Project> listProjects = null;

        try
        {
            listProjects = projectRepository.findAllProjectsInvestmentXIdXParams( id, nameCompany, nameProject,
                    startDate, endDate, pageable);
        }
        catch ( Exception e )
        {
            logger.error( "Ha ocurrido un error consultando los proyectos", e );
            throw new ServiceException( "Error reading projects from database", e );
        }

        logger.debug( "Saliendo del metodo getProjectsInvestmentXIdXParams" );
        return listProjects;
    }

    @Override
    public List<Project> getProjectsPriorityType(String priority)
    {
        logger.debug( "Entrando al metodo getProjectsPriorityType" );
        List<Project> listProjects = null;

        try
        {
            listProjects = projectRepository.findAllProjectsPriorityType(priority);
        }
        catch ( Exception e )
        {
            logger.error( "Ha ocurrido un error consultando los proyectos", e );
            throw new ServiceException( "Error reading projects from database", e );
        }

        logger.debug( "Saliendo del metodo getProjectsPriorityType" );
        return listProjects;

    }

    @Override
    public Project deleteActivity( Calendar calendar )
    {
        logger.debug( "Entrando al metodo deleteActivity" );
        final Project theProject = null;

        try
        {
            calendarRepository.delete( calendar );

        }
        catch ( Exception e )
        {
            logger.error( "Error deleting document in the database", e );
            throw new ServiceException( "Error deleting project in the database", e );
        }

        logger.debug( "Saliendo del metodo deleteActivity" );
        return theProject;
    }

    @Override
    public Project deleteResource( Resource resource )
    {
        logger.debug( "Entrando al metodo deleteResource" );
        final Project theProject = null;

        try
        {
            resourceRepository.delete( resource );

        }
        catch ( Exception e )
        {
            logger.error( "Error deleting document in the database", e );
            throw new ServiceException( "Error deleting project in the database", e );
        }

        logger.debug( "Saliendo del metodo deleteResource" );
        return theProject;
    }

    @Override
    public Project deleteReward( Reward reward )
    {
        logger.debug( "Entrando al metodo deleteReward" );
        final Project theProject = null;

        try
        {
            rewardRepository.delete( reward );

        }
        catch ( Exception e )
        {
            logger.error( "Error deleting reward in the database", e );
            throw new ServiceException( "Error deleting project in the database", e );
        }

        logger.debug( "Saliendo del metodo deleteReward" );
        return theProject;
    }

    @Override
    public void activateProject(Long id)
    {
        logger.debug( "Entrando al metodo activateProject" );
        try
        {
            Project project;
            project = projectRepository.findOne( id );
            project.setStatus( Status.ACTIVE );
            projectRepository.save(project );
        }
        catch (Exception e)
        {
            logger.error( "Error reading changing status from database", e );
            throw new ServiceException("Error changing project status from database", e);
        }

        logger.debug( "Saliendo del metodo activateProject" );
    }

    @Override
    public void deactivateProject(Long id)
    {
        logger.debug( "Entrando al metodo deactivateProject" );
        try
        {
            Project project;
            project = projectRepository.findOne( id );
            project.setStatus( Status.INACTIVE );
            projectRepository.save(project );
        }
        catch (Exception e)
        {
            logger.error( "Error reading changing status from database", e );
            throw new ServiceException("Error changing project status from database", e);
        }

        logger.debug( "Saliendo del metodo deactivateProject" );
    }

    @Override
    public void changePriorityProject(Long id, Priority priority)
    {
        logger.debug( "Entrando al metodo changePriorityProject" );
        try
        {
            Project project;
            project = projectRepository.findOne( id );
            project.setPriorityType(priority);
            projectRepository.save(project );
        }
        catch (Exception e)
        {
            logger.error( "Error reading changing priority type from database", e );
            throw new ServiceException("Error changing project priority type from database", e);
        }

        logger.debug( "Saliendo del metodo changePriorityProject" );
    }
    //endregion

    //region Metodos privados


    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  sendEmailCreatePitch
     * Descripcion:             Metodo que envia un correo al crearse un proyecto con los
     * archivos adjuntos
     * @version                 1.0
     * @author                  buchyo
     * @since                   11/02/2016
     *
     * @param project proyecto creado
     * @return
     */
    private boolean sendEmailCreatePitch(Project project)
    {
        try
        {
            final Template template = freemarkerConfiguration.getTemplate("mailCreatePitch.ftl");
            StringWriter output;
            MimeMessage msg;
            output = new StringWriter();
            template.process( fillParameters(project), output );
            msg = mailSender.createMimeMessage();       //Agrego los receptores y el asunto del correo
            msg.setRecipients( Message.RecipientType.TO, createReceiverslist() );
            msg.setSubject( "Confirmación Ficha de Proyecto en CrowdEI" );
            Multipart multipart;
            multipart = new MimeMultipart("related");
            MimeBodyPart messageBodyPart;               //Agrego el contenido del correo
            messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(output.toString(),"text/html");
            multipart.addBodyPart( messageBodyPart);
            msg.setContent( attachFile( project.getDocuments(), multipart ) );
            mailSender.send( msg );
            msg.setRecipients( Message.RecipientType.TO, project.getPerson().getEmail() );
            mailSender.send( msg );
            ContactInformation contactInformation = new ContactInformation();
            if (project.getPersonType() == PersonType.FISICA)
            {
                contactInformation = project.getContact().getContactInformation();
            }
            else if (project.getPersonType() == PersonType.MORAL)
            {
                contactInformation = project.getCompany().getContactInformation();
            }
            msg.setRecipients( Message.RecipientType.TO, contactInformation.getEmail() );
            mailSender.send( msg );
            return true;
        }
        catch ( NoResultException | TemplateException e )
        {
            logger.error(messagesConfig.userNotFound, e);
            throw new ServiceException( messagesConfig.userNotFound );
        }
        catch (MailSendException mse)
        {
            logger.error( "Error sending project info mail", mse );
        }
        catch ( IOException | MessagingException e )
        {
            logger.error(messagesConfig.messagingError, e);
            throw new ServiceException( messagesConfig.messagingError );
        }
        return false;
    }

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  fillParametersLocation
     * Descripcion:             Metodo que llena los parametros de dirección de la plantilla
     * @version                 1.0
     * @author                  martin
     * @since                   09/12/2016
     *
     * @param project datos del proyecto
     * @param params arreglo con los parametros
     * @return los parametros
     */
    private void fillParametersLocation(Project project, Map<String, Object> params) throws IOException
    {
        City ciudad;
        State estado;
        Country pais;

        Location location = new Location();
        if (project.getPersonType() == PersonType.FISICA)
        {
            location = project.getContact().getLocation();
        }
        else if (project.getPersonType() == PersonType.MORAL)
        {
            location = project.getCompany().getLocation();
        }

        params.put( "address", location.getAddress() );

        ciudad = cityRepository.getOne(location.getCity().getId());
        params.put( "city", ciudad.getName());

        params.put( "municipality", location.getMunicipality() );

        estado = stateRepository.getOne(ciudad.getState().getId());
        params.put( "state", estado.getName() );

        params.put( "postalCode", location.getPostalCode());

        pais = countryRepository.getOne(estado.getCountry().getId());
        params.put( "country", pais.getName());

        if (location.getLocationReference() == null)
            params.put( "reference", " " );
        else
            params.put( "reference", location.getLocationReference() );
    }

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  fillParametersContactInformation
     * Descripcion:             Metodo que llena los parametros de contacto, telefonos y correo de la plantilla
     * @version                 1.0
     * @author                  martin
     * @since                   09/12/2016
     *
     * @param project datos del proyecto
     * @param params arreglo con los parametros
     * @return los parametros
     */
    private void fillParametersContactInformation(Project project, Map<String, Object> params) throws IOException
    {
        ContactInformation contactInformation = new ContactInformation();
        if (project.getPersonType() == PersonType.FISICA)
        {
            contactInformation = project.getContact().getContactInformation();
        }
        else if (project.getPersonType() == PersonType.MORAL)
        {
            contactInformation = project.getCompany().getContactInformation();
        }
        params.put( "phoneOne", contactInformation.getPhoneOne() );
        if (contactInformation.getPhoneTwo() == null)
            params.put( "phoneTwo", " ");
        else
            params.put( "phoneTwo", contactInformation.getPhoneTwo() );
        if (contactInformation.getMobile() == null)
            params.put( "mobile", " " );
        else
            params.put( "mobile", contactInformation.getMobile() );
        params.put( "mail", contactInformation.getEmail() );
    }

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  fillParametersInternetInfo
     * Descripcion:             Metodo que llena los parametros de pagina web y redes sociales de la plantilla
     * @version                 1.0
     * @author                  martin
     * @since                   09/12/2016
     *
     * @param project datos del proyecto
     * @param params arreglo con los parametros
     * @return los parametros
     */
    private void fillParametersInternetInfo(Project project, Map<String, Object> params) throws IOException
    {
        InternetInfo internetInfo = new InternetInfo();
        if (project.getPersonType() == PersonType.FISICA)
        {
            internetInfo = project.getContact().getInternetInformation();
        }
        else if (project.getPersonType() == PersonType.MORAL)
        {
            internetInfo = project.getCompany().getInternetInformation();
        }
        if (internetInfo.getWebPage() == null)
            params.put( "webPage", " " );
        else
            params.put( "webPage", internetInfo.getWebPage() );
        if (internetInfo.getFacebook() == null)
            params.put( "facebook", " " );
        else
            params.put( "facebook", internetInfo.getFacebook() );
        if (internetInfo.getTwitter() == null)
            params.put( "twitter", " " );
        else
            params.put( "twitter", internetInfo.getTwitter() );
        if (internetInfo.getLinkedin() == null)
            params.put( "linkedin", " " );
        else
            params.put( "linkedin", internetInfo.getLinkedin() );
    }

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  fillParametersProject
     * Descripcion:             Metodo que llena los parametros del proyecto de la plantilla
     * @version                 1.0
     * @author                  martin
     * @since                   09/12/2016
     *
     * @param project datos del proyecto
     * @param params arreglo con los parametros
     * @return los parametros
     */
    private void fillParametersProject(Project project, Map<String, Object> params) throws IOException
    {
        params.put( "idPerson", project.getPerson().getId() );
        params.put( "namePerson", project.getPerson().getName() );
        params.put( "lastNamePerson", project.getPerson().getLastName() );
        params.put( "dateRegister", project.getDateRegister() );
        params.put( "idProject", project.getId() );
        params.put( "crowdfundingType", project.getCrowdfundingType().toString() );
        params.put( "personType", project.getPersonType().toString() );
        params.put( "generalObjetiveProject", (project.getGeneralObjetive() == null) ? "" : project.getGeneralObjetive() );
        params.put( "specificObjetivesProject", (project.getSpecificObjetives() == null) ? "" : project.getSpecificObjetives() );
        params.put( "descriptionProject", (project.getDescription() == null) ? "" : project.getDescription() );
        params.put( "impactSectorProject", project.getImpactSector().getDescription());
        params.put( "economicActivityProject", project.getEconomicActivity().getDescription() );
        params.put( "monthExecutionProject", project.getMonthExecution() );
        params.put( "objetiveProject", project.getObjetive() );
        params.put( "currencyProject", project.getCurrency().getDescription() );
        params.put("nameProject", project.getName());
        params.put("activities", project.getCalendars());
        params.put("resources", project.getResources());
        if (project.getCrowdfundingType() == CrowdfundingType.RECOMPENSA)
        {
            params.put("rewards", project.getRewards());
        }
        else
        {
            params.put("rewards", new ArrayList<Reward>());
        }
        if (project.getCrowdfundingType() == CrowdfundingType.INVERSION)
        {
            params.put( "companySharesProject", Double.valueOf(project.getCompanyShares()).doubleValue() );
            params.put( "minimumInvesment", Double.valueOf(project.getMinimumInvesment()).doubleValue() );
            params.put( "invesmentGoal", Double.valueOf(project.getInvesmentGoal()).doubleValue() );
        }
        else
        {
            params.put( "companySharesProject", 0 );
            params.put( "minimumInvesment", 0 );
            params.put( "invesmentGoal", 0 );
        }
    }

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  fillParametersCompany
     * Descripcion:             Metodo que llena los parametros de la compañía de la plantilla
     * @version                 1.0
     * @author                  martin
     * @since                   09/12/2016
     *
     * @param project datos del proyecto
     * @param params arreglo con los parametros
     * @return los parametros
     */
    private void fillParametersCompany(Project project, Map<String, Object> params) throws IOException
    {
        if (project.getPersonType() == PersonType.MORAL)
        {
            if (project.getCompany().getRfc() == null)
                params.put( "rfcCompany", " " );
            else
                params.put( "rfcCompany", project.getCompany().getRfc() );
            if (project.getCompany().getName() == null)
                params.put( "nameCompany", " " );
            else
                params.put( "nameCompany", project.getCompany().getName() );
        }
        else
        {
            params.put( "nameCompany", " " );
            params.put( "rfcCompany", " " );
        }
    }

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  fillParametersContact
     * Descripcion:             Metodo que llena los parametros de el usuario contacto, persona fisica
     *                          de la plantilla
     * @version                 1.0
     * @author                  martin
     * @since                   09/12/2016
     *
     * @param project datos del proyecto
     * @param params arreglo con los parametros
     * @return los parametros
     */
    private void fillParametersContact(Project project, Map<String, Object> params) throws IOException
    {
        if (project.getPersonType() == PersonType.FISICA)
        {
            if (project.getContact().getFullname() == null)
                params.put( "fullnameContact", " " );
            else
                params.put( "fullnameContact", project.getContact().getFullname() );
        }
        else
        {
            params.put( "fullnameContact", " " );
        }
    }

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  fillParameters
     * Descripcion:             Metodo que llena los parametros de la plantilla
     * @version                 1.0
     * @author                  buchyo
     * @since                   17/02/2016
     *
     * @param project datos del proyecto
     * @return los parametros
     */
    private Map<String, Object> fillParameters(Project project) throws IOException
    {
        final Map<String, Object> params = new HashMap<>();

        fillParametersProject(project, params);
        fillParametersCompany(project, params);
        fillParametersContact(project, params);
        fillParametersContactInformation(project, params);
        fillParametersInternetInfo(project, params);
        fillParametersLocation(project, params);

        return params;
    }

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  attachFile
     * Descripcion:             Adjunta los archivos de un proyecto
     * @version                 1.0
     * @author                  buchyo
     * @since                   17/02/2016
     *
     * @param listaDocumentos lista de archivos a adjuntar
     * @return constructor para enviar archivos y datos a través de un servidor HTTP
     */
    private Multipart attachFile(List<Document> listaDocumentos, Multipart multipart) throws MessagingException
    {
        MimeBodyPart messageBodyPart;
        messageBodyPart = new MimeBodyPart();

        if (listaDocumentos != null)
        {
            for (Document document : listaDocumentos)
            {
                messageBodyPart = new MimeBodyPart();
                ByteArrayDataSource file ;
                file = new ByteArrayDataSource(document.getFile(),"application/octet-stream");
                messageBodyPart.setDataHandler(new DataHandler(file));
                messageBodyPart.setFileName(document.getName());
                multipart.addBodyPart(messageBodyPart);

            }

        }

        return multipart;
    }

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  createReceiverslist
     * Descripcion:             Metodo que arma la lista de receptores de los correos
     * @version                 1.0
     * @author                  buchyo
     * @since                   11/02/2016
     *
     * @return
     * @throws javax.mail.internet.AddressException
     */
    private InternetAddress[] createReceiverslist() throws AddressException
    {
        String[] listaReceptores;
        listaReceptores = receivers.split( ";" );

        InternetAddress[] dest;
        dest = new InternetAddress[listaReceptores.length];

        for (int i = 0; i < dest.length; i++)
        {
            dest[i] = new InternetAddress( listaReceptores[i]) ;
        }

        return dest;
    }


    //endregion
}
