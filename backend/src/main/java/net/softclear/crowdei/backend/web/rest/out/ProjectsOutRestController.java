package net.softclear.crowdei.backend.web.rest.out;



import net.softclear.crowdei.backend.config.SettingsConfig;
import net.softclear.crowdei.backend.model.jpa.Investment;
import net.softclear.crowdei.backend.model.jpa.Project;
import net.softclear.crowdei.backend.model.types.PersonType;
import net.softclear.crowdei.backend.model.types.Status;
import net.softclear.crowdei.backend.service.ProjectService;
import net.softclear.crowdei.backend.util.CollectionUtils;
import net.softclear.crowdei.backend.util.DateUtils;
import net.softclear.crowdei.backend.web.rest.out.to.projectResponse.GetProjectsDetailResponse;
import net.softclear.crowdei.backend.web.rest.out.to.projectRequest.FindProjectsRequest;
import net.softclear.crowdei.backend.web.rest.out.to.projectResponse.GetProjectsXIdResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 *Sistema:                 Crowdei.
 *
 * Nombre:                  ProjectsRestController
 * Descripcion:             Clase que posee la definicion y la implementacion de los servicios
 * @version                 1.0
 * @author                  buchyo
 * @since                   14/01/2016
 *
 */
@RestController
public class ProjectsOutRestController
{
    //region Attributes

    @Autowired
    private ProjectService projectService;
    @Autowired
    private ModelMapper    modelMapper;
    @Autowired
    private DateUtils      dateUtils;
    @Autowired
    private SettingsConfig settingsConfig;

    //endregion

    //region Services


    /**
     * Nombre:                  findProjects.
     * Descripcion:             Metodo que consulta los proyectos dado un filtro
     * @version 1.0
     * @author guzmle
     * @since 12/4/2015
     *
     * @param request objeto con los datos del filtro para la consulta
     * @param page pagina de la informacion
     * @param size cantidad de registros
     * @return Objeto con la informacion de los proyectos en formato JSON
     */
    @RequestMapping( value = "find", method = RequestMethod.POST, produces = "application/json", consumes =
            "application/json" )
    public
    @ResponseBody
    GetProjectsXIdResponse findProjects( @RequestBody FindProjectsRequest request,
            @RequestParam( required = false ) Integer page, @RequestParam( required = false ) Integer size )
    {
        GetProjectsXIdResponse response;
        List<GetProjectsXIdResponse.Project> listResponse;
        List<Project> list;
        GetProjectsXIdResponse.Project obj;
        Pageable pageable = null;
        final int percentege = 100;

        response = new GetProjectsXIdResponse();
        listResponse = new ArrayList<>();

        if (page != null && size != null)
            pageable = new PageRequest(page, size);
        list = projectService.getProjects( request.getNameCompany(), request.getNameProject(),
                request.getIdEconomicActivity(), pageable );

        for (Project project : list)
        {
            obj = modelMapper.map(project, GetProjectsXIdResponse.Project.class);
            double obtained = 0;

            List<Investment> allInvestments = project.getInvestments();
            List<Investment> activeInvestment = allInvestments.stream().filter(p -> p.getStatus().equals(Status.ACTIVE)).collect(Collectors.toList());

            for(Investment investment : activeInvestment)
            {
                obtained += investment.getAmount();
            }
            obj.setObtained( cambiarFormato( obtained ) );

            List<Investment> distinctInvestment = activeInvestment.stream().filter(CollectionUtils.distinctByKey(p -> p.getPerson().getId())).collect(Collectors.toList());
            obj.setInvestors( distinctInvestment.size() );

            obj.setPercent( cambiarFormato( ( obtained / obj.getObjetive() ) * percentege ) );
            final int publicationDays = project.getPublicationDays();
            obj.setLeftDays( publicationDays - dateUtils.getDaysLeft( project.getDateRegister(), new Date(  ) ) );
            obj.setLimitDate(dateUtils.addDays(project.getDateRegister(), publicationDays));
            listResponse.add(obj);
        }

        response.setProjects( listResponse );
        return response;
    }

    /**
     * Nombre:                  getAllProjects.
     * Descripcion:             Servicio que obtiene los proyectos que esten activos
     * @version 1.0
     * @author guzmle
     * @since 12/4/2015
     *
     * @param page pagina de la informacion
     * @param size cantidad de registro por pagina
     * @return Objeto que posee la informacion en formato JSON
     */
    @RequestMapping( value = "findProjects", method = RequestMethod.GET, produces = "application/json" )
    public @ResponseBody GetProjectsXIdResponse getAllProjects( @RequestParam( required = false ) Boolean activeProjects,
                                                                @RequestParam( required = false ) Integer page,
                                                                @RequestParam( required = false ) Integer size )
    {
        Pageable pageable = null;
        GetProjectsXIdResponse.Project  obj;
        final int percentege = 100;
        List<Project> listProjects;
        GetProjectsXIdResponse response;
        List<GetProjectsXIdResponse.Project> projectsResponse;

        if ( page > 0 && size > 0 )
            pageable = new PageRequest( page, size );

        listProjects = projectService.getAllProjects( activeProjects, pageable );

        response = new GetProjectsXIdResponse();

        if ( listProjects != null )
        {
            projectsResponse = new ArrayList<>();
            for ( Project project : listProjects )
            {
                obj = modelMapper.map( project, GetProjectsXIdResponse.Project.class );
                double obtained = 0;

                List<Investment> allInvestments = project.getInvestments();
                List<Investment> activeInvestment = allInvestments.stream().filter(p -> p.getStatus().equals(Status.ACTIVE)).collect(Collectors.toList());
                List<GetProjectsXIdResponse.Investment> investments = new ArrayList<>();

                for(Investment investment : activeInvestment)
                {
                    obtained += investment.getAmount();

                    GetProjectsXIdResponse.Investment investmentObj;
                    investmentObj = modelMapper.map( investment, GetProjectsXIdResponse.Investment.class );
                    investments.add(investmentObj);
                }
                obj.setObtained( cambiarFormato(obtained) );

                List<Investment> distinctInvestment = activeInvestment.stream().filter(CollectionUtils.distinctByKey(p -> p.getPerson().getId())).collect(Collectors.toList());
                obj.setInvestors( distinctInvestment.size() );

                obj.setInvestments(investments);

                obj.setPercent( cambiarFormato( ( obtained / obj.getObjetive() ) * percentege ) );
                final int publicationDays = project.getPublicationDays();
                obj.setLeftDays( publicationDays - dateUtils.getDaysLeft( project.getDateRegister(), new Date(  ) ) );
                obj.setLimitDate(dateUtils.addDays(project.getDateRegister(), publicationDays));
                projectsResponse.add(obj);
            }

            response.setProjects(projectsResponse);
            response.setCount(new Long(projectsResponse.size()));
        }

        return response;
    }

    /**
     * Nombre:                  getProjectResponse.
     * Descripcion:             Metodo que obtiene la informacion de un proyecto
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/4/2015
     *
     * @param id identificador del proyecto
     * @return Objeto con la informacion del proyecto en formato JSON
     */
    @RequestMapping(value="/project/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    GetProjectsDetailResponse getProjectResponse(  @PathVariable long id)
    {
        Project project;
        Investment last = null;
        final int percentege = 100;
        double obtained = 0;
        GetProjectsDetailResponse response;
        project = projectService.getProject(id);
        response = modelMapper.map( project, GetProjectsDetailResponse.class );

        GetProjectsDetailResponse.Location location;
        GetProjectsDetailResponse.InternetInfo internetInfo;
        GetProjectsDetailResponse.ContactInfo contactInformation;
        if (project.getPersonType().equals(PersonType.FISICA)) {
            location = modelMapper.map(project.getContact().getLocation(), GetProjectsDetailResponse.Location.class);
            internetInfo = modelMapper.map(project.getContact().getInternetInformation(), GetProjectsDetailResponse.InternetInfo.class);
            contactInformation = modelMapper.map(project.getContact().getContactInformation(), GetProjectsDetailResponse.ContactInfo.class);
        }
        else
        {
            location = modelMapper.map(project.getCompany().getLocation(), GetProjectsDetailResponse.Location.class);
            internetInfo = modelMapper.map(project.getCompany().getInternetInformation(), GetProjectsDetailResponse.InternetInfo.class);
            contactInformation = modelMapper.map(project.getCompany().getContactInformation(), GetProjectsDetailResponse.ContactInfo.class);
        }
        response.setLocation(location);
        response.setInternetInformation(internetInfo);
        response.setContactInfo(contactInformation);

        List<Investment> allInvestments = project.getInvestments();
        List<Investment> activeInvestment = allInvestments.stream().filter(p -> p.getStatus().equals(Status.ACTIVE)).collect(Collectors.toList());

        for(Investment investment : activeInvestment)
        {
            obtained += investment.getAmount();
        }
        response.setObtained( cambiarFormato( obtained ) );

        List<Investment> distinctInvestment = activeInvestment.stream().filter(CollectionUtils.distinctByKey(p -> p.getPerson().getId())).collect(Collectors.toList());
        response.setInvestors( distinctInvestment.size() );

        response.setPercent( cambiarFormato( ( obtained / response.getObjetive() ) * percentege ) );
        final int publicationDays = project.getPublicationDays();
        response.setLeftDays( publicationDays - dateUtils.getDaysLeft( project.getDateRegister(), new Date(  ) ) );
        response.setLimitDate(dateUtils.addDays(project.getDateRegister(), publicationDays));

        if(project.getInvestments().size() > 0)
        {
            boolean stop = false;
            int index = project.getInvestments().size() - 1;
            while(!stop && index >= 0)
            {
                last = project.getInvestments().get( index );
                if(last.getStatus().equals( Status.ACTIVE ))
                   stop = true;
                else
                    index--;
            }
            if (last.getStatus().equals( Status.ACTIVE ))
            {
                response.setLastInvestment( last.getAmount() );
                response.setDateLastInvestment(dateUtils.getTimeLeft(last.getDate(), new Date(  ) ) );
            }
            else
            {
                response.setLastInvestment( ( double ) 0 );
                response.setDateLastInvestment(null);
            }
        }
        else
        {
            response.setLastInvestment( ( double ) 0 );
            response.setDateLastInvestment(null);
        }

        for (GetProjectsDetailResponse.Reward reward : response.getRewards())
        {
            List<Investment> investmentReward = activeInvestment.stream().filter(p -> p.getReward().getId() == reward.getId()).collect(Collectors.toList());
            reward.setQuantity(investmentReward.size());

            List<Investment> distinctInvestor = investmentReward.stream().filter(CollectionUtils.distinctByKey(p -> p.getPerson().getId())).collect(Collectors.toList());
            reward.setInvestors( distinctInvestor.size() );
        }

        return response;
    }
    //endregion

    //region metodos privados


    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  cambiarFormato
     * Descripcion:             Método que cambia el fotmarto ##,## a ##.##
     * @version                 1.0
     * @author                  buchyo
     * @since                   21/01/2016
     *
     * @param numero numero a convertir
     * @return nuevo numero
     */
    private double cambiarFormato(double numero)
    {
        DecimalFormatSymbols simbolo;
        simbolo = new DecimalFormatSymbols();
        simbolo.setDecimalSeparator('.');
        DecimalFormat df;
        df = new DecimalFormat("##########.##",simbolo);
        double retorno;

        retorno = Double.parseDouble(df.format(numero));

        return retorno;
    }

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  convertArrayByte
     * Descripcion:             Metodo que convierte un archivo en un arreglo de byt
     * @version                 1.0
     * @author                  buchyo
     * @since                   08/04/2016
     *
     * @param path ruta del archivo
     * @return arreglo de byte
     */
    private byte[] convertArrayByte(String path)
    {
        byte[] retorno;
        retorno = null;

        File fichero;
        fichero = new java.io.File(settingsConfig.pathAbsolute + path);

        FileInputStream ficheroStream;

        try
        {
            ficheroStream = new FileInputStream(fichero);
            retorno = new byte[(int)fichero.length()];
            ficheroStream.read(retorno);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }


        return retorno;
    }

    //endregion
}
