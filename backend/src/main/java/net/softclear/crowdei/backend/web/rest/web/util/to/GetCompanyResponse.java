package net.softclear.crowdei.backend.web.rest.web.util.to;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  GetCompanyResponse
 * Descripcion:             Clase que posee la definicion del JSON de respuesta del servicio que consulta las compañias
 * @version                 1.0
 * @author                  buchyo
 * @since                   11/03/2016
 *
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetCompanyResponse
{
    private Long count;
    private List<Company> companies;

    /**
     * Clase que posee la definicion de objeto json de company que recibe el servicio de crear proyecto.
     */
    @Data
    public static class Company
    {
        private Long                id;
        private String              rfc;
        private String              name;
        private Location            location;
        private ContactInformation  contactInformation;
        private InternetInformation internetInformation;
    }

    /**
     * Clase interna que posee la defincion para la entidad de location.
     */
    @Data
    public static class Location
    {
        private Long        id;
        private String      address;
        private String      municipality;
        private String      postalCode;
        private String      locationReference;
        private City        city;
    }

    /**
     * Clase interna que posee la defincion del JSON para la ciudad.
     */
    @Data
    public static class City
    {
        private Long   id;
        private String name;
        private State state;

    }


    /**
     * Clase interna que posee la defincion del JSON para el estado.
     */
    @Data
    public static class State
    {
        private Long   id;
        private String name;
        private Country country;

    }


    /**
     * Clase interna que posee la defincion del JSON para pais.
     */
    @Data
    public static class Country
    {
        private Long   id;
        private String name;

    }

    /**
     * Clase interna que posee la defincion para la entidad de contactInformation.
     */
    @Data
    public static class ContactInformation
    {
        private Long        id;
        private String      phoneOne;
        private String      phoneTwo;
        private String      mobile;
        private String      email;
    }

    /**
     * Clase interna que posee la defincion para la entidad de internetInformation.
     */
    @Data
    public static class InternetInformation
    {
        private Long id;
        private String webPage;
        private String linkedin;
        private String facebook;
        private String twitter;
    }
}
