package net.softclear.crowdei.backend.service.implementation;



import net.softclear.crowdei.backend.model.jpa.ExperienceLevel;
import net.softclear.crowdei.backend.repository.ExperienceRepository;
import net.softclear.crowdei.backend.service.ExperienceLevelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;



/**
 * Created by leonardo on 02/12/15.
 */
@Service
@Transactional
public class ExperienceLevelServiceImpl implements ExperienceLevelService
{
    //region Atributos

    private static Logger logger = LoggerFactory.getLogger( ExperienceLevelServiceImpl.class );
    @Autowired
    private ExperienceRepository repository;

    //endregion

    //region Implementacion


    @Override
    public List<ExperienceLevel> getAll()
    {
        logger.debug( "Entrando al metodo getAll" );
        List<ExperienceLevel> retorno;
        retorno = repository.findAll();
        logger.debug( "Saliendo del metodo getAll" );
        return retorno;
    }

    //endregion
}
