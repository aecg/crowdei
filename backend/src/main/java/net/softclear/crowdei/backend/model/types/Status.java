package net.softclear.crowdei.backend.model.types;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  Status
 * Descripcion:             Enumarado para los diferentes estado dentro de la aplicacion
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/11/2015
 *
 */
public enum Status
{
    /**
     * Estado activo.
     */
    ACTIVE,
    /**
     * Estado Inactivo.
     */
    INACTIVE,
    /**
     * Estado Eliminado.
     */
    DELETED,
    /**
     * Estado Finalizado proyecto.
     */
    ENDPUBLICATION
}
