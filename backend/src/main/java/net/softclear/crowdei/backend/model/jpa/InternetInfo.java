package net.softclear.crowdei.backend.model.jpa;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;

/**
 * Created by martin on 12/1/16.
 */
@Entity
@Table(name="internet_info")
@Setter
@Getter
public class InternetInfo
{

    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = true )
    private Long id;

    @Column( name = "web_page", nullable = true )
    private String webPage;

    @Column( name = "linkedin", nullable = true )
    private String linkedin;

    @Column( name = "facebook", nullable = true )
    private String facebook;

    @Column( name = "twitter", nullable = true )
    private String twitter;
}
