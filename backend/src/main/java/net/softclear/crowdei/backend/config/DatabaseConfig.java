package net.softclear.crowdei.backend.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  DatabaseConfig
 * Descripcion:             Clase de configuracion del acceso a datos
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories("net.softclear.crowdei.backend.repository")
public class DatabaseConfig
{
    //region Atributos

	@Value("${db.driverClassName}")
	private String dbDriverClassName;
    @Value("${db.url}")
    private String dbUrl;
    @Value("${db.urlTest}")
    private String dbUrlTest;
	@Value("${db.username}")
	private String dbUsername;
	@Value("${db.password}")
	private String dbPassword;

    //endregion

    //region Metodos


    /**
     * Nombre:                  dataSourceTest.
     * Descripcion:             Metodo que obtiene el data source cuando el perfil configurado es el de prueba
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     * @return Data source
     */
	@Bean
	@Profile("test")
	public DataSource dataSourceTest()
    {
        DriverManagerDataSource driverManagerDataSource;
        driverManagerDataSource = new DriverManagerDataSource();
        driverManagerDataSource.setDriverClassName(dbDriverClassName);
        driverManagerDataSource.setUrl(dbUrlTest);
        driverManagerDataSource.setUsername(dbUsername);
        driverManagerDataSource.setPassword(dbPassword);
        return driverManagerDataSource;
	}


    /**
     * Nombre:                  dataSource.
     * Descripcion:             Metodo que obtiene el data source cuando el perfil configurado es el de prod
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     * @return data source
     */
	@Bean
	@Profile("prod")
	public DriverManagerDataSource dataSource()
    {
        DriverManagerDataSource driverManagerDataSource;
        driverManagerDataSource = new DriverManagerDataSource();
		driverManagerDataSource.setDriverClassName(dbDriverClassName);
		driverManagerDataSource.setUrl(dbUrl);
		driverManagerDataSource.setUsername(dbUsername);
		driverManagerDataSource.setPassword(dbPassword);
		return driverManagerDataSource;
	}


    /**
     * Nombre:                  entityManagerFactory.
     * Descripcion:             Metodo que dadod el data source de la aplicacion obtiene el entity
     * manager de la aplicacion
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     * @param dataSource objeto con la informacion de la conexion
     * @return Fabrica para obtener el entity manager
     */
	@Bean
	@Autowired
	public EntityManagerFactory entityManagerFactory(DataSource dataSource)
    {
		HibernateJpaVendorAdapter vendorAdapter;
        vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setGenerateDdl(true);

		LocalContainerEntityManagerFactoryBean factory;
        factory = new LocalContainerEntityManagerFactoryBean();
		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setPackagesToScan("net.softclear.crowdei.backend.model.jpa");
		factory.setDataSource(dataSource);
		factory.afterPropertiesSet();

		return factory.getObject();
	}


    /**
     * Nombre:                  transactionManager.
     * Descripcion:             Metodo que manipula la transaccionabilidad de la aplicacion
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     * @param entityManagerFactory fabrica del entity manager
     * @return objeto con la informacion de la transacciones
     */
	@Bean
	@Autowired
	public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory)
    {
		JpaTransactionManager txManager;
        txManager = new JpaTransactionManager();
		txManager.setEntityManagerFactory(entityManagerFactory);
		return txManager;
	}

    //endregion
}
