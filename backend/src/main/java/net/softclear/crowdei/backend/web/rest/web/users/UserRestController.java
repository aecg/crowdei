package net.softclear.crowdei.backend.web.rest.web.users;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import net.softclear.crowdei.backend.config.SettingsConfig;
import net.softclear.crowdei.backend.exception.ServiceException;
import net.softclear.crowdei.backend.model.jpa.Authority;
import net.softclear.crowdei.backend.model.jpa.User;
import net.softclear.crowdei.backend.service.UserService;
import net.softclear.crowdei.backend.util.CollectionUtils;
import net.softclear.crowdei.backend.web.provider.UserProvider;
import net.softclear.crowdei.backend.web.rest.web.users.to.userRequest.UserModifyRequest;
import net.softclear.crowdei.backend.web.rest.web.users.to.userResponse.GetUserResponse;
import net.softclear.crowdei.backend.web.rest.web.users.to.userResponse.GetUsersResponse;
import net.softclear.crowdei.backend.web.rest.web.users.to.userResponse.UserLoginResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  UserRestController
 * Descripcion:             Clase que posee la definicion y la implementacion de los servicios que
 * involucran a la entidad usuario
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/4/2015
 *
 */
@RestController
@RequestMapping("/web/users*")
public class UserRestController
{
    //region Attributes

    @Autowired
    private ModelMapper  modelMapper;
    @Autowired
    private UserService  userService;
    @Autowired
    private UserProvider userProvider;
    @Autowired
    private SettingsConfig settingsConfig;

    //endregion

    //region Services


    /**
     * Nombre:                  getUser.
     * Descripcion:             Metodo que obtiene los datos del usuario
     * @version 1.0
     * @author guzmle
     * @since 12/4/2015
     *
     * @param id identificador del usuario
     * @return Datos del usuario en el formato JSON
     */
    @RequestMapping( value = "/{id}", method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    GetUserResponse getUser( @PathVariable String id )
    {
        GetUserResponse response;
        User obj;
        obj = userService.getUserByUsername( id );
        response = modelMapper.map( obj, GetUserResponse.class );
        return response;
    }


    /**
     * Nombre:                  register.
     * Descripcion:             Servicio que registra los datos de un usuario
     * @version 1.0
     * @author guzmle
     * @since 12/3/2015
     * @return Datos del usuario logeado
     */
    @RequestMapping( value = "/login", method = RequestMethod.GET, produces = "application/json" )
    public @ResponseBody UserLoginResponse getUserLogin()
    {
        UserLoginResponse response;
        response = new UserLoginResponse();
        response.setId( userProvider.getUser().getId() );
        response.setAuthorities( userProvider.getUser().getAuthoritiesToStrings() );
        response.setPerson( new UserLoginResponse.Person() );
        response.getPerson().setImage( userProvider.getUser().getPerson().getImage() );
        response.getPerson().setId( userProvider.getUser().getPerson().getId() );
        response.getPerson().setLastName( userProvider.getUser().getPerson().getLastName() );
        response.getPerson().setName( userProvider.getUser().getPerson().getName() );
        response.getPerson().setEmail( userProvider.getUser().getPerson().getEmail() );
        return response;
    }

    /**
     * Nombre:                  updateUser.
     * Descripcion:             Metodo que edita los datos de un usuario
     * @version 1.0
     * @author guzmle
     * @since 12/4/2015
     *
     * @param request datos del usuario en formato JSON
     */
    @RequestMapping( method = RequestMethod.PUT, consumes = "application/json" )
    @ResponseStatus( HttpStatus.OK )
    public void updateUser(  @RequestBody UserModifyRequest request )
    {
        try
        {
            User user;
            user = modelMapper.map(request, User.class);
            if (request.getPerson().getBytesPicture() != null)
            {
                loadFilesToAWS(request, user);
            }
            userService.updateUser(user);
        }
        catch (Exception e)
        {
            throw new ServiceException("Error saving user into database", e);
        }
    }

    /**
     * Nombre:                  loadFilesToAWS.
     * Descripcion:             Metodo que sube archivos al S3
     * @version                 1.0
     * @author                  martin
     * @since                   12/8/2016
     *
     * @param request objeto con los archivos a subir
     * @param user usuario a modificar
     * @return void
     */
    private void loadFilesToAWS(@RequestBody UserModifyRequest request, User user)
    {
        try
        {
            final AWSCredentials credentials =
                    new BasicAWSCredentials(settingsConfig.awsS3AccessKeyID, settingsConfig.awsS3SecretAccessKey);
            final AmazonS3 s3 = new AmazonS3Client(credentials);

            final String namePictureKey = loadUserPhotoToAWS(request, s3);
            user.getPerson().setImage(namePictureKey);
        }
        catch (Exception e)
        {
            //throw new ServiceException("Error loading files to AWS S3", e);
        }
    }

    /**
     * Nombre:                  loadProjectContactPhotoToAWS.
     * Descripcion:             Metodo que sube el archivo de imagen de un usuario al S3
     * @version                 1.0
     * @author                  martin
     * @since                   12/8/2016
     *
     * @param request objeto con el archivo a subir
     * @param s3 objeto para conexión al AWS S3
     * @return String nombre del archivo de imagen del contacto subido al S3
     */
    private String loadUserPhotoToAWS(@RequestBody UserModifyRequest request, AmazonS3 s3)
    {
        try
        {
            final String namePicture = request.getPerson().getNamePicture().replaceAll(" ","_");
            final String nameKey = request.getPerson().getId() + "_" + namePicture;
            final File filePicture = File.createTempFile(nameKey, request.getPerson().getExtensionPicture());
            filePicture.deleteOnExit();
            final OutputStream outPicture = new FileOutputStream(filePicture);
            outPicture.write(request.getPerson().getBytesPicture());
            outPicture.close();
            s3.putObject(new PutObjectRequest(settingsConfig.awsS3Bucket, nameKey, filePicture));
            return nameKey;
        }
        catch (Exception e)
        {
            //throw new ServiceException("Error loading files to AWS S3", e);
            return null;
        }
    }

    @RequestMapping( method = RequestMethod.GET, produces = "application/json" )
    public @ResponseBody GetUsersResponse getAllUsers(@RequestParam( required = false ) Boolean adminUsers,
                                                      @RequestParam( required = false ) Boolean activeUsers,
                                                     @RequestParam( required = false ) Integer page,
                                                     @RequestParam( required = false ) Integer size ) {

        Pageable pageable = null;
        GetUsersResponse.User  obj;
        List<User> listUsers;
        GetUsersResponse response;
        List<GetUsersResponse.User> usersResponse;

        if ( page > 0 && size > 0 )
            pageable = new PageRequest( page, size );

        listUsers = userService.getUsers( activeUsers, pageable );

        response = new GetUsersResponse();

        if ( listUsers != null )
        {
            usersResponse = new ArrayList<>();
            for ( User user : listUsers )
            {
                if (adminUsers) {
                    obj = modelMapper.map(user, GetUsersResponse.User.class);
                    usersResponse.add(obj);
                }
                else {
                    if (!containsRole(user.getAuthorities(), Authority.Role.ROLE_SUPER_ADMIN)) {
                        obj = modelMapper.map(user, GetUsersResponse.User.class);
                        usersResponse.add(obj);
                    }
                }
            }

            response.setUsers(usersResponse);
            response.setCount(new Long(usersResponse.size()));
        }

        return response;
    }

    @RequestMapping( value = "/activate/{id}", method = RequestMethod.POST )
    @ResponseStatus( HttpStatus.OK )
    public void activateUser( @PathVariable long id )
    {
        try
        {
            userService.activateUser( id );
        }
        catch (Exception e)
        {
            throw new ServiceException("Error activating user into database", e);
        }
    }

    @RequestMapping( value = "/deactivate/{id}", method = RequestMethod.POST )
    @ResponseStatus( HttpStatus.OK )
    public void deactivateUser( @PathVariable long id )
    {
        try
        {
            userService.deactivateUser( id );
        }
        catch (Exception e)
        {
            throw new ServiceException("Error deactivating user into database", e);
        }
    }

    //endregion

    public boolean containsRole(final List<Authority> list, final Authority.Role name){
        return list.stream().map(Authority::getAuthority).filter(name::equals).findFirst().isPresent();
    }

}
