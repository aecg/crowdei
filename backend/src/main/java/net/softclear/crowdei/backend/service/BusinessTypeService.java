package net.softclear.crowdei.backend.service;

import net.softclear.crowdei.backend.model.jpa.BusinessType;

import java.util.List;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  ExperienceLevelService
 * Descripcion:             Contrato para todos los servicios que involucran la entidad denivel de experiencia
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
public interface BusinessTypeService
{
    /**
     * Nombre:                  getAll.
     * Descripcion:             Metodo que obtiene todos los tipos de negocios
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/21/2015
     *
     * @return todos los tipos de negocios que se encuentran almacenados en la base de datos
     */
	List<BusinessType> getAll();
}
