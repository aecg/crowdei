package net.softclear.crowdei.backend.repository;

import net.softclear.crowdei.backend.model.jpa.Project;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  ContactRepositoryCustom
 * Descripcion:             Contrato para todas las operaciones personalizadas en la base de datos que
 * involucran la entidad contact
 * @version                 1.0
 * @author                  buchyo
 * @since                   14/03/2016
 *
 */
public interface ContactRepositoryCustom
{

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  findAllXIdCompany
     * Descripcion:             Metodo que consulta todos los contactos que pertenesca a una compañia en
     * particular dado un parametro de busqueda
     * @version                 1.0
     * @author                  buchyo
     * @since                   14/03/2016
     *
     * @param idCompany identificador de la compañia
     * @param nameContact nombre de la persona contact
     * @param pageable si es paginable
     * @return lista de proyectos con sus contactos
     */
    List<Project> findAllXIdCompany(Long idCompany, String nameContact, Pageable pageable);
}
