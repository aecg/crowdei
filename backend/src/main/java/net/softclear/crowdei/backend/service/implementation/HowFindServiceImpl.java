package net.softclear.crowdei.backend.service.implementation;



import net.softclear.crowdei.backend.model.jpa.HowFind;
import net.softclear.crowdei.backend.repository.HowFindRepository;
import net.softclear.crowdei.backend.service.HowFindService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;



/**
 * Created by leonardo on 02/12/15.
 */
@Service
@Transactional
public class HowFindServiceImpl implements HowFindService
{
    //region Atributos

    private static Logger logger = LoggerFactory.getLogger( HowFindServiceImpl.class );
    @Autowired
    private HowFindRepository repository;

    //endregion

    //region Implementacion


    @Override
    public List<HowFind> getAll()
    {
        logger.debug( "Entrando al metodo getAll" );

        List<HowFind> retorno;
        retorno = repository.findAll();

        logger.debug( "Saliendo del metodo getAll" );
        return retorno;
    }

    //endregion
}
