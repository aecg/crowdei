package net.softclear.crowdei.backend.web.rest.out;

import net.softclear.crowdei.backend.model.jpa.EconomicActivity;
import net.softclear.crowdei.backend.service.EconomicActivityService;
import net.softclear.crowdei.backend.web.rest.web.util.to.GetObjectMasterResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  UtilRestController
 * Descripcion:             Clase que posee la definicion y la implementacion de los servicios que involucran a
 * la entidad usuario
 * @version                 1.0
 * @author                  buchyo
 * @since                   17/03/2016
 *
 */
@RestController
public class UtilRestController
{
    //region Attributes

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private EconomicActivityService service;

    //endregion

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  getAll
     * Descripcion:             Metodo que obtiene todos las actividades economicas
     * @version                 1.0
     * @author                  buchyo
     * @since                   15/03/2016
     *
     * @return lista de las actividades economicas
     */
    @RequestMapping( value = "/economic-activity", method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    GetObjectMasterResponse getAll()
    {
        List<EconomicActivity> list;
        GetObjectMasterResponse response;
        List<GetObjectMasterResponse.Item> listResponse;
        GetObjectMasterResponse.Item item;

        list = service.getAll();
        response = new GetObjectMasterResponse();

        if ( list != null )
        {
            listResponse = new ArrayList<>();
            for ( EconomicActivity obj : list )
            {
                item = modelMapper.map( obj, GetObjectMasterResponse.Item.class );
                listResponse.add( item );
            }

            response.setList( listResponse );
        }
        return response;
    }
}
