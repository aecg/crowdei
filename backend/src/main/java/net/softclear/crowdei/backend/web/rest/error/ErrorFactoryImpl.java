package net.softclear.crowdei.backend.web.rest.error;

import org.springframework.stereotype.Component;

import net.softclear.crowdei.backend.exception.ServiceException;
import net.softclear.crowdei.backend.web.rest.error.to.ErrorResponse;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  ErrorFactoryImpl
 * Descripcion:             Clase que transforma una excepcion en un error entendible
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@Component
public class ErrorFactoryImpl implements ErrorFactory
{

	@Override
	public ErrorResponse exceptionFrom(Exception exception)
    {
        String message;
		ErrorCode code = ErrorCode.GENERIC_ERROR;
        message = exception.getMessage();
		
		if (exception instanceof ServiceException)
			code = ErrorCode.SERVICE_ERROR;
		
		return new ErrorResponse(code.getValue(), message);
	}

}
