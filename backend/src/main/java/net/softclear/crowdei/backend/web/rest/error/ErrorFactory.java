package net.softclear.crowdei.backend.web.rest.error;

import net.softclear.crowdei.backend.web.rest.error.to.ErrorResponse;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  ErrorFactory
 * Descripcion:             Interfaz para la fabrica de errores
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
public interface ErrorFactory
{
    /**
     * Nombre:                  exceptionFrom.
     * Descripcion:             Metodo que define el metodo que obtiene la excepcion
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/22/2015
     *
     * @param exception excepcion a procesar
     * @return excepcion procesada
     */
	ErrorResponse exceptionFrom(Exception exception);
}
