package net.softclear.crowdei.backend.service.implementation;


import net.softclear.crowdei.backend.model.jpa.ImpactSector;
import net.softclear.crowdei.backend.repository.ImpactSectorRepository;
import net.softclear.crowdei.backend.service.ImpactSectorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Sort;

import java.util.List;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  ImpactSectorServiceImpl
 * Descripcion:             Clase que implementa el contrato del servicio
 * @version                 1.0
 * @author                  buchyo
 * @since                   15/03/2016
 *
 */
@Service
@Transactional
public class ImpactSectorServiceImpl implements ImpactSectorService
{
    //region Atributos

    private static Logger logger = LoggerFactory.getLogger(ExperienceLevelServiceImpl.class);
    @Autowired
    private ImpactSectorRepository repository;

    //endregion

    @Override
    public List<ImpactSector> getAll()
    {
        logger.debug( "Entrando al metodo getAll" );
        List<ImpactSector> retorno;
        retorno = repository.findAll(new Sort(Sort.Direction.ASC, "description"));
        logger.debug( "Saliendo del metodo getAll" );
        return retorno;
    }
}
