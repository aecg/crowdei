package net.softclear.crowdei.backend.util;



import lombok.Getter;
import lombok.Setter;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  ConektaUtils
 * Descripcion:             Clase que permite el acceso a la informacion de mercado pago
 * @version                 1.0
 * @author                  camposer
 * @since                   12/15/2016
 *
 */
@Setter
@Getter
public class ConektaUtils
{
    private String tokenId;
    private String description;
    private Double amount;
    private String userName;
    private String email;
    private Long userId;
    private String projectId;
    private Long rewardId;
}
