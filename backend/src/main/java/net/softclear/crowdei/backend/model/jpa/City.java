package net.softclear.crowdei.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  City
 * Descripcion:             Clase que emula la entidad city dentro del sistema
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/9/2015
 *
 */
@Entity
@Table(name="city")
@Setter
@Getter
public class City
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;

    @Column( name = "name", nullable = false )
    private String name;

    @ManyToOne()
    @JoinColumn( name = "state_id" )
    private State state;
}
