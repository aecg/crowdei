package net.softclear.crowdei.backend.web.rest.web.util;

import net.softclear.crowdei.backend.model.jpa.ImpactSector;
import net.softclear.crowdei.backend.model.types.Priority;
import net.softclear.crowdei.backend.service.ImpactSectorService;
import net.softclear.crowdei.backend.web.rest.web.util.to.GetObjectMasterResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  PriorityRestController
 * Descripcion:             Clase que posee la definicion y la implementacion de los servicios que involucran a
 * la entidad usuario
 * @version                 1.0
 * @author                  buchyo
 * @since                   15/03/2016
 *
 */
@RestController
@RequestMapping("/web/utils*")
public class PriorityRestController
{
    @RequestMapping( value = "/priority", method = RequestMethod.GET, produces = "application/json" )
    public @ResponseBody List<Priority> getAll()
    {
        List<Priority> response = new ArrayList<Priority>(Arrays.asList(Priority.values()));
        return response;
    }
}
