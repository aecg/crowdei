package net.softclear.crowdei.backend.model.types;

/**
 * Created by martin on 12/1/16.
 */
public enum CrowdfundingType
{
    /**
     * Recompensa / Reward.
     */
    RECOMPENSA,
    /**
     * Inversion / Invesment / Equity.
     */
    INVERSION
}
