package net.softclear.crowdei.backend.model.jpa;

import lombok.Getter;
import lombok.Setter;
import net.softclear.crowdei.backend.model.types.CrowdfundingType;
import net.softclear.crowdei.backend.model.types.PersonType;
import net.softclear.crowdei.backend.model.types.Priority;
import net.softclear.crowdei.backend.model.types.Status;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  Project
 * Descripcion:             Clase para modelar la entidad proyecto
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@Entity
@Table(name="project")
@Setter
@Getter
public class Project
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = true )
    private Long id;

    @Column( name = "name", nullable = true )
    private String name;

    @Column( name = "brief_description", nullable = true )
    private String briefDescription;

    @Column( name = "description", nullable = true, columnDefinition="longblob" )
    private String description;

    @Column( name = "execution_place", nullable = true )
    private String executionPlace;

    @Column( name = "date_register", nullable = true )
    @Temporal( TemporalType.TIMESTAMP )
    private Date dateRegister;

    //Monto total requerido
    @Column( name = "objetive", nullable = true )
    private double objetive;

    @Column( name = "image", nullable = true )
    private String image;

    @Column( name = "video", nullable = true )
    private String video;

    @Column( name = "canvas_model", nullable = true )
    private String canvasModel;

    @Column( name = "general_objetive", nullable = true )
    private String generalObjetive;

    @Column( name = "specific_objetives", nullable = true )
    private String specificObjetives;

    @Column( name = "months_execution", nullable = true )
    private int monthExecution;

    @Column( name = "publication_days", nullable = true )
    private int publicationDays;

    @Column( name = "company_shares", nullable = true )
    private String companyShares;

    @Column( name = "minimum_invesment", nullable = true )
    private String minimumInvesment;

    @Column( name = "invesment_goal", nullable = true )
    private String invesmentGoal;

    @Enumerated( EnumType.STRING )
    @Column( name = "priority_type", nullable = true )
    private Priority priorityType;

    @Column( name = "position", nullable = true )
    private int position;

    @Enumerated( EnumType.STRING )
    @Column( nullable = false )
    private Status status;

    @Enumerated( EnumType.STRING )
    @Column( name = "person_type", nullable = false )
    private PersonType personType;

    @Enumerated( EnumType.STRING )
    @Column( name = "crowdfunding_type", nullable = false )
    private CrowdfundingType crowdfundingType;

    @ManyToOne(cascade= CascadeType.ALL)
    @JoinColumn( name = "company_id" )
    private Company company;

    //Antes era idPerson
    @ManyToOne()
    @JoinColumn( name = "person_id" )
    private Person person;

    @ManyToOne()
    @JoinColumn( name = "currency_id" )
    private Currency currency;

    @ManyToOne()
    @JoinColumn( name = "impact_sector_id" )
    private ImpactSector impactSector;

    @ManyToOne()
    @JoinColumn( name = "economic_activity_id" )
    private EconomicActivity economicActivity;

    @ManyToOne(cascade= CascadeType.ALL)
    @JoinColumn( name = "contact_id" )
    private Contact contact;

    @OneToMany( mappedBy = "project", cascade = CascadeType.ALL )
    private List<Document> documents;

    @OneToMany( mappedBy = "project", cascade = CascadeType.ALL )
    private List<Calendar> calendars;

    @OneToMany( mappedBy = "project", cascade = CascadeType.ALL )
    private List<Resource> resources;

    @OneToMany( mappedBy = "project" )
    private List<Investment> investments;

    @OneToMany( mappedBy = "project", cascade = CascadeType.ALL )
    private List<Reward> rewards;
}
