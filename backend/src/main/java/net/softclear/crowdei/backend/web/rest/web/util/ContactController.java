package net.softclear.crowdei.backend.web.rest.web.util;

import net.softclear.crowdei.backend.model.jpa.Project;
import net.softclear.crowdei.backend.service.ContactService;
import net.softclear.crowdei.backend.web.rest.web.util.to.FilterRequest;
import net.softclear.crowdei.backend.web.rest.web.util.to.GetContactResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  ContactController
 * Descripcion:             Clase que posee la definicion y la implementacion de los servicios que
 * involucran a la entidad contact
 * @version                 1.0
 * @author                  buchyo
 * @since                   14/03/2016
 *
 */
@RestController
@RequestMapping("/web/utils*")
public class ContactController
{
    //region Attributes

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ContactService contactService;

    //endregion

    //region Metodos

    /**
     * Sistema:                 Crowdei.
     *                                                              
     * Nombre:                  ContactController
     * Descripcion:             Método que trae todos los contactos de una compañia
     * dado un parametro de busqueda
     * @version                 1.0
     * @author                  buchyo
     * @since                   14/03/2016
     *
     * @param request id es el identificador de la compañia, searchParameter es el nombre del contacto
     * @param page pagina de la informacion
     * @param size cantidad de registros
     * @return lista de contactos
     */
    @RequestMapping( value = "/contact", method = RequestMethod.POST, produces = "application/json", consumes =
            "application/json" )
    public @ResponseBody
    GetContactResponse
    getContactXIdCompany(@RequestBody FilterRequest request, @RequestParam( required = false )
    Integer page, @RequestParam( required = false ) Integer size)
    {
        Pageable pageable = null;
        GetContactResponse.Contact  obj;

        List<Project> listProjects;
        GetContactResponse response;
        List<GetContactResponse.Contact> contactsResponse;

        if ( page != null && size != null )
            pageable = new PageRequest( page, size );

        listProjects = contactService.getCompanyXIdCompany(request.getId(), request.getSearchParameter(), pageable);

        response = new GetContactResponse();

        if ( listProjects != null )
        {
            contactsResponse = new ArrayList<>();
            for ( Project project : listProjects )
            {
                obj = modelMapper.map( project.getContact(), GetContactResponse.Contact.class );
                contactsResponse.add(obj);
            }

            response.setContacts(removeRepeated(contactsResponse));
        }

        return response;
    }

    //endregion

    //region Metodos privados
    private List<GetContactResponse.Contact> removeRepeated(List<GetContactResponse.Contact> listContacts)
    {
        List<GetContactResponse.Contact> retorno;
        retorno = new ArrayList<>();

        for(GetContactResponse.Contact contact : listContacts)
        {
            int i = 0;
            for ( GetContactResponse.Contact contactInt : retorno )
                if ( contact.getId() == contactInt.getId() )
                    i++;

            if ( i==0 )
                retorno.add( contact );
        }

        return retorno;
    }
    //endregion

}
