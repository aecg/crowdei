package net.softclear.crowdei.backend.config;



import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactoryBean;

import java.util.Properties;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  MailConfig
 * Descripcion:             Clase que se encarga de configurar la aplicacion para el envio de correo
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@Configuration
public class MailConfig
{

    @Value( "${email.host}" )
    private String host;

    @Value( "${email.from}" )
    private String from;

    @Value( "${email.port}" )
    private String port;

    @Value( "${email.subject}" )
    private String subject;

    @Value( "${email.username}" )
    private String username;

    @Value( "${email.password}" )
    private String password;


    /**
     * Nombre:                  javaMailService.
     * Descripcion:             Metodo que asigna los valores del servidor smtp para el envio de correo
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/22/2015
     *
     * @return objeto con la informacion del envio de correo
     */
    @Bean
    public JavaMailSender javaMailService()
    {
        final JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
        javaMailSender.setPassword( password );
        javaMailSender.setUsername( username );
        javaMailSender.setPort( Integer.parseInt( port ) );
        javaMailSender.setHost( host );

        javaMailSender.setJavaMailProperties( getMailProperties() );
        return javaMailSender;
    }


    /**
     * Nombre:                  freemarkerConfig.
     * Descripcion:             Metodo que inicializa el bean de freemarker
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/14/2015
     *
     * @return Returna la configuracion
     */
    @Bean(name = "freemarkerConfiguration")
    public FreeMarkerConfigurationFactoryBean freemarkerConfig()
    {
        final FreeMarkerConfigurationFactoryBean factory = new FreeMarkerConfigurationFactoryBean();
        factory.setTemplateLoaderPath( "classpath:templates" );
        factory.setDefaultEncoding( "UTF-8" );
        return factory;
    }


    /**
     * Nombre:                  getMailProperties.
     * Descripcion:             Metodo que obtiene las propiedades para enviar correo
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/14/2015
     *
     * @return propiedades para enviar correo
     */
    private Properties getMailProperties()
    {
        final Properties properties = new Properties();
        properties.setProperty("mail.transport.protocol", "smtp");
        properties.setProperty("mail.smtp.auth", "true");
        properties.setProperty("mail.smtp.starttls.enable", "true");
        properties.setProperty("mail.debug", "true");
        return properties;
    }


    /**
     * Nombre:                  simpleMailMessage.
     * Descripcion:             Metodo que arma de una vez in mensaje para
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/14/2015
     *
     * @return Mensje armado
     */
    @Bean
    public SimpleMailMessage simpleMailMessage()
    {
        final SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom(from);
        simpleMailMessage.setSubject(subject);
        return simpleMailMessage;
    }
}
