package net.softclear.crowdei.backend.web.rest.web.util;

import net.softclear.crowdei.backend.model.jpa.BusinessType;
import net.softclear.crowdei.backend.service.BusinessTypeService;
import net.softclear.crowdei.backend.web.rest.web.util.to.GetObjectMasterResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  UserRestController
 * Descripcion:             Clase que posee la definicion y la implementacion de los servicios que involucran a
 * la entidad usuario
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/4/2015
 *
 */
@RestController
@RequestMapping("/web/utils*")
public class BusinessTypeRestController
{
    //region Attributes

    /**
     * Objeto para mapear las respuesta.
     */
    @Autowired
    private ModelMapper modelMapper;

    /**
     * Objeto que posee los servicios para la entidad Business Tyoe.
     */
    @Autowired
    private BusinessTypeService service;

    //endregion

    //region Services


    /**
     * Nombre:                  getAllCountries.
     * Descripcion:             Metodo que obtiene todos los paises almacenados en la base de datos
     * @version 1.0
     * @author guzmle
     * @since 12/9/2015
     *
     * @return lista de paises
     */
    @RequestMapping( value = "/business-types", method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    GetObjectMasterResponse getAll()
    {
        List<BusinessType> list;
        GetObjectMasterResponse response;
        List<GetObjectMasterResponse.Item> listResponse;
        GetObjectMasterResponse.Item item;

        list = service.getAll();
        response = new GetObjectMasterResponse();

        if ( list != null )
        {
            listResponse = new ArrayList<>();
            for ( BusinessType obj : list )
            {
                item = modelMapper.map( obj, GetObjectMasterResponse.Item.class );
                listResponse.add( item );
            }


            response.setList( listResponse );
        }
        return response;
    }


    //endregion
}
