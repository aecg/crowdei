package net.softclear.crowdei.backend.repository;



import net.softclear.crowdei.backend.model.jpa.City;
import net.softclear.crowdei.backend.model.jpa.State;

import java.util.List;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  CityRepositoryCustom
 * Descripcion:             Contrato para el repositorio sobre la entidad ciudad
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/9/2015
 *
 */
public interface CityRepositoryCustom
{
    /**
     * Nombre:                  findByState.
     * Descripcion:             Metodo que consulta las ciudades de un estado
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/9/2015
     *
     * @param state datos del estado
     * @return lista de ciudades
     */
	List<City> findByState( State state );
}
