package net.softclear.crowdei.backend.web.rest.web.users.to.userRequest;



import lombok.Data;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  UserRecoverRequest
 * Descripcion:             Clase que posee la definicion del JSON para la peticion de la recuperacion del usuario
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@Data
public class UserRecoverRequest
{
    private String email;
}
