package net.softclear.crowdei.backend.service;



import net.softclear.crowdei.backend.model.jpa.Investment;
import net.softclear.crowdei.backend.util.ConektaUtils;
import net.softclear.crowdei.backend.util.MercadoPagoUtils;
import net.softclear.crowdei.backend.util.TransfererUtils;
import org.springframework.data.domain.Pageable;

import java.util.List;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  InvestmentService
 * Descripcion:             Contrato para todos los servicios que involucran inversiones
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/28/2015
 *
 */
public interface InvestmentService
{

    /**
     * Nombre:                  paymentProcessingPaypal.
     * Descripcion:             Metodo que procesa el pago de paypal
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/28/2015
     *
     * @param idUser identificador del usuario que realiza el pago
     * @param idProject identificador del proyecto
     * @param data informacion del pago
     */
    void paymentProcessingPaypal( long idUser, long idProject, long idReward, String data );


    /**
     * Nombre:                  paymentProcessingMercadoPago.
     * Descripcion:             Metodo que procesa el pago de paypal
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/28/2015
     *
     * @param idUser identificador del usuario que realiza el pago
     * @param idProject identificador del proyecto
     * @param data informacion del pago
     */
    void paymentProcessingMercadoPago( long idUser, long idProject, MercadoPagoUtils data );

    /**
     * Nombre:                  paymentProcessingConekta
     * Descripcion:             Metodo que procesa el pago de conekta
     * @version                 1.0
     * @author                  camposer
     * @since                   12/14/2016
     *
     * @param idUser identificador del usuario que realiza el pago
     * @param idProject identificador del proyecto
     * @param data informacion del pago
     */
    void paymentProcessingConekta( long idUser, long idProject, ConektaUtils data );


    /**
     * Nombre:                  paymentProcessingTransferencia.
     * Descripcion:             Metodo que procesa el pago de una transferencia
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/29/2015
     *@param idUser identificador del usuario que realiza el pago
     * @param idProject identificador del proyecto
     * @param data informacion del pago
     *
     * @return la transferencia registrada
     */
    Investment paymentProcessingTransferencia( long idUser, long idProject, TransfererUtils data );


    /**
     * Nombre:                  getMercadoPagoUrl.
     * Descripcion:             Metodo que obtiene la url que se requiere para realizar el pago por mercadoPago
     * @version                 1.0
     * @author                  guzmle
     * @since                   1/4/2016
     * @param data informacion del pago
     * @return Url del pago
     */
    String getMercadoPagoUrl( MercadoPagoUtils data );

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  getInvestmentsXIdProjectXIdUser
     * Descripcion:             Método que consulta todas las invesiones dado un proyecto y un usuario en partiular
     * @version                 1.0
     * @author                  buchyo
     * @since                   21/01/2016
     *
     * @param idUser identificador del usuario
     * @param idProject identificador del proyecto
     * @return lista de inversion
     */
    List<Investment> getInvestmentsXIdProjectXIdUser (Long idUser, Long idProject, Pageable pageable);

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  sendMailInvestmentMyProject
     * Descripcion:             Método que envia un correo a la persona contacto cuando alguien invierta en uno
     * de los proyectos que esta a cargo
     * @version                 1.0
     * @author                  buchyo
     * @since                   05/04/2016
     *
     * @param investment la inversion
     */
    void sendMailInvestmentMyProject(Investment investment);

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  findAllCompletPublication
     * Descripcion:             Método que trae todas las inversiones en el cual su proyecto
     * asociado haya terminado su tiempo de publicación
     * @version                 1.0
     * @author                  buchyo
     * @since                   06/04/2016
     *
     * @return lista de inversiones
     */
    List<Investment> findAllCompletPublication ();

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  sendMailCompletPublication
     * Descripcion:             Metodo que envía un correo a todas las personas que inviertieron en un proyecto
     * y este haya finalizado
     * @version                 1.0
     * @author                  buchyo
     * @since                   07/04/2016
     *
     */
    void sendMailCompletPublication(long idProject, Double totalAmountInvested, Double percent, String receptor);

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  sendMailInvestmentTransferMyProject
     * Descripcion:             Método que envia un correo a la persona contacto, estrategias integrales y la
     * persona que hizo la transferencia
     * @version                 1.0
     * @author                  buchyo
     * @since                   13/04/2016
     *
     * @param idTransfer
     */
    void sendMailInvestmentTransferMyProject(Long idTransfer);

    List<Investment> getInvestmentsXIdProject(Long idProject, Pageable pageable);

    void activateTransfer(Long id);
}
