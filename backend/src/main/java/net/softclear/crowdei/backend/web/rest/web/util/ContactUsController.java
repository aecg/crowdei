package net.softclear.crowdei.backend.web.rest.web.util;



import net.softclear.crowdei.backend.service.ContactUsService;
import net.softclear.crowdei.backend.web.rest.web.util.to.ContactUsCompanyRequest;
import net.softclear.crowdei.backend.web.rest.web.util.to.ContactUsRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;



/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  ContactUsController
 * Descripcion:             Servicio para todas las operaciones del contactenos
 * @version                 1.0
 * @author                  buchyo
 * @since                   11/02/2016
 *
 */
@Controller
public class ContactUsController
{
    @Autowired
    private ContactUsService contactUsService;

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  contactUs
     * Descripcion:             Metodo que envia un correo con un mensaje de la seccion de contactenos
     * @version                 1.0
     * @author                  buchyo
     * @since                   11/02/2016
     *
     * @param request
     */
    @RequestMapping( value = "/contactUs", method = RequestMethod.POST, produces = "application/json", consumes =
            "application/json" )
    @ResponseStatus( HttpStatus.OK )
    public void contactUs( @RequestBody ContactUsRequest request )
    {
        //contactUsService.sendMail( request.getName(), request.getLastName(), request.getEmail(), request
        //        .getPhone(), request.getMessage() );
        contactUsService.sendMail( request.getName(), request.getEmail(), request.getMessage() );
    }

    /**
     * Sistema:                 Crowdei.
     *                                                              
     * Nombre:                  ContactUsContactProject
     * Descripcion:             Método que envia un correo de contactenos a la persona
     * contacto de un proyecto
     * @version                 1.0
     * @author                  buchyo
     * @since                   29/03/2016
     *
     * @param request
     */
    @RequestMapping( value = "/contactUsContactProject", method = RequestMethod.POST, produces = "application/json",
            consumes = "application/json" )
    @ResponseStatus( HttpStatus.OK )
    public void contactUsContactProject( @RequestBody ContactUsCompanyRequest request )
    {
        contactUsService.sendMailContactProject(request.getIdProject(), request.getName(),
                request.getLastName(), request.getEmail(), request.getPhone(), request.getMessage());
    }
}
