package net.softclear.crowdei.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;



/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  ContactInformation
 * Descripcion:             Clase que modela la entidad ContactInformation
 * @version                 1.0
 * @author                  buchyo
 * @since                   03/03/2016
 *
 */
@Entity
@Table(name="contact_information")
@Setter
@Getter
public class ContactInformation
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO)
    @Column( unique = true, nullable = true)
    private Long id;

    @Column( name = "phone_one" , nullable = true)
    private String phoneOne;

    @Column( name = "phone_two", nullable = true)
    private String phoneTwo;

    @Column( name = "mobile", nullable = true)
    private String mobile;

    @Column( name = "email", nullable = true)
    private String email;
}
