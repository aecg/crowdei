package net.softclear.crowdei.backend.service.implementation;



import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import net.softclear.crowdei.backend.config.MessagesConfig;
import net.softclear.crowdei.backend.exception.ServiceException;
import net.softclear.crowdei.backend.model.jpa.Project;
import net.softclear.crowdei.backend.repository.ProjectRepository;
import net.softclear.crowdei.backend.service.ContactUsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.NoResultException;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;



/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  ContactUsServiceImpl
 * Descripcion:             Clase que implementa el contrato del servicio
 * @version                 1.0
 * @author                  buchyo
 * @since                   11/02/2016
 *
 */
@Service
@Transactional
public class ContactUsServiceImpl implements ContactUsService
{
    private static Logger logger = LoggerFactory.getLogger( UserServiceImpl.class );
    @Autowired
    private JavaMailSender mailSender;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private MessagesConfig messagesConfig;
    @Autowired
    private Configuration  freemarkerConfiguration;
    @Value( "${email.receivers}" )
    private String         receivers;


    @Override
    public void sendMail( String nombre, String apellido, String correo, String telefono, String mensaje )
    {
        logger.debug( "Entrando en el metodo retrieveUser" );
        try
        {
            final Template template = freemarkerConfiguration.getTemplate( "mailContactUs.ftl" );
            final Map<String, Object> params = new HashMap<>();

            StringWriter output;
            MimeMessage msg;
            output = new StringWriter();

            params.put( "nombre", nombre );
            params.put( "apellido", apellido );
            params.put( "correo", correo );
            params.put( "telefono", telefono );
            params.put( "mensaje", mensaje );
            template.process( params, output );


            msg = mailSender.createMimeMessage();
            msg.setRecipients( Message.RecipientType.TO, createReceiverslist() );
            msg.setSubject("Nuevo mensaje Contáctenos CrowdEI");
            msg.setText(output.toString(), "utf-8", "html");


            mailSender.send( msg );

        }
        catch ( NoResultException | TemplateException e )
        {
            logger.error( messagesConfig.userNotFound, e );
            throw new ServiceException( messagesConfig.userNotFound );
        }
        catch ( IOException | MessagingException e )
        {
            logger.error(messagesConfig.messagingError, e);
            throw new ServiceException( messagesConfig.messagingError );
        }
        logger.debug( "Saliendo del metodo retrieveUser" );

    }

    @Override
    public void sendMail(String nombre, String correo, String mensaje)
    {
        sendMail(nombre, "", correo, "", mensaje);
    }

    @Override
    public void sendMailContactProject( Long idProyecto,String nombre, String apellido, String correo, String telefono,
                                        String mensaje )
    {
        logger.debug( "Entrando en el metodo retrieveUser" );
        try
        {
            final Template template = freemarkerConfiguration.getTemplate( "mailContactUsProject.ftl" );
            final Map<String, Object> params = new HashMap<>();

            StringWriter output;
            MimeMessage msg;
            output = new StringWriter();
            Project project;

            project = projectRepository.findOne(idProyecto);

            params.put("nombreUsuario", project.getContact().getFullname());
            params.put("proyecto", project.getName());
            params.put( "nombre", nombre );
            params.put( "apellido", apellido );
            params.put( "correo", correo );
            params.put( "telefono", telefono );
            params.put( "mensaje", mensaje );
            template.process( params, output );


            msg = mailSender.createMimeMessage();
            msg.setRecipients( Message.RecipientType.TO, project.getContact().getContactInformation().getEmail() );
            msg.setSubject( "Contactar Reponsable de Proyecto en CrowdEI" );
            msg.setText( output.toString(), "utf-8", "html" );


            mailSender.send( msg );

        }
        catch ( NoResultException | TemplateException e )
        {
            logger.error( messagesConfig.userNotFound, e );
            throw new ServiceException( messagesConfig.userNotFound );
        }
        catch ( IOException | MessagingException e )
        {
            logger.error(messagesConfig.messagingError, e);
            throw new ServiceException( messagesConfig.messagingError );
        }
        logger.debug( "Saliendo del metodo retrieveUser" );

    }

    //region metodos privados


    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  createReceiverslist
     * Descripcion:             Metodo que arma la lista de receptores de los correos
     * @version                 1.0
     * @author                  buchyo
     * @since                   11/02/2016
     *
     * @return
     * @throws AddressException
     */
    private InternetAddress[] createReceiverslist() throws AddressException
    {
        String[] listaReceptores;
        listaReceptores = receivers.split( ";" );

        InternetAddress[] dest;
        dest = new InternetAddress[listaReceptores.length];

        for (int i = 0; i < dest.length; i++)
        {
            dest[i] = new InternetAddress( listaReceptores[i]) ;
        }

        return dest;
    }

    //enregion

}
