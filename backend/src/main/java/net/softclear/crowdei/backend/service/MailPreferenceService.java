package net.softclear.crowdei.backend.service;

import net.softclear.crowdei.backend.model.jpa.MailPreference;

import java.util.List;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  ExperienceLevelService
 * Descripcion:             Contrato para todos los servicios que involucran la entidad HowFind
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
public interface MailPreferenceService
{
    /**
     * Nombre:                  getAll.
     * Descripcion:             Metodo que obtiene todas las preferencias de correo
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/22/2015
     *
     * @return lista de preferencias de correo
     */
	List<MailPreference> getAll();
}
