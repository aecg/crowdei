package net.softclear.crowdei.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  Resource
 * Descripcion:             Clase para modelar la entidad Resource
 * @version                 1.0
 * @author                  buchyo
 * @since                   03/03/2016
 *
 */
@Entity
@Table(name="resource")
@Setter
@Getter
public class Resource
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;

    @Column( name = "concept", nullable = false )
    private String concept;

    @Column( name = "amount", nullable = false )
    private Double amount;

    @Column( name = "percentage", nullable = false )
    private Integer percentage;

    @ManyToOne()
    @JoinColumn( name = "project_id", nullable=false )
    private Project project;
}
