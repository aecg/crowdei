package net.softclear.crowdei.backend.web.rest.payments.to.request;



import lombok.Data;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  MercadoPagoRequest
 * Descripcion:             Clase que modela la peticion del pago por medio de mercadopago
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/29/2015
 *
 */
@Data
public class MercadoPagoRequest
{
    private String token;
    private Double amount;
    private String description;
    private String userName;
    private String email;
    private String paymentMethodId;
    private String successUrl;
    private String pendingUrl;
    private String cancelUrl;
    private String currencyId;
    private String data;
    private Long rewardId;
}
