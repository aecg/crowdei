package net.softclear.crowdei.backend.service;



import net.softclear.crowdei.backend.model.jpa.City;
import net.softclear.crowdei.backend.model.jpa.Country;
import net.softclear.crowdei.backend.model.jpa.State;

import java.util.List;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  LocationService
 * Descripcion:             Contrato para los sericios que involucran locaciones
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/9/2015
 *
 */
public interface LocationService
{
    /**
     * Nombre:                  getCountries.
     * Descripcion:             Metodo que obtiene la lista de paises
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/9/2015
     *
     * @return lista de paises
     */
    List<Country> getCountries();


    /**
     * Nombre:                  getStates.
     * Descripcion:             Metodo que obtiene la lista de estados de un pais
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/9/2015
     *
     * @param country datos del pais
     * @return lista de estados
     */
    List<State> getStates(Country country);


    /**
     * Nombre:                  getCities.
     * Descripcion:             Metodo que obtiene la lista de ciudades de un estado
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/9/2015
     *
     * @param state datos del estado
     * @return lista de ciudades
     */
    List<City> getCities(State state);

}
