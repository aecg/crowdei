package net.softclear.crowdei.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.List;


/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  Person
 * Descripcion:             Clase que modela la entidad person de la base de datos
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/1/2015
 *
 */
@Entity
@Table(name="person")
@Setter
@Getter
public class Person
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;

    @Column( name = "name", nullable = false )
    private String name;

    @Column( name = "last_name", nullable = false )
    private String lastName;

    @Column( name = "email", nullable = false)
    private String email;

    @Column( name = "sex", nullable = true)
    private String sex;

    @ManyToOne(cascade= CascadeType.ALL )
    @JoinColumn( name = "location_id" )
    private Location location;

    @Column( name = "image",  nullable = true)
    private String image;

    @Column( name = "description", nullable = true)
    private String description;

    @Column( name = "education", nullable = true)
    private String education;

    @Column( name = "interests", nullable = true)
    private String interests;

    @Column (name = "experience_description", nullable = true)
    private String experienceDescription;

    @Column (name = "amount",  nullable = true)
    private String amount;

    @ManyToOne()
    @JoinColumn( name = "how_find_id" )
    private HowFind howFind;

    @ManyToOne()
    @JoinColumn( name = "experience_level_id" )
    private ExperienceLevel experienceLevel;

    @ManyToMany()
    @JoinTable(
            name="user_mail_preference",
            joinColumns={@JoinColumn(name="person_id", referencedColumnName="id")},
            inverseJoinColumns={@JoinColumn(name="mail_preference_id", referencedColumnName="id")})
    private List<MailPreference> mailPreferenceList;

    @ManyToMany()
    @JoinTable(
            name="user_business_type",
            joinColumns={@JoinColumn(name="person_id", referencedColumnName="id")},
            inverseJoinColumns={@JoinColumn(name="business_type_id", referencedColumnName="id")})
    private List<BusinessType> businessTypeList;

    @ManyToMany()
    @JoinTable(
            name="user_reason_investment",
            joinColumns={@JoinColumn(name="person_id", referencedColumnName="id")},
            inverseJoinColumns={@JoinColumn(name="reason_investment_id", referencedColumnName="id")})
    private List<ReasonInvestment> reasonInvestmentList;

    @ManyToMany()
    @JoinTable(
            name="user_economic_activity",
            joinColumns={@JoinColumn(name="person_id", referencedColumnName="id")},
            inverseJoinColumns={@JoinColumn(name="economic_activity_id", referencedColumnName="id")})
    private List<EconomicActivity> economicActivityList;

}
