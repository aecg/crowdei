package net.softclear.crowdei.backend.web.rest.payments;

import net.softclear.crowdei.backend.config.MessagesConfig;
import net.softclear.crowdei.backend.model.jpa.Investment;
import net.softclear.crowdei.backend.service.InvestmentService;
import net.softclear.crowdei.backend.util.ConektaUtils;
import net.softclear.crowdei.backend.util.MercadoPagoUtils;
import net.softclear.crowdei.backend.util.TransfererUtils;
import net.softclear.crowdei.backend.web.rest.payments.to.request.ConektaRequest;
import net.softclear.crowdei.backend.web.rest.payments.to.request.MercadoPagoRequest;
import net.softclear.crowdei.backend.web.rest.payments.to.request.TransfererRequest;
import net.softclear.crowdei.backend.web.rest.payments.to.response.ConektaResponse;
import net.softclear.crowdei.backend.web.rest.payments.to.response.MercadoPagoResponse;
import net.softclear.crowdei.backend.web.rest.payments.to.response.TransferResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  PaymentsController
 * Descripcion:             Clase que se encarga de obtener los eventos de los pagos de paypal
 * mercadopago y transferencia
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/28/2015
 *
 */
@Controller
public class PaymentsController
{

    //region Attributes

    @Autowired
    private InvestmentService service;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private MessagesConfig messagesConfig;
    //endregion

    //region Metodos


    /**
     * Nombre:                  investmentPaypal.
     * Descripcion:             Metodo que procesa el pago de paypal
     * @version 1.0
     * @author guzmle
     * @since 12/29/2015
     *
     * @param requestBody informacion del pago
     * @param idUser identficador del usuario
     * @param idProject identificador del proyecto
     * @param response datos de la peticion
     */
    @RequestMapping( value = "user/{idUser}/project/{idProject}/reward/{idReward}/investment/paypal/", method = RequestMethod.POST,
            headers = "Accept=*" )
    @ResponseStatus( HttpStatus.OK )
    public void investmentPaypal( @RequestBody String requestBody, @PathVariable long idUser,
            @PathVariable long idProject, @PathVariable long idReward, HttpServletResponse response )
    {
        try
        {
            service.paymentProcessingPaypal( idUser, idProject, idReward, requestBody );
            response.sendRedirect( messagesConfig.appPath + "#/historyInvestments/" + idProject );
        }
        catch ( IOException e )
        {
            e.printStackTrace();
        }

    }


    /**
     * Nombre:                  investmentMercadoPago.
     * Descripcion:             Metodo que procesa el pago de mercado pago
     * @version 1.0
     * @author guzmle
     * @since 12/29/2015
     *
     * @param requestBody informacion del pago
     * @param idUser identificador del usuario
     * @param idProject identificador del proyecto
     */
    @RequestMapping( value = "user/{idUser}/project/{idProject}/investment/mercadopago/", method = RequestMethod
            .POST, consumes = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public void investmentMercadoPago( @RequestBody MercadoPagoRequest requestBody, @PathVariable long idUser,
            @PathVariable long idProject )
    {
        final MercadoPagoUtils obj = modelMapper.map( requestBody, MercadoPagoUtils.class );
        service.paymentProcessingMercadoPago( idUser, idProject, obj );
    }

    /**
     * Nombre:                  investmentConekta
     * Descripcion:             Método que procesa el pago de una transferencia
     * @version                 1.0
     * @author                  camposer
     * @since                   12/14/2016
     *
     * @param requestBody información del pago
     * @return respuesta con la url de la información
     */
    @RequestMapping( value = "user/{idUser}/project/{idProject}/investment/conekta/", method = RequestMethod
            .POST, consumes = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public void investmentConekta( @RequestBody ConektaRequest requestBody, @PathVariable long idUser,
            @PathVariable long idProject )
    {
        final ConektaUtils obj = modelMapper.map( requestBody, ConektaUtils.class );
        service.paymentProcessingConekta( idUser, idProject, obj );
    }    

    /**
     * Nombre:                  investmentTransferer.
     * Descripcion:             Metodo que procesa el pago de una transferencia
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/29/2015
     *
     * @param requestBody informacion del pago
     * @param idUser identificador del usuario
     * @param idProject identificador del proyecto
     * @return id de la transferencia
     */
    @RequestMapping( value = "user/{idUser}/project/{idProject}/investment/transferer/", method = RequestMethod
            .POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = "application/json")
    public @ResponseBody
    TransferResponse investmentTransferer( @RequestBody TransfererRequest requestBody, @PathVariable long idUser,
            @PathVariable long idProject )
    {
        TransferResponse response;
        Investment investment;

        final TransfererUtils obj = modelMapper.map( requestBody, TransfererUtils.class );
        investment = service.paymentProcessingTransferencia( idUser, idProject, obj );

        response = modelMapper.map( investment, TransferResponse.class );
        response.setIdTransfer(investment.getId());

        return response;
    }


    /**
     * Nombre:                  investmentTransferer.
     * Descripcion:             Metodo que procesa el pago de una transferencia
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/29/2015
     *
     * @param requestBody informacion del pago
     * @return respuesta con la url de la informacion
     */
    @RequestMapping( value="payments/mercadopago", method = RequestMethod.POST, consumes = "application/json",
            produces = "application/json")
    public @ResponseBody
    MercadoPagoResponse getMercadoPagoUrl( @RequestBody MercadoPagoRequest requestBody )
    {
        final MercadoPagoUtils obj = modelMapper.map( requestBody, MercadoPagoUtils.class );
        final MercadoPagoResponse response = new MercadoPagoResponse();
        final String url = service.getMercadoPagoUrl( obj );
        response.setUrl( url );

        return response;
    }

    //endregion
}

