package net.softclear.crowdei.backend.repository;

import net.softclear.crowdei.backend.model.jpa.User;
import org.springframework.data.domain.Pageable;

import java.util.List;


/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  UserRepositoryCustom
 * Descripcion:             Contrato para todas las operaciones personalizadas en la base de datos que
 * involucran la entidad usuario
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
public interface UserRepositoryCustom
{
    /**
     * Nombre:                  findByEmail.
     * Descripcion:             Metodo que obtiene un usuario dado su correo electronico
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/10/2015
     *
     * @param email email del usuario
     * @return datos del usuario completo
     */
    User findByEmail(String email);

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  findByEmail
     * Descripcion:             Metodo que verifica si existe algun usuario dado un correo
     * @version                 1.0
     * @author                  buchyo
     * @since                   13/04/2016
     *
     * @param email email del usuario
     * @return true si existe false en caso contrario
     */
    Boolean emailExist(String email);

    List<User> findAllUsers(Boolean activeUsers, Pageable pageable);
}
