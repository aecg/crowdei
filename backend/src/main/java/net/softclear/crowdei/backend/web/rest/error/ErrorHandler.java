package net.softclear.crowdei.backend.web.rest.error;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;

import net.softclear.crowdei.backend.web.rest.error.to.ErrorResponse;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  ErrorHandler
 * Descripcion:             Clase que posee los eventos cuando un servicio rest retorna una excepcion
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@ControllerAdvice
public class ErrorHandler
{
	private static Logger logger = LoggerFactory.getLogger(ErrorHandler.class);
	
	@Autowired
	private ErrorFactory errorFactory;


    /**
     * Nombre:                  handle.
     * Descripcion:             Metodo que se ejecuta cuando ocurre una excepcion en el controlador
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/22/2015
     *
     * @param exception Excepcion que arroja el controlador
     * @return Excepcion mapeada
     */
	@org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorResponse> handle(Exception exception)
    {
		logger.debug("ServiceException from controller", exception);
		exception.printStackTrace();
		
		return new ResponseEntity<>(errorFactory.exceptionFrom(exception), HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
