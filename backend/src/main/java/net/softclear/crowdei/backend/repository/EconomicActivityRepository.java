package net.softclear.crowdei.backend.repository;

import net.softclear.crowdei.backend.model.jpa.EconomicActivity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  economicActivityRepository
 * Descripcion:             Contrato para todas las operaciones de la base de datos que involucran la entidad
 * @version                 1.0
 * @author                  buchyo
 * @since                   15/03/2016
 *
 */
public interface EconomicActivityRepository extends JpaRepository<EconomicActivity, Long>
{
}
