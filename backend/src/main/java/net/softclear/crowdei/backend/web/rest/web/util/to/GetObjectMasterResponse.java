package net.softclear.crowdei.backend.web.rest.web.util.to;



import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;

import java.util.List;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  GetObjectMasterResponse
 * Descripcion:             Clase que posee la definicion de la respuesta JSON de todos los objetos que solo
 * poseen descripcion
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@Data
@JsonInclude(Include.NON_NULL)
public class GetObjectMasterResponse
{
    private List<Item> list;


    /**
     * Clase interna que posee la definicion del objeto de la lista.
     */
    @Data
    public static class Item
    {

        private Long id;
        private String description;

    }
}
