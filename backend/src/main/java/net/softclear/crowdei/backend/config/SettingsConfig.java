package net.softclear.crowdei.backend.config;



import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  MessagesConfig
 * Descripcion:             Clase que posee la defincion de todos los mensajes de error del sistema
 * @version                 1.0
 * @author                  buchyo
 * @since                   04/20/2016
 *
 */
@Component
public class SettingsConfig
{

    /**
     * Ruta del video.
     */
    @Value ( "${config.pathVideo}")
    public String pathAbsolute;

    /**
     * ruat del dominio.
     */
    @Value ( "${config.dominio}")
    public String dominio;

    /**
     * key del Captcha.
     */
    @Value ( "${config.keyCaptcha}")
    public String keyCaptcha;

    /**
     * AWS S3 Access Key ID.
     */
    @Value ( "${config.awsS3AccessKeyID}")
    public String awsS3AccessKeyID;

    /**
     * AWS S3 Secret Key.
     */
    @Value ( "${config.awsS3SecretAccessKey}")
    public String awsS3SecretAccessKey;

    /**
     * AWS S3 Bucket.
     */
    @Value ( "${config.awsS3Bucket}")
    public String awsS3Bucket;

    /**
     * AWS S3 Video Folder.
     */
    @Value ( "${config.awsS3VideoFolder}")
    public String awsS3VideoFolder;

    /**
     * Mercado Pago Client Id.
     */
    @Value ( "${mp.clientId}")
    public String mpClientId;

    /**
     * Mercoado Pago Client Secret.
     */
    @Value ( "${mp.clientSecret}")
    public String mpClientSecret;

    /**
     * Mercado Pago Sandbox.
     */
    @Value ( "${mp.sandbox}")
    public String mpSandbox;

    /**
     * Tiempo de publicación de un proyecto, por defecto, en dias.
     */
    @Value ( "${project.default.publication.days}")
    public int projectDefaultPublicationDays;
}
