package net.softclear.crowdei.backend.web.rest.web.users.to.userResponse;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  GetUsersResponse
 * Descripcion:             Clase que obtiene el JSON para el servicio que consulta todos los usuarios
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@Data
@JsonInclude(Include.NON_NULL)
public class GetUsersResponse
{
	private Long count;
	private List<User> users;


    /**
     * Clase interna que posee la defincion del JSON para la entidad User.
     */
	@Data
	public static class User
    {
		private Long id;
		private String username;
		private Boolean enabled;
		private Date createdAt;

		private Person person;
		private List<Authority> authorities;

		/**
		 * Clase que representa en JSON los datos de la Persona.
		 */
		@Data
		public static class Person
		{
			private Long id;
			private String name;
			private String lastName;
			private String email;
			private String sex;
			private String image;
			private String description;
			private String education;
			private String interests;
			private String experienceDescription;
			private String amount;
			private Item howFind;
			private Item experienceLevel;
			private Location location;
			private List<Item> mailPreferenceList;
			private List<Item> businessTypeList;
			private List<Item> reasonInvestmentList;
			private List<Item> economicActivityList;
		}


		/**
		 * Clase interna que posee la defincion del JSON para la entidades que solo tienen id.
		 */
		@Data
		public static class Item
		{
			private Long   id;
		}

		/**
		 * Clase interna que posee la defincion para la entidad de location.
		 */
		@Data
		public static class Location
		{
			private Long id;
			private String address;
			private String postalCode;
			private City city;
		}

		/**
		 * Clase interna que posee la defincion del JSON para la entidades que solo tienen id.
		 */
		@Data
		public static class City
		{
			private Long   id;
			private String name;
			private State state;
		}

		/**
		 * Clase interna que posee la defincion del JSON para la entidades que solo tienen id.
		 */
		@Data
		public static class State
		{
			private Long   id;
			private String name;
			private Country country;
		}

		/**
		 * Clase interna que posee la defincion del JSON para la entidades que solo tienen id.
		 */
		@Data
		public static class Country
		{
			private Long   id;
			private String name;
		}

		/**
		 * Clase que representa en JSON los datos de la Authority.
		 */
		@Data
		public static class Authority
		{
			private Long   id;
			private String authority;
		}

		public String getFullname() {
			return person.getName() + " " + person.getLastName();
		}

		public String getFulladdress() {
			String fulladdress = "";
			if (person.getLocation() != null) {
				if (person.getLocation().getAddress() != null)
				{
					fulladdress +=  person.getLocation().getAddress() + ", ";
				}
				if (person.getLocation().getPostalCode() != null)
				{
					fulladdress +=  person.getLocation().getPostalCode() + ", ";
				}
				if (person.getLocation().getCity() != null)
				{
					fulladdress +=  person.getLocation().getCity().getName() + ", ";
					if (person.getLocation().getCity().getState() != null)
					{
						fulladdress +=  person.getLocation().getCity().getState().getName() + ", ";
						if (person.getLocation().getCity().getState().getCountry() != null)
						{
							fulladdress +=  person.getLocation().getCity().getState().getCountry().getName();
						}
					}
				}
			}
			return fulladdress;
		}
	}
}
