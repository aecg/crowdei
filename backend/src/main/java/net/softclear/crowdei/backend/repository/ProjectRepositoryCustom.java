package net.softclear.crowdei.backend.repository;

import net.softclear.crowdei.backend.model.jpa.Project;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  ProjectRepositorycustom
 * Descripcion:             Contrato para todas las operaciones personalizadas en la base de datos que
 * involucran la entidad proyecto
 * @version                 1.0
 * @author                  buchyo
 * @since                   12/03/2015
 *
 */
public interface ProjectRepositoryCustom
{
    /**
     * Nombre:                  findAll.
     * Descripcion:             Metodo que consulta todos los proyectos dado el identificador del usuario
     * @version                 1.0
     * @author                  buchyo
     * @since                   12/03/2015
     *
     * @param idUsuario identificador del usuario
     * @param pageable si es paginable
     * @return lista de proyectos
     */
    List<Project> findAll(Long idUsuario, Pageable pageable);

    /**
     * Nombre:                  findAll.
     * Descripcion:             Metodo que consulta todos los proyectos dado unos parametros de busqueda
     * @version                 1.0
     * @author                  buchyo
     * @since                   12/03/2015
     *
     * @param nameCompany nombre de la compañia
     * @param nameProject nombre del proyecto
     * @param idEconomicActivity actividad economica del proyecto
     * @param pageable si es paginable
     * @return lista de proyectos
     */
    List<Project> findAll(String nameCompany, String nameProject, Long idEconomicActivity, Pageable pageable);

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  findAll
     * Descripcion:             Método que consulta todos los proyectos propios de un usuario y que hagan match con
     *                          los parámetros.
     * @version                 1.0
     * @author                  buchyo
     * @since                   04/01/2016
     *
     * @param id identificador del usuario
     * @param nameCompany nombre de la compañia
     * @param nameProject nombre del proyecto
     * @param idEconomicActivity actividad economica del proyecto
     * @param pageable si es paginable
     * @return Lista de proyectos que hagan match con los parametros
     */
    List<Project> findAll(Long id, String nameCompany, String nameProject, Long idEconomicActivity, Pageable
            pageable);

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  findAll
     * Descripcion:             Método que obtiene los proyectos que tienen alguna prioridad
     * @version                 1.0
     * @author                  buchyo
     * @since                   05/01/2016
     *
     * @return lista de proyectos
     */
    List<Project> findAll();

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  findAll
     * Descripcion:             Método que consulta todos los proyectos activos
     * @version                 1.0
     * @author                  buchyo
     * @since                   14/01/2016
     *
     * @return lista de proyectos
     */
    List<Project> findAllProjects(Boolean activeProjects, Pageable pageable);

    /**
     * Sistema:                 Crowdei.
     *                                                              
     * Nombre:                  findAllProjectsInvestment
     * Descripcion:             Método que trae todos los proyectos en los que invirtió el usuario
     * @version                 1.0
     * @author                  buchyo
     * @since                   18/01/2016
     *
     * @param idUsuario identificador del usuario
     * @return lista de proyectos
     */
    List<Project> findAllProjectsInvestment(Long idUsuario, Pageable pageable);

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  findAllProjectsInvestmentXIdXParams
     * Descripcion:             Metodo que consulta los proyectos  en los que invirtio el usuario dado un filtro
     * @version                 1.0
     * @author                  buchyo
     * @since                   25/01/2016
     *
     * @param id identificador del usuario
     * @param nameCompany nombre de la compañia
     * @param nameProject nombre del proyecto
     * @param startDate rango de fecha de la inversion
     * @param endDate rango de fecha de la inversion
     * @return Lista de proyectos que hagan match con los parametros
     */
    List<Project> findAllProjectsInvestmentXIdXParams(Long id, String nameCompany, String nameProject, Date
            startDate, Date endDate, Pageable pageable);

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  findAllProjectsPriorityType
     * Descripcion:             Método que obtiene los proyectos que tienen una prioridad en particular
     * @version                 1.0
     * @author                  buchyo
     * @since                   01/02/2016
     *
     * @param priority tipo de la prioridad a consultar
     * @return Objeto que posee la informacion en formato JSON
     */
    List<Project> findAllProjectsPriorityType(String priority);
}
