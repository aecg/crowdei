package net.softclear.crowdei.backend.web.rest.web.projects.to.projectResponse;

import lombok.Data;

import java.util.Date;
import java.util.List;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  CreateProjectResponse
 * Descripcion:             Clase que posee la definion del obj JSON de la respuesta del servicio de crear proyecto.
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/21/2015
 *
 */
@Data
public class CreateProjectResponse
{
    private Long           id;
    private String         name;
    private String         description;
    private Date           dateRegister;
    private double         objetive;
    private byte[]         image;
    private byte[]         video;
    private String         generalObjetive;
    private String         specificObjetives;
    private int            monthExecution;
    private String         companyShares;
    private String         priorityType;
    private int            position;
    private String         status;

    private Company company;
    private Person  idPerson;
    private Location location;
    private Currency currency;
    private ImpactSector impactSector;
    private EconomicActivity economicActivity;
    private Contact contact;

    private List<Document> documents;
    private List<Calendar> calendars;
    private List<Resource> resources;


    /**
     * Clase que representa en JSON los datos de la compania.
     */
    @Data
    public static class Company
    {
        private Long   id;
    }

    /**
     * Clase interna que posee la defincion para la entidad de location.
     */
    @Data
    public static class Location
    {
        private Long        id;
    }

    /**
     * Clase que representa en JSON los datos de la Persona.
     */
    @Data
    public static class Person
    {
        private Long   id;
    }

    /**
     * Clas interna que posee la definicion del JSON para el objeto currency.
     */
    @Data
    public static class Currency
    {
        private Long        id;
    }

    /**
     * Clas interna que posee la definicion del JSON para el objeto impactSector.
     */
    public static class ImpactSector
    {
        private Long        id;
    }

    /**
     * Clas interna que posee la definicion del JSON para el objeto economicActivity.
     */
    public static class EconomicActivity
    {
        private Long        id;
    }

    /**
     * Clase que posee la definicion de objeto json de contact que recibe el servicio de crear proyecto.
     */
    @Data
    public static class Contact
    {
        private Long                id;
    }

    /**
     * Clase que posee la definicion de objeto json de Document que recibe el servicio de crear proyecto.
     */
    @Data
    public static class Document
    {
        private Long           id;
        private byte[]         file;
        private String         name;
    }

    /**
     * Clas interna que posee la definicion del JSON para el objeto calendar.
     */
    @Data
    public static class Calendar
    {
        private Long id;
        private String activity;
        private Date estimatedDate;
    }

    /**
     * Clas interna que posee la definicion del JSON para el objeto resource.
     */
    @Data
    public static class Resource
    {
        private Long id;
        private String concept;
        private Double amount;
        private Integer percentage;
        private Date estimatedDate;
    }
}
