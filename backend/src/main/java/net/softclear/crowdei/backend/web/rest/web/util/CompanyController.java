package net.softclear.crowdei.backend.web.rest.web.util;


import net.softclear.crowdei.backend.model.jpa.Project;
import net.softclear.crowdei.backend.service.CompanyService;
import net.softclear.crowdei.backend.web.rest.web.util.to.FilterRequest;
import net.softclear.crowdei.backend.web.rest.web.util.to.GetCompanyResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  CompanyController
 * Descripcion:             Clase que posee la definicion y la implementacion de los servicios que
 * involucran a la entidad company
 * @version                 1.0
 * @author                  buchyo
 * @since                   11/03/2016
 *
 */
@RestController
@RequestMapping("/web/utils*")
public class CompanyController
{
    //region Attributes

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private CompanyService companyService;

    //endregion

    //region Metodos

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  getCompanyXIdUser
     * Descripcion:             Método que trae todas las compañias creadas por un usuario
     * dado un parametro de busqueda
     * @version                 1.0
     * @author                  buchyo
     *
     * @param request id es el identificador del usuario, searchParameter es el nombre de la compania
     * @param page pagina de la informacion
     * @param size cantidad de registros
     * @return lista de compañia
     */
    @RequestMapping( value = "/company", method = RequestMethod.POST, produces = "application/json", consumes =
            "application/json" )
    public @ResponseBody GetCompanyResponse
    getCompanyXIdUser(@RequestBody FilterRequest request, @RequestParam( required = false )
    Integer page, @RequestParam( required = false ) Integer size)
    {
        Pageable pageable = null;
        GetCompanyResponse.Company  obj;

        List<Project> listProjects;
        GetCompanyResponse response;
        List<GetCompanyResponse.Company> companiesResponse;

        if ( page != null && size != null )
            pageable = new PageRequest( page, size );

        listProjects = companyService.getCompanyXIdUser( request.getId(), request.getSearchParameter(), pageable );

        response = new GetCompanyResponse();

        if ( listProjects != null )
        {
            companiesResponse = new ArrayList<>();
            for ( Project project : listProjects )
            {
                obj = modelMapper.map( project.getCompany(), GetCompanyResponse.Company.class );
                companiesResponse.add(obj);
            }

            response.setCompanies(removeRepeated(companiesResponse));
        }

        return response;
    }
    //endregion

    //region Metodos privados
    private List<GetCompanyResponse.Company> removeRepeated(List<GetCompanyResponse.Company> listCompanies)
    {
        List<GetCompanyResponse.Company> retorno;
        retorno = new ArrayList<>();

        for(GetCompanyResponse.Company company : listCompanies)
        {
            int i = 0;
            for ( GetCompanyResponse.Company companyInt : retorno )
                if ( company.getId() == companyInt.getId() )
                    i++;

            if ( i==0 )
                retorno.add( company );
        }

        return retorno;
    }
    //endregion
}
