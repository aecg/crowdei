package net.softclear.crowdei.backend.service.implementation;



import net.softclear.crowdei.backend.model.jpa.MailPreference;
import net.softclear.crowdei.backend.repository.MailPreferenceRepository;
import net.softclear.crowdei.backend.service.MailPreferenceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;



/**
 * Created by leonardo on 02/12/15.
 */
@Service
@Transactional
public class MailPreferenceServiceImpl implements MailPreferenceService
{
    //region Atributos

    private static Logger logger = LoggerFactory.getLogger( MailPreferenceServiceImpl.class );
    @Autowired
    private MailPreferenceRepository repository;

    //endregion

    //region Implementacion


    @Override
    public List<MailPreference> getAll()
    {
        logger.debug( "Entrando al metodo getAll" );
        List<MailPreference> retorno;
        retorno = repository.findAll();
        logger.debug( "Saliendo del metodo getAll" );
        return retorno;
    }

    //endregion
}
