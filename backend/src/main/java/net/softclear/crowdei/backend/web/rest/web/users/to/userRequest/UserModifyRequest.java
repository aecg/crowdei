package net.softclear.crowdei.backend.web.rest.web.users.to.userRequest;



import lombok.Data;
import java.util.List;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  UserModifyRequest
 * Descripcion:             Clase que posee la definicion del JSON de la peticion de la modificacion
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@Data
public class UserModifyRequest
{
    private String  username;
    private String  password;
    private Person  person;


    /**
     * Clase interna que posee la defincion del JSON para la entidad Person.
     */
    @Data
    public static class Person
    {
        private Long id;
        private String name;
        private String lastName;
        private String email;
        private String sex;
        private String image;
        private byte[] bytesPicture;
        private String namePicture;
        private String extensionPicture;
        private String description;
        private String education;
        private String interests;
        private String experienceDescription;
        private String amount;
        private Item howFind;
        private Item experienceLevel;
        private Location location;
        private List<Item> mailPreferenceList;
        private List<Item> businessTypeList;
        private List<Item> reasonInvestmentList;
        private List<Item> economicActivityList;
    }


    /**
     * Clase interna que posee la defincion del JSON para la entidades que solo tienen id.
     */
    @Data
    public static class Item
    {
        private Long   id;
    }

    /**
     * Clase interna que posee la defincion para la entidad de location.
     */
    @Data
    public static class Location
    {
        private Long id;
        private String address;
        private String postalCode;
        private Item city;
    }
}
