package net.softclear.crowdei.backend.model.types;

/**
 * Created by martin on 11/29/16.
 */
public enum PersonType
{
    /**
     * Personal Natural.
     */
    FISICA,
    /**
     * Persona Jurídica.
     */
    MORAL
}
