package net.softclear.crowdei.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  EconomicActivity
 * Descripcion:             Clase para modelar la entidad EconomicActivity
 * @version                 1.0
 * @author                  buchyo
 * @since                   03/03/2016
 *
 */
@Entity
@Table(name="economic_activity")
@Setter
@Getter
public class EconomicActivity
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;

    @Column( name = "description", nullable = false )
    private String description;
}
