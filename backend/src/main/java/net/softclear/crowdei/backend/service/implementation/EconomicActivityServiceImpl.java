package net.softclear.crowdei.backend.service.implementation;


import net.softclear.crowdei.backend.model.jpa.EconomicActivity;
import net.softclear.crowdei.backend.repository.EconomicActivityRepository;
import net.softclear.crowdei.backend.service.EconomicActivityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  EconomicActivityServiceImpl
 * Descripcion:             Clase que implementa el contrato del servicio
 * @version                 1.0
 * @author                  buchyo
 * @since                   15/03/2016
 *
 */
@Service
@Transactional
public class EconomicActivityServiceImpl implements EconomicActivityService
{
    //region Atributos

    private static Logger logger = LoggerFactory.getLogger(ExperienceLevelServiceImpl.class);
    @Autowired
    private EconomicActivityRepository repository;

    //endregion

    @Override
    public List<EconomicActivity> getAll()
    {
        logger.debug( "Entrando al metodo getAll" );
        List<EconomicActivity> retorno;
        retorno = repository.findAll(new Sort(Sort.Direction.ASC, "id"));
        logger.debug( "Saliendo del metodo getAll" );
        return retorno;
    }
}
