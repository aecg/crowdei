package net.softclear.crowdei.backend.web.rest.web.users.to.userResponse;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  GetUserResponse
 * Descripcion:             Clase que posee la defincion del JSON que es respondido para los servicios que
 * obtienen los datos del Usuario
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@Data
@JsonInclude(Include.NON_NULL)
public class GetUserResponse
{
    private Long id;
    private String username;
    private Boolean enabled;
    private Date createdAt;
    private Person person;


    /**
     * Clase interna que posee la defincion del JSON para la entidad Person.
     */
    @Data
    public static class Person
    {
        private Long       id;
        private String     name;
        private String     lastName;
        private String     email;
        private String     sex;
        private String     image;
        private String     description;
        private String     education;
        private String     interests;
        private String     experienceDescription;
        private String     amount;
        private Item       howFind;
        private Item       experienceLevel;
        private Location   location;
        private List<Item> mailPreferenceList;
        private List<Item> businessTypeList;
        private List<Item> reasonInvestmentList;
        private List<Item> economicActivityList;
    }


    /**
     * Clase interna que posee la defincion del JSON para las entidades que solo poseen descripcion.
     */
    @Data
    public static class Item
    {
        private Long   id;
        private String description;
    }


    /**
     * Clase interna que posee la defincion del JSON para la ciudad.
     */
    @Data
    public static class ItemCity
    {
        private Long   id;
        private String name;
        private ItemState state;

    }


    /**
     * Clase interna que posee la defincion del JSON para el estado.
     */
    @Data
    public static class ItemState
    {
        private Long   id;
        private String name;
        private ItemCountry country;

    }


    /**
     * Clase interna que posee la defincion del JSON para pais.
     */
    @Data
    public static class ItemCountry
    {
        private Long   id;
        private String name;

    }


    /**
     * Clase interna que posee la defincion para la entidad de location.
     */
    @Data
    public static class Location
    {
        private Long id;
        private String address;
        private String postalCode;
        private ItemCity city;
    }
}
