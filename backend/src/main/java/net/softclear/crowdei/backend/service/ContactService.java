package net.softclear.crowdei.backend.service;

import net.softclear.crowdei.backend.model.jpa.Project;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  ContactService
 * Descripcion:             Contrato para los servicios que involucran la entidad contact
 * @version                 1.0
 * @author                  buchyo
 * @since                   14/03/2016
 *
 */
public interface ContactService
{
    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  getCompanyXIdCompany
     * Descripcion:             Metodo que consulta todos los contactos que pertenesca a una compañia en
     * particular dado un parametro de busqueda
     * @version                 1.0
     * @author                  buchyo
     * @since                   14/03/2016
     *
     * @param idCompany identificador de la compañia
     * @param nameContact nombre de la persona contact
     * @return lista de proyectos con sus contactos
     */
    List<Project> getCompanyXIdCompany(Long idCompany, String nameContact, Pageable pageable);
}
