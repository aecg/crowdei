package net.softclear.crowdei.backend.service;

import net.softclear.crowdei.backend.model.jpa.HowFind;

import java.util.List;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  ExperienceLevelService
 * Descripcion:             Contrato para todos los servicios que involucran la entidad denivel de experiencia
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
public interface HowFindService
{
    /**
     * Sistema:                 CrowdEI.
     *
     * Nombre:                  HowFindService
     * Descripcion:             Metodo que obtiene todos los como nos encontró
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/22/2015
     *
     * @return lista con todos los como nos encontro
     */
	List<HowFind> getAll();
}
