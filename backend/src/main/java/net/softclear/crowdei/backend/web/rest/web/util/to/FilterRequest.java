package net.softclear.crowdei.backend.web.rest.web.util.to;

import lombok.Data;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  filterRequest
 * Descripcion:             Objeto que posee la definicion del JSON que es necesario para realizar una busqueda
 * @version                 1.0
 * @author                  buchyo
 * @since                   11/03/2016
 *
 */
@Data
public class FilterRequest
{
    private Long id;
    private String searchParameter;
}
