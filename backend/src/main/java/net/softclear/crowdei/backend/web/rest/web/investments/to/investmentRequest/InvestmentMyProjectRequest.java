package net.softclear.crowdei.backend.web.rest.web.investments.to.investmentRequest;

import lombok.Data;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  investmentMyProjectRequest
 * Descripcion:             Objeto que posee la definicion del JSON que es necesario para enviar un mensaje
 * @version                 1.0
 * @author                  buchyo
 * @since                   05/04/2016
 *
 */
@Data
public class InvestmentMyProjectRequest
{
    private Long idInvestment;
}
