package net.softclear.crowdei.backend.web.editor;

import java.beans.PropertyEditorSupport;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  DateEditor
 * Descripcion:             Clase que edita el formato de la fecha
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@Component
public class DateEditor extends PropertyEditorSupport
{

    private static Logger logger = LoggerFactory.getLogger( DateEditor.class );


    @Override
    public void setAsText( String text )
    {
        try
        {
            long milliseconds;
            milliseconds = Long.parseLong( text );
            this.setValue( new Date( milliseconds ) );
        }
        catch ( NumberFormatException e )
        {
            logger.error( "Ha ocurrido un error obteniendo la fecha como string", e );
        }
    }


    @Override
    public String getAsText()
    {
        Date date;
        date = (Date) this.getValue();
        return date != null ? Long.toString(date.getTime()) : "";
    }

}
