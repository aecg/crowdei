package net.softclear.crowdei.backend.web.rest.out.to.projectResponse;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  GetProjectsCompletPublicationResponse
 * Descripcion:             Objeto que posee los atributos de respuesta cuando se consulta los proyectos
 * que ya haya finalizado el tiempo de publicación
 * @version                 1.0
 * @author                  buchyo
 * @since                   20/04/2016
 *
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetProjectsCompletPublicationResponse
{
    private Long             count;
    private List<Project> investments;

    /**
     * Clase interna que posee la definicion del JSON para el objeto Project.
     */
    @Data
    public static class Project
    {
        private Long id;
        private String nameProject;
        private String generalObjetive;
        private Double totalAmountInvested;
        private Double percent;
        private String symbolCurrency;
        private String descriptionCurrency;
        private Double objetivo;
        private String emailPersonCreateProject;
        private String emailContact;
        private List<Investment> investments;
    }

    /**
     * Clase interna que posee la definicion del JSON para el objeto Investment.
     */
    @Data
    public static class Investment
    {
        private String status;
        private Person person;
    }

    /**
     * Clase interna que posee la definicion del JSON para el objeto Person.
     */
    @Data
    public static class Person
    {
        private Long id;
        private String status;
        private Person person;
        private String email;
        private List<MailPreference> mailPreferenceList;
    }

    /**
     * Clase interna que posee la definicion del JSON para el objeto MailPreference.
     */
    @Data
    public static class MailPreference
    {
        private Long id;
        private String description;
    }
}
