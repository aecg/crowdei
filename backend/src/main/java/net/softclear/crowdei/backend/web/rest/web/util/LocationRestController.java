package net.softclear.crowdei.backend.web.rest.web.util;

import net.softclear.crowdei.backend.model.jpa.City;
import net.softclear.crowdei.backend.model.jpa.Country;
import net.softclear.crowdei.backend.model.jpa.State;
import net.softclear.crowdei.backend.service.LocationService;
import net.softclear.crowdei.backend.web.rest.web.util.to.GetObjectLocationResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  UserRestController
 * Descripcion:             Clase que posee la definicion y la implementacion de los servicios que
 * involucran a la entidad usuario
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/4/2015
 *
 */
@RestController
@RequestMapping("/web/utils*")
public class LocationRestController
{
    //region Attributes

    @Autowired
    private ModelMapper     modelMapper;
    @Autowired
    private LocationService service;

    //endregion

    //region Services

    /**
     * Nombre:                  getAllCountries.
     * Descripcion:             Metodo que obtiene todos los paises almacenados en la base de datos
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/9/2015
     *
     * @return lista de paises
     */
    @RequestMapping( value = "/locations/countries", method = RequestMethod.GET, produces = "application/json" )
    public @ResponseBody
    GetObjectLocationResponse getAllCountries( )
    {
        List<Country> list;
        GetObjectLocationResponse.Item  item;
        GetObjectLocationResponse response;
        List<GetObjectLocationResponse.Item> listResponse;

        response = new GetObjectLocationResponse();
        list = service.getCountries();
        if (list != null)
        {
            listResponse = new ArrayList<>();
            for(Country obj : list)
            {
                item = modelMapper.map( obj, GetObjectLocationResponse.Item.class );
                listResponse.add( item );
            }

            response.setList( listResponse);
        }
        return response;
    }


    /**
     * Nombre:                  getAllStates.
     * Descripcion:             Metodo que obtiene todos los estado de un pais
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/9/2015
     *
     * @param id identificador del pais
     * @return lista de estados
     */
    @RequestMapping( value = "/locations/countries/{id}/states", method = RequestMethod.GET, produces =
            "application/json" )
    public  @ResponseBody  GetObjectLocationResponse getAllStates( @PathVariable long id )
    {
        Country country;
        List<GetObjectLocationResponse.Item> listResponse;
        GetObjectLocationResponse.Item  item;
        GetObjectLocationResponse response;
        List<State> list;


        country= new Country();
        country.setId( id );
        list = service.getStates(country);
        response = new GetObjectLocationResponse();

        if (list != null)
        {
            listResponse = new ArrayList<>();
            for(State obj : list)
            {
                item = modelMapper.map( obj, GetObjectLocationResponse.Item.class );
                listResponse.add( item );
            }

            response.setList( listResponse);
        }
        return response;
    }


    /**
     * Nombre:                  getAllCities.
     * Descripcion:             Metodo que obtiene todas las ciudades de un estado
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/9/2015
     *
     * @param id identificador del estado
     * @return lista de ciudades
     */
    @RequestMapping( value = "/locations/states/{id}/cities", method = RequestMethod.GET, produces =
            "application/json" )
    public @ResponseBody GetObjectLocationResponse getAllCities( @PathVariable long id )
    {
        State state;
        List<City> list;
        GetObjectLocationResponse response;
        List<GetObjectLocationResponse.Item> listResponse;
        GetObjectLocationResponse.Item  item;

        state = new State();
        state.setId( id );
        list = service.getCities(state);
        response = new GetObjectLocationResponse();

        if (list != null)
        {
            listResponse = new ArrayList<>();
            for(City obj : list)
            {
                item = modelMapper.map( obj, GetObjectLocationResponse.Item.class );
                listResponse.add( item );
            }

            response.setList( listResponse);
        }
        return response;
    }
    //endregion
}
