package net.softclear.crowdei.backend.web.rest.payments.to.response;



import lombok.Data;



/**
 * Sistema:                 CrowdEI.
 *
 * Descripcion:             Clase que hace de respuesta para la solicitud del link de conekta
 * @version                 1.0
 * @author                  camposer
 * @since                   12/14/2016
 */
@Data
public class ConektaResponse
{
}
