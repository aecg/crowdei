package net.softclear.crowdei.backend.web.rest.web.investments.to.investmentResponse;



import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import net.softclear.crowdei.backend.model.jpa.Investment;

import java.util.Date;
import java.util.List;



/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  GetInvestmentsResponse
 * Descripcion:             Objeto que posee los atributos de respuesta cuando se consulta las inversiones dado un
 * usuario y proyecto particular
 * @version                 1.0
 * @author                  buchyo
 * @since                   21/01/2016
 *
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetInvestmentsResponse
{
    private Long             count;
    private Long             countProjects;
    private List<Investment> investments;

    /**
     * Clase interna que posee la definicion del JSON para el objeto Investment.
     */
    @Data
    public static class Investment
    {
        private Long id;
        private Date date;
        private Double amount;
        private String status;
        private String methodPayment;
        private Project project;
    }

    /**
     * Clase interna que posee la definicion del JSON para el objeto Project.
     */
    @Data
    public static class Project
    {
        private Long id;
        private String name;
        private Currency currency;
    }

    /**
     * Clase interna que posee la definicion del JSON para el objeto Currency.
     */
    @Data
    public static class Currency
    {
        private String description;
        private String symbol;
    }

}
