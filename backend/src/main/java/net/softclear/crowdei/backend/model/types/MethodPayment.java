package net.softclear.crowdei.backend.model.types;



/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  methodPayment
 * Descripcion:             Enumerado para los diferentes metodos de pago
 * @version                 1.0
 * @author                  buchyo
 * @since                   02/02/2016
 *
 */
public enum MethodPayment
{
    /**
     * Paypal.
     */
    PAYPAL,

    /**
     * MercadoPago.
     */
    MERCADOPAGO,

    /**
     * Transferencia.
     */
    TRANSFER,
    
    /**
     * Conekta.
     */
    CONEKTA;
}

