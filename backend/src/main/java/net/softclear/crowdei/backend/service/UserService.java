package net.softclear.crowdei.backend.service;

import net.softclear.crowdei.backend.repository.UserRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import net.softclear.crowdei.backend.model.jpa.User;

import java.util.List;


/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  UserService
 * Descripcion:             Contrato para los servicios que involucran la entidad Usuario
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
public interface UserService
{
    /**
     * Nombre:                  createUser.
     * Descripcion:             Metodo que se usa para crear un usuario
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     * @param user datos del usuario
     * @return usuario creado
     */
	User createUser(User user, String captcha);


    /**
     * Nombre:                  getUser.
     * Descripcion:             Metodo que consulta los datos de un usuario por su identificador
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     * @param id identificador del usuario
     * @return datos del usuario
     */
	User getUser(Long id);


    /**
     * Nombre:                  getUserByUsername.
     * Descripcion:             Metodo que consulta los datos de un usuario dado su nombre de usuario
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     * @param username nombre de usuario
     * @return datos del usuario
     */
	User getUserByUsername(String username);


    /**
     * Nombre:                  changePassword.
     * Descripcion:             Metodo que realiza el cambio de contraseña de un usuario
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     * @param username login del usuario que se le va a cambiar la contraseña
     * @param password contraseña anterio
     */
	void changePassword(String username, String password);


    /**
     * Nombre:                  getUsers.
     * Descripcion:             Metodo que consulta los datos de los usuarios que hagan match con
     * los parametros de busqueda
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     * @param activeUsers si eel usuario está activo
     * @param pageable si es paginable
     * @return lista de usuario
     */
	List<User> getUsers(Boolean activeUsers, Pageable pageable);


    /**
     * Nombre:                  retrieveUser.
     * Descripcion:             Metodo que realiza la logica para el envio del correo que el usuario
     * utiliza para la recuperacion
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/3/2015
     *
     * @param email email del usuario
     */
    void retrieveUser(String email);


    /**
     * Nombre:                  setUserRepository.
     * Descripcion:             Metodo que permite asignar la instancia del repositorio para ser mockeado
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/22/2015
     *
     * @param userRepository datos del repositorio
     */
    void setUserRepository(UserRepository userRepository);


    /**
     * Nombre:                  updateUser.
     * Descripcion:             Metodo que modifica los datos del usuario
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/3/2015
     *
     * @param user datos el usuario
     */
    void updateUser( User user );


    /**
     * Nombre:                  validateUser.
     * Descripcion:             Metodo que valida las credenciales de un usuario
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/22/2015
     *
     * @param username user name del usuario
     * @param password password del usuario
     * @return Datos del usuario
     */
    User validateUser(String username, String password);

    void activateUser(Long id);

    void deactivateUser(Long id);

}
