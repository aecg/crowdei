package net.softclear.crowdei.backend.web.rest.web.projects.to.projectRequest;



import lombok.Data;


/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  FindProjectsXIdXParamsRequest
 * Descripcion:             Objeto que posee la definicion del JSON que es necesario para consultar proyectos
 * @version                 1.0
 * @author                  buchyo
 * @since                   01/04/2016
 *
 */
@Data
public class FindProjectsXIdXParamsRequest
{
    private Long idUser;
    private String nameCompany;
    private String nameProject;
    private Long idEconomicActivity;
}
