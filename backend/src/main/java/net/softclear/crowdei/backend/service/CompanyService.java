package net.softclear.crowdei.backend.service;

import net.softclear.crowdei.backend.model.jpa.Project;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  CompanyService
 * Descripcion:             Contrato para los servicios que involucran la entidad company
 * @version                 1.0
 * @author                  buchyo
 * @since                   11/03/2016
 *
 */
public interface CompanyService
{
    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  getCompanyXIdUser
     * Descripcion:             Metodo que consulta todas las compañias que haya registrado un usuario
     * dado un parametro de busqueda
     * @version                 1.0
     * @author                  buchyo
     * @since                   11/03/2016
     *
     * @param idUsuario identificador del usuario
     * @param nameCompany nombre de la compañia
     * @return lista de proyectos con su compañia
     */
    List<Project> getCompanyXIdUser(Long idUsuario, String nameCompany, Pageable pageable);
}
