package net.softclear.crowdei.backend.model.jpa;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  Authority
 * Descripcion:             Clase que representa la entidad autoridad que sirve para almacenar la informacion de los
 * privilegios de los usuarios.
 *
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/21/2015
 *
 */
@Entity
@Table(name = "authority")
@Setter
@Getter
public class Authority implements Serializable
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 50)
	private Role authority;

	public static enum Role {
		/**
		 * Role admin
		 */
		ROLE_ADMIN,
		/**
		 * Role super admin
		 */
		ROLE_SUPER_ADMIN
	}

	@ManyToOne
	@JoinColumn(name="user_id", nullable=false)
	private User user;

	@Override
	public String toString() {
		return this.authority.toString();
	}
}
