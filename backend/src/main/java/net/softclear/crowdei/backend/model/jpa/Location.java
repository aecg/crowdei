package net.softclear.crowdei.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  Location
 * Descripcion:             Clase que modela la entidad location
 * @version                 1.0
 * @author                  buchyo
 * @since                   03/03/2016
 *
 */
@Entity
@Table(name="location")
@Setter
@Getter
public class Location
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;

    @Column( name = "address", nullable = true )
    private String address;

    @Column( name = "municipality", nullable = true )
    private String municipality;

    @Column( name = "postal_code", nullable = true )
    private String postalCode;

    @Column( name = "location_reference", nullable = true )
    private String locationReference;

    @ManyToOne()
    @JoinColumn( name = "city_id" )
    private City city;
}
