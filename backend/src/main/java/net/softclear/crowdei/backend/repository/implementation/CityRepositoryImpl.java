package net.softclear.crowdei.backend.repository.implementation;



import net.softclear.crowdei.backend.model.jpa.City;
import net.softclear.crowdei.backend.model.jpa.State;
import net.softclear.crowdei.backend.repository.CityRepositoryCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  UserRepositoryImpl
 * Descripcion:             Clase que mimplementa el contrato para las operaciones personalizadas para la
 *  entidad usuario
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
public class CityRepositoryImpl implements CityRepositoryCustom
{
    //region Atributos

    /**
     * Objeto que posee la definicion de la persistencia.
     */
    @PersistenceContext
    private EntityManager entityManager;

    //endregion

    //region Implementacion


    /**
     * Nombre:                  findByState.
     * Descripcion:             Metodo que consulta las ciudades de un estado
     *
     * @param state datos del estado
     *
     * @return lista de ciudades
     *
     * @version 1.0
     * @author guzmle
     * @since 12/9/2015
     */
    @Override
    public List<City> findByState( State state )
    {
        List<Predicate> predicates;
        CriteriaQuery<City> criteriaQuery;
        Root<City> root;
        CriteriaBuilder criteriaBuilder;
        Join joinEstado;
        Query query;
        List<City> response;


        predicates = new ArrayList<>();
        criteriaBuilder = entityManager.getCriteriaBuilder();
        criteriaQuery = criteriaBuilder.createQuery( City.class );
        root = criteriaQuery.from( City.class );
        joinEstado = root.join( "state" );


        predicates.add( criteriaBuilder.equal( joinEstado.get( "id" ), state.getId() ) );
        criteriaQuery.where( predicates.toArray( new Predicate[ predicates.size() ] ) );

        query = entityManager.createQuery(criteriaQuery);
        response = query.getResultList();

        return response;
    }

    //endregion
}
