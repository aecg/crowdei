package net.softclear.crowdei.backend.service.implementation;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import net.softclear.crowdei.backend.config.MessagesConfig;
import net.softclear.crowdei.backend.exception.ServiceException;
import net.softclear.crowdei.backend.model.jpa.Authority;
import net.softclear.crowdei.backend.model.jpa.User;
import net.softclear.crowdei.backend.repository.AuthorityRepository;
import net.softclear.crowdei.backend.repository.PersonRepository;
import net.softclear.crowdei.backend.repository.UserRepository;
import net.softclear.crowdei.backend.service.UserService;
import net.softclear.crowdei.backend.util.UtilString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.persistence.NoResultException;
import java.io.IOException;
import java.io.StringWriter;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  UserServiceImpl
 * Descripcion:             Clase que implementa el contrato del servicio
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
@Service
@Transactional
public class UserServiceImpl implements UserService
{
    //region Atributos

    private static Logger logger = LoggerFactory.getLogger( UserServiceImpl.class );
    @Autowired
    private JavaMailSender      mailSender;
    @Autowired
    private UserRepository      userRepository;
    @Autowired
    private AuthorityRepository authorityRepository;
    @Autowired
    private PersonRepository    personRepository;
    @Autowired
    private UtilString          utilString;
    @Autowired
    private MessagesConfig      messagesConfig;
    @Autowired
    private Configuration       freemarkerConfiguration;
    @Value( "${email.pathRecover}" )
    private String              pathRecover;

    //endregion

    //region Metodos


    /**
     * Nombre:                  createUserValidation.
     * Descripcion:             Metodo que realiza la validacion de los datos del usuario
     * @version 1.0
     * @author guzmle
     * @since 11/30/2015
     *
     * @param user datos del usuario a validar
     */
    private void createUserValidation( User user, String captcha )
    {
        logger.debug( "Entrando en el metodo createUserValidation" );
        if ( user == null )
            throw new ServiceException( messagesConfig.userNotNull );

        if ( user.getUsername() == null )
            throw new ServiceException( messagesConfig.usernameNotNull );

        if ( user.getPassword() == null )
            throw new ServiceException( messagesConfig.passwordNotNull );

        if ( user.getPerson() == null )
            throw new ServiceException( messagesConfig.userDataNotNull );

        if ( user.getPerson().getEmail() == null )
            throw new ServiceException( messagesConfig.emailNotNull );

        if ( user.getPerson().getName() == null )
            throw new ServiceException( messagesConfig.nameNotNull );

        if ( user.getPerson().getLastName() == null )
            throw new ServiceException( messagesConfig.lastNameNotNull );

        boolean isCaptchaValide;
        isCaptchaValide = utilString.validateCaptcha( captcha );
        if ( !isCaptchaValide )
            throw new ServiceException( messagesConfig.captchaWrong );

        logger.debug( "Saliendo del metodo createUserValidation" );
    }


    /**
     * Nombre:                  changePasswordValidation.
     * Descripcion:             Metodo que realiza la validacion del cambio de contrasñea
     * @version 1.0
     * @author guzmle
     * @since 11/30/2015
     *
     * @param username login del usuario
     * @param password contraseña anterior
     */
    private void changePasswordValidation( String username, String password )
    {
        logger.debug( "Entrando en el metodo changePasswordValidation" );
        if ( username == null )
            throw new ServiceException( messagesConfig.usernameNotNull );

        if ( password == null )
            throw new ServiceException( messagesConfig.emailNotNull );

        if ( username.split( ";" ).length < 2 )
            throw new ServiceException( messagesConfig.dataCorrupt );
        logger.debug( "Saliendo del metodo changePasswordValidation" );

	}


	@Override
	public User createUser(User user, String captcha)
    {
        logger.debug( "Entrando en el metodo createUser" );
        Authority authority;
		createUserValidation(user, captcha);
		
		if (userRepository.findByUsername(user.getUsername()) != null)
			throw new ServiceException( messagesConfig.usernameExist );

        if (userRepository.emailExist(user.getPerson().getEmail()))
            throw new ServiceException( messagesConfig.emailExist );
		
		user.setEnabled(true);
		user.setCreatedAt(new Date());
		try
        {
			user.setPassword(utilString.getHash(user.getPassword()));
            userRepository.save(user);
            authority = new Authority();
            authority.setUser( user );
            authority.setAuthority( Authority.Role.ROLE_ADMIN );
            authorityRepository.save(authority);

            sendMailRegister(user);
		}
        catch (Exception e)
        {
            logger.error("Error saving user into database", e);
			throw new ServiceException("Error saving user into database", e);
		}

        logger.debug( "Saliendo del metodo createUser" );
		return user;
	}


	@Override
	public User getUserByUsername(String username)
    {
        logger.debug( "Entrando en el metodo getUserByUsername" );
		try
        {
            logger.debug( "Saliendo del metodo getUserByUsername" );
			return userRepository.findByUsername(username);
		}
        catch (Exception e)
        {
            logger.error("Error reading user from database", e);
			throw new ServiceException("Error reading user from database", e);
		}
	}


    @Override
	public void changePassword(String username, String password)
    {
        logger.debug( "Entrando en el metodo changePassword" );
        changePasswordValidation( username, password );
        final long id = Long.parseLong( username.split( ";" )[ 0 ] );
        final User user = userRepository.findOne( id );

		if (user == null)
			throw new ServiceException( messagesConfig.userNotFound );

		try
        {
            user.setPassword( utilString.getHash(password) );
            userRepository.save( user );
		}
        catch (Exception e)
        {
            logger.error("Error saving user into database", e);
			throw new ServiceException("Error saving user into database", e);
		}
        logger.debug( "Saliendo del metodo changePassword" );
	}


	@Override
	public List<User> getUsers(Boolean activeUsers, Pageable pageable)
    {
        logger.debug( "Entrando en el metodo getUsers" );
        List<User> listUsers = null;
		try
        {
            listUsers = userRepository.findAllUsers( activeUsers, pageable );
		}
        catch (Exception e)
        {
            logger.error("Error reading users from database", e);
			throw new ServiceException("Error reading users from database", e);
		}
        logger.debug( "Saliendo del metodo getUsers" );
        return listUsers;
    }


    @Override
    public void retrieveUser( String email )
    {

        logger.debug( "Entrando en el metodo retrieveUser" );
        try
        {
            final Template template = freemarkerConfiguration.getTemplate("mail.ftl");
            final Map<String, Object> params = new HashMap<>();
            User user;
            String value;
            String pathPage;
            StringWriter output;
            MimeMessage msg;

            output = new StringWriter();
            user = userRepository.findByEmail( email );
            value = user.getId() + ";" + utilString.getHash( user.getUsername() );
            pathPage = pathRecover + value;
            params.put( "nombre", user.getPerson().getName() );
            params.put( "apellido", user.getPerson().getLastName() );
            params.put( "url", pathPage );
            template.process( params, output );

            msg = mailSender.createMimeMessage();
            msg.setRecipients( Message.RecipientType.TO, user.getPerson().getEmail() );
            msg.setSubject( "Recuperación Contraseña de Usuario CrowdEI" );
            msg.setText( output.toString(), "utf-8", "html");


            mailSender.send( msg );

        }
        catch ( NoSuchAlgorithmException e )
        {
            logger.error("Error armando la url de recuperacion", e);
            throw new ServiceException("Error armando la url de recuperacion", e);
        }
        catch ( NoResultException | TemplateException e )
        {
            logger.error(messagesConfig.userNotFound, e);
            throw new ServiceException( messagesConfig.userNotFound );
        }
        catch ( IOException | MessagingException e )
        {
            logger.error(messagesConfig.messagingError, e);
            throw new ServiceException( messagesConfig.messagingError );
        }
        logger.debug( "Saliendo del metodo retrieveUser" );
    }


    @Override
    public void setUserRepository(UserRepository obj)
    {
        logger.debug( "Entrando en el metodo setUserRepository" );
        this.userRepository = obj;
        logger.debug( "Saliendo del metodo setUserRepository" );
    }


    @Override
    public void updateUser( User user )
    {
        logger.debug( "Entrando en el metodo updateUser" );

        try
        {
            User obj = userRepository.findByUsername( user.getUsername() );
            user.getPerson().setId( obj.getPerson().getId() );
            personRepository.save( user.getPerson() );
            obj = userRepository.findByUsername( user.getUsername() );
            if(user.getPassword() != null && !obj.getPassword().equals(  utilString.getHash( user.getPassword())))
                obj.setPassword(  utilString.getHash( user.getPassword()) );
            userRepository.save( obj );
        }
        catch ( Exception e )
        {
            logger.error("Error editando al usuario", e);
            throw new ServiceException("Error editando al usuario", e);
        }
        logger.debug( "Saliendo del metodo updateUser" );
    }


    @Override
    public User validateUser( String username, String password )
    {
        logger.debug( "Entrando en el metodo validateUser" );
        User response;

        try
        {
            response = userRepository.findByUsername( username );
            if(!response.getPassword().equals(  utilString.getHash( password)  ))
                throw new ServiceException("Credenciales invalidas");


        }
        catch ( NoSuchAlgorithmException e )
        {
            logger.error("Credenciales invalidas", e);
            throw new ServiceException("Credenciales invalidas", e);
        }
        logger.debug( "Saliendo del metodo validateUser" );
        return response;
    }


    @Override
	public User getUser(Long id)
    {
        logger.debug( "Entrando en el metodo getUser" );
		try
        {
            logger.debug( "Saliendo del metodo getUser" );
			return userRepository.findOne(id);
		}
        catch (Exception e)
        {
            logger.error("Error reading user from database", e);
			throw new ServiceException("Error reading user from database", e);
		}
	}

    @Override
    public void activateUser(Long id) {
        logger.debug( "Entrando al metodo activateUser" );
        try
        {
            User user;
            user = userRepository.findOne( id );
            user.setEnabled(true);
            userRepository.save(user );
        }
        catch (Exception e)
        {
            logger.error( "Error reading changing status from database", e );
            throw new ServiceException("Error changing user status from database", e);
        }

        logger.debug( "Saliendo del metodo activateUser" );
    }

    @Override
    public void deactivateUser(Long id) {
        logger.debug( "Entrando al metodo deactivateUser" );
        try
        {
            User user;
            user = userRepository.findOne( id );
            user.setEnabled(false);
            userRepository.save(user );
        }
        catch (Exception e)
        {
            logger.error( "Error reading changing status from database", e );
            throw new ServiceException("Error changing user status from database", e);
        }

        logger.debug( "Saliendo del metodo deactivateUser" );
    }

    //endregion

    //region Metodos privados

    private void sendMailRegister(User user)
    {
        logger.debug( "Entrando en el metodo sendMailRegister" );
        try
        {
            final Template template = freemarkerConfiguration.getTemplate( "mailUserRegister.ftl" );
            final Map<String, Object> params = new HashMap<>();
            StringWriter output;
            MimeMessage msg;
            output = new StringWriter();

            params.put( "nombre", user.getPerson().getName() );
            params.put( "apellido", user.getPerson().getLastName() );
            template.process( params, output );

            msg = mailSender.createMimeMessage();
            msg.setRecipients( Message.RecipientType.TO, user.getPerson().getEmail() );
            msg.setSubject( "Confirmación Registro de Usuario CrowdEI" );
            msg.setText( output.toString(), "utf-8", "html" );
            mailSender.send( msg );

        }
        catch ( NoResultException | TemplateException e )
        {
            logger.error( messagesConfig.userNotFound, e );
            throw new ServiceException( messagesConfig.userNotFound );
        }
        catch ( IOException | MessagingException e )
        {
            logger.error(messagesConfig.messagingError, e);
            throw new ServiceException( messagesConfig.messagingError );
        }
        logger.debug( "Saliendo del metodo sendMailRegister" );
    }
    //enregion
}
