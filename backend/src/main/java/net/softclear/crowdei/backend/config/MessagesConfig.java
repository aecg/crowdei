package net.softclear.crowdei.backend.config;



import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  MessagesConfig
 * Descripcion:             Clase que posee la defincion de todos los mensajes de error del sistema
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@Component
public class MessagesConfig
{
    /**
     * Mensaje de validacion para que el usuario no sea null.
     */
    @Value( "${messages.userNoNull}" )
    public String userNotNull;

    /**
     * Mensaje de validacion para que el nombre usuario no se encuentre repetido.
     */
    @Value( "${messages.userNameExist}" )
    public String usernameExist;

    /**
     * Mensaje de validacion para que el nombre usuario no sea null.
     */
    @Value( "${messages.usernameNoNull}" )
    public String usernameNotNull;

    /**
     * Mensaje de validacion para que el password del usuario no se encuentre en null.
     */
    @Value( "${messages.passwordNoNull}" )
    public String passwordNotNull;

    /**
     * Mensaje de validacion para que la informacion del usuario no se encuentre en null.
     */
    @Value( "${messages.userDataNoNull}" )
    public String userDataNotNull;

    /**
     * Mensaje de validacion para que el correo del usuario no se encuentre en null.
     */
    @Value( "${messages.emailNotNull}" )
    public String emailNotNull;

    /**
     * Mensaje de validacion para que el nombre de pila del usuario no se encuentre en null.
     */
    @Value( "${messages.nameNotNull}" )
    public String nameNotNull;

    /**
     * Mensaje de validacion para que el apellido del usuario no se encuentre en null.
     */
    @Value( "${messages.lastNameNotNull}" )
    public String lastNameNotNull;

    /**
     * Mensaje de validacion para cuando el captcha de un error.
     */
    @Value( "${messages.captchaWrong}" )
    public String captchaWrong;

    /**
     * Mensaje de validacion para cuando no se consiga informacion del usuario.
     */
    @Value( "${messages.userNotFound}" )
    public String userNotFound;

    /**
     * Mensaje de validacion para cuando la informacion del usuario se encuentre corrupta.
     */
    @Value( "${messages.dataCorrupt}" )
    public String dataCorrupt;

    /**
     * Mensaje de error general.
     */
    @Value( "${messages.messagingError}" )
    public String messagingError;

    /**
     * Mensaje de error general.
     */
    @Value( "${path.webapplication}" )
    public String appPath;

    /**
     * Mensaje de error para cuando el email ya existe.
     */
    @Value ( "${messages.emailExist}" )
    public String emailExist;
}
