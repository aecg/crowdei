package net.softclear.crowdei.backend.web.rest.auth;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import net.softclear.crowdei.backend.config.SettingsConfig;
import net.softclear.crowdei.backend.model.jpa.Person;
import net.softclear.crowdei.backend.model.jpa.User;
import net.softclear.crowdei.backend.service.UserService;
import net.softclear.crowdei.backend.web.rest.web.users.to.userRequest.UserLoginRequest;
import net.softclear.crowdei.backend.web.rest.web.users.to.userRequest.UserRecoverRequest;
import net.softclear.crowdei.backend.web.rest.web.users.to.userRequest.UserRegisterRequest;
import net.softclear.crowdei.backend.web.rest.web.users.to.userResponse.UserLoginResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URL;


/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  OAuthController
 * Descripcion:             Servicio para todas las operaciones del oauth * @version                 1.0
 * @author                  guzmle
 * @since                   12/3/2015
 *
 */
@Controller
public class OAuthController
{
    @Autowired
    private TokenStore  tokenStore;
    @Autowired
    private UserService userService;
    @Autowired
    private SettingsConfig settingsConfig;


    /**
     * Nombre:                  logout.
     * Descripcion:             Servicio que realiza el logout de la aplicacion
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/22/2015
     *
     * @param request datos de la peticion
     */
    @RequestMapping( value = "/oauth/revoke-token", method = RequestMethod.GET )
    @ResponseStatus( HttpStatus.OK )
    public void logout( HttpServletRequest request )
    {
        String authHeader;
        String tokenValue;
        OAuth2AccessToken accessToken;

        authHeader = request.getHeader( "Authorization" );
        if ( authHeader != null )
        {
            tokenValue = authHeader.replace( "Bearer", "" ).trim();
            accessToken = tokenStore.readAccessToken( tokenValue );
            tokenStore.removeAccessToken( accessToken );
        }
    }


    /**
     * Nombre:                  register.
     * Descripcion:             Servicio que registra los datos de un usuario
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/3/2015
     *
     * @param user datos del usuario
     * @throws Exception la excepcion
     */
    @RequestMapping( value = "/oauth/register", method = RequestMethod.POST, consumes = "application/json" )
    @ResponseStatus( HttpStatus.OK )
    public void register( @RequestBody UserRegisterRequest user ) throws Exception
    {
        User obj;
        obj = new User();
        obj.setPassword( user.getPassword() );
        obj.setUsername( user.getUsername() );
        obj.setPerson( new Person() );
        obj.getPerson().setName( user.getName() );
        obj.getPerson().setLastName( user.getLastname() );
        obj.getPerson().setEmail( user.getEmail() );

        if (user.getEducation() != null)
            obj.getPerson().setEducation(user.getEducation());

        if (user.getInterests() != null)
            obj.getPerson().setInterests(user.getInterests());

        if (user.getImage() != null)
        {
            AWSCredentials credentials;
            byte[] imageBytes;
            URL url;
            url = new URL(user.getImage());
            // read image direct from url
            BufferedImage image;
            image = ImageIO.read(url);
            // write image to outputstream
            ByteArrayOutputStream baos;
            baos = new ByteArrayOutputStream();
            ImageIO.write(image, "jpg", baos);
            baos.flush();
            // get bytes
            imageBytes = baos.toByteArray();

            final String nameImageKey = user.getName() + "_avatar";
            final File file = File.createTempFile(nameImageKey, ".jpg");
            file.deleteOnExit();
            final OutputStream out = new FileOutputStream(file);
            out.write(imageBytes);
            out.close();
            credentials = new BasicAWSCredentials(settingsConfig.awsS3AccessKeyID, settingsConfig.awsS3SecretAccessKey);
            final AmazonS3 s3 = new AmazonS3Client(credentials);
            s3.putObject(new PutObjectRequest(settingsConfig.awsS3Bucket, nameImageKey, file));
            obj.getPerson().setImage(nameImageKey);
        }

        userService.createUser( obj, user.getCaptcha() );
    }


    /**
     * Nombre:                  login.
     * Descripcion:             Metodo que obtiene los datos del usuario que se encuentra logeado
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/22/2015
     *
     * @param user datos del usuario
     * @return Datos del usuario
     */
    @RequestMapping( value = "/oauth/login", method = RequestMethod.POST, consumes = "application/json",
            produces = "application/json")
    public @ResponseBody UserLoginResponse login( @RequestBody UserLoginRequest user )
    {
        User obj;
        obj = userService.validateUser( user.getUsername(), user.getPassword() );

        final UserLoginResponse response;
        response = new UserLoginResponse();
        response.setId( obj.getId() );
        response.setPerson( new UserLoginResponse.Person() );
        response.getPerson().setImage( obj.getPerson().getImage() );
        response.getPerson().setLastName( obj.getPerson().getLastName() );
        response.getPerson().setName( obj.getPerson().getName() );
        return response;
    }


    /**
     * Nombre:                  retrieveUser.
     * Descripcion:             Metodo que recupera los datos de un usuario
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/3/2015
     *
     * @param request nombre del usuario
     */
    @RequestMapping( value = "/oauth/recover_pass", method = RequestMethod.POST, consumes = "application/json" )
    @ResponseStatus( HttpStatus.OK )
    public void retrieveUser( @RequestBody UserRecoverRequest request )
    {
        userService.retrieveUser( request.getEmail() );
    }


    /**
     * Nombre:                  changePassword.
     * Descripcion:             Metodo que cambia el password de un usuario
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/3/2015
     *
     * @param request identficador del usuario (username)
     */
    @RequestMapping( value = "/oauth", method = RequestMethod.PUT, consumes = "application/json" )
    @ResponseStatus( HttpStatus.OK )
    public void changePassword( @RequestBody UserLoginRequest request )
    {
        userService.changePassword(request.getUsername(), request.getPassword());
    }

}
