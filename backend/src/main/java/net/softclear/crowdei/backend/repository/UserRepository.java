package net.softclear.crowdei.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import net.softclear.crowdei.backend.model.jpa.User;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  UserRepository
 * Descripcion:             Contrato para todas las operaciones de la base de datos que involucran la entidad usuario
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
public interface UserRepository extends JpaRepository<User, Long>, UserRepositoryCustom
{
    /**
     * Nombre:                  findByUsername.
     * Descripcion:             Metodo que consulta los datos del usuario dado el login del mismo
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     * @param username login del usuario
     * @return Datos del usuario
     */
	User findByUsername(String username);
}
