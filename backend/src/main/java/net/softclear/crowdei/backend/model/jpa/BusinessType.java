package net.softclear.crowdei.backend.model.jpa;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  BusinessType
 * Descripcion:             Clase que modela el objeto BussinessType de la base de daos
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/1/2015
 *
 */
@Entity
@Table(name="business_type")
@Setter
@Getter
public class BusinessType
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;

    @Column( name = "description", nullable = false )
    private String description;

}
