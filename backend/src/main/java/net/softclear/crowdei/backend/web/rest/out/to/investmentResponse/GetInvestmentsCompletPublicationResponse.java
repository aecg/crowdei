package net.softclear.crowdei.backend.web.rest.out.to.investmentResponse;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  GetInvestmentsCompletPublication
 * Descripcion:             Objeto que posee los atributos de respuesta cuando se consulta las inversiones
 * de un proyecto que ya finalizo su publicacion
 * @version                 1.0
 * @author                  buchyo
 * @since                   06/04/2016
 *
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetInvestmentsCompletPublicationResponse
{
    private Long             count;
    private List<Investment> investments;

    /**
     * Clase interna que posee la definicion del JSON para el objeto Investment.
     */
    @Data
    public static class Investment
    {
        private String nameProject;
        private Double percent;
        private Double amountInvested;
        private String emailUser;
        private String symbolCurrency;
        private String descriptionCurrency;
        private Long idProject;
        private Long idPerson;
    }
}
