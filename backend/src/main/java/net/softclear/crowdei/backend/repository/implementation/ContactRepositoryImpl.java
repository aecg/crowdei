package net.softclear.crowdei.backend.repository.implementation;

import net.softclear.crowdei.backend.model.jpa.Project;
import net.softclear.crowdei.backend.repository.ContactRepositoryCustom;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  ContactRepositoryImpl
 * Descripcion:             Clase que mimplementa el contrato para las operaciones personalizadas
 * para la entidad contact
 * @version                 1.0
 * @author                  buchyo
 * @since                   14/03/2016
 *
 */
public class ContactRepositoryImpl implements ContactRepositoryCustom
{
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Project> findAllXIdCompany(Long idCompany, String nameContact, Pageable pageable)
    {
        CriteriaBuilder criteriaBuilder;
        criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Project> criteriaQuery;
        criteriaQuery = criteriaBuilder.createQuery(Project.class);

        List<Predicate> and;
        and = new ArrayList<>();
        List<Predicate> or;
        or = new ArrayList<>();

        Root<Project> root;
        root = criteriaQuery.from( Project.class );

        Join contact;
        contact = root.join( "contact" );

        if ( nameContact != null && nameContact.length() > 0 )
        {
            or.add(criteriaBuilder.like(contact.<String>get("fullname"), "%" + nameContact + "%"));
        }
        if (idCompany != null && idCompany > 0)
            and.add(criteriaBuilder.equal(root.get("company"), idCompany));

        if (and.size() > 0)
            criteriaQuery.where(and.toArray(new Predicate[and.size()]));
        if (or.size() > 0)
            criteriaQuery.where(or.toArray(new Predicate[or.size()]));
        //if (and.size() > 0 && or.size() > 0)
        //    criteriaQuery.where(criteriaBuilder.and(and.toArray(new Predicate[and.size()])),
        //            criteriaBuilder.and(criteriaBuilder.or(or.toArray(new Predicate[or.size()]))));

        Query query;
        query = entityManager.createQuery(criteriaQuery);

        return query.getResultList();
    }


}
