package net.softclear.crowdei.backend.service;

import net.softclear.crowdei.backend.model.jpa.Currency;

import java.util.List;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  CurrencyService
 * Descripcion:             contrato para todos los servicios que involucran la entidad currency
 * @version                 1.0
 * @author                  buchyo
 * @since                   15/03/2016
 *
 */
public interface CurrencyService
{
    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  getAll
     * Descripcion:             Metodo que obtiene todos tipos de monedas
     * @version                 1.0
     * @author                  buchyo
     * @since                   15/03/2016
     *
     * @return lista de los tipos de moneda
     */
    List<Currency> getAll();
}
