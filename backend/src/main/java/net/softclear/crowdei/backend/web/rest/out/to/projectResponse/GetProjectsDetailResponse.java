package net.softclear.crowdei.backend.web.rest.out.to.projectResponse;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import net.softclear.crowdei.backend.model.types.CrowdfundingType;
import net.softclear.crowdei.backend.model.types.Priority;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  GetProjectsXIdResponse
 * Descripcion:             Objeto que posee los atributos de respuesta cuando se consulta los projectos por id
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/4/2015
 *
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetProjectsDetailResponse
{
    private Long           id;
    private String         personType;
    private String         name;
    private String         description;
    private String		   briefDescription;
    private Date           dateRegister;
    private double         objetive;
    private String         status;
    //private Long           idPerson;
    private String         generalObjetive;
    private String         specificObjetives;
    private int            monthExecution;
    private String         productResult;
    private String         salesEarning;
    private String         finalComment;
    private String         companyShares;
    private String         minimumInvesment;
    private String         invesmentGoal;
    //private String         monthlyInterest;
    private String         anotherIncentive;
    private Priority	   priorityType;
    private CrowdfundingType crowdfundingType;
    private String executionPlace;

    private String         image;
    private String         video;
    private String         canvasModel;

    private Company        company;
    private Contact contact;
    private Location location;
    private Currency currency;
    private ImpactSector impactSector;
    private EconomicActivity economicActivity;
    private Person	person;

    private List<Document> documents;
    private List<Calendar> calendars;
    private List<Resource> resources;
    private List<Reward>   rewards;

    //Calculados
    private Double         obtained;
    private int            investors;
    private Double         percent;
    private Long           leftDays;
    private Double         lastInvestment;
    private String         dateLastInvestment;

    private Date   limitDate;
    private InternetInfo internetInformation;
    private ContactInfo contactInfo;

    /**
     * Clase interna que posee la definicion del JSON para el objeto company.
     */
    @Data
    public static class Company
    {
        private Long           id;
        private String         rfc;
        private String         name;
    }

    /**
     * Clase interna que posee la defincion para la entidad de location.
     */
    @Data
    public static class Location
    {
        private Long id;
        private String address;
        private String municipality;
        private String postalCode;
        private String locationReference;
        private City   city;
    }

    /**
     * Clase interna que posee la defincion del JSON para la ciudad.
     */
    @Data
    public static class City
    {
        private Long   id;
        private String name;
        private State state;

    }


    /**
     * Clase interna que posee la defincion del JSON para el estado.
     */
    @Data
    public static class State
    {
        private Long   id;
        private String name;
        private Country country;

    }


    /**
     * Clase interna que posee la defincion del JSON para pais.
     */
    @Data
    public static class Country
    {
        private Long   id;
        private String name;
        private String iso;
    }

    /**
     * Clas interna que posee la definicion del JSON para el objeto currency.
     */
    @Data
    public static class Currency
    {
        private Long           id;
        private String description;
        private String symbol;
        private String code;
    }

    /**
     * Clas interna que posee la definicion del JSON para el objeto impactSector.
     */
    @Data
    public static class ImpactSector
    {
        private Long           id;
        private String description;
    }

    /**
     * Clas interna que posee la definicion del JSON para el objeto economicActivity.
     */
    @Data
    public static class EconomicActivity
    {
        private Long           id;
        private String description;
    }

    /**
     * Clas interna que posee la definicion del JSON para el objeto documento.
     */
    @Data
    public static class Document
    {
        private Long           id;
        private byte[]         file;
        private String         name;
    }


    /**
     * Clas interna que posee la definicion del JSON para el objeto calendar.
     */
    @Data
    public static class Calendar
    {
        private Long id;
        private String activity;
        private Date estimatedDate;
    }

    /**
     * Clas interna que posee la definicion del JSON para el objeto resource.
     */
    @Data
    public static class Resource
    {
        private Long id;
        private String concept;
        private Double amount;
        private Integer percentage;
        private Date estimatedDate;
    }

    /**
     * Clase interna que posee la defincion para la entidad de contacto.
     */
    @Data
    public static class Contact
    {
        private Long id;
        private String fullname;
        private String photo;
    }

    /**
     * Clase interna que posee la defincion para la entidad de contactInformation.
     */
    @Data
    public static class ContactInfo
    {
        private Long id;
        private String phoneOne;
        private String phoneTwo;
        private String mobile;
        private String email;
    }

    /**
     * Clase interna que posee la definicion del JSON para el objeto Person.
     */
    @Data
    public static class Person
    {
        private Long id;
        private String name;
        private String lastName;
    }

    /**
     * Clase interna que posee la definicion del JSON para el objeto Reward.
     */
    @Data
    public static class Reward
    {
    	private Long id;
        private String description;
        private double amount;
        private int available;
        private Date estimatedDate;
        private int quantity;
        private int investors;
    }

    /**
     * Clase interna que posee la definicion del JSON para el objeto InternetInfo.
     */
    @Data
    public static class InternetInfo
    {
        private Long id;
        private String webPage;
        private String linkedin;
        private String facebook;
        private String twitter;
    }
}
