package net.softclear.crowdei.backend.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  CorsFilter
 * Descripcion:             Clase que configura el tipo de peticion que se puede atender
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/21/2015
 *
 */
public class CorsFilter implements Filter
{


	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException
    {
		HttpServletResponse httpResponse;
        HttpServletRequest httpRequest;

        httpResponse = (HttpServletResponse) response;
		httpRequest = (HttpServletRequest) request;
		
		httpResponse.setHeader("Access-Control-Allow-Origin", "*");
		httpResponse.setHeader("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
		httpResponse.setHeader("Access-Control-Max-Age", "3600");
		httpResponse.setHeader("Access-Control-Allow-Headers", "authorization,"
        		+ "content-type,"
        		+ "content-length,"
        		+ "accept");

	    if ("OPTIONS".equalsIgnoreCase(httpRequest.getMethod()))
            httpResponse.setStatus( HttpServletResponse.SC_OK );
        else
            chain.doFilter( request, response );
	}


	@Override
	public void destroy()
    {
    }


	@Override
	public void init(FilterConfig arg0) throws ServletException
    {
    }

}
