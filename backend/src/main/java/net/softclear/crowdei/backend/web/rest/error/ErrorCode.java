package net.softclear.crowdei.backend.web.rest.error;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  ErrorCode
 * Descripcion:             Enumerado que permite asignar un codigo de error a una excepcion
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
public enum ErrorCode
{
    /**
     * codigo de error para cualquier excepcion.
     */
    SERVICE_ERROR( 100L ),

    /**
     * codigo de error para una excepcion generica.
     */
    GENERIC_ERROR( 200L );

    private Long value;


    ErrorCode( Long code )
    {
        this.value = code;
    }


    public Long getValue()
    {
        return value;
    }
}
