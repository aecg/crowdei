package net.softclear.crowdei.backend.web.rest.web.projects.to.projectRequest;



import lombok.Data;

import java.util.Date;
import java.util.List;


/**
 * Sistema:                 CrowdEI.
 *                                                              
 * Nombre:                  ChangePriorityProjectRequest
 * Descripcion:             Metodo que posee la defincion del JSON para la peticion de modifcacion de proyecto
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@Data
public class ChangePriorityProjectRequest
{
    private Long   id;
    private String priorityType;
}
