package net.softclear.crowdei.backend.web.rest.error.to;

import lombok.AllArgsConstructor;
import lombok.Data;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  ErrorResponse
 * Descripcion:             Clase que posee la definicion del error en formato JSON
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@Data
@AllArgsConstructor
public class ErrorResponse
{
	private Long code;
	private String message;
}
