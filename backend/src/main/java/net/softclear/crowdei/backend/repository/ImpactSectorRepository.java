package net.softclear.crowdei.backend.repository;

import net.softclear.crowdei.backend.model.jpa.ImpactSector;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  ImpactSectorRepository
 * Descripcion:             Contrato para todas las operaciones de la base de datos que involucran la entidad
 * @version                 1.0
 * @author                  buchyo
 * @since                   15/03/2016
 *
 */
public interface ImpactSectorRepository extends JpaRepository<ImpactSector, Long>
{
}
