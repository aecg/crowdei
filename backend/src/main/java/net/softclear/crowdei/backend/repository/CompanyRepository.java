package net.softclear.crowdei.backend.repository;



import net.softclear.crowdei.backend.model.jpa.Company;
import org.springframework.data.jpa.repository.JpaRepository;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  BusinessTypeRepository
 * Descripcion:             Contrato para todas las operaciones de la base de datos que involucran la entidad
 * businessType
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
public interface CompanyRepository extends JpaRepository<Company, Long>, CompanyRepositoryCustom
{
}
