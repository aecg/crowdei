package net.softclear.crowdei.backend.service;

import net.softclear.crowdei.backend.model.jpa.EconomicActivity;

import java.util.List;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  EconomicActivityService
 * Descripcion:             Contrato para todos los servicios que involucran la entidad economicActivity
 * @version                 1.0
 * @author                  buchyo
 * @since                   15/03/2016
 *
 */
public interface EconomicActivityService
{
    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  EconomicActivityService
     * Descripcion:             Metodo que obtiene todas las actividades economicas
     * @version                 1.0
     * @author                  buchyo
     * @since                   15/03/2016
     *
     * @return lista de las actividades economicas
     */
    List<EconomicActivity> getAll();
}
