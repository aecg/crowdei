package net.softclear.crowdei.backend.web.rest.web.users.to.userRequest;



import lombok.Data;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  UserRegisterRequest
 * Descripcion:             Clase que con la definicion JSON para la peticion del registro del usuario
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@Data
public class UserRegisterRequest
{
    private String name;
    private String username;
    private String password;
    private String email;
    private String lastname;
    private String captcha;
    private String image;
    private String education;
    private String interests;
}
