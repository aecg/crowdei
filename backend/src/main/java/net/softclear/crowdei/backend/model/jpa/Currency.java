package net.softclear.crowdei.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  Currency
 * Descripcion:             Clase para modelar la moneda
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@Entity
@Table(name="currency")
@Setter
@Getter
public class Currency
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;

    @Column( name = "description", nullable = false )
    private String description;

    @Column( name = "symbol", nullable = false )
    private String symbol;

    @Column( name = "code", nullable = false )
    private String code;
}
