package net.softclear.crowdei.backend.web.rest.web.projects.to.projectResponse;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Date;
import java.util.List;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  GetProjectsXIdResponse
 * Descripcion:             Objeto que posee los atributos de respuesta cuando se consulta los projectos por id
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/4/2015
 *
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetProjectsXIdResponse
{
    private Long count;
    private List<Project> projects;


    /**
     * Clase interna que posee la definicion del JSON para el objeto Proyecto.
     */
    @Data
    public static class Project
    {
        private Long id;
        private String name;
        private String description;
		private String briefDescription;
        private Date dateRegister;
        private double objetive;
        private String image;
        private Double obtained;
        private int investors;
        private Double percent;
        private Long leftDays;
        private String status;
        private Currency currency;
        private Location location;
        private String personType;
        private String crowdfundingType;
        private Contact contact;
        private Company company;
        private Date   limitDate;
    }

    /**
     * Clase que posee la definicion de objeto json de contact que recibe el servicio de crear proyecto.
     */
    @Data
    public static class Contact
    {
        private Long id;
        private String fullname;
        private String photo;
    }

    /**
     * Clase que posee la definicion de objeto json de company que recibe el servicio de crear proyecto.
     */
    @Data
    public static class Company
    {
        private Long id;
        private String rfc;
        private String name;
    }

    /**
     * Clas interna que posee la definicion del JSON para el objeto currency.
     */
    @Data
    public static class Currency
    {
        private Long           id;
        private String description;
        private String symbol;
        private String code;
    }

    /**
     * Clase interna que posee la defincion para la entidad de location.
     */
    @Data
    public static class Location
    {
        private Long id;
        private City   city;
    }

    /**
     * Clase interna que posee la defincion del JSON para la ciudad.
     */
    @Data
    public static class City
    {
        private Long   id;
        private String name;
        private State state;

    }


    /**
     * Clase interna que posee la defincion del JSON para el estado.
     */
    @Data
    public static class State
    {
        private Long   id;
        private String name;
        private Country country;

    }


    /**
     * Clase interna que posee la defincion del JSON para pais.
     */
    @Data
    public static class Country
    {
        private Long   id;
        private String name;
        private String iso;
    }
}
