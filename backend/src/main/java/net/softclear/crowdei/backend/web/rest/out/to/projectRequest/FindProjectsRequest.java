package net.softclear.crowdei.backend.web.rest.out.to.projectRequest;



import lombok.Data;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  FindProjectsRequest
 * Descripcion:             Objeto que posee la definicion del JSON que es necesario para consultar proyectos
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@Data
public class FindProjectsRequest
{
    private String nameCompany;
    private String nameProject;
    private Long idEconomicActivity;
}
