package net.softclear.crowdei.backend.util;



import lombok.Getter;
import lombok.Setter;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  MercadoPagoUtils
 * Descripcion:             Clase que permite el acceso a la informacion de mercado pago
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/29/2015
 *
 */
@Setter
@Getter
public class MercadoPagoUtils
{
    private String token;
    private Double amount;
    private String description;
    private String currencyId;
    private String userName;
    private String email;
    private String paymentMethodId;
    private String successUrl;
    private String pendingUrl;
    private String cancelUrl;
    private String data;
    private Long rewardId;
}
