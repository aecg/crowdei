package net.softclear.crowdei.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;


/**
 * Sistema:                 Crowdei.
 *                                                              
 * Nombre:                  Calendar                                                    
 * Descripcion:             Clase para modela la entidad Calendar
 * @version                 1.0
 * @author                  buchyo
 * @since                   03/03/2016
 *
 */
@Entity
@Table(name = "calendar")
@Setter
@Getter
public class Calendar
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;

    @Column( name = "activity", nullable = true )
    private String activity;

    @Column( name = "estimated_date", nullable = true )
    @Temporal( TemporalType.TIMESTAMP )
    private Date estimatedDate;

    @ManyToOne()
    @JoinColumn( name = "project_id", nullable=false )
    private Project project;
}

