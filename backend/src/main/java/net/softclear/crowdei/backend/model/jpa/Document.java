package net.softclear.crowdei.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  Document
 * Descripcion:             Clase para modelar los datos del documento
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@Entity
@Table(name="document")
@Setter
@Getter
public class Document
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;

    @Column( name = "file", nullable = false, columnDefinition="mediumblob" )
    private byte[] file;

    @Column( name = "name", nullable = false )
    private String name;

    @ManyToOne()
    @JoinColumn( name = "project_id", nullable=false )
    private Project project;
}
