package net.softclear.crowdei.backend.web.rest.payments.to.request;



import lombok.Data;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  MercadoPagoRequest
 * Descripcion:             Clase que modela la peticion del pago por medio de mercadopago
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/29/2015
 *
 */
@Data
public class TransfererRequest
{
    private String token;
    private Double amount;
    private Long rewardId;
}
