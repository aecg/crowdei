package net.softclear.crowdei.backend.service;

import net.softclear.crowdei.backend.model.jpa.ImpactSector;

import java.util.List;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  ImpactSectorService
 * Descripcion:             Contrato para todos los servicios que involucran la entidad impactSector
 * @version                 1.0
 * @author                  buchyo
 * @since                   15/03/2016
 *
 */
public interface ImpactSectorService
{
    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  getAll
     * Descripcion:             Metodo que obtiene todas los sectores de impacto
     * @version                 1.0
     * @author                  buchyo
     * @since                   15/03/2016
     *
     * @return lista de los sectores de impacto
     */
    List<ImpactSector> getAll();
}
