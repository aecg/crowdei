	package net.softclear.crowdei.backend.web.rest.web.investments;



import net.softclear.crowdei.backend.exception.ServiceException;
import net.softclear.crowdei.backend.model.jpa.Investment;
import net.softclear.crowdei.backend.model.jpa.Project;
import net.softclear.crowdei.backend.model.types.Status;
import net.softclear.crowdei.backend.service.InvestmentService;
import net.softclear.crowdei.backend.service.ProjectService;
import net.softclear.crowdei.backend.util.CollectionUtils;
import net.softclear.crowdei.backend.web.rest.web.investments.to.investmentRequest.InvestmentTransferMyProjectRequest;
import net.softclear.crowdei.backend.web.rest.web.investments.to.investmentResponse.GetInvestmentsResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.GET;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


    /**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  InvestmentsRestController
 * Descripcion:             Clase que posee la definicion y la implementacion de los servicios
 * @version                 1.0
 * @author                  yoly
 * @since                   01/21/2016
 *
 */
@RestController
@RequestMapping("web/investments*")
public class InvestmentsRestController
{
    //region Atributos

    @Autowired
    private InvestmentService investmentService;
    @Autowired
    private ModelMapper       modelMapper;
    @Autowired
    private ProjectService projectService;


    //endegion

    //region Services


    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  getInvestmentsXIdProject
     * Descripcion:             Servicio que obtiene las inversiones
     * @version 1.0
     * @author buchyo
     * @since 21/01/2016
     *
     * @param idUser identificador del usuario
     * @param idProject identificador del proyecto
     * @param page pagina de la informacion
     * @param size cantidad de registro por pagina
     * @return Objeto que posee la informacion en formato JSON
     */
    @RequestMapping( method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    GetInvestmentsResponse getInvestmentsXIdProjectXIdUser( @RequestParam( required = false ) Long idUser,
            @RequestParam( required = false ) Long idProject, @RequestParam( required = false ) Integer page,
            @RequestParam( required = false ) Integer size )
    {
        Pageable pageable = null;
        GetInvestmentsResponse.Investment obj;
        List<Investment> listInvestment;
        GetInvestmentsResponse response;
        List<GetInvestmentsResponse.Investment> investmentResponse;

        GetInvestmentsResponse.Project objProject = new GetInvestmentsResponse.Project();

        if (idProject != null) {
            Project project = projectService.getProject(idProject);
            objProject = modelMapper.map(project, GetInvestmentsResponse.Project.class);
        }

        if ( page != null && size != null )
            pageable = new PageRequest( page, size );

        listInvestment = investmentService.getInvestmentsXIdProjectXIdUser( idUser, idProject, pageable );
        response = new GetInvestmentsResponse();

        if ( listInvestment != null )
        {

            List<Investment> activeInvestment = listInvestment.stream().filter(p -> p.getStatus().equals(Status.ACTIVE)).collect(Collectors.toList());

            investmentResponse = new ArrayList<>();

            for ( Investment investment : activeInvestment )
            {
                obj = modelMapper.map( investment, GetInvestmentsResponse.Investment.class );
                obj.setProject(objProject);
                investmentResponse.add( obj );
            }

            List<Investment> distinctProjects = activeInvestment.stream().filter(CollectionUtils.distinctByKey(p -> p.getProject().getId())).collect(Collectors.toList());

            response.setInvestments( investmentResponse );
            response.setCount( new Long(investmentResponse.size()) );
            response.setCountProjects( new Long(distinctProjects.size()) );
        }

        return response;
    }

    /**
     * Sistema:                 Crowdei.
     *                                                              
     * Nombre:                  investmentTransferMyProject                                                    
     * Descripcion:             Método que envia un correo a la persona contacto, estrategias integrales y la
     * persona que hizo la transferencia
     * @version                 1.0
     * @author                  buchyo
     * @since                   13/04/2016
     *
     * @param request
     */
    @RequestMapping( value = "/investmentTransferMyProject", method = RequestMethod.POST, produces = "application/json",
            consumes = "application/json" )
    @ResponseStatus( HttpStatus.OK )
    public void investmentTransferMyProject( @RequestBody InvestmentTransferMyProjectRequest request )
    {
        investmentService.sendMailInvestmentTransferMyProject(request.getIdTransfer());
    }


    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  getInvestmentsXIdProject
     * Descripcion:             Servicio que obtiene las inversiones
     * @version 1.0
     * @author jamaris
     * @since 03/03/2017
     *
     * @param idProject identificador del proyecto
     * @param page pagina de la informacion
     * @param size cantidad de registro por pagina
     * @return Objeto que posee la informacion en formato JSON
     */
    @RequestMapping( value = "/investmentXIdProject",  method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    GetInvestmentsResponse getInvestmentsXIdProject( @RequestParam( required = false ) Long idProject,
                                                     @RequestParam( required = false ) Integer page,
                                                     @RequestParam( required = false ) Integer size )
    {
        Pageable pageable = null;
        GetInvestmentsResponse.Investment obj;
        List<Investment> listInvestment;
        GetInvestmentsResponse response;
        List<GetInvestmentsResponse.Investment> investmentResponse;

        GetInvestmentsResponse.Project objProject = new GetInvestmentsResponse.Project();

        if (idProject != null) {
            Project project = projectService.getProject(idProject);
            objProject = modelMapper.map(project, GetInvestmentsResponse.Project.class);
        }

        if ( page != null && size != null )
            pageable = new PageRequest( page, size );

        listInvestment = investmentService.getInvestmentsXIdProject( idProject, pageable );
        response = new GetInvestmentsResponse();

        if ( listInvestment != null )
        {
            investmentResponse = new ArrayList<>();

            for ( Investment investment : listInvestment )
            {
                obj = modelMapper.map( investment, GetInvestmentsResponse.Investment.class );
                obj.setProject(objProject);
                investmentResponse.add( obj );
            }

            response.setInvestments( investmentResponse );
            response.setCount( new Long(investmentResponse.size()) );
        }

        return response;
    }

    @RequestMapping( value = "/activate/{id}", method = RequestMethod.POST )
    @ResponseStatus( HttpStatus.OK )
    public void activateTransfer( @PathVariable long id )
    {
        try
        {
            investmentService.activateTransfer( id );
        }
        catch (Exception e)
        {
            throw new ServiceException("Error activating transfer investment into database", e);
        }
    }
    //endregion

    //region metodos privados

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  cambiarFormato
     * Descripcion:             Método que cambia el fotmarto ##,## a ##.##
     * @version                 1.0
     * @author                  buchyo
     * @since                   21/01/2016
     *
     * @param numero
     * @return
     */
    private double cambiarFormato(double numero)
    {
        DecimalFormatSymbols simbolo;
        simbolo = new DecimalFormatSymbols();
        simbolo.setDecimalSeparator('.');
        DecimalFormat df;
        df = new DecimalFormat("##########.##",simbolo);
        double retorno;

        retorno = Double.parseDouble(df.format(numero));

        return retorno;
    }

    //endregion
}
