package net.softclear.crowdei.backend.service.implementation;


import net.softclear.crowdei.backend.model.jpa.Currency;
import net.softclear.crowdei.backend.repository.CurrencyRepository;
import net.softclear.crowdei.backend.service.CurrencyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  CurrencyServiceImpl
 * Descripcion:             Clase que implementa el contrato del servicio
 * @version                 1.0
 * @author                  buchyo
 * @since                   15/03/2016
 *
 */
@Service
@Transactional
public class CurrencyServiceImpl implements CurrencyService
{
    //region Atributos

    private static Logger logger = LoggerFactory.getLogger(ExperienceLevelServiceImpl.class);
    @Autowired
    private CurrencyRepository repository;

    //endregion
    @Override
    public List<Currency> getAll()
    {
        logger.debug( "Entrando al metodo getAll" );
        List<Currency> retorno;
        retorno = repository.findAll();
        logger.debug( "Saliendo del metodo getAll" );
        return retorno;
    }
}
