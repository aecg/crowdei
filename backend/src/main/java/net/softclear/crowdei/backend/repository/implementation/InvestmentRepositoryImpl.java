package net.softclear.crowdei.backend.repository.implementation;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Pageable;

import net.softclear.crowdei.backend.model.jpa.Investment;
import net.softclear.crowdei.backend.model.types.Status;
import net.softclear.crowdei.backend.repository.InvestmentRepositoryCustom;



/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  InvestmentRepositoryImpl
 * Descripcion:             Clase que mimplementa el contrato para las operaciones personalizadas
 * para la entidad investment
 * @version                 1.0
 * @author                  buchyo
 * @since                   21/01/2016
 *
 */
public class InvestmentRepositoryImpl implements InvestmentRepositoryCustom
{
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Investment> findAll( Long idUser, Long idProject, Pageable pageable )
    {
        CriteriaBuilder criteriaBuilder;
        CriteriaQuery<Investment> criteriaQuery;
        Root<Investment> root;
        List<Investment> retorno;

        criteriaBuilder = entityManager.getCriteriaBuilder();
        criteriaQuery = criteriaBuilder.createQuery( Investment.class );

        root = criteriaQuery.from( Investment.class );

        if (idProject != null)
        {
            criteriaQuery.where( criteriaBuilder.and( criteriaBuilder.equal( root.get( "person" ), idUser ),
                    criteriaBuilder.equal( root.get( "project" ), idProject ) ) );
        }
        else
        {
            criteriaQuery.where( criteriaBuilder.equal( root.get( "person" ), idUser ) );
        }
        criteriaQuery.orderBy( criteriaBuilder.desc( root.get( "date" ) ) );

        Query query;
        query = entityManager.createQuery( criteriaQuery );

        retorno = query.getResultList();

        return retorno;
    }

    @Override
    public List<Investment> findAll(Long idProject, Pageable pageable)
    {
        CriteriaBuilder criteriaBuilder;
        CriteriaQuery<Investment> criteriaQuery;
        Root<Investment> root;
        List<Investment> retorno;

        criteriaBuilder = entityManager.getCriteriaBuilder();
        criteriaQuery = criteriaBuilder.createQuery( Investment.class );

        root = criteriaQuery.from( Investment.class );

        if (idProject != null)
        {
            criteriaQuery.where( criteriaBuilder.equal( root.get( "project" ), idProject ) );
        }
        criteriaQuery.orderBy( criteriaBuilder.desc( root.get( "date" ) ) );

        Query query;
        query = entityManager.createQuery( criteriaQuery );

        retorno = query.getResultList();

        return retorno;
    }

    @Override
    public List<Investment> findAllCompletPublication()
    {
        CriteriaBuilder criteriaBuilder;
        CriteriaQuery<Investment> criteriaQuery;
        Root<Investment> root;
        List<Investment> retorno;
        Join project;

        criteriaBuilder = entityManager.getCriteriaBuilder();
        criteriaQuery = criteriaBuilder.createQuery(Investment.class);

        criteriaBuilder = entityManager.getCriteriaBuilder();
        criteriaQuery = criteriaBuilder.createQuery( Investment.class );

        root = criteriaQuery.from( Investment.class );
        project = root.join( "project" );

        CriteriaQuery<Investment> where;
        where = criteriaQuery.where(criteriaBuilder.and(criteriaBuilder.equal(project.get("status"), Status.ACTIVE ),
                criteriaBuilder.equal(root.get("status"), Status.ACTIVE)));

        Query query;
        query = entityManager.createQuery( criteriaQuery );

        retorno = query.getResultList();

        return retorno;
    }

}
