package net.softclear.crowdei.backend.web.rest.web.projects.to.projectResponse;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Date;
import java.util.List;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  GetFindProjectsResponse
 * Descripcion:             Clase que posee la definicion del JSON de respuesta del servicio que consulta los proyectos
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetFindProjectsResponse
{
    private List<Project> projects;


    /**
     * Clase interna que posee la definicion del JSON para la data de proyecto.
     */
    @Data
    public static class Project
    {
        private Long id;
        private String name;
        private String description;
        private Date dateStart;
        private Date dateEnd;
        private String objetive;
        private String idea;
        private String market;
        private String person;
        private String finance;
        private String exitStrategy;
        private String reward;
        private String status;
    }
}
