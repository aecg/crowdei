package net.softclear.crowdei.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  Contact
 * Descripcion:             Clase para modela la entidad contacto
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@Entity
@Table(name="contact")
@Setter
@Getter
public class Contact
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;

    @Column( name = "fullname", nullable = false )
    private String fullname;

    @Column( name = "photo", nullable = true )
    private String photo;

    @ManyToOne(cascade= CascadeType.ALL)
    @JoinColumn( name = "contact_information_id" )
    private ContactInformation contactInformation;

    @ManyToOne(cascade= CascadeType.ALL )
    @JoinColumn( name = "location_id" )
    private Location location;

    @ManyToOne(cascade= CascadeType.ALL)
    @JoinColumn( name = "internet_info_id" )
    private InternetInfo internetInformation;

}
