package net.softclear.crowdei.backend.repository;



import net.softclear.crowdei.backend.model.jpa.Calendar;
import org.springframework.data.jpa.repository.JpaRepository;



/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  CalendarRepository
 * Descripcion:             Contrato para todas las operaciones de la base de datos que involucran la entidad
 * @version                 1.0
 * @author                  buchyo
 * @since                   07/03/2016
 *
 */
public interface CalendarRepository extends JpaRepository<Calendar, Long>
{
}
