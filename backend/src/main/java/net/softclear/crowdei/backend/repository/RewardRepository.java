package net.softclear.crowdei.backend.repository;



import net.softclear.crowdei.backend.model.jpa.Reward;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  RewardRepository
 * Descripcion:             Contrato para todas las operaciones de la base de datos que involucran la entidad
 * @version                 1.0
 * @author                  buchyo
 * @since                   07/03/2016
 *
 */
public interface RewardRepository extends JpaRepository<Reward, Long>
{
}
