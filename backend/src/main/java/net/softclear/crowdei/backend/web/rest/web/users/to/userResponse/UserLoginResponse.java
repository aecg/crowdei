package net.softclear.crowdei.backend.web.rest.web.users.to.userResponse;



import lombok.Data;

import java.util.List;


/**
 * Sistema:                 CrowdEI.
 *                                                              
 * Nombre:                  UserLoginResponse
 * Descripcion:             Clase con la definicion de la respuesta JSON del usuario logeado
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@Data
public class UserLoginResponse
{
    private long id;
    private Person person;
    private List<String> authorities;

    /**
     * Clase interna que posee la defincion del JSON para la entidad Person.
     */
    @Data
    public static class Person
    {
        private long id;
        private String     name;
        private String     lastName;
        private String     email;
        private String     image;
    }
}
