package net.softclear.crowdei.backend.web.rest.web.util;

import net.softclear.crowdei.backend.model.jpa.HowFind;
import net.softclear.crowdei.backend.service.HowFindService;
import net.softclear.crowdei.backend.web.rest.web.util.to.GetObjectMasterResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  UserRestController
 * Descripcion:             Clase que posee la definicion y la implementacion de los servicios que
 * involucran a la entidad usuario
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/4/2015
 *
 */
@RestController
@RequestMapping("/web/utils*")
public class HowFindRestController
{
    //region Attributes

    @Autowired
    private ModelMapper    modelMapper;
    @Autowired
    private HowFindService service;

    //endregion

    //region Services


    /**
     * Nombre:                  getAllCountries.
     * Descripcion:             Metodo que obtiene todos los paises almacenados en la base de datos
     * @version 1.0
     * @author guzmle
     * @since 12/9/2015
     *
     * @return lista de paises
     */
    @RequestMapping( value = "/how-find", method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    GetObjectMasterResponse getAll()
    {
        List<HowFind> list;
        List<GetObjectMasterResponse.Item> listResponse;
        GetObjectMasterResponse.Item item;
        GetObjectMasterResponse response;

        list = service.getAll();
        response = new GetObjectMasterResponse();

        if ( list != null )
        {
            listResponse = new ArrayList<>();
            for ( HowFind obj : list )
            {
                item = modelMapper.map( obj, GetObjectMasterResponse.Item.class );
                listResponse.add( item );
            }

            response.setList( listResponse );
        }
        return response;
    }

    //endregion
}
