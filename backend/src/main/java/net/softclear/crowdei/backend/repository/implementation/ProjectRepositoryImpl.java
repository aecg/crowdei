package net.softclear.crowdei.backend.repository.implementation;

import net.softclear.crowdei.backend.model.jpa.Project;
import net.softclear.crowdei.backend.model.types.Priority;
import net.softclear.crowdei.backend.model.types.Status;
import net.softclear.crowdei.backend.repository.ProjectRepositoryCustom;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  ProjectRepositoryImpl
 * Descripcion:             Clase que mimplementa el contrato para las operaciones personalizadas
 * para la entidad proyecto
 * @version                 1.0
 * @author                  buchyo
 * @since                   12/03/2015
 *
 */
public class ProjectRepositoryImpl implements ProjectRepositoryCustom
{
    @PersistenceContext
    private EntityManager entityManager;

    static final int LIMIT = 3;

    /**
     * Nombre:                  getPredicates.
     * Descripcion:             Metodo que arma los predicados
     * @version                 1.0
     * @author                  buchyo
     * @since                   12/03/2015
     *
     * @param root objeto principal para armar la consulta
     * @param criteriaBuilder objeto que posee la definicion de la consulta
     * @param idUsuario id del usuario
     * @return arreglo de predicados
     */
    private Predicate[] getPredicates(Root<Project> root, CriteriaBuilder criteriaBuilder, Long idUsuario)
    {
        List<Predicate> predicates;
        predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.equal(root.get("person"),idUsuario));
        predicates.add(criteriaBuilder.equal(root.get("status"), Status.ACTIVE ));
        Predicate[] retorno;
        retorno = null;

        if (predicates.size() > 0) // adding predicates if any
            retorno = predicates.toArray(new Predicate[predicates.size()]);

        return retorno;
    }

    /**
     * Nombre:                  getPredicates.
     * Descripcion:             Metodo que arma los predicados
     * @version                 1.0
     * @author                  buchyo
     * @since                   12/03/2015
     *
     * @param root objeto principal para armar la consulta
     * @param company objeto join paara armar la consulta
     * @param criteriaBuilder objeto que posee la definicion de la consulta
     * @param nameCompany nombre de la compañia
     * @param nameProject nombre del objeto
     * @return arreglo de predicados
     */
    private Predicate[] getPredicates(Root<Project> root, Join company, CriteriaBuilder criteriaBuilder,
            String nameCompany, String nameProject, Long idEconomicActivity)
    {
        List<Predicate> predicates;
        Predicate[] retorno;
        predicates = new ArrayList<>();
        retorno = null;

        if (nameCompany != null && nameCompany.length() > 0)
            predicates.add(criteriaBuilder.like( company.<String>get( "name" ), "%" + nameCompany + "%" ));

        if(nameProject != null && nameProject.length() > 0)
            predicates.add(criteriaBuilder.like(root.<String>get("name"), "%" + nameProject + "%"));

        if(idEconomicActivity != null && idEconomicActivity != 0)
            predicates.add( criteriaBuilder.equal(root.get("economicActivity"), idEconomicActivity));

        predicates.add(criteriaBuilder.equal(root.get("status"), Status.ACTIVE ));
        if (predicates.size() > 0) // adding predicates if any
            retorno = predicates.toArray(new Predicate[predicates.size()]);

        return retorno;
    }


    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  getPredicates
     * Descripcion:             Metodo que arma los predicados
     * @version                 1.0
     * @author                  buchyo
     * @since                   04/01/2016
     *
     * @param root objeto principal para armar la consulta
     * @param criteriaBuilder objeto que posee la definicion de la consulta
     * @param id identificador del usuario
     * @param nameCompany nombre de la compañia
     * @param nameProject nombre del objeto
     * @return arreglo de predicados
     */
    private Predicate[] getPredicates(Root<Project> root, CriteriaBuilder criteriaBuilder, Long id, String
            nameCompany, String nameProject, Long idEconomicActivity)
    {
        List<Predicate> predicates;
        predicates = new ArrayList<>();
        Join company;
        company = root.join( "company" );
        Predicate[] retorno;
        retorno = null;

        if ( nameCompany != null && nameCompany.length() > 0 )
            predicates.add( criteriaBuilder.like( company.<String>get( "name" ), "%" + nameCompany + "%" ) );

        if ( nameProject != null && nameProject.length() > 0 )
            predicates.add( criteriaBuilder.like( root.<String>get( "name" ), "%" + nameProject + "%" ) );

        if(idEconomicActivity != null && idEconomicActivity != 0)
            predicates.add( criteriaBuilder.equal(root.get("economicActivity"), idEconomicActivity));

        predicates.add( criteriaBuilder.equal( root.get( "status" ), Status.ACTIVE ) );
        predicates.add( criteriaBuilder.equal( root.get( "person" ), id ) );

        if ( predicates.size() > 0 ) // adding predicates if any
            retorno = predicates.toArray( new Predicate[ predicates.size() ] );

        return retorno;
    }

    private Predicate[] getPredicatesForInvestment(Root<Project> root, CriteriaBuilder criteriaBuilder, Long id, String
            nameCompany, String nameProject, Date startDate, Date endDate)
    {
        List<Predicate> predicates;
        predicates = new ArrayList<>();
        Join investment;
        Join company;

        investment = root.join( "investments");
        company = root.join( "company" );
        Predicate[] retorno;
        retorno = null;

        if ( nameCompany != null && nameCompany.length() > 0 )
            predicates.add( criteriaBuilder.like( company.<String>get( "name" ), "%" + nameCompany + "%" ) );

        if ( nameProject != null && nameProject.length() > 0 )
            predicates.add( criteriaBuilder.like( root.<String>get( "name" ), "%" + nameProject + "%" ) );

        if ( startDate != null && endDate != null )
        {
            Expression<Date> fecha;
            fecha = investment.get( "date" );
            predicates.add( criteriaBuilder.between( fecha, startDate, endDate ) );
        }

        predicates.add( criteriaBuilder.equal( investment.get( "person" ), id ) );

        if ( predicates.size() > 0 ) // adding predicates if any
            retorno = predicates.toArray( new Predicate[ predicates.size() ] );

        return retorno;
    }

    @Override
    public List<Project> findAll(Long idUsuario, Pageable pageable)
    {
        CriteriaBuilder criteriaBuilder;
        criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Project> criteriaQuery;
        criteriaQuery = criteriaBuilder.createQuery(Project.class);

        Predicate[] predicates;
        predicates = getPredicates( criteriaQuery.from( Project.class ), criteriaBuilder, idUsuario);
        if (predicates != null)
            criteriaQuery.where(predicates);

        Query query;
        query = entityManager.createQuery(criteriaQuery);

        return query.getResultList();
    }


    @Override
    public List<Project> findAll(String nameCompany, String nameProject, Long idEconomicActivity,
            Pageable pageable)
    {
        CriteriaBuilder criteriaBuilder;
        CriteriaQuery<Project> criteriaQuery;
        Root<Project> root;
        Join company;

        criteriaBuilder = entityManager.getCriteriaBuilder();
        criteriaQuery = criteriaBuilder.createQuery(Project.class);

        root = criteriaQuery.from(Project.class);
        company = root.join("company");

        Predicate[] predicates;
        predicates = getPredicates(root, company, criteriaBuilder, nameCompany, nameProject, idEconomicActivity);
        if (predicates != null)
            criteriaQuery.where( criteriaBuilder.and( predicates ));

        Query query;
        query = entityManager.createQuery(criteriaQuery);
        return query.getResultList();
    }

    @Override
    public List<Project> findAll(Long id, String nameCompany, String nameProject, Long idEconomicActivity, Pageable
            pageable)
    {
        CriteriaBuilder criteriaBuilder;
        CriteriaQuery<Project> criteriaQuery;
        Root<Project> root;

        criteriaBuilder = entityManager.getCriteriaBuilder();
        criteriaQuery = criteriaBuilder.createQuery(Project.class);

        root = criteriaQuery.from(Project.class);

        Predicate[] predicates;
        predicates = getPredicates( root, criteriaBuilder, id, nameCompany, nameProject, idEconomicActivity );

        if (predicates != null)
            criteriaQuery.where( criteriaBuilder.and( predicates ));

        Query query;
        query = entityManager.createQuery(criteriaQuery);
        return query.getResultList();
    }

    @Override
    public List<Project> findAll()
    {
        CriteriaBuilder criteriaBuilder;
        CriteriaQuery<Project> criteriaQuery;
        Root<Project> root;
        List<Project> retorno;

        criteriaBuilder = entityManager.getCriteriaBuilder();
        criteriaQuery = criteriaBuilder.createQuery(Project.class);
        root = criteriaQuery.from(Project.class);

        criteriaQuery.where( criteriaBuilder.and( criteriaBuilder.notEqual( root.get( "priorityType" ), Priority
                        .NOTAPPLY), criteriaBuilder.equal( root.get( "status" ), Status.ACTIVE )));

        criteriaQuery.orderBy( criteriaBuilder.asc( root.get( "position" ) ));

        Query query;
        query = entityManager.createQuery(criteriaQuery);

        retorno = limitAmountProject( query.getResultList() );

        return retorno;
    }


    @Override
    public List<Project> findAllProjects(Boolean activeProjects, Pageable pageable)
    {
        CriteriaBuilder criteriaBuilder;
        CriteriaQuery<Project> criteriaQuery;
        Root<Project> root;

        criteriaBuilder = entityManager.getCriteriaBuilder();
        criteriaQuery = criteriaBuilder.createQuery(Project.class);
        root = criteriaQuery.from( Project.class );

        if (activeProjects != null && activeProjects.booleanValue()) {
            criteriaQuery.where(criteriaBuilder.equal(root.get("status"), Status.ACTIVE));
        }

        Query query;
        query = entityManager.createQuery(criteriaQuery);
        return query.getResultList();
    }

    @Override
    public List<Project> findAllProjectsInvestment(Long idUsuario, Pageable pageable)
    {
        CriteriaBuilder criteriaBuilder;
        CriteriaQuery<Project> criteriaQuery;
        Root<Project> root;
        Join investment;
        List<Project> retorno;

        criteriaBuilder = entityManager.getCriteriaBuilder();
        criteriaQuery = criteriaBuilder.createQuery( Project.class );

        root = criteriaQuery.from(Project.class);
        investment = root.join( "investments");

        criteriaQuery.where(
                criteriaBuilder.and(
                        criteriaBuilder.equal( investment.get( "person" ), idUsuario ),
                        criteriaBuilder.equal( investment.get( "status" ), Status.ACTIVE )
                )
        );
        criteriaQuery.orderBy( criteriaBuilder.desc( investment.get( "date" ) ) );

        Query query;
        query = entityManager.createQuery(criteriaQuery);

        retorno = removeRepeated( query.getResultList() );

        return retorno;
    }

    @Override
    public List<Project> findAllProjectsInvestmentXIdXParams(Long id, String nameCompany, String nameProject, Date
            startDate, Date endDate, Pageable pageable)
    {
        CriteriaBuilder criteriaBuilder;
        CriteriaQuery<Project> criteriaQuery;
        Root<Project> root;
        List<Project> retorno;

        criteriaBuilder = entityManager.getCriteriaBuilder();
        criteriaQuery = criteriaBuilder.createQuery(Project.class);

        root = criteriaQuery.from(Project.class);

        Predicate[] predicates;
        predicates = getPredicatesForInvestment( root, criteriaBuilder, id, nameCompany, nameProject, startDate,
                endDate );

        if (predicates != null)
        {
            criteriaQuery.where( criteriaBuilder.and( predicates ));
            criteriaQuery.orderBy( criteriaBuilder.asc( root.get( "id" ) ) );
        }

        Query query;
        query = entityManager.createQuery(criteriaQuery);

        retorno = removeRepeated( query.getResultList() );

        return retorno;
    }

    @Override
    public List<Project> findAllProjectsPriorityType(String priority)
    {
        CriteriaBuilder criteriaBuilder;
        CriteriaQuery<Project> criteriaQuery;
        Root<Project> root;

        criteriaBuilder = entityManager.getCriteriaBuilder();
        criteriaQuery = criteriaBuilder.createQuery(Project.class);
        root = criteriaQuery.from(Project.class);


         criteriaQuery.where( criteriaBuilder.and( criteriaBuilder.equal( root.get( "priorityType" ), Priority
                         .valueOf( priority )),
                 criteriaBuilder.equal( root.get( "status" ), Status.ACTIVE ) ));

        criteriaQuery.orderBy( criteriaBuilder.asc( root.get( "position" ) ));

        Query query;
        query = entityManager.createQuery(criteriaQuery);


        return query.getResultList();
    }

    //region Metodos privados
    private List<Project> removeRepeated(List<Project> listProject)
    {
        List<Project> retorno;
        retorno = new ArrayList<>();

        for(Project project : listProject)
        {
            int i = 0;
            for ( Project projectInt : retorno )
                if ( project.getId() == projectInt.getId() )
                    i++;

            if ( i==0 )
                retorno.add( project );
        }

        return retorno;
    }

    private List<Project> limitAmountProject(List<Project> listProject)
    {
        List<Project> retorno;
        retorno = new ArrayList<>();

        int contJustpaid;
        int contLastinvestment;
        int contNew;
        int contOutstanding;
        int contSuccessfull;

        contJustpaid = 0;
        contLastinvestment  = 0;
        contNew  = 0;
        contOutstanding  = 0;
        contSuccessfull  = 0;

        for ( Project project : listProject )
        {
            if ((project.getPriorityType().equals( Priority.JUSTPAID )) && contJustpaid < LIMIT)
            {
                retorno.add( project );
                contJustpaid++;
            }
            else if ((project.getPriorityType().equals( Priority.LASTINVESTMENT)) && contLastinvestment < LIMIT)
            {
                retorno.add( project );
                contLastinvestment++;
            }
            else if ((project.getPriorityType().equals( Priority.NEW )) && contNew < LIMIT)
            {
                retorno.add( project );
                contNew++;
            }
            else if ((project.getPriorityType().equals( Priority.OUTSTANDING )) && contOutstanding < LIMIT)
            {
                retorno.add( project );
                contOutstanding++;
            }
            else if ((project.getPriorityType().equals( Priority.SUCCESSFULL )) && contSuccessfull < LIMIT)
            {
                retorno.add( project );
                contSuccessfull++;
            }

        }

        return retorno;
    }

    //endregion
}
