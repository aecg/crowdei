package net.softclear.crowdei.backend.service;

import net.softclear.crowdei.backend.model.jpa.Calendar;
import net.softclear.crowdei.backend.model.jpa.Document;
import net.softclear.crowdei.backend.model.jpa.Project;
import net.softclear.crowdei.backend.model.jpa.Resource;
import net.softclear.crowdei.backend.model.jpa.Reward;
import net.softclear.crowdei.backend.model.types.Priority;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  ProjectService
 * Descripcion:             Contrato para los servicios que involucran la entidad Project
 * @version                 1.0
 * @author                  buchyo
 * @since                   12/02/2015
 *
 */
public interface ProjectService
{
    /**
     * Nombre:                  getProjectsXId.
     * Descripcion:             Metodo que consulta todos los proyectos de un usuario por su identificador
     * @version                 1.0
     * @author                  buchyo
     * @since                   12/02/2015
     *
     * @param id identificador del usuario
     * @return lista de proyectos
     */
    List<Project> getProjectsXId(Long id, Pageable pageable);


    /**
     * Nombre:                  createProject.
     * Descripcion:             Metodo que se usa para crear un proyecto
     * @version                 1.0
     * @author                  buchyo
     * @since                   12/02/2015
     *
     * @param project datos del proyecto
     * @return proyecto creado
     */
    Project createProject(Project project);


    /**
     * Nombre:                  deleteDocument.
     * Descripcion:             Metodo que elimina los datos de un proyecto
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/22/2015
     *
     * @param document datos del documento a eliminar
     * @return datos del documento eliminado
     */
    Project deleteDocument(Document document);

    /**
     * Nombre:                  getProject.
     * Descripcion:             Metodo que consulta los datos de un proyecto por su identificador
     * @version                 1.0
     * @author                  buchyo
     * @since                   12/02/2015
     *
     * @param id identificador del proyecto
     * @return datos del proyecto
     */
    Project getProject(Long id);


    /**
     * Nombre:                  updateProject.
     * Descripcion:             Metodo que modifica un proyecto
     * @version                 1.0
     * @author                  buchyo
     * @since                   12/02/2015
     *
     * @param project datos del proyecto
     */
    void updateProject(Project project);


    /**
     * Nombre:                  changePassword.
     * Descripcion:             Metodo que consulta todos los proyectos que hagan match con los parametros
     * de busqueda
     * @version                 1.0
     * @author                  buchyo
     * @since                   12/02/2015
     *
     * @param nameCompany nombre de la compañia
     * @param nameProject nombre del proyecto
     * @param idEconomicActivity actividad economica del proyecto
     * @return Lista de proyectos que hagan match con los parametros
     */
    List<Project> getProjects(String nameCompany, String nameProject, Long idEconomicActivity, Pageable pageable);

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  getProjectsXIdXParams
     * Descripcion:             Método que consulta todos los proyectos propios de un usuario y que hagan match con
     *                          los parámetros.
     * @version                 1.0
     * @author                  buchyo
     * @since                   04/01/2016
     *
     * @param id identificador del usuario
     * @param nameCompany nombre de la compañia
     * @param nameProject nombre del proyecto
     * @param idEconomicActivity actividad economica del proyecto
     * @return Lista de proyectos que hagan match con los parametros
     */
    List<Project> getProjectsXIdXParams(Long id, String nameCompany, String nameProject,
                                        Long idEconomicActivity, Pageable pageable);

    /**
     * Nombre:                  deleteProject.
     * Descripcion:             Metodo que elimina un proyecto por su identificador
     * @version                 1.0
     * @author                  buchyo
     * @since                   12/02/2015
     * @param id identificador del proyecto
     */
    void deleteProject(Long id);

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  getProjectsPriority
     * Descripcion:             Método que obtiene los proyectos que tienen alguna prioridad
     * @version                 1.0
     * @author                  buchyo
     * @since                   05/01/2016
     *
     * @return lista de proyectos
     */
    List<Project> getProjectsPriority();

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  getAllProjects
     * Descripcion:             Metodo que consulta todos los proyectos que esten activos
     * @version                 1.0
     * @author                  buchyo
     * @since                   14/01/2016
     *
     * @return lista de proyectos
     */
    List<Project> getAllProjects(Boolean activeProjects, Pageable pageable);

    /**
     * Sistema:                 Crowdei.
     *                                                              
     * Nombre:                  getAllProjectsInvestment                                                    
     * Descripcion:             Método que trae todos los proyectos en los que invirtió el usuario
     * @version                 1.0
     * @author                  buchyo
     * @since                   18/01/2016
     *
     * @param id identificador del usuario
     * @return lista de proyectos
     */
    List<Project> getAllProjectsInvestment(Long id, Pageable pageable);

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  getProjectsInvestmentXIdXParams
     * Descripcion:             Metodo que consulta los proyectos  en los que invirtio el usuario dado un filtro
     * @version                 1.0
     * @author                  buchyo
     * @since                   25/01/2016
     *
     * @param id identificador del usuario
     * @param nameCompany nombre de la compañia
     * @param nameProject nombre del proyecto
     * @param startDate rango de fecha de la inversion
     * @param endDate rango de fecha de la inversion
     * @return Lista de proyectos que hagan match con los parametros
     */
    List<Project> getProjectsInvestmentXIdXParams(Long id, String nameCompany, String nameProject, Date startDate, Date
            endDate, Pageable pageable);

    /**
     * Sistema:                 Crowdei.
     *                                                              
     * Nombre:                  getProjectsPriorityType
     * Descripcion:             Método que obtiene los proyectos que tienen una prioridad en particular
     * @version                 1.0
     * @author                  buchyo
     * @since                   01/02/2016
     *
     * @param priority tipo de la prioridad a consultar
     * @return Objeto que posee la informacion en formato JSON
     */
    List<Project> getProjectsPriorityType(String priority);

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  deleteActivity
     * Descripcion:             Método que elimina una actividad del calendario
     * @version                 1.0
     * @author                  buchyo
     * @since                   07/03/2016
     *
     * @param calendar datos de la actividad a eliminar
     * @return datos del documento eliminado
     */
    Project deleteActivity(Calendar calendar);

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  deleteResource
     * Descripcion:             Método que elimina una actividad del calendario
     * @version                 1.0
     * @author                  buchyo
     * @since                   07/03/2016
     *
     * @param resource datos del recurso a eliminar
     * @return datos del documento eliminado
     */
    Project deleteResource(Resource resource);

    Project deleteReward(Reward reward);

    void activateProject(Long id);

    void deactivateProject(Long id);

    void changePriorityProject(Long id, Priority priority);
}
