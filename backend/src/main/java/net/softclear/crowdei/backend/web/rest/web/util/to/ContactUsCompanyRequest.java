package net.softclear.crowdei.backend.web.rest.web.util.to;

import lombok.Data;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  ContactUsCompanyRequest
 * Descripcion:             Objeto que posee la definicion del JSON que es necesario para enviar un mensaje
 * @version                 1.0
 * @author                  buchyo
 * @since                   29/03/2016
 *
 */
@Data
public class ContactUsCompanyRequest
{
    private Long idProject;
    private String name;
    private String lastName;
    private String email;
    private String phone;
    private String message;
}
