package net.softclear.crowdei.backend.service.implementation;



import java.io.IOException;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.NoResultException;

import com.conekta.Error;
import org.apache.commons.lang.NumberUtils;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.conekta.Charge;
import com.conekta.Conekta;
import com.mercadopago.MP;
import com.conekta.Error.*;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import net.softclear.crowdei.backend.config.MessagesConfig;
import net.softclear.crowdei.backend.config.SettingsConfig;
import net.softclear.crowdei.backend.exception.ServiceException;
import net.softclear.crowdei.backend.model.jpa.Investment;
import net.softclear.crowdei.backend.model.jpa.MailPreference;
import net.softclear.crowdei.backend.model.jpa.Person;
import net.softclear.crowdei.backend.model.jpa.Project;
import net.softclear.crowdei.backend.model.jpa.Reward;
import net.softclear.crowdei.backend.model.jpa.User;
import net.softclear.crowdei.backend.model.types.MethodPayment;
import net.softclear.crowdei.backend.model.types.Status;
import net.softclear.crowdei.backend.repository.InvestmentRepository;
import net.softclear.crowdei.backend.repository.PersonRepository;
import net.softclear.crowdei.backend.repository.ProjectRepository;
import net.softclear.crowdei.backend.repository.UserRepository;
import net.softclear.crowdei.backend.service.InvestmentService;
import net.softclear.crowdei.backend.util.ConektaUtils;
import net.softclear.crowdei.backend.util.MercadoPagoUtils;
import net.softclear.crowdei.backend.util.TransfererUtils;


/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  InvestmentServiceImpl
 * Descripcion:             Clase que implementa el servicio de inversiones
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/28/2015
 *
 */
@Service
@Transactional
public class InvestmentServiceImpl implements InvestmentService
{
    //region Atributos

    private static Logger logger = LoggerFactory.getLogger( InvestmentServiceImpl.class );
    @Autowired
    private InvestmentRepository repository;
    @Autowired
    private UserRepository       userRepository;
    @Autowired
    private JavaMailSender       mailSender;
    @Autowired
    private MessagesConfig       messagesConfig;
    @Autowired
    private Configuration        freemarkerConfiguration;
    @Autowired
    private ProjectRepository    projectRepository;
    @Value( "${email.receivers}" )
    private String               receivers;
    @Value( "${conekta.apiKey}" )
    private String				 conektaApiKey;
    @Autowired
    private PersonRepository     personRepository;
    @Autowired
    private SettingsConfig       settingsConfig;

    private static final String PAY_APPROVED        			= "approved";
    static final         int    CERO                			= 0;
    static final         int    INVERSIONMIPROYECTO 			= 2;
    static final		 String CONEKTA_CURRENCY				= "MXN";
    static final 		 String CONEKTA_LINE_ITEM_DESCRIPTION 	= "Inversión a través de CrowdEI"; 
    static final		 int 	CONEKTA_LINE_ITEM_QTY 			= 1;
    static final 		 String CONEKTA_LINE_ITEM_CATEGORY		= "Inversión CrowdEI";
    static final 		 String CONEKTA_DETAILS_PHONE			= "403-342-0642";
    static final		 String CONEKTA_PAID_STATUS				= "paid";

    //endregion

    //region Implementacion


    /**
     * Nombre:                  paymentProcessingPaypal.
     * Descripcion:             Metodo que procesa el pago de paypal
     *
     * @param idUser identificador del usuario que realiza el pago
     * @param idProject identificador del proyecto
     * @param data informacion del pago
     * @return la transferencia registrada
     * @version 1.0
     * @author guzmle
     * @since 12/28/2015
     */
    @Override
    public void paymentProcessingPaypal( long idUser, long idProject, long idReward, String data )
    {
        logger.debug( "Entrando al metodo paymentProcessingPaypal" );

        final String[] info = data.split( "&" );
        final HashMap<String, String> parameters = new HashMap<>();
        Investment investment;

        for ( String obj : info )
        {
            final String[] dupla = obj.split( "=" );
            if ( dupla.length > 1 )
                parameters.put( dupla[ 0 ], dupla[ 1 ] );
            else
                parameters.put( dupla[ 0 ], "" );

        }

        if ( parameters.get( "mc_gross" ) == null || parameters.get( "mc_gross" ).length() == 0 )
        {
            logger.error( "La rafaga recibida tiene errores " + data );
            throw new ServiceException( "La rafaga recibida tiene errores" );
        }

        if ( !NumberUtils.isNumber( parameters.get( "mc_gross" ) ) )
        {
            logger.error( "La rafaga recibida tiene errores " + data );
            throw new ServiceException( "La rafaga recibida tiene errores" );
        }
        final JSONObject obj = new JSONObject( parameters );
        investment = saveInvestment( idUser, idProject, idReward, Double.valueOf( parameters.get( "mc_gross" ) ), obj.toString(),
                Status.ACTIVE, MethodPayment.PAYPAL );

        if ( (parameters.get( "address_status" ) != null) && parameters.get( "address_status" ).equals( "confirmed" ) )
            sendMailInvestmentMyProject( investment );

        logger.debug( "Saliendo del metodo paymentProcessingPaypal" );

    }


    /**
     * Nombre:                  paymentProcessingMercadoPago.
     * Descripcion:             Metodo que procesa el pago de
     * Mercado pago
     *
     * @param idUser    identificador del usuario que realiza el pago
     * @param idProject identificador del proyecto
     * @param data      informacion del pago
     *
     * @version 1.0
     * @author guzmle
     * @since 12/28/2015
     */
    @Override
    public void paymentProcessingMercadoPago( long idUser, long idProject, MercadoPagoUtils data )
    {
        logger.debug( "Entrando al metodo paymentProcessingMercadoPago" );
        JSONObject payment;
        try
        {
            Investment investment;
            payment = new JSONObject( data.getData() );

            final String status = payment.getString( "collection_status" );

            if(status.equals( PAY_APPROVED ))
            {
                investment = saveInvestment( idUser, idProject, data.getRewardId(), data.getAmount(), payment.toString(),
                        Status.ACTIVE, MethodPayment.MERCADOPAGO);
                sendMailInvestmentMyProject(investment);
            }
            else
                throw new ServiceException( "El pago no se ha procesado" );

            logger.debug( "Saliendo del metodo paymentProcessingMercadoPago" );
        }
        catch ( Exception e )
        {
            logger.error( "Ha ocurrido un error procesando el pago de mercado pago", e );
            throw new ServiceException( e.getMessage(), e );
        }

    }

	@Override
	public void paymentProcessingConekta( long idUser, long idProject, ConektaUtils data ) 
	{
        logger.debug( "Entrando al metodo paymentProcessingConekta" );
        
		Investment investment = null;
		try {
			final Map<String, Object> paymentParams = new HashMap<>();
			paymentParams.put("description", data.getDescription());
			paymentParams.put("amount", (int)(data.getAmount() * 100));
			paymentParams.put("currency", CONEKTA_CURRENCY);
			paymentParams.put("reference_id", data.getProjectId());
			paymentParams.put("card", data.getTokenId());
			paymentParams.put("details", getDetails(data));

	        Conekta.setApiKey(conektaApiKey);
			Charge conektaCharge = Charge.create(new org.json.JSONObject(paymentParams));

			if (!CONEKTA_PAID_STATUS.equals(conektaCharge.status))
				throw new ServiceException("Transacción rechazada");

	        investment = saveInvestment( idUser, idProject, data.getRewardId(), data.getAmount(), conektaCharge.toString(),
	                Status.ACTIVE, MethodPayment.CONEKTA );
	        sendMailInvestmentMyProject( investment );
		} catch (Exception e) {
			throw new ServiceException(((Error)e).message_to_purchaser, e);
		}

		logger.debug( "Saliendo del metodo paymentProcessingConekta" );			
	}

	/**
     * Nombre:                  paymentProcessingTransferencia.
     * Descripcion:             Metodo que procesa el pago de
     * una transferencia
     *
     * @param idUser    identificador del usuario que realiza el pago
     * @param idProject identificador del proyecto
     * @param data      informacion del pago
     *
     * @version 1.0
     * @author guzmle
     * @since 12/29/2015
     */
    @Override
    public Investment paymentProcessingTransferencia( long idUser, long idProject, TransfererUtils data )
    {
        Investment retorno;

        retorno = saveInvestment( idUser,  idProject, data.getRewardId(), data.getAmount(), data.getToken(),
                Status.INACTIVE, MethodPayment.TRANSFER);

        return retorno;
    }


    /**
     * Nombre:                  getMercadoPagoUrl.
     * Descripcion:             Metodo que obtiene la url que se requiere para realizar el pago por mercadoPago
     * @version                 1.0
     * @author                  guzmle
     * @since                   1/4/2016
     * @param data informacion del pago
     * @return Url del pago
     */
    @Override
    public String getMercadoPagoUrl( MercadoPagoUtils data )
    {
        final MP mp = new MP(settingsConfig.mpClientId, settingsConfig.mpClientSecret);
        final JSONObject preference;
        String response;

        String preferenceData =
                "{" +
                    "'items': [{" +
                        "'title': '%s'," +
                        "" +
                        "'currency_id': 'MXN'," +
                        "'description': 'Inversión desde la página www.crowdei.com para el proyecto %s '," +
                        "'quantity': 1," +
                        "'unit_price': %s " +
                    "}]," +
                    "'payer': {" +
                        "'name': '%s'," +
                        "'email': '%s'" +
                    "}," +
                    "'payment_methods': {" +
                        "'excluded_payment_types': [" +
                        "{" +
                            "'id': 'ticket'" +
                        "}]," +
                        "'installments': 12," +
                        "'default_payment_method_id': null," +
                        "'default_installments': null" +
                    "}" +
                "}";

        try
        {
            mp.sandboxMode( Boolean.parseBoolean( settingsConfig.mpSandbox ) );
            preferenceData = String.format( preferenceData, data.getDescription(),data.getDescription(),
                    data.getAmount(), data.getUserName(), data.getEmail() );
            preference = mp.createPreference(preferenceData);
            if(mp.sandboxMode())
                response = preference.getJSONObject("response").getString("sandbox_init_point");
            else
                response = preference.getJSONObject("response").getString("init_point");
        }
        catch ( Exception e )
        {
            logger.error( "Ha ocurrido un error procesando el pago de mercado pago", e );
            throw new ServiceException( e.getMessage(), e );
        }

        return response;

    }


    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  getInvestmentsXIdProjectXIdUser
     * Descripcion:             Método que consulta todas las invesiones dado un proyecto y un usuario en partiular
     * @version                 1.0
     * @author                  buchyo
     * @since                   21/01/2016
     *
     * @param idUser identificador del usuario
     * @param idProject identificador del proyecto
     * @param pageable
     * @return
     */
    @Override
    public List<Investment> getInvestmentsXIdProjectXIdUser (Long idUser, Long idProject, Pageable pageable)
    {
        logger.debug( "Entrando al metodo getInvestmentsXIdProjectXIdUser" );
        List<Investment> listInvestments = null;

        try
        {
            listInvestments = repository.findAll( idUser, idProject, pageable );
        }
        catch ( Exception e )
        {
            logger.error( "Ha ocurrido un error consultando las inversiones", e );
            throw new ServiceException( "Error reading projects from database", e );
        }

        logger.debug( "Saliendo del metodo getInvestmentsXIdProjectXIdUser" );
        return listInvestments;
    }

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  sendMailInvestmentMyProject
     * Descripcion:             Método que envia un correo a la persona contacto cuando alguien invierta en uno
     * de los proyectos que esta a cargo
     * @version                 1.0
     * @author                  buchyo
     * @since                   05/04/2016
     *
     * @param investment la inversion
     */
    @Override
    public void sendMailInvestmentMyProject(Investment investment)
    {
        logger.debug( "Entrando en el metodo sendMailInvestmentMyProject" );
        try
        {
            final Template template = freemarkerConfiguration.getTemplate( "investmentMyProject.ftl" );
            final Map<String, Object> params = new HashMap<>();
            StringWriter output;
            MimeMessage msg;
            output = new StringWriter();
            Project project;
            Person person;
            project = projectRepository.findOne(investment.getProject().getId());
            person = personRepository.findOne(investment.getPerson().getId());
            investment.setPerson(person);
            investment.setProject(project);
            template.process( fillParameters(investment), output );
            //Estrategias Integrales
            msg = mailSender.createMimeMessage();
            msg.setRecipients( Message.RecipientType.TO, createReceiverslist() );
            msg.setSubject( "Confirmación Inversión en Proyecto en CrowdEI" );
            msg.setText( output.toString(), "utf-8", "html" );
            mailSender.send( msg );
            //El usuario quien está haciendo la inversión
            msg.setRecipients( Message.RecipientType.TO, investment.getPerson().getEmail() );
            mailSender.send( msg );
            //Persona responsable del proyecto, es quien lo creo
            if (investment.getProject().getPerson().getMailPreferenceList().size() > 0)
            {
                for (MailPreference mail : investment.getProject().getPerson().getMailPreferenceList())
                {
                    if (mail.getId() == INVERSIONMIPROYECTO)
                    {
                        msg.setRecipients( Message.RecipientType.TO, investment.getProject().getPerson().getEmail() );
                        mailSender.send( msg );
                    }
                }
            }
        }
        catch ( NoResultException | TemplateException e )
        {
            logger.error( messagesConfig.userNotFound, e );
            throw new ServiceException( messagesConfig.userNotFound );
        }
        catch ( IOException | MessagingException e )
        {
            logger.error(messagesConfig.messagingError, e);
            throw new ServiceException( messagesConfig.messagingError );
        }
        logger.debug( "Saliendo del metodo sendMailInvestmentMyProject" );
    }


    @Override
    public List<Investment> findAllCompletPublication ()
    {
        logger.debug( "Entrando al metodo findAllCompletPublication" );
        List<Investment> listInvestments = null;

        try
        {
            listInvestments = repository.findAllCompletPublication();
        }
        catch ( Exception e )
        {
            logger.error( "Ha ocurrido un error consultando las inversiones", e );
            throw new ServiceException( "Error reading projects from database", e );
        }

        logger.debug( "Saliendo del metodo findAllCompletPublication" );
        return listInvestments;
    }

    @Override
    public void sendMailCompletPublication(long idProject, Double totalAmountInvested, Double percent, String receptor)
    {
        logger.debug( "Entrando en el metodo sendMailCompletPublication" );
        try
        {
            final Template template = freemarkerConfiguration.getTemplate( "completePublicationProject.ftl" );
            final Map<String, Object> params = new HashMap<>();
            Project project;

            project = projectRepository.findOne(idProject);

            StringWriter output;
            MimeMessage msg;
            output = new StringWriter();

            params.put( "nombreProyecto", project.getName() );
            params.put( "objetivoGeneral", project.getGeneralObjetive() );
            params.put( "simbolo", project.getCurrency().getSymbol() );
            params.put( "descripcionMoneda", project.getCurrency().getDescription() );
            params.put( "montototal", totalAmountInvested );
            params.put( "porcentaje", percent );
            params.put( "objetivo", project.getObjetive() );
            template.process( params, output );


            msg = mailSender.createMimeMessage();
            msg.setRecipients( Message.RecipientType.TO, receptor );
            msg.setSubject( "Cierre Período Publicación Proyecto en CrowdEI" );
            msg.setText( output.toString(), "utf-8", "html" );


            mailSender.send( msg );

        }
        catch ( NoResultException | TemplateException e )
        {
            logger.error( messagesConfig.userNotFound, e );
            throw new ServiceException( messagesConfig.userNotFound );
        }
        catch ( IOException | MessagingException e )
        {
            logger.error(messagesConfig.messagingError, e);
            throw new ServiceException( messagesConfig.messagingError );
        }
        logger.debug( "Saliendo del metodo sendMailCompletPublication" );
    }

    @Override
    public void sendMailInvestmentTransferMyProject(Long idTransfer)
    {
        logger.debug( "Entrando en el metodo sendMailInvestmentTransferMyProject" );
        try
        {
            final Template template = freemarkerConfiguration.getTemplate( "mailInvestmentTransferMyProject.ftl" );
            final Map<String, Object> params = new HashMap<>();
            StringWriter output;
            MimeMessage msg;
            output = new StringWriter();
            Investment investment;
            investment = repository.getOne(idTransfer);

            template.process( fillParametersTransfer(investment), output );

            //Estrategias Integrales
            msg = mailSender.createMimeMessage();
            msg.setRecipients( Message.RecipientType.TO, createReceiverslist() );
            msg.setSubject( "Recepción Transferencia Electrónica CrowdEI" );
            msg.setText( output.toString(), "utf-8", "html" );
            mailSender.send( msg );

            //El usuario quien está haciendo la inversión
            msg.setRecipients( Message.RecipientType.TO, investment.getPerson().getEmail() );
            mailSender.send( msg );

            //Persona responsable del proyecto, es quien lo creo
            if (investment.getProject().getPerson().getMailPreferenceList().size() > 0)
            {
                for (MailPreference mail : investment.getProject().getPerson().getMailPreferenceList())
                {
                    if (mail.getId() == INVERSIONMIPROYECTO)
                    {
                        msg.setRecipients( Message.RecipientType.TO, investment.getProject().getPerson().getEmail() );
                        mailSender.send( msg );
                    }
                }
            }
        }
        catch ( NoResultException | TemplateException e )
        {
            logger.error( messagesConfig.userNotFound, e );
            throw new ServiceException( messagesConfig.userNotFound );
        }
        catch ( IOException | MessagingException e )
        {
            logger.error(messagesConfig.messagingError, e);
            throw new ServiceException( messagesConfig.messagingError );
        }
        logger.debug( "Saliendo del metodo sendMailInvestmentTransferMyProject" );
    }

    /**
     * Nombre:                  saveInvestment.
     * Descripcion:             Metodo que almacena la informacion de la inversion
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/29/2015
     *
     * @param idUser identificador del usuario
     * @param idProject identificador del proyecto
     * @param amount monto de la inversion
     * @param data informacion de la respuesta
     * @param status estado de la inversion
     */
    private Investment saveInvestment(long idUser, long idProject, long idReward, double amount, String data, Status status,
                                      MethodPayment methodPayment)
    {
        logger.debug( "Entrando al metodo saveInvestment" );
        Investment retorno;

        try
        {
            final Investment investment = new Investment();
            final User user = userRepository.getOne( idUser );
            investment.setPerson( user.getPerson() );
            investment.setProject( new Project() );

            investment.setAmount( amount );
            investment.setDate( new Date() );
            investment.setData( data );
            investment.getProject().setId( idProject );
            investment.setStatus( status );
            investment.setMethodPayment( methodPayment );

            if (idReward > 0) {
                investment.setReward(new Reward());
                investment.getReward().setId(idReward);
            }
            else {
                investment.setReward(null);
            }

            retorno = repository.save( investment );
        }
        catch ( Exception e )
        {
            logger.error( "Ha ocurrido un error guardando la inversion", e );
            throw new ServiceException( "Ha ocurrido un error guardando la inversion", e  );
        }
        logger.debug( "Saliendo del metodo saveInvestment" );

        return retorno;
    }

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  createReceiverslist
     * Descripcion:             Metodo que arma la lista de receptores de los correos
     * @version                 1.0
     * @author                  buchyo
     * @since                   11/02/2016
     *
     * @return
     * @throws javax.mail.internet.AddressException
     */
    private InternetAddress[] createReceiverslist() throws AddressException
    {
        String[] listaReceptores;
        listaReceptores = receivers.split( ";" );

        InternetAddress[] dest;
        dest = new InternetAddress[listaReceptores.length];

        for (int i = 0; i < dest.length; i++)
        {
            dest[i] = new InternetAddress( listaReceptores[i]) ;
        }

        return dest;
    }

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  fillParametersTransfer
     * Descripcion:             Metodo que llena los parametros de la plantilla
     * @version                 1.0
     * @author                  buchyo
     * @since                   22/04/2016
     *
     * @param investment datos del proyecto
     * @return los parametros
     */
    private Map<String, Object> fillParametersTransfer(Investment investment) throws IOException
    {
        final Map<String, Object> params = new HashMap<>();
        final Map<String, Object> retorno;

        DateFormat fecha;
        fecha = new SimpleDateFormat("yyyy/MM/dd");

        params.put( "nombre", investment.getPerson().getName() );
        params.put( "apellido", investment.getPerson().getLastName() );
        params.put( "codigoTransferencia", investment.getData() );
        params.put( "simbolo", investment.getProject().getCurrency().getSymbol() );
        params.put( "monto", investment.getAmount() );
        params.put( "descripcionMoneda", investment.getProject().getCurrency().getDescription() );
        params.put( "fechaRegistro", fecha.format(investment.getDate()) );
        params.put( "nombreProjecto", investment.getProject().getName());
        retorno = params;

        return retorno;
    }

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  fillParameters
     * Descripcion:             Metodo que llena los parametros de la plantilla
     * @version                 1.0
     * @author                  buchyo
     * @since                   22/04/2016
     *
     * @param investment datos del proyecto
     * @return los parametros
     */
    private Map<String, Object> fillParameters(Investment investment) throws IOException
    {
        final Map<String, Object> params = new HashMap<>();
        final Map<String, Object> retorno;

        DateFormat fecha;
        fecha = new SimpleDateFormat("yyyy/MM/dd");

        params.put( "nameUser", investment.getPerson().getName() );
        params.put( "lastNameUser", investment.getPerson().getLastName() );
        params.put( "date", fecha.format(investment.getDate()) );
        params.put( "nameProject", investment.getProject().getName() );
        params.put( "amount", investment.getAmount() );
        params.put( "symbolCurrency", investment.getProject().getCurrency().getSymbol() );
        params.put( "descriptionCurrency", investment.getProject().getCurrency().getDescription() );
        retorno = params;

        return retorno;
    }

	private void sendAsyncMailInvestmentMyProject( Investment investment )
	{
    	new Thread(new Runnable() {
			@Override
			public void run() {
				sendMailInvestmentMyProject(investment);
			}
		}).start();
	}

    private Map<String, Object> getDetails( ConektaUtils data ) 
    {
    	final Map<String, Object> details = new HashMap<>();
    	details.put("name", data.getUserName());
    	details.put("phone", CONEKTA_DETAILS_PHONE);
    	details.put("email", data.getEmail());
    	
    	final Map<String, Object> customer = new HashMap<>();
    	customer.put("logged_in", true);
    	customer.put("successful_purchases", 1);
    	customer.put("created_at", new Date().getTime() / 1000);
    	customer.put("updated_at", new Date().getTime() / 1000);
    	customer.put("offline_payments", 1);
    	customer.put("score", 10);
    	details.put("customer", customer);
    	
    	final Map<String, Object> lineItem = new HashMap<>();
    	lineItem.put("name", data.getDescription());
    	lineItem.put("description", CONEKTA_LINE_ITEM_DESCRIPTION);
    	lineItem.put("unit_price", (int)(data.getAmount() * 100));
    	lineItem.put("quantity", CONEKTA_LINE_ITEM_QTY);
    	lineItem.put("sku", "");
    	lineItem.put("category", CONEKTA_LINE_ITEM_CATEGORY);
		details.put("line_items", Collections.singletonList(lineItem));
		
		final Map<String, Object> billingAddress = new HashMap<>();
		billingAddress.put("street1", "");
		billingAddress.put("street2", "");
		billingAddress.put("street3", "");
		billingAddress.put("city", "");
		billingAddress.put("state", "");
		billingAddress.put("zip", "");
		billingAddress.put("country", "");
		billingAddress.put("tax_id", "");
		billingAddress.put("company_name", "");
		billingAddress.put("phone", "");
		billingAddress.put("email", "");
		details.put("billing_address", billingAddress);
		
		return details;
	}

    @Override
    public List<Investment> getInvestmentsXIdProject(Long idProject, Pageable pageable)
    {
        logger.debug( "Entrando al metodo getInvestmentsXIdProject" );
        List<Investment> listInvestments = null;

        try
        {
            listInvestments = repository.findAll( idProject, pageable );
        }
        catch ( Exception e )
        {
            logger.error( "Ha ocurrido un error consultando las inversiones", e );
            throw new ServiceException( "Error reading projects from database", e );
        }

        logger.debug( "Saliendo del metodo getInvestmentsXIdProject" );
        return listInvestments;
    }

    @Override
    public void activateTransfer(Long id)
    {
        logger.debug( "Entrando al metodo activateTransfer" );
        try
        {
            Investment investment;
            investment = repository.findOne( id );
            investment.setStatus( Status.ACTIVE );
            repository.save( investment );
        }
        catch (Exception e)
        {
            logger.error( "Error reading changing status from database", e );
            throw new ServiceException("Error changing investment status from database", e);
        }

        logger.debug( "Saliendo del metodo activateTransfer" );
    }
    //endregion

}
