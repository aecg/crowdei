package net.softclear.crowdei.backend.web.rest.out;



import net.softclear.crowdei.backend.model.jpa.Investment;
import net.softclear.crowdei.backend.model.jpa.Project;
import net.softclear.crowdei.backend.model.types.Status;
import net.softclear.crowdei.backend.service.ProjectService;
import net.softclear.crowdei.backend.util.CollectionUtils;
import net.softclear.crowdei.backend.util.DateUtils;
import net.softclear.crowdei.backend.web.rest.out.to.projectResponse.GetProjectsXPriority;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  LandingRestController
 * Descripcion:             Clase que posee la definicion y la implementacion de los servicios
 * @version                 1.0
 * @author                  buchyo
 * @since                   06/01/2016
 *
 */
@RestController
public class LandingRestController
{
    //region Attributes

    @Autowired
    private ProjectService projectService;
    @Autowired
    private ModelMapper    modelMapper;
    @Autowired
    private DateUtils      dateUtils;

    static final int PERCENTEGE = 100;

    //endregion

    //region Services

    /**
     *
     * Sistema:                 Crowdei.
     *
     * Nombre:                  getProjectsPriority
     * Descripcion:             Método que obtiene los proyectos que tienen alguna prioridad
     * @version                 1.0
     * @author                  buchyo
     * @since                   05/01/2016
     *
     * @return Objeto que posee la informacion en formato JSON
     */
    @RequestMapping( value = "priority", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    GetProjectsXPriority getProjectsPriority()
    {
        List<Project> listProjects;
        GetProjectsXPriority response;
        listProjects = projectService.getProjectsPriority();
        response = new GetProjectsXPriority();

        if ( listProjects != null )
        {
            response = convertProject( listProjects );
        }
        return response;
    }


    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  GetProjectsPriorityType
     * Descripcion:             Método que obtiene los proyectos que tienen una prioridad en particular
     * @version                 1.0
     * @author                  buchyo
     * @since                   01/02/2016
     *
     * @param priority tipo de la prioridad a consultar
     * @return Objeto que posee la informacion en formato JSON
     */
    @RequestMapping(value = "priorityType/{priority}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody GetProjectsXPriority getProjectsPriorityType(@PathVariable String priority)
    {
        List<Project> listProjects;
        GetProjectsXPriority response;
        listProjects = projectService.getProjectsPriorityType(priority);
        response = new GetProjectsXPriority();

        if ( listProjects != null )
        {
            response = convertProject( listProjects );
        }

        return response;
    }


    //end region

    //region Métodos privados
    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  convertProject
     * Descripcion:             Método que transforma una lista de proyecto en un objeto GetProjectsXPriority
     * @version                 1.0
     * @author                  buchyo
     * @since                   06/01/2016
     *
     * @param listProjects lista de proyectos
     * @return objeto GetProjectsXPriority
     */
    private GetProjectsXPriority convertProject(List<Project> listProjects)
    {
        GetProjectsXPriority response;
        response = new GetProjectsXPriority();
        GetProjectsXPriority.Project  obj;
        List<GetProjectsXPriority.Project> projectsResponse;
        Investment last = null;
        Investment invesment = null;
        projectsResponse = new ArrayList<>();
        for ( Project project : listProjects )
        {
            obj = modelMapper.map( project, GetProjectsXPriority.Project.class );

            List<Investment> allInvestments = project.getInvestments();
            List<Investment> activeInvestment = allInvestments.stream().filter(p -> p.getStatus().equals(Status.ACTIVE)).collect(Collectors.toList());

            double obtained = 0;
            for(Investment investment : activeInvestment)
            {
                obtained += investment.getAmount();
            }
            obj.setObtained( cambiarFormato(obtained));

            List<Investment> distinctInvestment = activeInvestment.stream().filter(CollectionUtils.distinctByKey(p -> p.getPerson().getId())).collect(Collectors.toList());
            obj.setInvestors( distinctInvestment.size() );

            obj.setPercent( cambiarFormato( ( obtained / obj.getObjetive() ) * PERCENTEGE ) );
            final int publicationDays = project.getPublicationDays();
            obj.setLeftDays( publicationDays - dateUtils.getDaysLeft( project.getDateRegister(), new Date(  ) ) );
            obj.setLimitDate(dateUtils.addDays(project.getDateRegister(), publicationDays));

            if(project.getInvestments().size() > 0)
            {
                for ( int index = 0; index <= project.getInvestments().size() - 1; index++ )
                {
                    invesment = project.getInvestments().get( index );
                    if((invesment.getStatus().equals( Status.ACTIVE )) && index != project.getInvestments().size())
                        last = project.getInvestments().get( index );
                }
                if ( last != null )
                {
                    obj.setLastInvestment( last.getAmount() );
                    obj.setDateLastInvestment( dateUtils.getTimeLeft( last.getDate(), new Date() ) );
                }
                else
                    obj.setLastInvestment((double) 0);
            }
            else
                obj.setLastInvestment((double) 0);
            projectsResponse.add( obj );
        }
        response.setProjects(projectsResponse);
        return response;
    }

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  cambiarFormato
     * Descripcion:             Método que cambia el fotmarto ##,## a ##.##
     * @version                 1.0
     * @author                  buchyo
     * @since                   21/01/2016
     *
     * @param numero numero a convertir
     * @return nuevo numero
     */
    private double cambiarFormato(double numero)
    {
        DecimalFormatSymbols simbolo;
        simbolo = new DecimalFormatSymbols();
        simbolo.setDecimalSeparator('.');
        DecimalFormat df;
        df = new DecimalFormat("##########.##",simbolo);
        double retorno;

        retorno = Double.parseDouble(df.format(numero));

        return retorno;
    }

    //endregion
}
