package net.softclear.crowdei.backend.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  BeanConfig
 * Descripcion:             Clase de configuracion
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
@Configuration
public class BeanConfig
{
    /**
     * Nombre:                  modelMapper.
     * Descripcion:             Metodo que obtiene el wraper de las entidades
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/21/2015
     *
     * @return Objeto Mapeado
     */
	@Bean
	public ModelMapper modelMapper()
    {
		return new ModelMapper();
	}
}
