package net.softclear.crowdei.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  Company
 * Descripcion:             Clase que modela la entidad company
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/1/2015
 *
 */
@Entity
@Table(name="company")
@Setter
@Getter
public class Company
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = true )
    private Long id;

    @Column( name = "rfc", nullable = true )
    private String rfc;

    @Column( name = "name", nullable = true )
    private String name;

    @ManyToOne(cascade= CascadeType.ALL )
    @JoinColumn( name = "internet_info_id" )
    private InternetInfo internetInformation;

    @ManyToOne(cascade= CascadeType.ALL )
    @JoinColumn( name = "location_id" )
    private Location location;

    @ManyToOne(cascade= CascadeType.ALL)
    @JoinColumn( name = "contact_information_id" )
    private ContactInformation contactInformation;

}
