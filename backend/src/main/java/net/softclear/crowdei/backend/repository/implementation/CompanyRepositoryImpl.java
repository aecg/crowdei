package net.softclear.crowdei.backend.repository.implementation;

import net.softclear.crowdei.backend.model.jpa.Project;
import net.softclear.crowdei.backend.repository.CompanyRepositoryCustom;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  CompanyRepositoryImpl
 * Descripcion:             Clase que mimplementa el contrato para las operaciones personalizadas
 * para la entidad company
 * @version                 1.0
 * @author                  buchyo
 * @since                   11/03/2016
 *
 */
public class CompanyRepositoryImpl implements CompanyRepositoryCustom
{
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Nombre:                  getPredicates.
     * Descripcion:             Metodo que arma los predicados
     * @version                 1.0
     * @author                  buchyo
     * @since                   12/03/2015
     *
     * @param root objeto principal para armar la consulta
     * @param criteriaBuilder objeto que posee la definicion de la consulta
     * @param idUsuario id del usuario
     * @return arreglo de predicados
     */
    private Predicate[] getPredicates(Root<Project> root, CriteriaBuilder criteriaBuilder,
                                      Long idUsuario, String nameCompany)
    {
        List<Predicate> predicates;
        predicates = new ArrayList<>();

        Join company;
        company = root.join( "company" );

        if ( nameCompany != null && nameCompany.length() > 0 )
            predicates.add( criteriaBuilder.like( company.<String>get( "name" ), "%" + nameCompany + "%" ) );

        predicates.add(criteriaBuilder.equal(root.get("person"),idUsuario));
        Predicate[] retorno;
        retorno = null;

        if (predicates.size() > 0) // adding predicates if any
            retorno = predicates.toArray(new Predicate[predicates.size()]);

        return retorno;
    }

    @Override
    public List<Project> findAllXIdUser(Long idUsuario, String nameCompany, Pageable pageable)
    {
        CriteriaBuilder criteriaBuilder;
        criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Project> criteriaQuery;
        criteriaQuery = criteriaBuilder.createQuery(Project.class);

        Predicate[] predicates;
        predicates = getPredicates( criteriaQuery.from( Project.class ), criteriaBuilder, idUsuario, nameCompany);
        if (predicates != null)
            criteriaQuery.where(predicates);

        Query query;
        query = entityManager.createQuery(criteriaQuery);

        return query.getResultList();
    }
}
