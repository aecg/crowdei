package net.softclear.crowdei.backend.web.rest.web.projects;

import net.softclear.crowdei.backend.config.SettingsConfig;
import net.softclear.crowdei.backend.exception.ServiceException;
import net.softclear.crowdei.backend.model.jpa.Calendar;
import net.softclear.crowdei.backend.model.jpa.Company;
import net.softclear.crowdei.backend.model.jpa.Contact;
import net.softclear.crowdei.backend.model.jpa.ContactInformation;
import net.softclear.crowdei.backend.model.jpa.Document;
import net.softclear.crowdei.backend.model.jpa.InternetInfo;
import net.softclear.crowdei.backend.model.jpa.Investment;
import net.softclear.crowdei.backend.model.jpa.Location;
import net.softclear.crowdei.backend.model.jpa.Project;
import net.softclear.crowdei.backend.model.jpa.Resource;
import net.softclear.crowdei.backend.model.types.PersonType;
import net.softclear.crowdei.backend.model.types.Status;
import net.softclear.crowdei.backend.service.ProjectService;
import net.softclear.crowdei.backend.util.CollectionUtils;
import net.softclear.crowdei.backend.util.DateUtils;
import net.softclear.crowdei.backend.web.rest.web.projects.to.projectRequest.ChangePriorityProjectRequest;
import net.softclear.crowdei.backend.web.rest.web.projects.to.projectRequest.FindProjectsInvestmentXIdXParamsRequest;
import net.softclear.crowdei.backend.web.rest.web.projects.to.projectRequest.FindProjectsXIdXParamsRequest;
import net.softclear.crowdei.backend.web.rest.web.projects.to.projectRequest.ProjectModifyRequest;
import net.softclear.crowdei.backend.web.rest.web.projects.to.projectResponse.CreateProjectRequest;
import net.softclear.crowdei.backend.web.rest.web.projects.to.projectResponse.CreateProjectResponse;
import net.softclear.crowdei.backend.web.rest.web.projects.to.projectResponse.GetProjectsInvestmentResponse;
import net.softclear.crowdei.backend.web.rest.web.projects.to.projectResponse.GetProjectsXIdResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.io.File;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;

/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  ProjectsRestController
 * Descripcion:             Clase que posee la definicion y la implementacion de los servicios
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/4/2015
 *
 */
@RestController
@RequestMapping("web/projects*")
public class ProjectsRestController
{
    //region Attributes

    @Autowired
    private ProjectService projectService;
    @Autowired
    private ModelMapper    modelMapper;
    @Autowired
    private DateUtils      dateUtils;
    @Autowired
    private SettingsConfig settingsConfig;

    //endregion

    //region Services


    /**
     * Nombre:                  getProjectsXIdResponse.
     * Descripcion:             Servicio que obtiene los proyectos activos de un usuario en particular
     * @version 1.0
     * @author guzmle
     * @since 12/4/2015
     *
     * @param id identificador del usuario
     * @param page pagina de la informacion
     * @param size cantidad de registro por pagina
     * @return Objeto que posee la informacion en formato JSON
     */
    @RequestMapping( method = RequestMethod.GET, produces = "application/json" )
    public @ResponseBody GetProjectsXIdResponse getProjectsXIdResponse( @RequestParam( required = false ) Long id,
            @RequestParam( required = false ) Integer page, @RequestParam( required = false ) Integer size )
    {
        Pageable pageable = null;
        GetProjectsXIdResponse.Project  obj;
        final int percentege = 100;
        List<Project> listProjects;
        GetProjectsXIdResponse response;
        List<GetProjectsXIdResponse.Project> projectsResponse;

        if ( page != null && size != null )
            pageable = new PageRequest( page, size );

        listProjects = projectService.getProjectsXId( id, pageable );

        response = new GetProjectsXIdResponse();

        if ( listProjects != null )
        {
            projectsResponse = new ArrayList<>();
            for ( Project project : listProjects )
            {
                obj = modelMapper.map( project, GetProjectsXIdResponse.Project.class );
                double obtained = 0;

                List<Investment> allInvestments = project.getInvestments();
                List<Investment> activeInvestment = allInvestments.stream().filter(p -> p.getStatus().equals(Status.ACTIVE)).collect(Collectors.toList());

                for(Investment investment : activeInvestment)
                {
                    obtained += investment.getAmount();
                }
                obj.setObtained( cambiarFormato( obtained ));

                List<Investment> distinctInvestment = activeInvestment.stream().filter(CollectionUtils.distinctByKey(p -> p.getPerson().getId())).collect(Collectors.toList());
                obj.setInvestors( distinctInvestment.size() );

                obj.setPercent( cambiarFormato( ( obtained / obj.getObjetive() ) * percentege ) );
                final int publicationDays = project.getPublicationDays();
                obj.setLeftDays( publicationDays - dateUtils.getDaysLeft( project.getDateRegister(), new Date(  ) ) );
                obj.setLimitDate(dateUtils.addDays(project.getDateRegister(), publicationDays));
                projectsResponse.add(obj);
            }

            response.setProjects(projectsResponse);
            response.setCount(new Long(projectsResponse.size()));
        }

        return response;
    }

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  getAllProjectsInvestment
     * Descripcion:             Método que trae todos los proyectos en los que invirtió el usuario
     * @version                 1.0
     * @author                  buchyo
     * @since                   18/01/2016
     *
     * @param id identificador del usuario
     * @param page pagina de la informacion
     * @param size cantidad de registro por pagina
     * @return Objeto que posee la informacion en formato JSON
     */
    @RequestMapping(value = "/findInvestment", method = RequestMethod.GET, produces = "application/json" )
    public @ResponseBody
    GetProjectsInvestmentResponse getAllProjectsInvestment(@RequestParam( required = false ) Long id,
            @RequestParam( required = false ) Integer page, @RequestParam( required = false ) Integer size)
    {
        Pageable pageable = null;
        GetProjectsInvestmentResponse.Project  obj;
        final int percentege = 100;
        List<Project> listProjects;
        GetProjectsInvestmentResponse response;
        List<GetProjectsInvestmentResponse.Project> projectsResponse;

        if ( page != null && size != null )
            pageable = new PageRequest( page, size );

        listProjects = projectService.getAllProjectsInvestment( id, pageable );

        response = new GetProjectsInvestmentResponse();

        if ( listProjects != null )
        {
            projectsResponse = new ArrayList<>();
            for ( Project project : listProjects )
            {
                obj = modelMapper.map( project, GetProjectsInvestmentResponse.Project.class );
                double obtained = 0;

                List<Investment> allInvestments = project.getInvestments();
                List<Investment> activeInvestment = allInvestments.stream().filter(p -> p.getStatus().equals(Status.ACTIVE)).collect(Collectors.toList());

                for(Investment investment : activeInvestment)
                {
                    obtained += investment.getAmount();
                }
                obj.setObtained( cambiarFormato( obtained ));

                List<Investment> distinctInvestment = activeInvestment.stream().filter(CollectionUtils.distinctByKey(p -> p.getPerson().getId())).collect(Collectors.toList());
                obj.setInvestors( distinctInvestment.size() );

                obj.setPercent( cambiarFormato( ( obtained / obj.getObjetive() ) * percentege ) );
                final int publicationDays = project.getPublicationDays();
                obj.setLeftDays( publicationDays - dateUtils.getDaysLeft( project.getDateRegister(), new Date(  ) ) );
                obj.setLimitDate(dateUtils.addDays(project.getDateRegister(), publicationDays));

                projectsResponse.add(obj);
            }

            response.setProjects(projectsResponse);
        }

        return response;
    }

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  findProjectsXIdXParams
     * Descripcion:             Metodo que consulta los proyectos dado un filtro
     * @version                 1.0
     * @author                  buchyo
     * @since                   04/01/2016
     *
     * @param request objeto con los datos del filtro para la consulta
     * @param page pagina de la informacion
     * @param size cantidad de registros
     * @return Objeto con la informacion de los proyectos en formato JSON
     */
    @RequestMapping( value = "/findXIdXParams", method = RequestMethod.POST, produces = "application/json", consumes =
            "application/json" )
    public @ResponseBody GetProjectsXIdResponse findProjectsXIdXParams( @RequestBody FindProjectsXIdXParamsRequest
            request, @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size)
    {
        GetProjectsXIdResponse response;
        List<GetProjectsXIdResponse.Project> listResponse;
        List<Project> list;
        GetProjectsXIdResponse.Project obj;
        Pageable pageable = null;
        final int percentege = 100;

        response = new GetProjectsXIdResponse();
        listResponse = new ArrayList<>();

        if (page != null && size != null)
            pageable = new PageRequest(page, size);
        list = projectService.getProjectsXIdXParams( request.getIdUser(), request.getNameCompany(),
                request.getNameProject(), request.getIdEconomicActivity(), pageable );

        for (Project project : list)
        {
            obj = modelMapper.map(project, GetProjectsXIdResponse.Project.class);
            double obtained = 0;

            List<Investment> allInvestments = project.getInvestments();
            List<Investment> activeInvestment = allInvestments.stream().filter(p -> p.getStatus().equals(Status.ACTIVE)).collect(Collectors.toList());

            for(Investment investment : activeInvestment)
            {
                if(investment.getStatus().equals( Status.ACTIVE ))
                    obtained += investment.getAmount();
            }
            obj.setObtained( cambiarFormato( obtained )  );

            List<Investment> distinctInvestment = activeInvestment.stream().filter(CollectionUtils.distinctByKey(p -> p.getPerson().getId())).collect(Collectors.toList());
            obj.setInvestors( distinctInvestment.size() );

            obj.setPercent( cambiarFormato( ( obtained / obj.getObjetive() ) * percentege) );
            final int publicationDays = project.getPublicationDays();
            obj.setLeftDays( publicationDays - dateUtils.getDaysLeft( project.getDateRegister(), new Date(  ) ) );
            obj.setLimitDate(dateUtils.addDays(project.getDateRegister(), publicationDays));
            listResponse.add(obj);
        }

        response.setProjects( listResponse );
        return response;
    }

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  findProjectsXIdXParams
     * Descripcion:             Metodo que consulta los proyectos  en los que invirtio el usuario dado un filtro
     * @version                 1.0
     * @author                  buchyo
     * @since                   25/01/2016
     *
     * @param request objeto con los datos del filtro para la consulta
     * @param page pagina de la informacion
     * @param size cantidad de registros
     * @return Objeto con la informacion de los proyectos en formato JSON
     */
    @RequestMapping( value = "/findProjectsInvestmentXIdXParams", method = RequestMethod.POST, produces =
            "application/json", consumes = "application/json" )
    public @ResponseBody GetProjectsXIdResponse findProjectsInvestmentXIdXParams( @RequestBody
    FindProjectsInvestmentXIdXParamsRequest request, @RequestParam(required = false) Integer page, @RequestParam
            (required = false) Integer size)
    {
        GetProjectsXIdResponse response;
        List<GetProjectsXIdResponse.Project> listResponse;
        List<Project> list;
        GetProjectsXIdResponse.Project obj;
        Pageable pageable = null;
        final int percentege = 100;

        response = new GetProjectsXIdResponse();
        listResponse = new ArrayList<>();

        if (page != null && size != null)
            pageable = new PageRequest(page, size);
        list = projectService.getProjectsInvestmentXIdXParams( request.getIdUser(), request.getNameCompany(),
                request.getNameProject(), request.getStartDate(), request.getEndDate(), pageable );

        for (Project project : list)
        {
            obj = modelMapper.map(project, GetProjectsXIdResponse.Project.class);
            double obtained = 0;

            List<Investment> allInvestments = project.getInvestments();
            List<Investment> activeInvestment = allInvestments.stream().filter(p -> p.getStatus().equals(Status.ACTIVE)).collect(Collectors.toList());

            for(Investment investment : activeInvestment)
            {
                obtained += investment.getAmount();
            }
            obj.setObtained( cambiarFormato( obtained ) );

            List<Investment> distinctInvestment = activeInvestment.stream().filter(CollectionUtils.distinctByKey(p -> p.getPerson().getId())).collect(Collectors.toList());
            obj.setInvestors( distinctInvestment.size() );

            obj.setPercent( cambiarFormato( ( obtained / obj.getObjetive() ) * percentege) );
            final int publicationDays = project.getPublicationDays();
            obj.setLeftDays( publicationDays - dateUtils.getDaysLeft( project.getDateRegister(), new Date(  ) ) );
            obj.setLimitDate(dateUtils.addDays(project.getDateRegister(), publicationDays));
            listResponse.add(obj);
        }

        response.setProjects( listResponse );
        return response;
    }

    /**
     * Nombre:                  loadProjectVideoToAWS.
     * Descripcion:             Metodo que sube el archivo de video de un proyecto al S3
     * @version                 1.0
     * @author                  martin
     * @since                   12/8/2016
     *
     * @param request objeto con el archivo a subir
     * @param s3 objeto para conexión al AWS S3
     * @param nameProject nombre del proyecto
     * @return String nombre del archivo de video subido al S3
     */
    private String loadProjectVideoToAWS(@RequestBody CreateProjectRequest request, AmazonS3 s3, String nameProject)
    {
        try
        {
            final String nameVideo = request.getNameVideo().replaceAll(" ","_");
            final String nameVideoKey = request.getIdPerson().getId() + "_" + nameProject + "_" + nameVideo;
            final File file = File.createTempFile(nameVideoKey, request.getExtension());
            file.deleteOnExit();
            final OutputStream out = new FileOutputStream(file);
            //out.write(request.getVideo());
            out.close();
            s3.putObject(new PutObjectRequest(settingsConfig.awsS3Bucket, nameVideoKey, file));
            return nameVideoKey;
        }
        catch (Exception e)
        {
            //throw new ServiceException("Error loading files to AWS S3", e);
            return null;
        }
    }

    /**
     * Nombre:                  loadProjectImageToAWS.
     * Descripcion:             Metodo que sube el archivo de imagen de un proyecto al S3
     * @version                 1.0
     * @author                  martin
     * @since                   12/8/2016
     *
     * @param request objeto con el archivo a subir
     * @param s3 objeto para conexión al AWS S3
     * @param nameProject nombre del proyecto
     * @return String nombre del archivo de imagen subido al S3
     */
    private String loadProjectImageToAWS(@RequestBody CreateProjectRequest request, AmazonS3 s3, String nameProject)
    {
        try
        {
            final String imageName = request.getNameImg().replaceAll(" ","_");
            final String imageNameKey = request.getIdPerson().getId() + "_" + nameProject + "_" + imageName;
            final File fileImage = File.createTempFile(imageNameKey, request.getExtensionImg());
            fileImage.deleteOnExit();
            final OutputStream outImage = new FileOutputStream(fileImage);
            outImage.write(request.getImage());
            outImage.close();
            s3.putObject(new PutObjectRequest(settingsConfig.awsS3Bucket, imageNameKey, fileImage));
            return imageNameKey;
        }
        catch (Exception e)
        {
            //throw new ServiceException("Error loading files to AWS S3", e);
            return null;
        }
    }
    private String loadProjectImageToAWS(@RequestBody ProjectModifyRequest request, AmazonS3 s3, String nameProject)
    {
        try
        {
            final String imageName = request.getNameImg().replaceAll(" ","_");
            final String imageNameKey = request.getPerson().getId() + "_" + nameProject + "_" + imageName;
            final File fileImage = File.createTempFile(imageNameKey, request.getExtensionImg());
            fileImage.deleteOnExit();
            final OutputStream outImage = new FileOutputStream(fileImage);
            outImage.write(request.getImage());
            outImage.close();
            s3.putObject(new PutObjectRequest(settingsConfig.awsS3Bucket, imageNameKey, fileImage));
            return imageNameKey;
        }
        catch (Exception e)
        {
            //throw new ServiceException("Error loading files to AWS S3", e);
            return null;
        }
    }

    /**
     * Nombre:                  loadProjectCanvasModelToAWS.
     * Descripcion:             Metodo que sube el archivo del modelo canvas de un proyecto al S3
     * @version                 1.0
     * @author                  martin
     * @since                   12/8/2016
     *
     * @param request objeto con el archivo a subir
     * @param s3 objeto para conexión al AWS S3
     * @param nameProject nombre del proyecto
     * @return String nombre del archivo del modelo canvas subido al S3
     */
    private String loadProjectCanvasToAWS(@RequestBody CreateProjectRequest request, AmazonS3 s3, String nameProject)
    {
        try
        {
            final String nameCanvasModel = request.getNameImgCanvas().replaceAll(" ","_");
            final String nameCanvasModelKey = request.getIdPerson().getId() + "_" + nameProject + "_" + nameCanvasModel;
            final File fileCanvasModel = File.createTempFile(nameCanvasModelKey, request.getExtensionImgCanvas());
            fileCanvasModel.deleteOnExit();
            final OutputStream outCanvasModel = new FileOutputStream(fileCanvasModel);
            outCanvasModel.write(request.getCanvasModel());
            outCanvasModel.close();
            s3.putObject(new PutObjectRequest(settingsConfig.awsS3Bucket, nameCanvasModelKey, fileCanvasModel));
            return nameCanvasModelKey;
        }
        catch (Exception e)
        {
            //throw new ServiceException("Error loading files to AWS S3", e);
            return null;
        }
    }
    private String loadProjectCanvasToAWS(@RequestBody ProjectModifyRequest request, AmazonS3 s3, String nameProject)
    {
        try
        {
            final String nameCanvasModel = request.getNameImgCanvas().replaceAll(" ","_");
            final String nameCanvasModelKey = request.getPerson().getId() + "_" + nameProject + "_" + nameCanvasModel;
            final File fileCanvasModel = File.createTempFile(nameCanvasModelKey, request.getExtensionImgCanvas());
            fileCanvasModel.deleteOnExit();
            final OutputStream outCanvasModel = new FileOutputStream(fileCanvasModel);
            outCanvasModel.write(request.getCanvasModel());
            outCanvasModel.close();
            s3.putObject(new PutObjectRequest(settingsConfig.awsS3Bucket, nameCanvasModelKey, fileCanvasModel));
            return nameCanvasModelKey;
        }
        catch (Exception e)
        {
            //throw new ServiceException("Error loading files to AWS S3", e);
            return null;
        }
    }

    /**
     * Nombre:                  loadProjectContactPhotoToAWS.
     * Descripcion:             Metodo que sube el archivo de imagen del contacto de un proyecto al S3
     * @version                 1.0
     * @author                  martin
     * @since                   12/8/2016
     *
     * @param request objeto con el archivo a subir
     * @param s3 objeto para conexión al AWS S3
     * @param nameProj nombre del proyecto
     * @return String nombre del archivo de imagen del contacto subido al S3
     */
    private String loadProjectContactPhotoToAWS(@RequestBody CreateProjectRequest request, AmazonS3 s3, String nameProj)
    {
        try
        {
            final String contactNamePicture = request.getContact().getNamePicture().replaceAll(" ","_");
            final String nameKey = request.getIdPerson().getId() + "_" + nameProj + "_" + contactNamePicture;
            final File fileContactPicture = File.createTempFile(nameKey, request.getContact().getExtensionPicture());
            fileContactPicture.deleteOnExit();
            final OutputStream outContactPicture = new FileOutputStream(fileContactPicture);
            outContactPicture.write(request.getContact().getBytesPicture());
            outContactPicture.close();
            s3.putObject(new PutObjectRequest(settingsConfig.awsS3Bucket, nameKey, fileContactPicture));
            return nameKey;
        }
        catch (Exception e)
        {
            //throw new ServiceException("Error loading files to AWS S3", e);
            return null;
        }
    }
    private String loadProjectContactPhotoToAWS(@RequestBody ProjectModifyRequest request, AmazonS3 s3, String nameProj)
    {
        try
        {
            final String contactNamePicture = request.getContact().getNamePicture().replaceAll(" ","_");
            final String nameKey = request.getPerson().getId() + "_" + nameProj + "_" + contactNamePicture;
            final File fileContactPicture = File.createTempFile(nameKey, request.getContact().getExtensionPicture());
            fileContactPicture.deleteOnExit();
            final OutputStream outContactPicture = new FileOutputStream(fileContactPicture);
            outContactPicture.write(request.getContact().getBytesPicture());
            outContactPicture.close();
            s3.putObject(new PutObjectRequest(settingsConfig.awsS3Bucket, nameKey, fileContactPicture));
            return nameKey;
        }
        catch (Exception e)
        {
            //throw new ServiceException("Error loading files to AWS S3", e);
            return null;
        }
    }

    /**
     * Nombre:                  loadFilesToAWS.
     * Descripcion:             Metodo que sube archivos al S3
     * @version                 1.0
     * @author                  martin
     * @since                   12/8/2016
     *
     * @param request objeto con los archivos a subir
     * @param obj proyecto a crear
     * @return void
     */
    private void loadFilesToAWS(@RequestBody CreateProjectRequest request, Project obj)
    {
        try
        {
            final AWSCredentials credentials =
                    new BasicAWSCredentials(settingsConfig.awsS3AccessKeyID, settingsConfig.awsS3SecretAccessKey);
            final AmazonS3 s3 = new AmazonS3Client(credentials);
            final String nameProject = request.getName().replaceAll(" ","_");

            //final String nameVideoKey = loadProjectVideoToAWS(request, s3, nameProject);
            //obj.setVideo(nameVideoKey);

            final String imageNameKey = loadProjectImageToAWS(request, s3, nameProject);
            obj.setImage(imageNameKey);

            final String nameCanvasModelKey = loadProjectCanvasToAWS(request, s3, nameProject);
            obj.setCanvasModel(nameCanvasModelKey);

            if ((obj.getPersonType() == PersonType.FISICA) && (obj.getContact() != null))
            {
                final String contactNamePictureKey = loadProjectContactPhotoToAWS(request, s3, nameProject);
                obj.getContact().setPhoto(contactNamePictureKey);
            }
        }
        catch (Exception e)
        {
            //throw new ServiceException("Error loading files to AWS S3", e);
        }
    }
    private void loadFilesToAWS(@RequestBody ProjectModifyRequest request, Project obj)
    {
        try
        {
            final AWSCredentials credentials =
                    new BasicAWSCredentials(settingsConfig.awsS3AccessKeyID, settingsConfig.awsS3SecretAccessKey);
            final AmazonS3 s3 = new AmazonS3Client(credentials);
            final String nameProject = request.getName().replaceAll(" ","_");

            if (request.getImage() != null && request.getImage().length > 0)
            {
                final String imageNameKey = loadProjectImageToAWS(request, s3, nameProject);
                obj.setImage(imageNameKey);
            }
            else {
                obj.setImage(request.getNameImg() + request.getExtensionImg());
            }

            if (request.getCanvasModel() != null && request.getCanvasModel().length > 0) {
                final String nameCanvasModelKey = loadProjectCanvasToAWS(request, s3, nameProject);
                obj.setCanvasModel(nameCanvasModelKey);
            }
            else {
                obj.setCanvasModel(request.getNameImgCanvas() + request.getExtensionImgCanvas());
            }

            if ((obj.getPersonType() == PersonType.FISICA) && (obj.getContact() != null))
            {
                if (request.getContact() != null &&
                        request.getContact().getBytesPicture() != null && request.getContact().getBytesPicture().length > 0) {
                    final String contactNamePictureKey = loadProjectContactPhotoToAWS(request, s3, nameProject);
                    obj.getContact().setPhoto(contactNamePictureKey);
                }
                else {
                    if (request.getContact() != null) {
                        obj.getContact().setPhoto(request.getContact().getNamePicture() + request.getContact().getExtensionPicture());
                    }
                }
            }
        }
        catch (Exception e)
        {
            //throw new ServiceException("Error loading files to AWS S3", e);
        }
    }

    /**
     * Nombre:                  createProject.
     * Descripcion:             Metodo que crea un proyecto en la base de datos
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/4/2015
     *
     * @param request objeto con los datos del proyecto a insertar
     * @return Objeto con la informacion del objeto creado en JSON
     */
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public @ResponseBody CreateProjectResponse createProject(@RequestBody CreateProjectRequest request)
    {
        CreateProjectResponse response;
        try
        {
            Project obj;
            Project retorno;
            obj = modelMapper.map( request, Project.class );
            obj.setContact(null);
            obj.setCompany(null);
            setProjectObjects(obj, request);

            loadFilesToAWS(request, obj);

            retorno = projectService.createProject(obj);

            if(request.getDocumentsToDelete() != null && request.getDocumentsToDelete().size() > 0 )
            {
                for(CreateProjectRequest.Document document : request.getDocumentsToDelete())
                    projectService.deleteDocument( modelMapper.map( document, Document.class ));
            }
            response = modelMapper.map( retorno, CreateProjectResponse.class );
        }
        catch (Exception e)
        {
            throw new ServiceException("Error saving project into database", e);
        }

        return response;
    }

    private void setProjectObjects(Project project, ProjectModifyRequest request)
    {
        InternetInfo internetInfo;
        internetInfo = modelMapper.map( request.getInternetInformation(), InternetInfo.class );
        Location location;
        location = modelMapper.map( request.getLocation(), Location.class );
        ContactInformation contactInformation;
        contactInformation = modelMapper.map( request.getContactInfo(), ContactInformation.class );

        if (project.getPersonType() == PersonType.FISICA)
        {
            Contact contact;
            contact = modelMapper.map( request.getContact(), Contact.class);
            contact.setInternetInformation(internetInfo);
            contact.setLocation(location);
            contact.setContactInformation(contactInformation);
            project.setContact(contact);
        }
        else if (project.getPersonType() == PersonType.MORAL)
        {
            Company company;
            company = modelMapper.map( request.getCompany(), Company.class);
            company.setInternetInformation(internetInfo);
            company.setLocation(location);
            company.setContactInformation(contactInformation);
            project.setCompany(company);
        }
    }

    private void setProjectObjects(Project project, CreateProjectRequest request)
    {
        InternetInfo internetInfo;
        internetInfo = modelMapper.map( request.getInternetInformation(), InternetInfo.class );
        Location location;
        location = modelMapper.map( request.getLocation(), Location.class );
        ContactInformation contactInformation;
        contactInformation = modelMapper.map( request.getContactInformation(), ContactInformation.class );

        if (project.getPersonType() == PersonType.FISICA)
        {
            Contact contact;
            contact = modelMapper.map( request.getContact(), Contact.class);
            contact.setInternetInformation(internetInfo);
            contact.setLocation(location);
            contact.setContactInformation(contactInformation);
            project.setContact(contact);
        }
        else if (project.getPersonType() == PersonType.MORAL)
        {
            Company company;
            company = modelMapper.map( request.getCompany(), Company.class);
            company.setInternetInformation(internetInfo);
            company.setLocation(location);
            company.setContactInformation(contactInformation);
            project.setCompany(company);
        }

    }

    /**
     * Nombre:                  updateProject.
     * Descripcion:             Metodo que edita los datos de un proyecto
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/4/2015
     *
     * @param request objeto con los datos a modificar del proyecto
     */
    @RequestMapping( method = RequestMethod.PUT, consumes = "application/json" )
    @ResponseStatus( HttpStatus.OK )
    public void updateProject(  @RequestBody ProjectModifyRequest request )
    {
        try
        {
            Project project;
            project = modelMapper.map(request, Project.class);
            project.setContact(null);
            project.setCompany(null);
            setProjectObjects(project, request);

            loadFilesToAWS(request, project);

            projectService.updateProject( project );

            if(request.getDocumentsToDelete() != null && request.getDocumentsToDelete().size() > 0 )
            {
                for(ProjectModifyRequest.Document document : request.getDocumentsToDelete())
                    projectService.deleteDocument( modelMapper.map( document, Document.class ));
            }

            if(request.getCalendarsToDelete() != null && request.getCalendarsToDelete().size() > 0 )
            {
                for(ProjectModifyRequest.Calendar calendar : request.getCalendarsToDelete())
                    projectService.deleteActivity( modelMapper.map( calendar, Calendar.class ) );
            }

            if(request.getResourcesToDelete() != null && request.getResourcesToDelete().size() > 0 )
            {
                for(ProjectModifyRequest.Resource resource : request.getResourcesToDelete())
                projectService.deleteResource( modelMapper.map( resource, Resource.class ) );
            }

            //if(request.getRewardsToDelete() != null && request.getRewardsToDelete().size() > 0 )
            //{
            //    for(ProjectModifyRequest.Reward reward : request.getRewardsToDelete())
            //        projectService.deleteReward( modelMapper.map( reward, Reward.class ) );
            //}

        }
        catch (Exception e)
        {
            throw new ServiceException("Error saving user into database", e);
        }
    }


    /**
     * Nombre:                  updateUser.
     * Descripcion:             Metodo que cambia el password de un usuario
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/3/2015
     *
     */
    @RequestMapping( value = "/{id}",  method = RequestMethod.DELETE )
    @ResponseStatus( HttpStatus.OK )
    public void deleteProject(  @PathVariable long id )
    {
        try
        {
            projectService.deleteProject( id );
        }
        catch (Exception e)
        {
            throw new ServiceException("Error deleting project into database", e);
        }
    }

    @RequestMapping( value = "/activate/{id}", method = RequestMethod.POST )
    @ResponseStatus( HttpStatus.OK )
    public void activateProject( @PathVariable long id )
    {
        try
        {
            projectService.activateProject( id );
        }
        catch (Exception e)
        {
            throw new ServiceException("Error deleting project into database", e);
        }
    }

    @RequestMapping( value = "/deactivate/{id}", method = RequestMethod.POST )
    @ResponseStatus( HttpStatus.OK )
    public void deactivateProject( @PathVariable long id )
    {
        try
        {
            projectService.deactivateProject( id );
        }
        catch (Exception e)
        {
            throw new ServiceException("Error deleting project into database", e);
        }
    }

    @RequestMapping( value = "/change-priority", method = RequestMethod.PUT, consumes = "application/json" )
    @ResponseStatus( HttpStatus.OK )
    public void changePriorityType(  @RequestBody ChangePriorityProjectRequest request )
    {
        try
        {
            Project project = modelMapper.map(request, Project.class);
            projectService.changePriorityProject( project.getId(), project.getPriorityType() );
        }
        catch (Exception e)
        {
            throw new ServiceException("Error deleting project into database", e);
        }
    }
    //endregion

    //region metodos privados


    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  cambiarFormato
     * Descripcion:             Método que cambia el fotmarto ##,## a ##.##
     * @version                 1.0
     * @author                  buchyo
     * @since                   21/01/2016
     *
     * @param numero numero a convertir
     * @return nuevo numero
     */
    private double cambiarFormato(double numero)
    {
        DecimalFormatSymbols simbolo;
        simbolo = new DecimalFormatSymbols();
        simbolo.setDecimalSeparator('.');
        DecimalFormat df;
        df = new DecimalFormat("##########.##",simbolo);
        double retorno;

        retorno = Double.parseDouble(df.format(numero));

        return retorno;
    }

    //endregion
}
