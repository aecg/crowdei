package net.softclear.crowdei.backend.web.rest.payments.to.response;



import lombok.Data;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  MercadoPagoResponse
 * Descripcion:             Clase que hace de respuesta para la solicitud del link de mercado pago
 * @version                 1.0
 * @author                  guzmle
 * @since                   1/4/2016
 *
 */
@Data
public class MercadoPagoResponse
{
    private String url;
}
