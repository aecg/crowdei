package net.softclear.crowdei.backend.repository;

import net.softclear.crowdei.backend.model.jpa.Project;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  CompanyRepositoryCustom
 * Descripcion:             Contrato para todas las operaciones personalizadas en la base de datos que
 * involucran la entidad company
 * @version                 1.0
 * @author                  buchyo
 * @since                   11/03/2016
 *
 */
public interface CompanyRepositoryCustom
{
    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  findAll
     * Descripcion:             Metodo que consulta todas las compañias que haya registrado un usuario
     * dado un parametro de busqueda
     * @version                 1.0
     * @author                  buchyo
     * @since                   11/03/2016
     *
     * @param idUsuario identificador del usuario
     * @param nameCompany nombre de la compañia
     * @param pageable si es paginable
     * @return lista de proyectos con su compañia
     */
    List<Project> findAllXIdUser(Long idUsuario, String nameCompany, Pageable pageable);
}
