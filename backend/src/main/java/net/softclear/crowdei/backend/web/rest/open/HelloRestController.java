package net.softclear.crowdei.backend.web.rest.open;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  HelloRestController
 * Descripcion:             Clase que posee la definicion del servicio rest que permite
 * testear que todos se encuentren levantados
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@RestController
@RequestMapping("/open*")
public class HelloRestController
{
    /**
     * Nombre:                  hello.
     * Descripcion:             Metodo que verifica que los servicios se encuentren levantaods
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/22/2015
     *
     * @return si esta bien retorna hello
     */
	@RequestMapping(value = "/hello", produces = "text/plain; charset=utf-8")
	public @ResponseBody String hello()
    {
		return "Hello";
	}
}
