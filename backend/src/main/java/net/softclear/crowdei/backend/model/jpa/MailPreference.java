package net.softclear.crowdei.backend.model.jpa;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  mail_preference
 * Descripcion:             Clase que modela la entidad mail_Preference dentro del sistema
 * @version                 1.0
 * @author                  buchyo
 * @since                   12/08/2015
 *
 */
@Entity
@Table(name = "mail_preference")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MailPreference
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;

    @Column( name = "description", nullable = false )
    private String description;
}
