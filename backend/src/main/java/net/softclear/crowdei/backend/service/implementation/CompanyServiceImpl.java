package net.softclear.crowdei.backend.service.implementation;

import net.softclear.crowdei.backend.exception.ServiceException;
import net.softclear.crowdei.backend.model.jpa.Project;
import net.softclear.crowdei.backend.repository.CompanyRepository;
import net.softclear.crowdei.backend.service.CompanyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  CompanyServiceImpl
 * Descripcion:             Clase que implementa el contrato del servicio
 * @version                 1.0
 * @author                  buchyo
 * @since                   11/03/2016
 *
 */
@Service
@Transactional
public class CompanyServiceImpl implements CompanyService
{
    //region Atributos
    private static Logger logger = LoggerFactory.getLogger(ProjectServiceImpl.class);
    @Autowired
    private CompanyRepository companyRepository;
    //endregion

    //region Metodos
    @Override
    public List<Project> getCompanyXIdUser(Long idUsuario, String nameCompany, Pageable pageable)
    {
        logger.debug( "Entrando al metodo getCompanyXIdUser" );
        List<Project> listProjects = null;

        try
        {
            listProjects = companyRepository.findAllXIdUser(idUsuario, nameCompany, pageable);

        }
        catch ( Exception e )
        {
            logger.error( "Ha ocurrido un error consultando las compañias de un usuario", e );
            throw new ServiceException( "Error reading projects from database", e );
        }

        logger.debug( "Saliendo del metodo getCompanyXIdUser" );
        return listProjects;
    }
    //endregion
}
