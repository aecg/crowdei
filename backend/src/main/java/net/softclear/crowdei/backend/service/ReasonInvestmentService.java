package net.softclear.crowdei.backend.service;

import net.softclear.crowdei.backend.model.jpa.ReasonInvestment;

import java.util.List;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  ExperienceLevelService
 * Descripcion:             Contrato para todos los servicios que involucran la entidad HowFind
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
public interface ReasonInvestmentService
{
    /**
     * Nombre:                  getAll.
     * Descripcion:             Metodo que obtiene todas las razones de inversiones
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/22/2015
     *
     * @return lista de razones de inversion
     */
	List<ReasonInvestment> getAll();
}
