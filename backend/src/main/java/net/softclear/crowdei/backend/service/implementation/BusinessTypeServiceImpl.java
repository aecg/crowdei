package net.softclear.crowdei.backend.service.implementation;



import net.softclear.crowdei.backend.model.jpa.BusinessType;
import net.softclear.crowdei.backend.repository.BusinessTypeRepository;
import net.softclear.crowdei.backend.service.BusinessTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  ExperienceLevelServiceImpl
 * Descripcion:             Clase que implementa el contrato para las operaciones de nivel de experiencia
 * @version                 1.0
 * @author                  guzmle
 * @since                   02/12/15
 *
 */
@Service
@Transactional
public class BusinessTypeServiceImpl implements BusinessTypeService
{
    //region Atributos

    private static Logger logger = LoggerFactory.getLogger( BusinessTypeServiceImpl.class );

    /**
     * Repositorio para la entidad de tipos de negocios.
     */
    @Autowired
    private BusinessTypeRepository repository;

    //endregion

    //region Implementacion


    @Override
    public List<BusinessType> getAll()
    {
        logger.debug( "Entrando al metodo getAll" );

        List<BusinessType> response;
        response = repository.findAll();

        logger.debug( "Saliendo del metodo getAll" );
        return response;
    }

    //endregion
}
