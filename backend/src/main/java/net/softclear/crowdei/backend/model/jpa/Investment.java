package net.softclear.crowdei.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;
import net.softclear.crowdei.backend.model.types.MethodPayment;
import net.softclear.crowdei.backend.model.types.Status;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  Investment
 * Descripcion:             Clase para modelar la entidad inversion
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@Entity
@Table(name="investment")
@Setter
@Getter
public class Investment
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;

    @Temporal( TemporalType.TIMESTAMP )
    @Column( name = "date", nullable = false )
    private Date date;

    @Column( name = "amount", nullable = false )
    private Double amount;

    @Column( name = "data", nullable = true, length = 2000 )
    private String data;

    @Enumerated( EnumType.STRING )
    @Column( nullable = false )
    private Status status;

    @Enumerated( EnumType.STRING)
    @Column( nullable = false )
    private MethodPayment methodPayment;

    @ManyToOne()
    @JoinColumn( name = "project_id" )
    Project project;

    @ManyToOne()
    @JoinColumn( name = "person_id" )
    Person person;

    @ManyToOne()
    @JoinColumn( name = "reward_id" )
    Reward reward;

}
