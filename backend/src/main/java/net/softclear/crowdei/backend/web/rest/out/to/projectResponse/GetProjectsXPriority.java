package net.softclear.crowdei.backend.web.rest.out.to.projectResponse;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Date;
import java.util.List;



/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  GetProjectsXPriority
 * Descripcion:             Objeto que posee los atributos de respuesta cuando se consulta los projectos por
 *                          su prioridad
 * @version                 1.0
 * @author                  buchyo
 * @since                   06/01/2016
 *
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetProjectsXPriority
{
    private Long          count;
    private List<Project> projects;

    /**
     * Clase interna que posee la definicion del JSON para el objeto Proyecto.
     */
    @Data
    public static class Project
    {
        private Long   id;
        private String name;
        private String description;
        private String briefDescription;
        private Date   dateRegister;
        private double objetive;
        private String image;
        private Double obtained;
        private int    investors;
        private Double percent;
        private Long   leftDays;
        private String status;
        private String priorityType;
        private int    position;
        private Double lastInvestment;
        private String dateLastInvestment;
        private String personType;
        private Contact contact;
        private Currency currency;
        private Person person;
        private Company company;
        private Date   limitDate;
        private EconomicActivity economicActivity;

    }

    /**
     * Clase que posee la definicion de objeto json de contact que recibe el servicio de crear proyecto.
     */
    @Data
    public static class Contact
    {
        private Long id;
        private String fullname;
        private String photo;
    }

    /**
     * Clase interna que posee la defincion del JSON para la ciudad.
     */
    @Data
    public static class City
    {
        private Long   id;
        private String name;
        private State state;

    }


    /**
     * Clase interna que posee la defincion del JSON para el estado.
     */
    @Data
    public static class State
    {
        private Long   id;
        private String name;
        private Country country;

    }


    /**
     * Clase interna que posee la defincion del JSON para pais.
     */
    @Data
    public static class Country
    {
        private Long   id;
        private String name;
        private String iso;
    }


    /**
     * Clase interna que posee la definicion para la entidad de currency.
     */
    @Data
    public static class Currency
    {
        private Long id;
        private String symbol;
    }


    /**
     * Clase interna que posee la definicion para la entidad de person.
     */
    @Data
    public static class Person
    {
        private String image;
    }


    /**
     * Clase que posee la definicion de objeto json de company que recibe el servicio de crear proyecto.
     */
    @Data
    public static class Company
    {
        private Long id;
        private String rfc;
        private String name;
    }


    /**
     * Clase interna que posee la definicion para la entidad de actividad económica.
     */
    @Data
    public static class EconomicActivity
    {
        private Long id;
    }
}
