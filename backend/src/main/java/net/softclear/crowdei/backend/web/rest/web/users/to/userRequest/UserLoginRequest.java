package net.softclear.crowdei.backend.web.rest.web.users.to.userRequest;



import lombok.Data;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  UserLoginRequest
 * Descripcion:             Clase que con la defincion JSON de la peticion de login del usuario
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@Data
public class UserLoginRequest
{
    private String username;
    private String password;
}
