package net.softclear.crowdei.backend.web.rest.web.projects.to.projectResponse;

import lombok.Data;

import java.util.Date;
import java.util.List;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  CreateProjectRequest
 * Descripcion:             Clase que posee la definicion de objeto json que recibe el servicio de crear proyecto
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/21/2015
 *
 */
@Data
public class CreateProjectRequest
{
    // tab 1
    private Long id;
    private String personType;
    private Contact contact;
    private Company company;
    private Location location;
    private ContactInformation contactInformation;
    private InternetInformation internetInformation;

    // tab 2
    private String name;
    private byte[] image;
    private String nameImg;
    private String extensionImg;
    private String video;
    private String nameVideo;
    private String extension;
    private String briefDescription;
    private String crowdfundingType;
    private String executionPlace;
    private String generalObjetive;
    private String specificObjetives;
    private ImpactSector impactSector;
    private EconomicActivity economicActivity;
    private int monthExecution;
    private List<Calendar> calendars;
    private byte[] canvasModel;
    private String nameImgCanvas;
    private String extensionImgCanvas;
    private String description;

    // tab 3
    private List<Reward> rewards;

    // tab 4
    private String companyShares;
    private String minimumInvesment;
    private String invesmentGoal;

    // tab 5
    private double objetive;
    private Currency currency;
    private List<Resource> resources;

    // tab 6
    private List<Document> documents;

    // internal data
    private Date dateRegister;
    private String priorityType;
    private int position;
    private String status;
    private Item  idPerson;
    private List<Document> documentsToDelete;


    /**
     * Clase que posee la definicion de objeto json de contact que recibe el servicio de crear proyecto.
     */
    @Data
    public static class Contact
    {
        private Long id;
        private String fullname;
        private byte[] bytesPicture;
        private String namePicture;
        private String extensionPicture;
    }

    /**
     * Clase que posee la definicion de objeto json de company que recibe el servicio de crear proyecto.
     */
    @Data
    public static class Company
    {
        private Long id;
        private String rfc;
        private String name;
    }

    /**
     * Clase interna que posee la defincion para la entidad de location.
     */
    @Data
    public static class Location
    {
        private Long id;
        private String address;
        private String municipality;
        private String postalCode;
        private String locationReference;
        private Item city;
    }

    /**
     * Clase interna que posee la defincion para la entidad de contactInformation.
     */
    @Data
    public static class ContactInformation
    {
        private Long id;
        private String phoneOne;
        private String phoneTwo;
        private String mobile;
        private String email;
    }

    /**
     * Clase interna que posee la defincion para la entidad de internetInformation.
     */
    @Data
    public static class InternetInformation
    {
        private Long id;
        private String webPage;
        private String linkedin;
        private String facebook;
        private String twitter;
    }

    /**
     * Clase interna que posee la definicion del JSON para el objeto impactSector.
     */
    @Data
    public static class ImpactSector
    {
        private Long id;
        private String description;
    }

    /**
     * Clase interna que posee la definicion del JSON para el objeto economicActivity.
     */
    @Data
    public static class EconomicActivity
    {
        private Long id;
        private String description;
    }

    /**
     * Clase interna que posee la definicion del JSON para el objeto calendar.
     */
    @Data
    public static class Calendar
    {
        private Long id;
        private String activity;
        private Date estimatedDate;
    }

    /**
     * Clase interna que posee la definicion del JSON para el objeto reward.
     */
    @Data
    public static class Reward
    {
        private Long id;
        private String description;
        private double amount;
        private int available;
        private Date estimatedDate;
    }

    /**
     * Clase interna que posee la definicion del JSON para el objeto currency.
     */
    @Data
    public static class Currency
    {
        private Long id;
        private String description;
        private String code;
    }

    /**
     * Clase interna que posee la definicion del JSON para el objeto resource.
     */
    @Data
    public static class Resource
    {
        private Long id;
        private String concept;
        private Double amount;
        private Integer percentage;
    }

    /**
     * Clase que posee la definicion de objeto json de Document que recibe el servicio de crear proyecto.
     */
    @Data
    public static class Document
    {
        private Long id;
        private byte[] file;
        private String name;
    }







    /**
     * Clase que posee la definicion de objeto json de person que recibe el servicio de crear proyecto.
     */
    @Data
    public static class Item
    {
        private Long   id;
    }


}
