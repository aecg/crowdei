package net.softclear.crowdei.backend.web.rest.payments.to.response;

import lombok.Data;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  TransferResponse
 * Descripcion:             Objeto que posee los atributos de respuesta cuando
 * guarda una transferencia
 * @version                 1.0
 * @author                  buchyo
 * @since                   13/04/2016
 *
 */
@Data
public class TransferResponse
{
    private Long idTransfer;
}
