package net.softclear.crowdei.backend.web.rest.web.projects.to.projectRequest;



import lombok.Data;

import java.util.Date;



/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  FindProjectsInvestmentXIdXParamsRequest
 * Descripcion:             Objeto que posee la definicion del JSON que es necesario para consultar proyectos
 *                          en los que invirtio un usuario
 * @version                 1.0
 * @author                  buchyo
 * @since                   04/03/2016
 *
 */
@Data
public class FindProjectsInvestmentXIdXParamsRequest
{
    private Long   idUser;
    private String nameCompany;
    private String nameProject;
    private Date   startDate;
    private Date   endDate;
}
