package net.softclear.crowdei.backend.web.rest.web.investments.to.investmentRequest;

import lombok.Data;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  InvestmentTransferMyProjectRequest
 * Descripcion:             Objeto que posee los atributos de consulta
 * @version                 1.0
 * @author                  buchyo
 * @since                   13/04/2016
 *
 */
@Data
public class InvestmentTransferMyProjectRequest
{
    private Long idTransfer;
}
