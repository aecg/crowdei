package net.softclear.crowdei.backend.model.types;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  Priority
 * Descripcion:             Enumarado para los diferentes tipos de prioridad dentro del proyecto
 * @version                 1.0
 * @author                  buchyo
 * @since                   06/01/2016
 */
public enum Priority
{
    /**
     * Destacado.
     */
    OUTSTANDING,
    /**
     * Ultima version.
     */
    LASTINVESTMENT,
    /**
     * Recien pagado.
     */
    JUSTPAID,
    /**
     * Nuevo.
     */
    NEW,
    /**
     * Exitoso.
     */
    SUCCESSFULL,
    /**
     * Destacado.
     */
    REGULAR,
    /**
     * No aplica.
     */
    NOTAPPLY

}
