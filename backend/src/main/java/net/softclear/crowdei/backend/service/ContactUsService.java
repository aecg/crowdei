package net.softclear.crowdei.backend.service;



/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  contactUsService
 * Descripcion:             Contrato para los servicios que involucran la seccion de contactenos
 * @version                 1.0
 * @author                  buchyo
 * @since                   11/02/2016
 *
 */
public interface ContactUsService
{
    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  sendMail
     * Descripcion:             Metodo que envia un correo de contactenos
     * @version                 1.0
     * @author                  buchyo
     * @since                   11/02/2016
     *
     * @param nombre nombre de la persona que hace el contacto
     * @param apellido persona de la persona que hace el contacto
     * @param correo correo de la persona que hace el contacto
     * @param telefono telefono de la persona que hace el contacto
     * @param mensaje mensaje de la persona que hace el contacto
     */
    void sendMail(String nombre, String apellido, String correo, String telefono, String mensaje);

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  sendMail
     * Descripcion:             Metodo que envia un correo de contactenos
     * @version                 1.0
     * @author                  buchyo
     * @since                   11/02/2016
     *
     * @param nombre nombre de la persona que hace el contacto
     * @param correo correo de la persona que hace el contacto
     * @param mensaje mensaje de la persona que hace el contacto
     */
    void sendMail(String nombre, String correo, String mensaje);

    /**
     *Sistema:                 Crowdei.
     *
     * Nombre:                  sendMailContactProject
     * Descripcion:             Método que envia un correo de contactenos a la persona
     * contacto de un proyecto
     * @version                 1.0
     * @author                  buchyo
     * @since                   29/03/2016
     *
     * @param idProyecto  id del proyecto
     * @param nombre nombre de la persona que hace el contacto
     * @param apellido persona de la persona que hace el contacto
     * @param correo correo de la persona que hace el contacto
     * @param telefono telefono de la persona que hace el contacto
     * @param mensaje mensaje de la persona que hace el contacto
     */
    void sendMailContactProject(Long idProyecto, String nombre, String apellido, String correo,
                                String telefono, String mensaje);

}
