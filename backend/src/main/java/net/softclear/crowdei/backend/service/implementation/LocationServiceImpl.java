package net.softclear.crowdei.backend.service.implementation;



import net.softclear.crowdei.backend.model.jpa.City;
import net.softclear.crowdei.backend.model.jpa.Country;
import net.softclear.crowdei.backend.model.jpa.State;
import net.softclear.crowdei.backend.repository.CityRepository;
import net.softclear.crowdei.backend.repository.CountryRepository;
import net.softclear.crowdei.backend.repository.StateRepository;
import net.softclear.crowdei.backend.service.LocationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  LocationServiceImpl
 * Descripcion:             Clase que implementa el contrato del servicio de locaciones
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/9/2015
 *
 */
@Service
@Transactional
public class LocationServiceImpl implements LocationService
{
    //region Atributos

    private static Logger logger = LoggerFactory.getLogger( LocationServiceImpl.class );
    @Autowired
    private CityRepository    cityRepository;
    @Autowired
    private StateRepository   stateRepository;
    @Autowired
    private CountryRepository countryRepository;
    //endregion

    //region Implementacion


    /**
     * Nombre:                  getCountries.
     * Descripcion:             Metodo que obtiene la lista de paises
     *
     * @return lista de paises
     *
     * @version 1.0
     * @author guzmle
     * @since 12/9/2015
     */
    @Override
    public List<Country> getCountries()
    {
        logger.debug( "Entrando al metodo getCountries" );

        final List<Country> response = countryRepository.findAll();

        logger.debug( "Saliendo del metodo getCountries" );
        return response;
    }


    /**
     * Nombre:                  getStates.
     * Descripcion:             Metodo que obtiene la lista de estados de un pais
     *
     * @param country datos del pais
     *
     * @return lista de estados
     *
     * @version 1.0
     * @author guzmle
     * @since 12/9/2015
     */
    @Override
    public List<State> getStates( Country country )
    {
        logger.debug( "Entrando al metodo getStates" );

        final List<State> response = stateRepository.findByCountry( country );

        logger.debug( "Saliendo del metodo getStates" );
        return response;
    }


    /**
     * Nombre:                  getCities.
     * Descripcion:             Metodo que obtiene la lista de ciudades de un estado
     *
     * @param state datos del estado
     *
     * @return lista de ciudades
     *
     * @version 1.0
     * @author guzmle
     * @since 12/9/2015
     */
    @Override
    public List<City> getCities( State state )
    {
        logger.debug( "Entrando al metodo getCities" );

        final List<City> response = cityRepository.findByState( state );

        logger.debug( "Saliendo del metodo getCities" );
        return response;
    }

    //endregion
}
