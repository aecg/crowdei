package net.softclear.crowdei.backend.service.implementation;



import net.softclear.crowdei.backend.model.jpa.ReasonInvestment;
import net.softclear.crowdei.backend.repository.ReasonInvestmentRepository;
import net.softclear.crowdei.backend.service.ReasonInvestmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;



/**
 * Created by leonardo on 02/12/15.
 */
@Service
@Transactional
public class ReasonInvestmentServiceImpl implements ReasonInvestmentService
{
    //region Atributos

    private static Logger logger = LoggerFactory.getLogger( ReasonInvestmentServiceImpl.class );
    @Autowired
    private ReasonInvestmentRepository repository;

    //endregion

    //region Implementacion


    @Override
    public List<ReasonInvestment> getAll()
    {
        logger.debug( "Entrando al metodo getAll" );

        List<ReasonInvestment> retorno;
        retorno = repository.findAll();

        logger.debug( "Saliendo del metodo getAll" );
        return retorno;
    }

    //endregion
}
