package net.softclear.crowdei.backend.service.implementation;

import net.softclear.crowdei.backend.exception.ServiceException;
import net.softclear.crowdei.backend.model.jpa.Project;
import net.softclear.crowdei.backend.repository.ContactRepository;
import net.softclear.crowdei.backend.service.ContactService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  ContactServiceImpl
 * Descripcion:             Clase que implementa el contrato del servicio
 * @version                 1.0
 * @author                  buchyo
 * @since                   14/03/2016
 *
 */
@Service
@Transactional
public class ContactServiceImpl implements ContactService
{
    //region Atributos
    private static Logger logger = LoggerFactory.getLogger(ProjectServiceImpl.class);
    @Autowired
    private ContactRepository contactRepository;
    //endregion

    //region Metodos
    @Override
    public List<Project> getCompanyXIdCompany(Long idCompany, String nameContact, Pageable pageable)
    {
        logger.debug( "Entrando al metodo getCompanyXIdCompany" );
        List<Project> listProjects = null;

        try
        {
            listProjects = contactRepository.findAllXIdCompany(idCompany, nameContact, pageable);

        }
        catch ( Exception e )
        {
            logger.error( "Ha ocurrido un error consultando los contactos de una compañia", e );
            throw new ServiceException( "Error reading projects from database", e );
        }

        logger.debug( "Saliendo del metodo getCompanyXIdCompany" );
        return listProjects;
    }
    //endregion
}
