package net.softclear.crowdei.backend.web.rest.web.util;

import net.softclear.crowdei.backend.model.jpa.ImpactSector;
import net.softclear.crowdei.backend.service.ImpactSectorService;
import net.softclear.crowdei.backend.web.rest.web.util.to.GetObjectMasterResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Sistema:                 Crowdei.
 *
 * Nombre:                  ImpactSectorRestController
 * Descripcion:             Clase que posee la definicion y la implementacion de los servicios que involucran a
 * la entidad usuario
 * @version                 1.0
 * @author                  buchyo
 * @since                   15/03/2016
 *
 */
@RestController
@RequestMapping("/web/utils*")
public class ImpactSectorRestController
{
    //region Attributes

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ImpactSectorService service;

    //endregion

    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  ImpactSectorRestController
     * Descripcion:             Metodo que obtiene todos los sectores de impacto
     * @version                 1.0
     * @author                  buchyo
     * @since                   15/03/2016
     *
     * @return lista de los sectores de impacto
     */
    @RequestMapping( value = "/impact-sector", method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    GetObjectMasterResponse getAll()
    {
        List<ImpactSector> list;
        GetObjectMasterResponse response;
        List<GetObjectMasterResponse.Item> listResponse;
        GetObjectMasterResponse.Item item;

        list = service.getAll();
        response = new GetObjectMasterResponse();

        if ( list != null )
        {
            listResponse = new ArrayList<>();
            for ( ImpactSector obj : list )
            {
                item = modelMapper.map( obj, GetObjectMasterResponse.Item.class );
                listResponse.add( item );
            }

            response.setList( listResponse );
        }
        return response;
    }
}
