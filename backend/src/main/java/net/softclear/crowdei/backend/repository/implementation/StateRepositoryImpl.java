package net.softclear.crowdei.backend.repository.implementation;



import net.softclear.crowdei.backend.model.jpa.Country;
import net.softclear.crowdei.backend.model.jpa.State;
import net.softclear.crowdei.backend.repository.StateRepositoryCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  UserRepositoryImpl
 * Descripcion:             Clase que mimplementa el contrato para las operaciones personalizadas para la
 * entidad usuario
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
public class StateRepositoryImpl implements StateRepositoryCustom
{
    //region Atributos

	@PersistenceContext
	private EntityManager entityManager;

    //endregion

    //region Implementacion


    /**
     * Nombre:                  findByCountry.
     * Descripcion:             Metodo que consulta los estados de una ciudad
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/9/2015
     *
     * @param country datos del pais
     * @return lista de estados
     */
    @Override
    public List<State> findByCountry( Country country )
    {
        List<Predicate> predicates;
        CriteriaBuilder criteriaBuilder;
        CriteriaQuery<State> criteriaQuery;
        Root<State> root;
        Join joinEstado;
        Query query;
        List<State> response;


        predicates = new ArrayList<>(  );
        criteriaBuilder = entityManager.getCriteriaBuilder();
        criteriaQuery = criteriaBuilder.createQuery( State.class );
        root = criteriaQuery.from( State.class );
        joinEstado = root.join( "country" );



        predicates.add( criteriaBuilder.equal( joinEstado.get( "id" ), country.getId() ) );
        criteriaQuery.where( predicates.toArray( new Predicate[ predicates.size() ] ) );
        query = entityManager.createQuery(criteriaQuery);
        response = query.getResultList();

        return response;
    }

    //endregion
}
