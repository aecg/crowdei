package net.softclear.crowdei.backend.service;

import net.softclear.crowdei.backend.model.jpa.ExperienceLevel;

import java.util.List;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  OAuthService
 * Descripcion:             Contrato para todos los servicios que involucran la autenticacion
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
public interface ExperienceLevelService
{
    /**
     * Nombre:                  getAll.
     * Descripcion:             Metodo que obtiene todas los niveles de experiencia
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/22/2015
     *
     * @return lista con todos los niveles de experiencia
     */
    List<ExperienceLevel> getAll();
}
