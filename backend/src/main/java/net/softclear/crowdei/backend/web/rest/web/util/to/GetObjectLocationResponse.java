package net.softclear.crowdei.backend.web.rest.web.util.to;



import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;

import java.util.List;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  GetObjectLocationResponse
 * Descripcion:             Clase que posee la definicion del JSON para la respuesta de toda consulta de locacion
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@Data
@JsonInclude(Include.NON_NULL)
public class GetObjectLocationResponse
{
    private List<Item> list;


    /**
     * Clase interna que posee la definicion del JSON de la locacion.
     */
    @Data
    public static class Item
    {

        private Long id;
        private String name;

    }
}
