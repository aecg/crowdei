package net.softclear.crowdei.backend.repository;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import net.softclear.crowdei.backend.config.BeanConfig;
import net.softclear.crowdei.backend.config.DatabaseConfig;
import net.softclear.crowdei.backend.config.PropertyConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@WebAppConfiguration
@ActiveProfiles("test")
@Transactional
@Ignore
public class RepositoryTest {
	@Configuration
	@ComponentScan(
			basePackages = "net.softclear.crowdei.backend.service",
			basePackageClasses = {
					BeanConfig.class,
					DatabaseConfig.class,
					PropertyConfig.class
			}			
	)
	public static class Config {

	}
}
