package net.softclear.crowdei.backend.service;

import net.softclear.crowdei.backend.repository.InvestmentRepository;
import net.softclear.crowdei.backend.repository.UserRepository;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import net.softclear.crowdei.backend.config.BeanConfig;
import net.softclear.crowdei.backend.config.DatabaseConfig;
import net.softclear.crowdei.backend.config.PropertyConfig;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactoryBean;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@WebAppConfiguration
@ActiveProfiles("test")
@Transactional
@Ignore
public class ServiceTest {

	@Configuration
	@ComponentScan(
			basePackages = {
                    "net.softclear.crowdei.backend.service",
                    "net.softclear.crowdei.backend.util"
			},
			basePackageClasses = {
					BeanConfig.class,
					DatabaseConfig.class,
					PropertyConfig.class
			}			
	)
	public static class Config
    {
        /**
         * Nombre:                  freemarkerConfig. Descripcion:             Metodo que inicializa el bean de
         * freemarker
         *
         * @return Returna la configuracion
         *
         * @version 1.0
         * @author guzmle
         * @since 12/14/2015
         */
        @Bean( name = "freemarkerConf" )
        public FreeMarkerConfigurationFactoryBean freemarkerConfig()
        {
            final FreeMarkerConfigurationFactoryBean factory = new FreeMarkerConfigurationFactoryBean();
            factory.setTemplateLoaderPath( "classpath:templates" );
            factory.setDefaultEncoding( "UTF-8" );
            return factory;
        }
    }

}
