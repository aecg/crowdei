package net.softclear.crowdei.backend.service;



import junit.framework.Assert;
import net.softclear.crowdei.backend.model.jpa.ExperienceLevel;
import net.softclear.crowdei.backend.model.jpa.User;
import net.softclear.crowdei.backend.repository.ExperienceRepository;
import net.softclear.crowdei.backend.repository.UserRepository;
import net.softclear.crowdei.backend.service.implementation.ExperienceLevelServiceImpl;
import net.softclear.crowdei.backend.service.implementation.OAuthServiceImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenStore;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyString;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  InvestmentServiceTest
 * Descripcion:             Clase que realiza las pruebas al servicio de inversiones
 * @version                 1.0
 * @author                  guzmle
 * @since                   1/5/2016
 *
 */
public class OAuthServiceTest extends ServiceTest
{
    //region Atributos
    @Mock
    private TokenStore       tokenStore;
    @Mock
    private OAuth2Authentication oauth;
    @Mock
    private UserRepository   userRepository;
    @InjectMocks
    private OAuthServiceImpl service;
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    private User user;

    //endregion

    //region SetUp


    @Before
    public void before()
    {
        MockitoAnnotations.initMocks( this );
        user = new User(  );
        Mockito.when( userRepository.findByUsername( anyString() )).thenReturn( user );
        Mockito.when( tokenStore.readAuthentication( anyString() )).thenReturn( oauth );
        Mockito.when( oauth.getName() ).thenReturn( "hola" );
    }

    //endregion


    @Test
    public void testGetUserByToken()
    {
        User response = service.getUserByToken( "token" );
        Assert.assertNotNull( response );
    }
}

