package net.softclear.crowdei.backend.repository;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import net.softclear.crowdei.backend.model.jpa.BusinessType;
import net.softclear.crowdei.backend.model.jpa.User;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;


import java.util.Date;



public class BusinessTypeRepositoryTest extends RepositoryTest {


    //region Atributos

    @Autowired
    private BusinessTypeRepository repository;

    //endregion

    //region inits

    @Before
    public void before()
    {
    }


    /**
     * Nombre:                  getUser
     * Descripcion:             Metodo que obtiene un usuario
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     * @param userName nombre del usuario
     * @return usuario
     */
    private BusinessType getObject(String description)
    {
        BusinessType retorno = new BusinessType();
        retorno.setDescription( description );

        return retorno;
    }

    //endregion


    /**
     * Nombre:                  saveTest
     * Descripcion:             Metodo que realiza la prueba que se guarde un objeto en la base de datos
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    @Transactional
    public void saveTest()
    {
        BusinessType obj = repository.save( getObject( "user" ) );
        assertTrue( obj.getId() != 0 );
    }



    /**
     * Nombre:                  saveTest
     * Descripcion:             Metodo que realiza la prueba que se guarde un objeto en la base de datos
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    public void updateTest()
    {
        BusinessType obj = repository.save( getObject( "user" ) );
        obj.setDescription( "123" );
        BusinessType objModified =  repository.save( obj );
        assertEquals( obj.getId(), objModified.getId() );
        assertEquals( objModified.getDescription(), "123" );
    }


    /**
     * Nombre:                  findAll
     * Descripcion:             Metodo que prueba que todos los objetos se busquen
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    public void findAll()
    {
        repository.save( getObject( "user1" ) );
        repository.save( getObject( "user2" ) );
        repository.save( getObject( "user3" ) );

        Pageable pageable = new PageRequest(0, 3);
        Page<BusinessType> firstPage = repository.findAll(pageable);

        assertTrue(firstPage.getTotalPages() > 0);
        assertTrue( firstPage.getTotalElements() > 0 );
        assertEquals( firstPage.getNumber(), 0 );
    }
}
