package net.softclear.crowdei.backend.service;



import junit.framework.Assert;
import net.softclear.crowdei.backend.model.jpa.City;
import net.softclear.crowdei.backend.model.jpa.Country;
import net.softclear.crowdei.backend.model.jpa.State;
import net.softclear.crowdei.backend.repository.CityRepository;
import net.softclear.crowdei.backend.repository.CountryRepository;
import net.softclear.crowdei.backend.repository.StateRepository;
import net.softclear.crowdei.backend.service.implementation.LocationServiceImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  InvestmentServiceTest
 * Descripcion:             Clase que realiza las pruebas al servicio de inversiones
 * @version                 1.0
 * @author                  guzmle
 * @since                   1/5/2016
 *
 */
public class LocationServiceTest extends ServiceTest
{
    //region Atributos
    @Mock
    private CityRepository      cityRepository;
    @Mock
    private StateRepository     stateRepository;
    @Mock
    private CountryRepository   countryRepository;
    @InjectMocks
    private LocationServiceImpl service;
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    private List<State>   listState;
    private List<City>    listCity;
    private List<Country> listCountry;

    //endregion

    //region SetUp


    @Before
    public void before()
    {
        MockitoAnnotations.initMocks( this );
        listState = new ArrayList<>();
        listState.add( new State() );
        listCity = new ArrayList<>();
        listCity.add( new City() );
        listCountry = new ArrayList<>();
        listCountry.add( new Country() );

    }

    //endregion


    @Test
    public void testGetCountries()
    {
        Mockito.when( countryRepository.findAll() ).thenReturn( listCountry );

        List<Country> response = service.getCountries();
        Assert.assertNotNull( response );
        Assert.assertTrue( response.size() == 1 );
    }

    @Test
    public void testGetCities()
    {
        Mockito.when( cityRepository.findByState( any( State.class ) ) ).thenReturn( listCity );

        List<City> response = service.getCities( new State ());
        Assert.assertNotNull( response );
        Assert.assertTrue( response.size() == 1 );
    }

    @Test
    public void testGetStates()
    {
        Mockito.when( stateRepository.findByCountry( any( Country.class ) ) ).thenReturn( listState );

        List<State> response = service.getStates( new Country() );
        Assert.assertNotNull( response );
        Assert.assertTrue( response.size() == 1 );
    }
}

