package net.softclear.crowdei.backend.repository;

import net.softclear.crowdei.backend.model.jpa.Person;
import net.softclear.crowdei.backend.model.jpa.User;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;


import java.util.Date;

import static org.junit.Assert.*;



public class UserRepositoryTest extends RepositoryTest {

    //region Atributos

    @Autowired
    private UserRepository userRepository;

    //endregion

    //region inits

    @Before
    public void before()
    {
    }


    /**
     * Nombre:                  getUser
     * Descripcion:             Metodo que obtiene un usuario
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     * @param userName nombre del usuario
     * @return usuario
     */
    private User getUser(String userName)
    {
        User user = new User();
        user.setCreatedAt( new Date(  ) );
        user.setEnabled( true );
        user.setPassword( userName );
        user.setUsername( userName );
        user.setPerson( new Person() );
        user.getPerson().setEmail( "email" );
        user.getPerson().setName( "name" );
        user.getPerson().setLastName( "lastname" );

        return user;
    }

    //endregion


    /**
     * Nombre:                  saveTest
     * Descripcion:             Metodo que realiza la prueba que se guarde un objeto en la base de datos
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    public void saveTest()
    {
        User obj = userRepository.save( getUser( "user" ) );
        assertTrue( obj.getId() != 0 );
    }



    /**
     * Nombre:                  saveTest
     * Descripcion:             Metodo que realiza la prueba que se guarde un objeto en la base de datos
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    public void updateTest()
    {
        User obj = userRepository.save( getUser( "user" ) );
        obj.setPassword( "123" );
        User objModified =  userRepository.save( obj );
        assertEquals( obj.getId(), objModified.getId() );
        assertEquals( objModified.getPassword(), "123" );
    }


    /**
     * Nombre:                  findAll
     * Descripcion:             Metodo que prueba que todos los objetos se busquen
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    public void findAll()
    {
        userRepository.save( getUser( "user1" ) );
        userRepository.save( getUser( "user2" ) );
        userRepository.save( getUser( "user3" ) );

        Pageable pageable = new PageRequest(0, 3);
        Page<User> firstPage = userRepository.findAll(pageable);

        assertTrue(firstPage.getTotalPages() > 0);
        assertTrue( firstPage.getTotalElements() > 0 );
        assertEquals( firstPage.getNumber(), 0 );
    }


    /**
     * Nombre:                  findAll
     * Descripcion:             Metodo que prueba que todos los objetos se busquen
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    public void testFindByEmail()
    {
        userRepository.save( getUser( "user1" ) );
        User obj = userRepository.findByEmail( "email" );
        assertNotNull( obj );
        assertEquals( obj.getPerson().getEmail(), "email" );
    }


    /**
     * Nombre:                  findAll
     * Descripcion:             Metodo que prueba que todos los objetos se busquen
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    public void testFindByUserName()
    {
        userRepository.save( getUser( "user1" ) );
        User obj = userRepository.findByUsername( "user1" );
        assertNotNull( obj );
        assertEquals( obj.getUsername(), "user1" );
    }
}
