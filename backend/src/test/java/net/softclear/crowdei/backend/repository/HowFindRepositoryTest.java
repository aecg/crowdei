package net.softclear.crowdei.backend.repository;



import net.softclear.crowdei.backend.model.jpa.HowFind;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  HowFindRepositoryTest
 * Descripcion:             Clase que prueba el repositorio de business type
 * @version                 1.0
 * @author                  guzmle
 * @since                   02/12/15
 *
 */
public class HowFindRepositoryTest extends RepositoryTest {

    //region Atributos

    @Autowired
    private HowFindRepository repository;

    //endregion

    //region inits

    @Before
    public void before()
    {
    }


    /**
     * Nombre:                  getUser
     * Descripcion:             Metodo que obtiene un usuario
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     * @param description nombre del HowFind
     * @return usuario
     */
    private HowFind getObject(String description)
    {
        HowFind retorno = new HowFind();
        retorno.setDescription( description );

        return retorno;
    }

    //endregion


    /**
     * Nombre:                  saveTest
     * Descripcion:             Metodo que realiza la prueba que se guarde un objeto en la base de datos
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    public void saveTest()
    {
        HowFind obj = repository.save( getObject( "user" ) );
        assertTrue( obj.getId() != 0 );
    }


    /**
     * Nombre:                  saveTest
     * Descripcion:             Metodo que realiza la prueba que se guarde un objeto en la base de datos
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    public void updateTest()
    {
        HowFind obj = repository.save( getObject( "user" ) );
        obj.setDescription( "123" );
        HowFind objModified =  repository.save( obj );
        assertEquals( obj.getId(), objModified.getId() );
        assertEquals( objModified.getDescription(), "123" );
    }


    /**
     * Nombre:                  findAll
     * Descripcion:             Metodo que prueba que todos los objetos se busquen
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    public void findAll()
    {
        repository.save( getObject( "user1" ) );
        repository.save( getObject( "user2" ) );
        repository.save( getObject( "user3" ) );

        Pageable pageable = new PageRequest(0, 3);
        Page<HowFind> firstPage = repository.findAll(pageable);

        assertTrue(firstPage.getTotalPages() > 0);
        assertTrue( firstPage.getTotalElements() > 0 );
        assertEquals( firstPage.getNumber(), 0 );
    }
}
