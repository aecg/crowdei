package net.softclear.crowdei.backend.repository;



import net.softclear.crowdei.backend.model.jpa.Investment;
import net.softclear.crowdei.backend.model.jpa.Project;
import net.softclear.crowdei.backend.model.types.MethodPayment;
import net.softclear.crowdei.backend.model.types.Status;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  InvestmentRepositoryTest
 * Descripcion:             Clase que prueba el repositorio de business type
 * @version                 1.0
 * @author                  guzmle
 * @since                   02/12/15
 *
 */
public class InvestmentRepositoryTest extends RepositoryTest {

    //region Atributos

    @Autowired
    private InvestmentRepository repository;
    @Autowired
    private ProjectRepository projectRepository;
    private Project              project;

    //endregion

    //region inits


    @Before
    public void before()
    {
        project = new Project();
        project.setName( "name" );
        project.setImage( "" );
        project.setPerson( null );
        project.setStatus( Status.DELETED );
        projectRepository.save( project );
    }


    /**
     * Nombre:                  getUser
     * Descripcion:             Metodo que obtiene un usuario
     * @version 1.0
     * @author guzmle
     * @since 11/30/2015
     *
     * @param description nombre del Investment
     * @return usuario
     */
    private Investment getObject( String description )
    {
        Investment retorno = new Investment();
        retorno.setProject( project );
        retorno.setAmount( 10.00 );
        retorno.setStatus( Status.ACTIVE );
        retorno.setData( "data" );
        retorno.setDate( new Date() );
        retorno.setMethodPayment( MethodPayment.PAYPAL );

        return retorno;
    }

    //endregion


    /**
     * Nombre:                  saveTest
     * Descripcion:             Metodo que realiza la prueba que se guarde un objeto en la base de datos
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    public void saveTest()
    {
        Investment obj = repository.save( getObject( "user" ) );
        assertTrue( obj.getId() != 0 );
    }


    /**
     * Nombre:                  saveTest
     * Descripcion:             Metodo que realiza la prueba que se guarde un objeto en la base de datos
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    public void updateTest()
    {
        Investment obj = repository.save( getObject( "user" ) );
        obj.setStatus( Status.INACTIVE );
        Investment objModified =  repository.save( obj );
        assertEquals( obj.getId(), objModified.getId() );
        assertEquals( objModified.getStatus(), Status.INACTIVE );
    }


    /**
     * Nombre:                  findAll
     * Descripcion:             Metodo que prueba que todos los objetos se busquen
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    public void findAll()
    {
        repository.save( getObject( "user1" ) );
        repository.save( getObject( "user2" ) );
        repository.save( getObject( "user3" ) );

        Pageable pageable = new PageRequest(0, 3);
        Page<Investment> firstPage = repository.findAll(pageable);

        assertTrue(firstPage.getTotalPages() > 0);
        assertTrue( firstPage.getTotalElements() > 0 );
        assertEquals( firstPage.getNumber(), 0 );
    }
}
