package net.softclear.crowdei.backend.service;



import junit.framework.Assert;
import net.softclear.crowdei.backend.exception.ServiceException;
import net.softclear.crowdei.backend.model.jpa.Investment;
import net.softclear.crowdei.backend.model.jpa.Person;
import net.softclear.crowdei.backend.model.jpa.User;
import net.softclear.crowdei.backend.repository.InvestmentRepository;
import net.softclear.crowdei.backend.repository.UserRepository;
import net.softclear.crowdei.backend.service.implementation.InvestmentServiceImpl;
import net.softclear.crowdei.backend.util.MercadoPagoUtils;
import net.softclear.crowdei.backend.util.TransfererUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  InvestmentServiceTest
 * Descripcion:             Clase que realiza las pruebas al servicio de inversiones
 * @version                 1.0
 * @author                  guzmle
 * @since                   1/5/2016
 *
 */
public class InvestmentServiceTest extends ServiceTest
{
    //region Atributos
    @Mock
    private InvestmentRepository  repository;
    @Mock
    private UserRepository        userRepository;
    @InjectMocks
    private InvestmentServiceImpl service;
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    private User user;

    //endregion

    //region SetUp


    @Before
    public void before()
    {
        MockitoAnnotations.initMocks( this );
        user = new User();
        user.setId( ( long ) 1 );
        user.setPerson( new Person() );
        user.getPerson().setId( ( long ) 1 );
    }

    //endregion


//    @Test
//    public void getMercadoPagoUrlTest()
//    {
//        MercadoPagoUtils data = new MercadoPagoUtils();
//        data.setAmount( 100.00 );
//        data.setDescription( "Esto es una prueba" );
//        data.setCurrencyId( "USD" );
//
//        String url = service.getMercadoPagoUrl( data );
//        Assert.assertTrue( url.length() > 0 );
//    }


//    @Test
//    public void testPaymentProcessingPaypalTestSucess()
//    {
//        String data = "mc_gross=12.00&protection_eligibility=Eligible&address_status=confirmed&item_number1=8" +
//                "&payer_id=V8ANLUL7MKSNN&tax=0.00&address_street=1+Main+St&payment_date=11%3A33%3A39+Dec+30%2C+2015" +
//                "+PST" +
//                "&payment_status=Completed&charset=windows-1252&address_zip=95131&mc_shipping=0.00&mc_handling=0.00&" +
//                "first_name=test&mc_fee=0.65&address_country_code=US&address_name=test+buyer&notify_version=3.8&" +
//                "custom=&payer_status=verified&business=guzmleinternet-facilitator%40gmail.com&" +
//                "address_country=United+States&num_cart_items=1&mc_handling1=0.00&address_city=San+Jose&" +
//                "payer_email=guzmleinternet-buyer%40gmail.com&verify_sign=AFcWxV21C7fd0v3bYYYRCpSSRl31AJ1xSQZ58vS3ZLJ" +
//                "5vcm.cmbljXYi&mc_shipping1=0.00&tax1=0.00&txn_id=50W595515W496905L&payment_type=instant&last_name=buyer" +
//                "&item_name1=Nombre+del+proyecto&address_state=CA&receiver_email=guzmleinternet-facilitator%40gmail.com&" +
//                "payment_fee=0.65&quantity1=1&receiver_id=8N9L9HLBAKQCL&txn_type=cart&mc_gross_1=12.00&mc_currency=USD&" +
//                "residence_country=US&test_ipn=1&transaction_subject=&payment_gross=12.00&auth=AqbtksiVsa0REgJYh23CPf." +
//                "gb9ZYGAbI40KLxvLBckuihmMdxeyMxSKWGYX0cWDUqhyboJaFeikCbTtPD3-SD8w";
//
//
//        Mockito.when( userRepository.getOne( anyLong() ) ).thenReturn( user );
//        Mockito.when(repository.save( any(Investment.class) )).thenReturn( new Investment() );
//        service.paymentProcessingPaypal( 1, 1, data );
//    }


    @Test
    public void testPaymentProcessingPaypalTestErrorRafaga()
    {
        thrown.expect( ServiceException.class );
        String data = "protection_eligibility=Eligible&address_status=confirmed&item_number1=8" +
                "&payer_id=V8ANLUL7MKSNN&tax=0.00&address_street=1+Main+St&payment_date=11%3A33%3A39+Dec+30%2C+2015" +
                "+PST" +
                "&payment_status=Completed&charset=windows-1252&address_zip=95131&mc_shipping=0.00&mc_handling=0.00&" +
                "first_name=test&mc_fee=0.65&address_country_code=US&address_name=test+buyer&notify_version=3.8&" +
                "custom=&payer_status=verified&business=guzmleinternet-facilitator%40gmail.com&" +
                "address_country=United+States&num_cart_items=1&mc_handling1=0.00&address_city=San+Jose&" +
                "payer_email=guzmleinternet-buyer%40gmail.com&verify_sign=AFcWxV21C7fd0v3bYYYRCpSSRl31AJ1xSQZ58vS3ZLJ" +
                "5vcm.cmbljXYi&mc_shipping1=0.00&tax1=0.00&txn_id=50W595515W496905L&payment_type=instant&last_name=buyer" +
                "&item_name1=Nombre+del+proyecto&address_state=CA&receiver_email=guzmleinternet-facilitator%40gmail.com&" +
                "payment_fee=0.65&quantity1=1&receiver_id=8N9L9HLBAKQCL&txn_type=cart&mc_gross_1=12.00&mc_currency=USD&" +
                "residence_country=US&test_ipn=1&transaction_subject=&payment_gross=12.00&auth=AqbtksiVsa0REgJYh23CPf." +
                "gb9ZYGAbI40KLxvLBckuihmMdxeyMxSKWGYX0cWDUqhyboJaFeikCbTtPD3-SD8w";


        Mockito.when( userRepository.getOne( anyLong() ) ).thenReturn( user );
        Mockito.when(repository.save( any(Investment.class) )).thenReturn( new Investment() );
        service.paymentProcessingPaypal( 1, 1, 1, data );
    }


    @Test
    public void testPaymentProcessingPaypalTestErrorAmount()
    {
        thrown.expect( ServiceException.class );
        String data = "mc_gross=Leo&protection_eligibility=Eligible&address_status=confirmed&item_number1=8" +
                "&payer_id=V8ANLUL7MKSNN&tax=0.00&address_street=1+Main+St&payment_date=11%3A33%3A39+Dec+30%2C+2015" +
                "+PST" +
                "&payment_status=Completed&charset=windows-1252&address_zip=95131&mc_shipping=0.00&mc_handling=0.00&" +
                "first_name=test&mc_fee=0.65&address_country_code=US&address_name=test+buyer&notify_version=3.8&" +
                "custom=&payer_status=verified&business=guzmleinternet-facilitator%40gmail.com&" +
                "address_country=United+States&num_cart_items=1&mc_handling1=0.00&address_city=San+Jose&" +
                "payer_email=guzmleinternet-buyer%40gmail.com&verify_sign=AFcWxV21C7fd0v3bYYYRCpSSRl31AJ1xSQZ58vS3ZLJ" +
                "5vcm.cmbljXYi&mc_shipping1=0.00&tax1=0.00&txn_id=50W595515W496905L&payment_type=instant&last_name=buyer" +
                "&item_name1=Nombre+del+proyecto&address_state=CA&receiver_email=guzmleinternet-facilitator%40gmail.com&" +
                "payment_fee=0.65&quantity1=1&receiver_id=8N9L9HLBAKQCL&txn_type=cart&mc_gross_1=12.00&mc_currency=USD&" +
                "residence_country=US&test_ipn=1&transaction_subject=&payment_gross=12.00&auth=AqbtksiVsa0REgJYh23CPf." +
                "gb9ZYGAbI40KLxvLBckuihmMdxeyMxSKWGYX0cWDUqhyboJaFeikCbTtPD3-SD8w";


        Mockito.when( userRepository.getOne( anyLong() ) ).thenReturn( user );
        Mockito.when(repository.save( any( Investment.class ) )).thenReturn( new Investment() );
        service.paymentProcessingPaypal( 1, 1, 1, data );
    }


    @Test
    public void testPaymentProcessingPaypalTestErrorFindUser()
    {
        thrown.expect( ServiceException.class );
        String data = "mc_gross=12.00&protection_eligibility=Eligible&address_status=confirmed&item_number1=8" +
                "&payer_id=V8ANLUL7MKSNN&tax=0.00&address_street=1+Main+St&payment_date=11%3A33%3A39+Dec+30%2C+2015" +
                "+PST" +
                "&payment_status=Completed&charset=windows-1252&address_zip=95131&mc_shipping=0.00&mc_handling=0.00&" +
                "first_name=test&mc_fee=0.65&address_country_code=US&address_name=test+buyer&notify_version=3.8&" +
                "custom=&payer_status=verified&business=guzmleinternet-facilitator%40gmail.com&" +
                "address_country=United+States&num_cart_items=1&mc_handling1=0.00&address_city=San+Jose&" +
                "payer_email=guzmleinternet-buyer%40gmail.com&verify_sign=AFcWxV21C7fd0v3bYYYRCpSSRl31AJ1xSQZ58vS3ZLJ" +
                "5vcm.cmbljXYi&mc_shipping1=0.00&tax1=0.00&txn_id=50W595515W496905L&payment_type=instant&last_name=buyer" +
                "&item_name1=Nombre+del+proyecto&address_state=CA&receiver_email=guzmleinternet-facilitator%40gmail.com&" +
                "payment_fee=0.65&quantity1=1&receiver_id=8N9L9HLBAKQCL&txn_type=cart&mc_gross_1=12.00&mc_currency=USD&" +
                "residence_country=US&test_ipn=1&transaction_subject=&payment_gross=12.00&auth=AqbtksiVsa0REgJYh23CPf." +
                "gb9ZYGAbI40KLxvLBckuihmMdxeyMxSKWGYX0cWDUqhyboJaFeikCbTtPD3-SD8w";


        Mockito.when( userRepository.getOne( anyLong() ) ).thenThrow( new ServiceException() );
        service.paymentProcessingPaypal( 1, 1, 1, data );
    }


    @Test
    public void testPaymentProcessingPaypalTestErrorSaveInvestment()
    {
        thrown.expect( ServiceException.class );
        String data = "mc_gross=12.00&protection_eligibility=Eligible&address_status=confirmed&item_number1=8" +
                "&payer_id=V8ANLUL7MKSNN&tax=0.00&address_street=1+Main+St&payment_date=11%3A33%3A39+Dec+30%2C+2015" +
                "+PST" +
                "&payment_status=Completed&charset=windows-1252&address_zip=95131&mc_shipping=0.00&mc_handling=0.00&" +
                "first_name=test&mc_fee=0.65&address_country_code=US&address_name=test+buyer&notify_version=3.8&" +
                "custom=&payer_status=verified&business=guzmleinternet-facilitator%40gmail.com&" +
                "address_country=United+States&num_cart_items=1&mc_handling1=0.00&address_city=San+Jose&" +
                "payer_email=guzmleinternet-buyer%40gmail.com&verify_sign=AFcWxV21C7fd0v3bYYYRCpSSRl31AJ1xSQZ58vS3ZLJ" +
                "5vcm.cmbljXYi&mc_shipping1=0.00&tax1=0.00&txn_id=50W595515W496905L&payment_type=instant&last_name=buyer" +
                "&item_name1=Nombre+del+proyecto&address_state=CA&receiver_email=guzmleinternet-facilitator%40gmail.com&" +
                "payment_fee=0.65&quantity1=1&receiver_id=8N9L9HLBAKQCL&txn_type=cart&mc_gross_1=12.00&mc_currency=USD&" +
                "residence_country=US&test_ipn=1&transaction_subject=&payment_gross=12.00&auth=AqbtksiVsa0REgJYh23CPf." +
                "gb9ZYGAbI40KLxvLBckuihmMdxeyMxSKWGYX0cWDUqhyboJaFeikCbTtPD3-SD8w";


        Mockito.when( userRepository.getOne( anyLong() ) ).thenReturn( user );
        Mockito.when(repository.save( any( Investment.class ) )).thenThrow( new ServiceException() );
        service.paymentProcessingPaypal( 1, 1, 1, data );
    }


//    @Test
//    public void testPaymentProcessingMercadoPagoSuccess()
//    {
//        MercadoPagoUtils data = new MercadoPagoUtils();
//        data.setAmount( 100.00 );
//        data.setData( "{'back_url':null,'collection_id':'1451940541','collection_status':'approved'," +
//                "'preference_id':'176161379-ec3f946a-0ff3-4b9b-8dda-aa9f80acdb33','external_reference':null," +
//                "'payment_type':'account_money'}" );
//
//        Mockito.when( userRepository.getOne( anyLong() ) ).thenReturn( user );
//        Mockito.when(repository.save( any(Investment.class) )).thenReturn( new Investment() );
//        service.paymentProcessingMercadoPago( 1, 1, data );
//    }


    @Test
    public void testPaymentProcessingMercadoPagoErrorStatus()
    {
        thrown.expect( ServiceException.class );
        MercadoPagoUtils data = new MercadoPagoUtils();
        data.setAmount( 100.00 );
        data.setData( "{'back_url':null,'collection_id':'1451940541','collection_status':'rejected'," +
                "'preference_id':'176161379-ec3f946a-0ff3-4b9b-8dda-aa9f80acdb33','external_reference':null," +
                "'payment_type':'account_money'}" );
        service.paymentProcessingMercadoPago( 1, 1, data );
    }


    @Test
    public void testPaymentProcessingMercadoPagoErrorFindUser()
    {
        thrown.expect( ServiceException.class );

        MercadoPagoUtils data = new MercadoPagoUtils();
        data.setAmount( 100.00 );
        data.setData( "{'back_url':null,'collection_id':'1451940541','collection_status':'approved'," +
                "'preference_id':'176161379-ec3f946a-0ff3-4b9b-8dda-aa9f80acdb33','external_reference':null," +
                "'payment_type':'account_money'}" );


        Mockito.when( userRepository.getOne( anyLong() ) ).thenThrow( new ServiceException() );
        service.paymentProcessingMercadoPago( 1, 1, data );
    }


    @Test
    public void testPaymentProcessingMercadoPagoErrorSaveInvestment()
    {
        thrown.expect( ServiceException.class );

        MercadoPagoUtils data = new MercadoPagoUtils();
        data.setAmount( 100.00 );
        data.setData( "{'back_url':null,'collection_id':'1451940541','collection_status':'approved'," +
                "'preference_id':'176161379-ec3f946a-0ff3-4b9b-8dda-aa9f80acdb33','external_reference':null," +
                "'payment_type':'account_money'}" );

        Mockito.when( userRepository.getOne( anyLong() ) ).thenReturn( user );
        Mockito.when(repository.save( any( Investment.class ) )).thenThrow( new ServiceException() );
        service.paymentProcessingMercadoPago( 1, 1, data );
    }


    @Test
    public void testPaymentProcessingTransferSuccess()
    {
        TransfererUtils data = new TransfererUtils();
        data.setAmount( 100.00 );
        data.setToken( "123" );

        Mockito.when( userRepository.getOne( anyLong() ) ).thenReturn( user );
        Mockito.when(repository.save( any(Investment.class) )).thenReturn( new Investment() );
        service.paymentProcessingTransferencia( 1, 1, data );
    }


    @Test
    public void testPaymentProcessingTransferErrorFindUser()
    {
        thrown.expect( ServiceException.class );

        TransfererUtils data = new TransfererUtils();
        data.setAmount( 100.00 );
        data.setToken( "123" );


        Mockito.when( userRepository.getOne( anyLong() ) ).thenThrow( new ServiceException() );
        service.paymentProcessingTransferencia( 1, 1, data );
    }


    @Test
    public void testPaymentProcessingTransferenciaErrorSaveInvestment()
    {
        thrown.expect( ServiceException.class );

        TransfererUtils data = new TransfererUtils();
        data.setAmount( 100.00 );
        data.setToken( "123" );

        Mockito.when( userRepository.getOne( anyLong() ) ).thenReturn( user );
        Mockito.when(repository.save( any( Investment.class ) )).thenThrow( new ServiceException() );
        service.paymentProcessingTransferencia( 1, 1, data );
    }
}

