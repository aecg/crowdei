package net.softclear.crowdei.backend.repository;



import net.softclear.crowdei.backend.model.jpa.Contact;
import net.softclear.crowdei.backend.model.jpa.ContactInformation;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  CompanyEstatusRepositoryTest
 * Descripcion:             Clase que prueba el repositorio de business type
 * @version                 1.0
 * @author                  guzmle
 * @since                   02/12/15
 *
 */
public class ContactRepositoryTest extends RepositoryTest {

    //region Atributos

    @Autowired
    private ContactRepository repository;

    //endregion

    //region inits

    @Before
    public void before()
    {
    }


    /**
     * Nombre:                  getUser
     * Descripcion:             Metodo que obtiene un usuario
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     * @param position nombre del usuario
     * @return usuario
     */
    private Contact getObject(String position)
    {
        Contact retorno = new Contact();
        retorno.setFullname( "name" );
        retorno.setContactInformation( new ContactInformation() );
        retorno.getContactInformation( ).setEmail( "dasda" );
        retorno.getContactInformation( ).setMobile( "dasda" );
        retorno.getContactInformation( ).setPhoneOne( "dasda" );
        retorno.getContactInformation( ).setPhoneTwo( "dasda" );

        return retorno;
    }

    //endregion

    /**
     * Nombre:                  saveTest
     * Descripcion:             Metodo que realiza la prueba que se guarde un objeto en la base de datos
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    public void saveTest()
    {
        Contact obj = repository.save( getObject( "user" ) );
        assertTrue( obj.getId() != 0 );
    }


    /**
     * Nombre:                  saveTest
     * Descripcion:             Metodo que realiza la prueba que se guarde un objeto en la base de datos
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    public void updateTest()
    {
        Contact obj = repository.save( getObject( "user" ) );
        obj.setFullname( "123" );
        Contact objModified =  repository.save( obj );
        assertEquals( obj.getId(), objModified.getId() );
        assertEquals( objModified.getFullname(), "123" );
    }


    /**
     * Nombre:                  findAll
     * Descripcion:             Metodo que prueba que todos los objetos se busquen
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    public void findAll()
    {
        repository.save( getObject( "user1" ) );
        repository.save( getObject( "user2" ) );
        repository.save( getObject( "user3" ) );

        Pageable pageable = new PageRequest(0, 3);
        Page<Contact> firstPage = repository.findAll(pageable);

        assertTrue(firstPage.getTotalPages() > 0);
        assertTrue( firstPage.getTotalElements() > 0 );
        assertEquals( firstPage.getNumber(), 0 );
    }
}
