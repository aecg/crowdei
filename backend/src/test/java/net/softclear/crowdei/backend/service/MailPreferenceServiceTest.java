package net.softclear.crowdei.backend.service;



import junit.framework.Assert;
import net.softclear.crowdei.backend.model.jpa.BusinessType;
import net.softclear.crowdei.backend.model.jpa.MailPreference;
import net.softclear.crowdei.backend.repository.BusinessTypeRepository;
import net.softclear.crowdei.backend.repository.MailPreferenceRepository;
import net.softclear.crowdei.backend.service.implementation.BusinessTypeServiceImpl;
import net.softclear.crowdei.backend.service.implementation.MailPreferenceServiceImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  InvestmentServiceTest
 * Descripcion:             Clase que realiza las pruebas al servicio de inversiones
 * @version                 1.0
 * @author                  guzmle
 * @since                   1/5/2016
 *
 */
public class MailPreferenceServiceTest extends ServiceTest
{
    //region Atributos
    @Mock
    private MailPreferenceRepository  repository;
    @InjectMocks
    private MailPreferenceServiceImpl service;
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    private List<MailPreference> list;

    //endregion

    //region SetUp


    @Before
    public void before()
    {
        MockitoAnnotations.initMocks( this );
        list = new ArrayList<>();
        list.add( new MailPreference() );
        list.add( new MailPreference() );
    }

    //endregion


    @Test
    public void testGetAll()
    {
        Mockito.when( repository.findAll() ).thenReturn( list );

        List<MailPreference> response = service.getAll();
        Assert.assertNotNull( response );
        Assert.assertTrue( response.size() == 2 );
    }
}

