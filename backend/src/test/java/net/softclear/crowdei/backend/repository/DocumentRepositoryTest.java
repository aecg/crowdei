package net.softclear.crowdei.backend.repository;



import net.softclear.crowdei.backend.model.jpa.Currency;
import net.softclear.crowdei.backend.model.jpa.Document;
import net.softclear.crowdei.backend.model.jpa.Project;
import net.softclear.crowdei.backend.model.types.Status;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  CurrencyRepositoryTest
 * Descripcion:             Clase que prueba el repositorio de business type
 * @version                 1.0
 * @author                  guzmle
 * @since                   02/12/15
 *
 */
public class DocumentRepositoryTest extends RepositoryTest {

    //region Atributos

    @Autowired
    private DocumentRepository repository;
    @Autowired
    private ProjectRepository projectRepository;
    private Project            project;

    //endregion

    //region inits


    @Before
    public void before()
    {
        project = new Project();
        project.setName( "name" );
        project.setImage( "" );
        project.setPerson( null );
        project.setStatus( Status.DELETED );
        projectRepository.save( project );
    }


    /**
     * Nombre:                  getUser
     * Descripcion:             Metodo que obtiene un usuario
     * @version 1.0
     * @author guzmle
     * @since 11/30/2015
     *
     * @param description nombre del currency
     * @return usuario
     */
    private Document getObject( String description )
    {
        Document retorno = new Document();
        retorno.setName( description );
        retorno.setFile( new byte[ 1 ] );
        retorno.setProject( project );

        return retorno;
    }

    //endregion


    /**
     * Nombre:                  saveTest
     * Descripcion:             Metodo que realiza la prueba que se guarde un objeto en la base de datos
     * @version 1.0
     * @author guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    public void saveTest()
    {
        Document obj = repository.save( getObject( "user" ) );
        assertTrue( obj.getId() != 0 );
    }



    /**
     * Nombre:                  saveTest
     * Descripcion:             Metodo que realiza la prueba que se guarde un objeto en la base de datos
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    public void updateTest()
    {
        Document obj = repository.save( getObject( "user" ) );
        obj.setName( "123" );
        Document objModified =  repository.save( obj );
        assertEquals( obj.getId(), objModified.getId() );
        assertEquals( objModified.getName(), "123" );
    }


    /**
     * Nombre:                  findAll
     * Descripcion:             Metodo que prueba que todos los objetos se busquen
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    public void findAll()
    {
        repository.save( getObject( "user1" ) );
        repository.save( getObject( "user2" ) );
        repository.save( getObject( "user3" ) );

        Pageable pageable = new PageRequest(0, 3);
        Page<Document> firstPage = repository.findAll(pageable);

        assertTrue(firstPage.getTotalPages() > 0);
        assertTrue( firstPage.getTotalElements() > 0 );
        assertEquals( firstPage.getNumber(), 0 );
    }
}
