package net.softclear.crowdei.backend.service;



import junit.framework.Assert;
import net.softclear.crowdei.backend.model.jpa.BusinessType;
import net.softclear.crowdei.backend.repository.BusinessTypeRepository;
import net.softclear.crowdei.backend.service.implementation.BusinessTypeServiceImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  InvestmentServiceTest
 * Descripcion:             Clase que realiza las pruebas al servicio de inversiones
 * @version                 1.0
 * @author                  guzmle
 * @since                   1/5/2016
 *
 */
public class BusinessTypeServiceTest extends ServiceTest
{
    //region Atributos
    @Mock
    private BusinessTypeRepository  repository;
    @InjectMocks
    private BusinessTypeServiceImpl service;
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    private List<BusinessType> list;

    //endregion

    //region SetUp


    @Before
    public void before()
    {
        MockitoAnnotations.initMocks( this );
        list = new ArrayList<>(  );
        list.add( new BusinessType() );
        list.add( new BusinessType() );
    }

    //endregion


    @Test
    public void testGetAll()
    {
        Mockito.when( repository.findAll() ).thenReturn( list );

        List<BusinessType> response = service.getAll();
        Assert.assertNotNull( response );
        Assert.assertTrue( response.size() == 2 );
    }
}

