package net.softclear.crowdei.backend.repository;



import net.softclear.crowdei.backend.model.jpa.*;
import net.softclear.crowdei.backend.model.types.Priority;
import net.softclear.crowdei.backend.model.types.Status;
import net.softclear.crowdei.backend.repository.implementation.ProjectRepositoryImpl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;




/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  HowFindRepositoryTest
 * Descripcion:             Clase que prueba el repositorio de business type
 * @version                 1.0
 * @author                  guzmle
 * @since                   02/12/15
 *
 */
public class ProjectRepositoryTest extends RepositoryTest {
//region Atributos

    @Autowired
    private ProjectRepository repository;
    @Autowired
    private UserRepository    userRepository;
    private User              user;

    //endregion

    //region inits


    @Before
    public void before()
    {
        user = new User();
        user.setCreatedAt( new Date() );
        user.setEnabled( true );
        user.setPassword( "password" );
        user.setUsername( "username" );
        user.setPerson( new Person() );
        user.getPerson().setEmail( "email" );
        user.getPerson().setName( "name" );
        user.getPerson().setLastName( "lastname" );
        userRepository.save( user );
    }


    /**
     * Nombre:                  getUser
     * Descripcion:             Metodo que obtiene un usuario
     * @version 1.0
     * @author guzmle
     * @since 11/30/2015
     *
     * @param description nombre del Investment
     * @return usuario
     */
    private Project getObject( String description )
    {
        Project retorno = new Project();
        retorno.setName( "name" );
        retorno.setImage( "" );
        retorno.setPerson( user.getPerson() );
        Date hoy = new Date();
        retorno.setDateRegister( hoy );
        retorno.setStatus( Status.ACTIVE );
        retorno.setCompany( new Company() );
        retorno.getCompany().setName( "company" );
        retorno.setPriorityType( Priority.OUTSTANDING );
        retorno.setPosition( 1 );

        return retorno;
    }

    //endregion


    /**
     * Nombre:                  saveTest
     * Descripcion:             Metodo que realiza la prueba que se guarde un objeto en la base de datos
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    public void saveTest()
    {
        Project obj = repository.save( getObject( "user" ) );
        assertTrue( obj.getId() != 0 );
    }


    /**
     * Nombre:                  saveTest
     * Descripcion:             Metodo que realiza la prueba que se guarde un objeto en la base de datos
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    public void updateTest()
    {
        Project obj = repository.save( getObject( "user" ) );
        obj.setName( "prueba" );
        Project objModified =  repository.save( obj );
        assertEquals( obj.getId(), objModified.getId() );
        assertTrue( objModified.getName().equals( "prueba" ) );
    }


    /**
     * Nombre:                  findAll
     * Descripcion:             Metodo que prueba que todos los objetos se busquen
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    public void findAll()
    {
        repository.save( getObject( "user1" ) );
        repository.save( getObject( "user2" ) );
        repository.save( getObject( "user3" ) );

        Pageable pageable = new PageRequest(0, 3);
        Page<Project> firstPage = repository.findAll(pageable);

        assertTrue(firstPage.getTotalPages() > 0);
        assertTrue( firstPage.getTotalElements() > 0 );
        assertEquals( firstPage.getNumber(), 0 );
    }


    /**
     * Nombre:                  findAll
     * Descripcion:             Metodo que prueba que todos los objetos se busquen
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    public void findAllById()
    {
        repository.save( getObject( "user3" ) );

        Pageable pageable = new PageRequest(0, 3);
        List<Project> firstPage = repository.findAll(user.getPerson().getId(), pageable);

        assertTrue( firstPage.size() > 0 );
    }


    /**
     * Nombre:                  findAll
     * Descripcion:             Metodo que prueba que todos los objetos se busquen
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    public void findAllByIdNoActive()
    {
        Project obj = getObject( "user3" );
        obj.setStatus( Status.DELETED );
        repository.save( obj );

        Pageable pageable = new PageRequest(0, 3);
        List<Project> firstPage = repository.findAll(user.getPerson().getId(), pageable);

        assertTrue(firstPage.size() == 0);
    }


    /**
     * Nombre:                  findAll
     * Descripcion:             Metodo que prueba que todos los objetos se busquen
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
//    @Test
//    public void findAllByNameCompany()
//    {
//        Date hoy = new Date();
//        Date manana = new Date( hoy.getTime() + 86400000 );
//        Date ayer = new Date( hoy.getTime() - 86400000 );
//        Project obj = getObject( "user3" );
//        repository.save( obj );
//
//        Pageable pageable = new PageRequest(0, 3);
//
//        List<Project> firstPage = repository.findAll("company", null, ayer, manana, pageable );
//
//        assertTrue( firstPage.size() > 0 );
//    }


    /**
     * Nombre:                  findAll
     * Descripcion:             Metodo que prueba que todos los objetos se busquen
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
//    @Test
//    public void findAllByNameProject()
//    {
//        Date hoy = new Date();
//        Date manana = new Date( hoy.getTime() + 86400000 );
//        Date ayer = new Date( hoy.getTime() - 86400000 );
//        Project obj = getObject( "user3" );
//        repository.save( obj );
//
//        Pageable pageable = new PageRequest(0, 3);
//
//        List<Project> firstPage = repository.findAll(null, "name", ayer, manana, pageable );
//
//        assertTrue(firstPage.size() > 0);
//    }


    /**
     * Nombre:                  findAll
     * Descripcion:             Metodo que prueba que todos los objetos se busquen
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
//    @Test
//    public void findAllByAll()
//    {
//        Date hoy = new Date();
//        Date manana = new Date( hoy.getTime() + 86400000 );
//        Date ayer = new Date( hoy.getTime() - 86400000 );
//        Project obj = getObject( "user3" );
//        repository.save( obj );
//
//        Pageable pageable = new PageRequest(0, 3);
//
//        List<Project> firstPage = repository.findAll(null, null, ayer, manana, pageable );
//
//        assertTrue(firstPage.size() > 0);
//    }


    /**
     * Nombre:                  findAll
     * Descripcion:             Metodo que prueba que todos los objetos se busquen
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
//    @Test
//    public void findAllByIdNameCompany()
//    {
//        Date hoy = new Date();
//        Date manana = new Date( hoy.getTime() + 86400000 );
//        Date ayer = new Date( hoy.getTime() - 86400000 );
//        Project obj = getObject( "user3" );
//        repository.save( obj );
//
//        Pageable pageable = new PageRequest(0, 3);
//
//        List<Project> firstPage = repository.findAll(user.getPerson().getId(), "company", null, hoy,
//                pageable );
//
//        assertTrue(firstPage.size() > 0);
//    }


    /**
     * Nombre:                  findAll
     * Descripcion:             Metodo que prueba que todos los objetos se busquen
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
//    @Test
//    public void findAllByIdNameProject()
//    {
//        Date hoy = new Date();
//        Date manana = new Date( hoy.getTime() + 86400000 );
//        Date ayer = new Date( hoy.getTime() - 86400000 );
//        Project obj = getObject( "user3" );
//        repository.save( obj );
//
//        Pageable pageable = new PageRequest(0, 3);
//
//        List<Project> firstPage = repository.findAll(user.getPerson().getId(), null, "name", hoy, pageable );
//
//        assertTrue(firstPage.size() > 0);
//    }


    /**
     * Nombre:                  findAll
     * Descripcion:             Metodo que prueba que todos los objetos se busquen
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
//    @Test
//    public void findAllByIdAll()
//    {
//        Date hoy = new Date();
//        Date manana = new Date( hoy.getTime() + 86400000 );
//        Date ayer = new Date( hoy.getTime() - 86400000 );
//        Project obj = getObject( "user3" );
//        repository.save( obj );
//
//        Pageable pageable = new PageRequest(0, 3);
//
//        List<Project> firstPage = repository.findAll(user.getPerson().getId(), null, null, hoy, pageable );
//
//        assertTrue(firstPage.size() > 0);
//    }


    /**
     * Sistema:                 Crowdei.
     *
     * Nombre:                  findAll
     * Descripcion:             Método que prueba que todos los proyectos con prioridad
     * @version                 1.0
     * @author                  buchyo
     * @since                   08/01/2016
     *
     */
    @Test
    public void findAllPriority()
    {
        Project obj = getObject( "user3" );
        repository.save( obj );

        List<Project> projects = repository.findAll();
        assertTrue( projects.size() > 0 );

    }
}
