package net.softclear.crowdei.backend.service;

import freemarker.template.Configuration;
import net.softclear.crowdei.backend.config.MessagesConfig;
import net.softclear.crowdei.backend.exception.ServiceException;
import net.softclear.crowdei.backend.model.jpa.Authority;
import net.softclear.crowdei.backend.model.jpa.Person;
import net.softclear.crowdei.backend.model.jpa.User;
import net.softclear.crowdei.backend.repository.AuthorityRepository;
import net.softclear.crowdei.backend.repository.PersonRepository;
import net.softclear.crowdei.backend.repository.UserRepository;
import net.softclear.crowdei.backend.service.implementation.UserServiceImpl;
import net.softclear.crowdei.backend.util.UtilString;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.util.Assert;

import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.persistence.NoResultException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;



public class UserServiceTest extends ServiceTest
{

    //region Atributos
    @Autowired
    private JavaMailSender      mail;
    @Mock
    private JavaMailSender      mailSender;
    @Mock
    private UserRepository      userRepository;
    @Mock
    private AuthorityRepository authorityRepository;
    @Mock
    private PersonRepository    personRepository;
    @Mock
    private UtilString          utilString;
    @Mock
    private MessagesConfig      messagesConfig;
    @Autowired
    private Configuration       freemarkerConf;
    @Mock
    private Configuration       freemarkerConfiguration;
    @InjectMocks
    private UserServiceImpl     service;
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    private User user;
    //endregion


    @Before
    public void setUp() throws NoSuchAlgorithmException
    {
        MockitoAnnotations.initMocks( this );
        user = getUser();
        Mockito.when( utilString.validateCaptcha( anyString() ) ).thenReturn( true );
        Mockito.when( utilString.getHash( anyString() ) ).thenCallRealMethod();
        Mockito.when( userRepository.findByUsername( anyString() ) ).thenReturn( null );
        Mockito.when( userRepository.findByEmail( anyString() ) ).thenReturn( user );
        Mockito.when( userRepository.save( any( User.class ) ) ).thenReturn( user );
        Mockito.when( authorityRepository.save( any( Authority.class ) ) ).thenReturn( new Authority() );
    }


    private User getUser()
    {
        User user = new User();
        user.setUsername( "username" );
        user.setPassword( "password" );
        user.setCreatedAt( new Date() );
        user.setEnabled( true );
        user.setPerson( new Person() );
        user.getPerson().setLastName( "apellido" );
        user.getPerson().setName( "nombre" );
        user.getPerson().setEmail( "email" );
        return user;
    }

//
//    @Test
//    public void testCreateUserSucceful() throws NoSuchAlgorithmException
//    {
//        User obj = service.createUser( user, "1234" );
//        Assert.notNull( obj );
//    }


    @Test
    public void testCreateUserNull() throws NoSuchAlgorithmException
    {
        thrown.expect( ServiceException.class );
        service.createUser( null, "1234" );
    }


    @Test
    public void testCreateUserUserNameNull() throws NoSuchAlgorithmException
    {
        thrown.expect( ServiceException.class );

        user.setUsername( null );
        User obj = service.createUser( user, "1234" );
        Assert.notNull( obj );
    }


    @Test
    public void testCreateUserPasswordNull() throws NoSuchAlgorithmException
    {
        thrown.expect( ServiceException.class );

        user.setPassword( null );
        User obj = service.createUser( user, "1234" );
        Assert.notNull( obj );
    }


    @Test
    public void testCreateUserPersonNull() throws NoSuchAlgorithmException
    {
        thrown.expect( ServiceException.class );

        user.setPerson( null );
        User obj = service.createUser( user, "1234" );
        Assert.notNull( obj );
    }


    @Test
    public void testCreateUserEmailNull() throws NoSuchAlgorithmException
    {
        thrown.expect( ServiceException.class );

        user.getPerson().setEmail( null );
        User obj = service.createUser( user, "1234" );
        Assert.notNull( obj );
    }


    @Test
    public void testCreateUserNameNull() throws NoSuchAlgorithmException
    {
        thrown.expect( ServiceException.class );

        user.getPerson().setName( null );
        User obj = service.createUser( user, "1234" );
        Assert.notNull( obj );
    }


    @Test
    public void testCreateUserLastNameNull() throws NoSuchAlgorithmException
    {
        thrown.expect( ServiceException.class );

        user.getPerson().setLastName( null );
        User obj = service.createUser( user, "1234" );
        Assert.notNull( obj );
    }


    @Test
    public void testCreateUserCaptchaWrong() throws NoSuchAlgorithmException
    {
        thrown.expect( ServiceException.class );
        Mockito.when( utilString.validateCaptcha( anyString() ) ).thenReturn( false );
        User obj = service.createUser( user, "1234" );
        Assert.notNull( obj );
    }


    @Test
    public void testCreateUserLoginExistError() throws NoSuchAlgorithmException
    {
        thrown.expect( ServiceException.class );
        Mockito.when( userRepository.findByUsername( anyString() ) ).thenReturn( user );
        User obj = service.createUser( user, "1234" );
        Assert.notNull( obj );
    }


    @Test
    public void testCreateUserSaveUserError() throws NoSuchAlgorithmException
    {
        thrown.expect( ServiceException.class );
        Mockito.when( userRepository.save( any( User.class ) ) ).thenThrow( new RuntimeException() );
        service.createUser( user, "1234" );
    }


    @Test
    public void testCreateUserSaveAuthorityError() throws NoSuchAlgorithmException
    {
        thrown.expect( ServiceException.class );
        Mockito.when( authorityRepository.save( any( Authority.class ) ) ).thenThrow( new RuntimeException(  ) );
        service.createUser( user, "1234" );
    }


    @Test
    public void testGetUserByUsernameSuccess()
    {
        Mockito.when( userRepository.findByUsername( anyString() ) ).thenReturn( user );
        User obj = service.getUserByUsername( "username" );
        Assert.notNull( obj );
    }


    @Test
    public void testGetUserByUsernameError()
    {
        thrown.expect( ServiceException.class );
        Mockito.when( userRepository.findByUsername( anyString() ) ).thenThrow( new RuntimeException() );
        service.getUserByUsername( "username" );
    }


    @Test
    public void testChangePasswordSuccess()
    {
        Mockito.when( userRepository.findOne( anyLong() ) ).thenReturn( user );
        service.changePassword( "1;123", "password" );
    }


    @Test
    public void testChangePasswordUserNameError()
    {
        thrown.expect( ServiceException.class );
        Mockito.when( userRepository.findOne( anyLong() ) ).thenReturn( user );
        service.changePassword( null, "password" );
    }


    @Test
    public void testChangePasswordPasswordError()
    {
        thrown.expect( ServiceException.class );
        Mockito.when( userRepository.findOne( anyLong() ) ).thenReturn( user );
        service.changePassword( "1;123", null );
    }


    @Test
    public void testChangePasswordUserNameError2()
    {
        thrown.expect( ServiceException.class );
        Mockito.when( userRepository.findOne( anyLong() ) ).thenReturn( user );
        service.changePassword( "1123", null );
    }


    @Test
    public void testChangePasswordUseNullError()
    {
        thrown.expect( ServiceException.class );
        Mockito.when( userRepository.findOne( anyLong() ) ).thenReturn( null );
        service.changePassword( "1;123", "password" );
    }


    @Test
    public void testChangePasswordSaveError()
    {
        thrown.expect( ServiceException.class );

        Mockito.when( userRepository.findOne( anyLong() ) ).thenReturn( user );
        Mockito.when( userRepository.save( any( User.class ) ) ).thenThrow( new RuntimeException() );
        service.changePassword( "1;123", "password" );
    }


    @Test
    public void testGetUsers()
    {
        List<User> list = new ArrayList<>(  );
        list.add( new User() );
        Page<User> page = new PageImpl<>( list );
        Mockito.when( userRepository.findAll( any( Pageable.class ) ) ).thenReturn( page );

        Pageable pageable = new PageRequest(0, 3);
        List<User> obj = service.getUsers( true, pageable );

        assertTrue( obj.size() > 0 );
    }


    @Test
    public void testGetUsersError()
    {
        thrown.expect( ServiceException.class );

        Mockito.when( userRepository.findAll( any( Pageable.class ) ) ).thenThrow( new RuntimeException() );

        Pageable pageable = new PageRequest(0, 3);
        service.getUsers( true, pageable );
    }


    @Test
    public void testRetrieveUserSuccess() throws IOException
    {
        Mockito.doNothing().when( mailSender ).send( any( MimeMessage.class ) );
        Mockito.when( mailSender.createMimeMessage() ).thenReturn( mail.createMimeMessage() );
        Mockito.when( freemarkerConfiguration.getTemplate( anyString() ) ).thenReturn(
                freemarkerConf.getTemplate( "mail.ftl" ) );
        service.retrieveUser( "email" );

    }


    @Test
    public void testRetrieveUserEmailError() throws IOException
    {
        thrown.expect( ServiceException.class );
        Mockito.doNothing().when( mailSender ).send( any( MimeMessage.class ) );
        Mockito.when( mailSender.createMimeMessage() ).thenReturn( mail.createMimeMessage() );
        Mockito.when( freemarkerConfiguration.getTemplate( anyString() ) ).thenReturn( freemarkerConf.getTemplate(
                "mail.ftl" ) );
        Mockito.when( userRepository.findByEmail( anyString() ) ).thenThrow( new NoResultException() );
        service.retrieveUser( "email" );

    }


    @Test
    public void testRetrieveUserMessagingExceptionError() throws IOException
    {
        thrown.expect( ServiceException.class );
        Mockito.when( mailSender.createMimeMessage() ).thenReturn( mail.createMimeMessage() );
        Mockito.when( freemarkerConfiguration.getTemplate( anyString() ) ).thenThrow( new IOException() );

        service.retrieveUser( "email" );

    }


    @Test
    public void testUpdateUserSuccess() throws NoSuchAlgorithmException
    {
        Mockito.when( userRepository.findByUsername( anyString() ) ).thenReturn( user );

        Mockito.when( utilString.getHash( anyString() ) ).thenReturn( user.getPassword() );

        service.updateUser( user );


    }


    @Test
    public void testUpdateUserPasswordSuccess() throws NoSuchAlgorithmException
    {
        Mockito.when( userRepository.findByUsername( anyString() ) ).thenReturn( user );

        Mockito.when( utilString.getHash( anyString() ) ).thenReturn( "pass" );

        service.updateUser( user );

    }


    @Test
    public void testUpdateUserError() throws NoSuchAlgorithmException
    {
        thrown.expect( ServiceException.class );
        Mockito.when( userRepository.findByUsername( anyString() ) ).thenReturn( user );

        Mockito.when( utilString.getHash( anyString() ) ).thenReturn( "pass" );
        Mockito.when( userRepository.save( any( User.class ) ) ).thenThrow( new RuntimeException() );
        service.updateUser( user );

    }


    @Test
    public void testValidateUserSuccess() throws NoSuchAlgorithmException
    {
        thrown.expect( ServiceException.class );
        Mockito.when( utilString.getHash( anyString() ) ).thenReturn( "password" );
        Mockito.when( userRepository.findByUsername( anyString() ) ).thenReturn( user );
        User obj = service.validateUser( "username", "password" );

        Assert.notNull( obj );

    }


    @Test
    public void testValidateUserInvalid() throws NoSuchAlgorithmException
    {
        thrown.expect( ServiceException.class );

        Mockito.when( utilString.getHash( anyString() ) ).thenReturn( "123" );
        Mockito.when( userRepository.findByUsername( anyString() ) ).thenReturn( user );

        service.validateUser( "username", "password" );
    }
}